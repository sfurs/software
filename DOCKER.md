## Prerequisites
- Docker Privilege in CSIL > You will need to talk to Jonathan for the steps to get access
- Be on a CSIL Computer - We have not and likely will not test this for regular ubuntu installations or VMs. I higly recommend using the computers in ASB 9820, as these computers have RTX 2080 GPUs.

## Running Skynet with Docker on Ubuntu

1. Clone and build grSim. and clone Skynet:
```bash
mkdir -p ~/SFU_Robot_Soccer
cd ~/SFU_Robot_Soccer
if [ ! -d "grSim" ]; then
    git clone https://github.com/RoboCup-SSL/grSim.git
fi
cd grSim \
  && mkdir build
cd build \
  && cmake -DCMAKE_INSTALL_PREFIX=/usr/local .. \
  && make
cd ~/SFU_Robot_Soccer/
mkdir sfurs
cd sfurs
if [ ! -d "software" ]; then
    git clone https://gitlab.com/sfurs/software.git
    cd software
else 
    cd software
    git fetch origin
    git pull
fi
```

You may already have this software installed if you have run this previously. Checkout to your branch of choice if you are not pulling from develop.

```bash
git checkout <branch-name>
```

2. (Recommended) Build `app` image using remote `software/env` image. 

- Build application using the `env` Docker image in our GitLab image repository.
- Recommended since we can use the `env` image in the repository to build the application so we don't need to reinstall all the Ubuntu packages.

Need to authenticate with our image repository first to pull the `software/env` image.
```bash
docker login registry.gitlab.com/sfurs/software
```

The `app` image will build from our remote `software/env:latest-develop` image by default.
```bash
docker build -f Dockerfile -t skynet .
```

2. (Alternative) Build `app` using local `software/env` image. 

- If you want to build the images entirely yourself, generally not recommended since package versions may be mismatching with image tested in CI.

Build the Docker `env` image. This is the Ubuntu environment containing all packages needed to build and run the code.
```bash
docker build -f Dockerfile.env -t software/env:latest .
```

Build the Docker `app` image. This application contains our actual code.
```bash
docker build -f Dockerfile --build-arg="ENV_IMAGE=software/env" --build-arg="ENV_TAG=latest" -t skynet .
```

3. Allow X server connections:
```bash
xhost +local:docker
```

4. Run the container with network access:
```bash
docker run -it \
    --env="DISPLAY" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --network="host" \
    skynet
```

Note that you will need to run grSim simultaniously with skynet to recieve the field data.
```bash
~/SFU_Robot_Soccer/grSim/bin/grSim
```

## Port Information
Default ports used for communication with grSim:
- 10003: Vision data
- 10020: Referee data
- 10301: Blue team control
- 10302: Yellow team control

The `--network="host"` flag allows the container to share the host's network stack, giving it access to all local ports. This is the simplest way to ensure communication with grSim.

## Troubleshooting
If you see connection issues:
1. Verify grSim is running and using the correct ports
2. Check that the ports in Skynet's UI match grSim's configuration
3. Make sure no firewall rules are blocking the communication



### Temp MACOS Section

```bash
# Install if not already installed
brew install --cask xquartz

# Start XQuartz
open -a XQuartz
```

```bash
# Get your IP
export IP=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')

# Allow X11 forwarding
xhost + $IP

# Build and run the container
docker build -t skynet .
docker run -it \
    -e DISPLAY=$IP:0 \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v ~/.Xauthority:/root/.Xauthority \
    --network host \
    skynet
```

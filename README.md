## Software UML Diagram
![UML_diagram](src/res/RS_UML.svg)

# Click the following to go to the install instructions for your OS:
[macOS](#macOS) [Windows](#windows) [Ubuntu](#ubuntu) 

## Table of Contents

1. [Directory Structure](#dir)
2. [Building Serial Connection Support (WIP)](#buildserial)
3. [Connecting Serial Devices (WIP)](#connserial)
4. [Setup Git](#git)
5. [GrSim](#grSim)
6. [Rebuilding the Software](#rebuild)
7. [macOS Install and Build Script](#macOS)
7a. [macOS With Serial (Firmware Team)](#macOS-firmware)
8. [Windows Install Instructions](#windows)
9. [Ubuntu Installation Instructions](#ubuntu)
9a. [Ubuntu With Serial (Firmware Team)](#ubuntu-firmware)
9b. [Docker](#docker)

## <a name="dir"></a>Directory Structure
1. [root](https://gitlab.com/sfurs/software/-/tree/develop?ref_type=heads)  
   - Root contains cmake file, this README.md, and a .gitignore. Usually, you will put your build folder here.
   1. [Source Files (src)](https://gitlab.com/sfurs/software/-/tree/develop/src?ref_type=heads)  
      - Where you put all the files pertaining to building Skynet. Contains the [main app](https://gitlab.com/sfurs/software/-/blob/develop/src/main.cpp?ref_type=heads) as well.
      1. [GUI Files](https://gitlab.com/sfurs/software/-/tree/develop/src/gui?ref_type=heads)  
         1. [Resources (Turfs, Robots, etc.)](https://gitlab.com/sfurs/software/-/tree/develop/src/gui/resources?ref_type=heads)
      2. [Networking (UDP Layer)](https://gitlab.com/sfurs/software/-/tree/develop/src/network?ref_type=heads)
      3. [Agent](https://gitlab.com/sfurs/software/-/tree/develop/src/agent?ref_type=heads)
      4. [Worldstate](https://gitlab.com/sfurs/software/-/tree/develop/src/worldstate?ref_type=heads)
      5. [Shared Files](https://gitlab.com/sfurs/software/-/tree/develop/src/shared?ref_type=heads)
      6. [Resource Folder](https://gitlab.com/sfurs/software/-/tree/develop/src/res?ref_type=heads)
      7. [Protobuf Files](https://gitlab.com/sfurs/software/-/tree/develop/src/phc?ref_type=heads)
      8. [Python](https://gitlab.com/sfurs/software/-/tree/develop/src/python?ref_type=heads)
    2. [Utility Files(utils)](https://gitlab.com/sfurs/software/-/tree/develop/utils?ref_type=heads)
      - Where you put all of the seperate utility programs related to the application, but not built with the applications.
      1. [Game Logs](https://gitlab.com/sfurs/software/-/tree/develop/utils/gamelogs?ref_type=heads)

### <a name="buildserial"></a>Building Serial Connection Support

When installing QT6 install the `serial port` option

In ubuntu jammy(22.04) the apt package is `libqt6serialport6-dev` , and can be installed using `sudo apt install libqt6serialport6-dev`.

In ubuntu mantic(23.10), noble(24.04), and oracular the package is `qt6-serialport-dev`

On MacOS, installing QT6 will automatically install the required serialport libraries via homebrew. No extra steps are needed.

To enable the testing serial connection:
- First, `cd build` as normal.
- Now, instead of running `cmake ..` run `cmake .. -DENABLE_SERIAL=ON`. Rebuild the application using `make` afterwards.

After it is built, you should be able to run the application as normal and have the connection to serial working.

If you want to disable the serial connection, you can run `cmake .. -DENABLE_SERIAL=OFF` and then `make`.

If it is not built with serial support, you will not be able to use the 'wire' option in the GUI.

### <a name="connserial"></a>Connecting serial devices (WIP)

On Windows you may need to passthrough the usb device to WSL
see `https://learn.microsoft.com/en-us/windows/wsl/connect-usb` for more details (untested)

In ubuntu you may need to add your user account to the group `dialup` by running
`sudo usermod -a -G dialout username`

### <a name="git"></a>Setup Git

1. [Create an SSH key](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair)

2. [Add the key to your GitLab account](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account)

- This is particularly useful, especially if you don't want to verify your account each time you attempt to push to your branch.

## <a name="grSim"></a>GrSim

GrSim is the simulator our software connects to. While testing make sure both `skynet` and `GrSim` are running.

GrSim will be automatically built by the build scripts listed below.

If desired, build instructions can be found in the GrSim repo https://github.com/RoboCup-SSL/grSim. 

We are specifically using @Jza3's (Jimmy Z, Thanks!) fork of GrSim (grsim6), which can be found here: https://gitlab.com/Jza3/grsim6

This fork is beneficial to us because it uses Qt6, fixing many issues we were having with the original GrSim install conflicting with Skynet.

## <a name="rebuild"></a>Rebuilding the software
This script was highly requested as some people decided it takes a bit too much effort to think to do a fresh build of the project.
This script should only be used when you have already built the software and you want to do a fresh build.

### Please do a fresh build any time you modify a python script.

```bash
#!/bin/bash
cd ~/SFU_Robot_Soccer/sfurs/software
rm -rf build
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

If you want to build with serial support, you can run `cmake -DCMAKE_BUILD_TYPE=Debug -DENABLE_SERIAL=ON ..` instead.

If you want to clean out just the python scripts in the build folder, run:
```bash
rm -rf python_scripts/
make clean
cmake ..
make
```

Otherwise, you can just run `make` in the build folder.

## <a name="macOS"></a>macOS install and build script
This script below assumes that you do not have any of the current libraries already installed.
You will need homebrew in order for this to work. [Install Here](https://brew.sh/).

```zsh
#!/bin/zshrc
brew update
brew doctor
brew install python
brew install pybind11
brew install cmake pkg-config
brew tap robotology/formulae
brew install robotology/formulae/ode
brew install qt@6 protobuf@21 abseil ode boost vulkan-headers mesa eigen
brew link --force --overwrite protobuf@21
xcode-select --install

export PATH="/opt/homebrew/opt/qt/bin:$PATH"
export LDFLAGS="$LDFLAGS -L/opt/homebrew/opt/qt/lib"
export CPPFLAGS="$CPPFLAGS -I/opt/homebrew/opt/qt/include"
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/opt/homebrew/opt/qt/lib/pkgconfig"

export LDFLAGS="$LDFLAGS -L/opt/homebrew/opt/protobuf@21/lib"
export CPPFLAGS="$CPPFLAGS -I/opt/homebrew/opt/protobuf@21/include"
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/opt/homebrew/opt/protobuf@21/lib/pkgconfig"

export LDFLAGS="$LDFLAGS -L/opt/homebrew/opt/vulkan-headers/lib"
export CPPFLAGS="$CPPFLAGS -I/opt/homebrew/opt/vulkan-headers/include"
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/opt/homebrew/opt/vulkan-headers/lib/pkgconfig"

export BOOST_ROOT="/opt/homebrew/opt/boost"

export CMAKE_PREFIX_PATH="/opt/homebrew/opt/qt@5;/opt/homebrew/opt/qt;/opt/homebrew/opt/protobuf@21;/opt/homebrew/opt/vulkan-headers;/opt/homebrew/opt/boost"

source ~/.zshrc

cd ~
mkdir -p SFU_Robot_Soccer
cd SFU_Robot_Soccer

if [ ! -d "ode-0.16.4" ]; then
  curl -L -O https://bitbucket.org/odedevs/ode/downloads/ode-0.16.4.tar.gz
  tar -xf ode-0.16.4.tar.gz
  cd ode-0.16.4/
  ./configure --enable-double-precision
  make
  sudo make install
  cd ..
fi

if [ ! -d "grSim" ]; then
  git clone https://gitlab.com/Jza3/grSim6 --recurse-submodules grSim
fi
cd grSim
mkdir -p build
cd build
cmake -DCMAKE_PREFIX_PATH="/opt/homebrew/opt/qt@6" -DCMAKE_INSTALL_PREFIX=/usr/local ..
make

cd ~/SFU_Robot_Soccer/
mkdir -p sfurs
cd sfurs
if [ ! -d "software" ]; then
  git clone https://gitlab.com/sfurs/software.git
fi
cd software
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DENABLE_SERIAL=OFF ..
make
```

Once done, open another MacOS terminal and run the following command to open **GrSim**:
```
~/SFU_Robot_Soccer/grSim/bin/grSim
```
On the other terminal, run the following command to open **skynet**:
```
~/SFU_Robot_Soccer/sfurs/software/build/skynet.app/Contents/MacOS/skynet
```

### If a script in macOS is broken, feel free to try this to reset your libraries in macOS using this script
```zsh
#!/bin/zsh
brew uninstall cmake pkg-config robotology/formulae/ode qt@6 protobuf@21 abseil boost vulkan-headers mesa

brew unlink cmake pkg-config robotology/formulae/ode qt@6 protobuf@21 abseil boost vulkan-headers mesa

rm -rf /usr/local/opt/qt@6
rm -rf /usr/local/opt/protobuf@21
rm -rf /usr/local/opt/ode
rm -rf /usr/local/opt/abseil
rm -rf /usr/local/opt/boost
rm -rf /usr/local/opt/vulkan-headers
rm -rf /usr/local/opt/mesa

find /usr/local/bin -type l -exec sh -c 'test ! -e "{}" && echo "{}"' \; -delete

brew cleanup
```

### <a name="macOS-firmware"></a>macOS With Serial (Firmware Team)
```zsh
#!/bin/zshrc
brew update
brew doctor
brew install python
brew install pybind11
brew install cmake pkg-config
brew tap robotology/formulae
brew install robotology/formulae/ode
brew install qt@6 protobuf@21 abseil ode boost vulkan-headers mesa
brew link --force --overwrite protobuf@21
xcode-select --install

export PATH="/opt/homebrew/opt/qt/bin:$PATH"
export LDFLAGS="$LDFLAGS -L/opt/homebrew/opt/qt/lib"
export CPPFLAGS="$CPPFLAGS -I/opt/homebrew/opt/qt/include"
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/opt/homebrew/opt/qt/lib/pkgconfig"

export LDFLAGS="$LDFLAGS -L/opt/homebrew/opt/protobuf@21/lib"
export CPPFLAGS="$CPPFLAGS -I/opt/homebrew/opt/protobuf@21/include"
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/opt/homebrew/opt/protobuf@21/lib/pkgconfig"

export LDFLAGS="$LDFLAGS -L/opt/homebrew/opt/vulkan-headers/lib"
export CPPFLAGS="$CPPFLAGS -I/opt/homebrew/opt/vulkan-headers/include"
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/opt/homebrew/opt/vulkan-headers/lib/pkgconfig"

export BOOST_ROOT="/opt/homebrew/opt/boost"

export CMAKE_PREFIX_PATH="/opt/homebrew/opt/qt@5;/opt/homebrew/opt/qt;/opt/homebrew/opt/protobuf@21;/opt/homebrew/opt/vulkan-headers;/opt/homebrew/opt/boost"

source ~/.zshrc

cd ~
mkdir -p SFU_Robot_Soccer
cd SFU_Robot_Soccer

if [ ! -d "ode-0.16.4" ]; then
  curl -L -O https://bitbucket.org/odedevs/ode/downloads/ode-0.16.4.tar.gz
  tar -xf ode-0.16.4.tar.gz
  cd ode-0.16.4/
  ./configure --enable-double-precision
  make
  sudo make install
  cd ..
fi

if [ ! -d "grSim" ]; then
  git clone https://gitlab.com/Jza3/grSim6 --recurse-submodules grSim
fi
cd grSim
mkdir -p build
cd build
cmake -DCMAKE_PREFIX_PATH="/opt/homebrew/opt/qt@6" -DCMAKE_INSTALL_PREFIX=/usr/local ..
make

cd ~/SFU_Robot_Soccer/
mkdir -p sfurs
cd sfurs
if [ ! -d "software" ]; then
  git clone https://gitlab.com/sfurs/software.git
fi
cd software
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DENABLE_SERIAL=ON ..
make
```

## <a name="windows"></a>Windows Install Instructions 

1. Ensure you have WSL2 installed.

Run `wsl --version` within the Windows Terminal application to check \
or find more information here https://learn.microsoft.com/en-us/windows/wsl/install

2. Ensure Ubuntu is up to date, run the below command to update and install packages 

`sudo apt-get update && apt-get upgrade && apt install build-essential`

If your Ubuntu version is 22.04 or lower, then the version of CMake will be too old to build the software properly.
If there is an error about your CMake version, please use run the `ugprade-cmake-ubuntu.sh` in our project repo to upgrade CMake to the latest version.

3. Continue by following the Ubuntu install instructions with this exception. \
~~When downloading the QT6 installer you must move it from your windows file system into Ubuntu's.~~ \
~~Do this by typing `\\wsl$\Ubuntu` into the Windows File Explorer~~ \
~~You can now move the QT6 installer from your Windows download path `C:\Users\usr\Downloads`~~
~~into your Ubuntu's file system.~~ \
You should be able to ignore this and move forward without manually installing QT6.


## <a name="ubuntu"></a>Installation in Ubuntu 

A large majority of people will be able to install the software using just the script below for Ubuntu. Attempt this script first; if it doesnt work, more detailed instructions will be below.

### All in one (+ n) steps
What you will need:
- Ubuntu *(see above)*
- Your GitLab credentials
  - Replace `https://gitlab.com/sfurs/software.git` below with `git@gitlab.com:sfurs/software.git` if you're using SSH

Paste all of the following into your Linux terminal and press enter.

```bash
#!/bin/bash
sudo apt update && sudo apt upgrade -y ; sudo apt autoremove -y

cd ~
mkdir SFU_Robot_Soccer
cd SFU_Robot_Soccer
sudo apt install -y git build-essential cmake pkg-config qt6-base-dev \
                    libgl1-mesa-dev libprotobuf-dev protobuf-compiler libeigen3-dev \
                    libode-dev python3 python3-pip python3-dev python3-pybind11 libboost-all-dev
cd ~/SFU_Robot_Soccer/
git clone --recursive https://gitlab.com/jza3/grSim6.git grSim
cd grSim \
  && mkdir build
cd build \
  && cmake .. \
  && make

cd ~/SFU_Robot_Soccer/
mkdir sfurs
cd sfurs
git clone https://gitlab.com/sfurs/software.git
cd software \
  && mkdir build
cd build \
  && cmake -DENABLE_SERIAL=OFF -DCMAKE_BUILD_TYPE=Debug .. \
  && make
```

Once done, open another Linux terminal and run the following command to open **GrSim**:
```
~/SFU_Robot_Soccer/grSim/bin/grSim
```
On the other terminal, run the following comand to open **skynet**:
```
~/SFU_Robot_Soccer/sfurs/software/build/skynet
```

### Installing and Running SSL-Game-Controller and SSL-Remote-Control

To install **SSL-Game-Controller**, run the following commands:

```
cd ~/SFU_Robot_Soccer
wget https://github.com/RoboCup-SSL/ssl-game-controller/releases/download/v3.12.8/ssl-game-controller_v3.12.8_linux_amd64    
chmod +x ssl-game-controller_v3.12.8_linux_amd64    
```
Then run the following command to open it:
```
~/SFU_Robot_Soccer/ssl-game-controller_v3.12.8_linux_amd64  
```

Then you would open http://127.0.0.1:8081/ in browser to open the UI.

If you are using **Mac** to install **SSL-Game-Controller**:
```
cd ~/SFU_Robot_Soccer
git clone git@github.com:RoboCup-SSL/ssl-game-controller.git
```
Make sure you have Node and Go installed. 
```
brew update
brew install node
brew install go
```
Build the project.
```
cd ssl-game-controller/
make install
```
Run it once first to generate the config folder.
```
go run cmd/ssl-game-controller/main.go
```
Locate `config/ssl-game-controller.yaml` and change `time-acquisition-mode: system` to `time-acquisition-mode: vision`.  

Start grSim, also run the game controller. You should be able to send commands to skynet via the game controller ui at http://127.0.0.1:8081/.




**SSL-Remote-Control** installation is similar:

```
cd ~/SFU_Robot_Soccer
wget https://github.com/RoboCup-SSL/ssl-remote-control/releases/download/v1.3.2/ssl-remote-control_v1.3.2_linux_amd64
chmod +x ssl-remote-control_v1.3.2_linux_amd64
```
Then run the following command:
```
~/SFU_Robot_Soccer/ssl-remote-control_v1.3.2_linux_amd64
```
And then open http://localhost:8084/ in browser.



# You should only use these instructions below if something isn't working!

All of these things should already be accounted for within the install scripts.

### Set up QT6

1. Make a free, opensource account on https://www.qt.io.
    Make sure to check "I am an individual, not associated with a company" when possible. 

2. Download qt6 from here
[qt6 download link](https://www.qt.io/cs/c/?cta_guid=074ddad0-fdef-4e53-8aa8-5e8a876d6ab4&signature=AAH58kGLerGUHMLG15kyHD08kVrQlkZS_Q&pageId=12602948080&placement_guid=99d9dd4f-5681-48d2-b096-470725510d34&click=3b4a11f2-e697-4309-b02e-dc99db76a031&hsutk=&canon=https%3A%2F%2Fwww.qt.io%2Fdownload-open-source&portal_id=149513&redirect_url=APefjpECj9lat8eGkx-KR55RdOz30Vf_CdndDsCD4u7t6Nszjk2qcyvIXciJTScVLD07yr2S0CNBpdbZTialRpzfRbGrBWckMJHgfkHnxpYehp7tHkyQmitqumttjbo7jFBlQpUfturd)


3.  Make the file an executable

```chmod +x qt-unified-linux-x64-<version-number-here>-online.run```

4.  Then run the file

```./qt-unified-linux-x64-<version-number-here>-online.run```

5.  Sign in within the 'qt setup' window

Once at the 'Installation folder" section select **QT design studio** and **QT 6.5 for desktop development**

### Download Protobuf Compiler
`sudo apt install libprotobuf-dev protobuf-compiler` gets you the compiler \
Check it is working with `protoc --version`


### Clone and Build our Repo

1. In the desired folder run `git clone git@gitlab.com:sfurs/software.git`

2. Once in the `software/` folder run ``` mkdir build && cd build```

3. Still in the build folder run `cmake ..` and then `make .`


### Run the software!

1. simply run `./skynet`


### <a name="ubuntu-firmware"></a>Ubuntu With Serial (Firmware Team)

```bash
#!/bin/bash
sudo apt update && sudo apt upgrade -y ; sudo apt autoremove -y

cd ~
mkdir SFU_Robot_Soccer
cd SFU_Robot_Soccer
sudo apt install -y git build-essential cmake pkg-config qt6-base-dev \
                    libgl1-mesa-dev libprotobuf-dev protobuf-compiler libeigen3-dev \
                    libode-dev python3 python3-pip python3-dev python3-pybind11 libboost-all-dev
cd ~/SFU_Robot_Soccer/
git clone --recursive https://gitlab.com/jza3/grSim6.git grSim
cd grSim \
  && mkdir build
cd build \
  && cmake .. \
  && make

cd ~/SFU_Robot_Soccer/
mkdir sfurs
cd sfurs
git clone https://gitlab.com/sfurs/software.git
cd software \
  && mkdir build
cd build \
  && cmake -DENABLE_SERIAL=ON -DCMAKE_BUILD_TYPE=Debug .. \
  && make
```

## <a name="docker"></a>
Refer to [DOCKER.md](DOCKER.md) for instructions on how to run the software in Docker.

#ifndef AGENT_H
#define AGENT_H

#include <QObject>
#include "worldstate/worldStateManager.h"
#include "controls/AgentControl.h"
#include "python/AgentPython.h"
#include "controls/Navigator.h"
#include "controls/specifications/NavigatorCommand.h"
#include "controls/specifications/RotationSpec.h"
#include "agent/fsm/PlaySelectionFSM.h"
#include "agent/fsm/PlaySelection.h"

#include <QMap>
#include <string>

extern QMap<PlaySelection, std::string> playSelectionToString;

class Agent : public QObject {
    Q_OBJECT
    public:
        Agent(QObject* parent, Navigator* nav, AgentPython* agentPython, WorldStateManager* wsm, AgentControl* control, bool createTest);
        ~Agent();
        WorldState* getAgentState();
        void updateWorldState(WorldState* ws);
        void start();
    signals:
        void navigateTo(int id, float x, float y, RotationSpec rotationSpec);
        void clearAndNavigateTo(int id, float x, float y, RotationSpec rotationSpec);
        void halt(int id);
        void haltAll();
        void unhalt(int id);
        void unhaltAll();
        void clearQueue(int id); 
        void clearAllQueues();
    public slots:
        void transitionToPlaySelection(PlaySelection playSelection);
        void tick();
    private:
        bool isTester;

        AgentPython* agentPython;
        Navigator* navigator;
        WorldStateManager* worldStateManager;
        AgentControl* control;
        StrategyDecision calculateStrategyDecision(WorldState worldState);
        bool isAgentControlManual();
        void run();
        void runTest();

        // for FSMs
        WorldState agentWorldState;
        GameState agentGameState;
        PlaySelectionFSM playSelectionFSM;

        // for tick
        PlaySelection previousPlaySelection;
        PlaySelection currentPlaySelection;
};

#endif

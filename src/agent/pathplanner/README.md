# Path Planning System

## Overview

This project implements a grid-based path planning system for a robotics application, utilizing a hexagonal grid layout. The system includes two primary modules:

1. **Grid Module (`Grid.cpp`)**: Manages the creation, initialization, and updating of the hexagonal grid based on real-time world data.
2. **Path Planner Module (`PathPlanner.cpp`)**: (Partially provided) Responsible for calculating optimal paths through the grid using pathfinding algorithms.

## Features

- **Hexagonal Grid Representation:**
  - The grid is based on a hexagonal tiling pattern, providing efficient neighbor traversal and natural movement patterns for robotics applications.
  - Dynamic grid size based on the field dimensions and hexagon side length.

- **Real-Time Updates:**
  - The grid updates in real-time based on the positions of robots and the ball.
  - Supports collision detection and marks grid cells as occupied or available.

- **Coordinate Conversion:**
  - Converts between Cartesian coordinates and hexagonal grid indices.
  - Includes algorithms for accurate rounding of fractional coordinates to the nearest hexagon.

## File Descriptions

### `Grid.cpp`
- **Grid Initialization:** Creates the hexagonal grid based on field dimensions.
- **Grid Updating:** Updates grid status based on the positions of robots (blue and yellow teams) and the ball.
- **Neighbor Retrieval:** Provides functionality to get neighboring hexagons for pathfinding.
- **Coordinate Mapping:** Functions to convert Cartesian coordinates to grid indices and vice versa.

### `PathPlanner.cpp`
- **Pathfinding Algorithms:** Implements algorithms (e.g., A*, Dijkstra) to find optimal paths between points on the grid.
- **Obstacle Handling:** Considers occupied cells and dynamic obstacles while planning paths.

## Usage

1. **Grid Initialization:**
   ```cpp
   Grid grid(fieldLength, fieldWidth, hexSideLength);
   ```

2. **Updating the Grid:**
   ```cpp
   grid.updateGrid(worldState);
   ```

3. **Path Planning:**
   ```cpp
   PathPlanner planner;
   auto path = planner.findPath(start, goal, grid);
   ```

4. **Debugging:**
   ```cpp
   grid.printHexagons();
   ```

## Dependencies

- **Standard C++ Libraries:**
  - `<cmath>`, `<limits>`, `<stdexcept>`, `<iostream>`
- **Qt Framework:**
  - `QVector` for dynamic array management

## Potential Improvements

- Implement advanced obstacle avoidance techniques.
- Optimize pathfinding algorithms for faster computation.
- Add support for multiple dynamic entities and real-time path recalculation.


#include "Grid.h"
#include <cmath>
#include <limits>
#include <stdexcept>
#include <iostream>

float ROBOT_RADIUS = 90.0;
float BALL_RADIUS = 21.336;
// Constructor
Grid::Grid(double fieldLength, double fieldWidth, double hexSideLength)
    : fieldLength(fieldLength), fieldWidth(fieldWidth), s(hexSideLength)
{
    H = s * sqrt(3);         // Height of hexagon
    W = 2 * s;               // Width of hexagon
    vertSpacing = H;         // Vertical spacing between hex centers
    horizSpacing = 1.5 * s;   // Horizontal spacing between hex centers (skipping)

    // Center of the field corresponds to Cartesian (0, 0)
    originX = fieldLength / 2.0;
    originY = fieldWidth / 2.0;

    // Calculate the number of columns and rows needed
    numColumns = floor((fieldWidth-W) / horizSpacing) +1;  // because y dimension is 'fieldWidth'
    numRows    = floor(fieldLength / vertSpacing);   // because x dimension is 'fieldLength'
    initializeGrid();
}

/*void Grid::initializeGrid()
{
    grid.clear();

    double startX = -originX; // Starting X-coordinate
    double startY = -originY; // Starting Y-coordinate

    double minX = startX;    // To track the minimum X coverage
    double maxX = startX;    // To track the maximum X coverage
    double minY = startY;    // To track the minimum Y coverage
    double maxY = startY;    // To track the maximum Y coverage

    for (int col = 0;; ++col) {
        // Compute X-coordinate for the column
        double centerX = startX + (col * vertSpacing);
        //4500
        //3000
        // Stop adding columns when we exceed the field width
        if ((centerX + H) > originX) {
            //std::cout<<"the max center x "<<centerX<<"\n";
            break;
        }

        QVector<Hexagon> hexagons;

        for (int row = 0;; ++row) {
            // Compute Y-coordinate for the row, accounting for staggering
            
            double centerY;
            if (col % 2 == 0) {
                centerY = startY + (row * horizSpacing);
            } else {
                centerY = startY + (row * horizSpacing) + (horizSpacing / 2);
            }
            if(centerY<0){
                //std::cout<<"the negitive center y "<<centerY<<"\n";
            }
            // Stop adding rows when we exceed the field height
            if ((centerY + horizSpacing) > originY) {
                //std::cout<<"the max center y "<<centerY<<"\n";
                break;
            }

            if (centerX < minX) minX = centerX;
            if (centerX > maxX) maxX = centerX;
            if (centerY < minY) minY = centerY;
            if (centerY > maxY) maxY = centerY;
            //4350 2825.58
            // Check if the hexagon is partially outside the bounds
            bool isPartial = false;
            if (centerX - W / 2 < -originX || centerX + W / 2 > originX ||
                centerY - H / 2 < -originY || centerY + H / 2 > originY) {
                isPartial = true;
            }

            // Add the hexagon to the row
            if(centerX==maxX){
                std::cout << "-------------------------------------=======----------\n";
                std::cout << "-------------------------------------=======----------\n";
                std::cout << centerX <<" "<<centerY<<"\n";
                std::cout << "-------------------------------------=======----------\n";
                std::cout << "-------------------------------------=======----------\n";
            }
            hexagons.push_back({centerX, centerY, isPartial, 'e', false, -1, 0, col, row, nullptr});
        }

        // Add the column to the grid
        grid.push_back(hexagons);
    }
    std::cout << "Coverage Report:\n";
    std::cout << "X-Axis Coverage: Min = " << minX << ", Max = " << maxX << "\n";
    std::cout << "Y-Axis Coverage: Min = " << minY << ", Max = " << maxY << "\n";
}*/

// Initialize the grid
void Grid::initializeGrid()
{
    grid.clear();

    double startX = -originX; // Starting X-coordinate
    double startY = -originY; // Starting Y-coordinate

    for(int col = 0; col < numColumns; ++col){
        // all the x coord in a row are the same because the col are aligned
        double centerY = startY + (col * horizSpacing);
        QVector<Hexagon> hexagons;
        for(int row = 0; row < numRows; ++row){
            
            double centerX;
            // Y values are staggered because it is hexagonal grid,
            // so the Y values start at different values

            if (col % 2 == 0) {
                centerX = startX + (row * H);
            } else {
                centerX = startX + (row * H) + (H / 2);  // Staggered offset
            }

            //checking if the hexagons are outside the x and y range

            hexagons.push_back({centerX, centerY, false, 'e', false, -1, 0, col, row,nullptr});
        }
        grid.push_back(hexagons);
    }
}

// Update the grid based on robots and ball positions
void Grid::updateGrid(const WorldState &state)
{
    int id = 1;
    BallData ball = state.ballData;
    QVector<RobotData> blue_robots = state.blueRobotData;
    // Reset the grid
    for (auto &row : grid){
        for (auto &hex : row)
        {
            hex.available = 0;
            hex.type = 'e';
            hex.id = -1;
            hex.cameFrom = nullptr;
        }
    }
    // Update with blue_robots
    for (const auto &robot : blue_robots)
    {
        Hexagon *hex = getHexFromCoordinates(robot.x, robot.y);
        if (hex)
        {
            QVector<Hexagon*> neighbours = getNeighbors(hex->indX,hex->indY);
            hex->available = 1;
            hex->isBlueTeam = true;
            hex->id = robot.id;
            for(auto n_hex: neighbours){
                n_hex->available =1;
                n_hex->isBlueTeam = true;
                hex->type = 'r';
                n_hex->id = robot.id;
            }
        }
    }
    QVector<RobotData> yellow_robots = state.yellowRobotData;
    for (const auto &robot : yellow_robots)
    {
        Hexagon *hex = getHexFromCoordinates(robot.x, robot.y);
        if (hex)
        {

            QVector<Hexagon*> neighbours = getNeighbors(hex->indX,hex->indY);
            hex->available = 1;
            hex->isBlueTeam = false;
            hex->type = 'r';
            hex->id = robot.id;
            for(auto n_hex: neighbours){
                n_hex->available =1;
                n_hex->isBlueTeam = false;
                n_hex->id = robot.id;
            }
        }
    }
    // Update with ball
    Hexagon *ballHex = getHexFromCoordinates(ball.x, ball.y);
    if (ballHex)
    {
        ballHex->available = 1;
        ballHex->type = 'b';
        ballHex->id = -1;
    }
}

/*void markOccupiedHexes(float x, float y, float radius) {
    // Determine the bounding box for the object
    float minX = x - radius;
    float maxX = x + radius;
    float minY = y - radius;
    float maxY = y + radius;

    QVector<Hexagon*> neighbours = getNeighbors(hex->indX,hex->indY);
    // Iterate over all hexagons within the bounding box
    for (auto hex : grid) {
        float distance = std::sqrt(std::pow(x - hex.centerX, 2) + std::pow(y - hex.centerY, 2));
        if (distance <= radius) {
            hex.available = 1;
        }
    }
}

void Grid::updateGrid(const WorldState& worldState) {
    // Clear previous grid occupancy
    for (auto &row : grid){
        for (auto &hex : row)
        {
            hex.available = 0;
            hex.type = 'e';
            hex.id = -1;
        }
    }

    // Mark hexagons occupied by robots
    for (const RobotData& robot : worldState.blueRobotData) {
        markOccupiedHexes(robot.x, robot.y, ROBOT_RADIUS);
    }
    for (const RobotData& robot : worldState.yellowRobotData) {
        markOccupiedHexes(robot.x, robot.y, ROBOT_RADIUS);
    }

    // Mark hexagons occupied by the ball
    markOccupiedHexes(worldState.ballData.x, worldState.ballData.y, BALL_RADIUS);
}*/


// Convert from pixel (x,y) to axial (q,r)
std::pair<double, double> pixelToAxialFlat(double x, double y, double s)
{
    // s = side length
    double q = ((std::sqrt(3.0) / 3.0) * x - (1.0 / 3.0) * y) / s;
    double r = ((2.0 / 3.0) * y) / s;
    return {q, r};
}

// Convert double axial (q, r) -> int axial (Q, R) by rounding in cube space
std::pair<int,int> axialRound(double q, double r)
{
    // 1) Convert axial -> cube
    double x = q;
    double z = r;
    double y = -x - z;

    // 2) Round x,y,z
    int rx = std::round(x);
    int ry = std::round(y);
    int rz = std::round(z);

    // 3) Fix any rounding drift so that rx+ry+rz == 0
    double dx = std::fabs(rx - x);
    double dy = std::fabs(ry - y);
    double dz = std::fabs(rz - z);

    if (dx > dy && dx > dz) {
        rx = -ry - rz;
    } else if (dy > dz) {
        ry = -rx - rz;
    } else {
        rz = -rx - ry;
    }

    // 4) Convert cube (rx,ry,rz) back to axial (Q,R)
    int Q = rx;
    int R = rz; // we used z = r
    return {Q, R};
}

Hexagon *Grid::getHexFromCoordinates(double x, double y)
{
    // 1) Shift so that (0,0) is the hex grid origin (if necessary).
    //    If your 'originX,originY' are the half-width/height,
    //    and (x,y) is already from -origin..+origin, maybe no shift needed.
    // double localX = x + someOffsetX;
    // double localY = y + someOffsetY;

    double localX = x + originX; 
    double localY = y + originY;

    // 2) Convert pixel -> double axial
    auto [aq, ar] = pixelToAxialFlat(localX, localY, s);

    // 3) Round (aq,ar) to integer axial
    auto [Q, R] = axialRound(aq, ar);

    // 4) Convert axial (Q,R) -> odd-q offset (col, row)
    // Make sure your formula matches how you create columns in initializeGrid().
    int col = R;
    int row = Q + ( (R - (R % 2)) / 2 );
    // 5) Boundary check
    if (col < 0 || col >= numColumns || row < 0 || row >= numRows) {
        return nullptr; // outside grid
    }

    // 6) Return pointer
    return &grid[col][row];
}


//Naive
// Convert Cartesian coordinates to the hexagon covering them
// Hexagon *Grid::getHexFromCoordinates(double x, double y)
// {
//     /*int col = (int)((y+originY)/horizSpacing);
//     int row;
//     if (col % 2 == 0) {
//         row = (int)((x+originX)/H);
//     } 
//     else {
//         row = (int)((x+originX - H/2)/H);  // Staggered offset
//     }*/
//     // Out of bounds
//     if (x < -originX || x > originX || y < -originY || y > originY)
//     {
//         return nullptr;
//     }

//     auto distance = [](double x1, double y1, double x2, double y2) {
//         return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
//     };

//     // {{rowIndex, colIndex}, distance}
//     std::pair<std::pair<int, int>, double> closestHex = {
//         {0, 0}, std::numeric_limits<double>::max()
//     };

//     for (int col = 0; col < numColumns; ++col) {
//         for (int row = 0; row < numRows; ++row) {
//             double centerX = grid[col][row].centerX;
//             double centerY = grid[col][row].centerY;

//             double dist = distance(centerX, centerY, x, y);

//             if (dist < closestHex.second) {
//                 closestHex = {{col, row}, dist};
//             }
//         }
//     }

//     // 3) Return pointer to that closest hex
//     auto [bestCol, bestRow] = closestHex.first;
//     return &grid[bestCol][bestRow];
// }

QVector<Hexagon*> Grid::getNeighbors(int col, int row)
{
    QVector<Hexagon*> neighbors;
    neighbors.reserve(6);  // a hex can have up to 6 neighbors

    bool isOddCol = ((col % 2) == 1);
    const auto& offsets = isOddCol ? oddColOffsets : evenColOffsets;
    // for each of the 6 directions
    for (auto& [dc, dr] : offsets) {
        int nc = col + dc; // neighbor col
        int nr = row + dr; // neighbor row
        // boundary check
        //std::cout<<numColumns<<","<<numRows<<"\n";
        if (nc >= 0 && nc < numColumns &&
            nr >= 0 && nr < numRows)
        {
            neighbors.push_back(&grid[nc][nr]);
            /*if(grid[nc][nr].available==0){ // Ensure it's traversable
                neighbors.push_back(&grid[nc][nr]);
            }*/
        }
    }

    return neighbors;
}


// Helper to round fractional hex coordinates to nearest integer
std::pair<int, int> Grid::hexRound(double q, double r)
{
    double s = -q - r;
    int qRounded = round(q);
    int rRounded = round(r);
    int sRounded = round(s);

    double qDiff = abs(qRounded - q);
    double rDiff = abs(rRounded - r);
    double sDiff = abs(sRounded - s);

    if (qDiff > rDiff && qDiff > sDiff)
    {
        qRounded = -rRounded - sRounded;
    }
    else if (rDiff > sDiff)
    {
        rRounded = -qRounded - sRounded;
    }

    return {qRounded, rRounded};
}

// Get the entire grid for external use
const QVector<QVector<Hexagon>> &Grid::getGrid() const
{
    return grid;
}

void Grid::printHexagons() {
    std::cout << "Hexagon Grid Representation (0 = Available, 1 = Unavailable):\n";
    for (int row = 0; row < grid.size(); ++row) {
        // Print spacing for staggered rows (flat-top hexagons)
        if (row % 2 != 0) {
            std::cout << " ";
        }
        for (int col = 0; col < grid[row].size(); ++col) {
            const Hexagon& hex = grid[row][col];
            std::cout << (hex.available == 0 ? "0 " : "1 ");
        }
        std::cout << "\n"; // Move to the next row
    }
}

double Grid::getHorizSpacing() const {
    return horizSpacing;
}

double Grid::getVertSpacing() const {
    return vertSpacing;
}
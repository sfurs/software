#ifndef GRID_H
#define GRID_H

#include <QVector>
#include <cmath>
#include <utility>
#include "../worldstate/worldState.h"
#include "../worldstate/worldStateManager.h"


struct Hexagon {
    double centerX;   // X coordinate of hexagon's center
    double centerY;   // Y coordinate of hexagon's center
    bool isPartial;   // Is the hexagon partially outside the field
    char type;        // 'b' for ball, 'r' for robot, 'e' for empty, 'l' for border
    bool isBlueTeam;   // True is the hex is a robot and belongs to Blue team and False if not
    int id;           // Robot ID if type is 'r', -1 otherwise
    short available;  // 0 = available, 1 = unavailable
    int indX;         // COl Index of the hexagon in the grid
    int indY;         // ROW Index of the hexagon in the grid
    Hexagon* cameFrom; // Pointer to the hexagon that came before this one
};

enum class HexPrintMode {
    GRID,
    LIST
};

class Grid {
public:
    double fieldLength;
    double fieldWidth;
    Grid(double fieldLength, double fieldWidth, double hexSideLength);

    // Initialize the grid
    void initializeGrid();

    // Update the grid based on robots and ball positions
    void updateGrid(const WorldState& state);

    // Find which hexagon covers a given Cartesian coordinate
    Hexagon* getHexFromCoordinates(double x, double y);
    //get the neighbours of a Hexagon
    QVector<Hexagon*> getNeighbors(int col, int row);

    // Get the entire grid for external use
    const QVector<QVector<Hexagon>>& getGrid() const;

    //print the grid
    void printHexagons();

    // Get the horizontal spacing of the hexagons
    double getHorizSpacing() const;
    // Get the vertical spacing of the hexagons
    double getVertSpacing() const;
    // Clear the cameFrom pointer for all hexagons
    void clearCameFrom();

private:
    double s;       // Side length of hexagon
    double W;       // Width of hexagon
    double H;       // Height of hexagon
    double horizSpacing; // Horizontal spacing between hex centers
    double vertSpacing;  // Vertical spacing between hex centers

    double originX; // Cartesian x-coordinate of the grid center
    double originY; // Cartesian y-coordinate of the grid center

    int numColumns;
    int numRows;

    QVector<QVector<Hexagon>> grid;

    const std::pair<int,int> evenColOffsets[6] = {
        { +1,  0 },
        { -1,  0 },
        {  0, -1 },
        { -1, -1 },
        { +1, -1 },
        {  0, +1 }
    };

    const std::pair<int,int> oddColOffsets[6] = {
        { +1,  0 },
        { -1,  0 },
        {  0, +1 },
        { -1, +1 },
        { +1, +1 },
        {  0, -1 }
    };

    // Helper functions
    std::pair<int, int> hexRound(double q, double r);
};

#endif // GRID_H
#ifndef PATHPLANNER_H
#define PATHPLANNER_H

#include <QVector>
#include <cmath>
#include <utility>
#include "Grid.h"
#include <unordered_set>
#include "../../worldstate/worldState.h"

/*
    PathPlanner is a class that plans a path for a robot to move to a goal.
    It currently uses A* to find the path and then moves the robot along the path.
*/
class PathPlanner {
public:
    PathPlanner(WorldStateManager *wsm);
    // Get path from start to goal
    QVector<Hexagon *> GetPath(Grid& grid, int start_id, Hexagon* goal);
    // Get path from start to closest available hexagon to goal
    QVector<Hexagon *> GetPathClosest(Grid& grid, int start_id, Hexagon* goal);
    // Construct path from start to goal
    QVector<Hexagon *> constructPath(Hexagon *start, Hexagon* goal);
    /**
     * @brief moveAlongPath moves the robot along the path with the given max speed
     * 
     * @param robotId 
     * @param path 
     * @param maxSpeed 
     * @param grid 
     */
    // Heuristic functions
    static double euclideanDistance(Hexagon* h, Hexagon* dest);
    // Compare distances between two hexagons
    static bool compareDistances(Hexagon* h1, Hexagon* h2, Hexagon* dest);
    // Calculate distance between two points
    static float calculateDistance(float x1, float y1, float x2, float y2);

private:
    WorldStateManager *worldStateManager;
    Grid *grid;
    Hexagon *start;
    Hexagon *goal;
    struct WeightedHexagon {
        Hexagon* hex;
        double score;  // Lower score is better

        // Custom comparison for priority queue (min heap)
        bool operator>(const WeightedHexagon& other) const {
            return score > other.score;
        }
    };
    Hexagon *findClosestAvailableHexagon(Grid &grid, Hexagon *target, Hexagon *start);

    // Find neighbours
    // QVector<Hexagon*> getNeighbors(const Hexagon& hex);
    // float heuristic(const Hexagon* a, const Hexagon* b);
};


#endif // PATHPLANNER_H


#include "PathPlanner.h"
#include "Grid.h"
#include <queue>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <cmath>
#include <limits>
#include <stdexcept>
#include <iostream>
#include <thread> // Required for std::this_thread::sleep_for
#include <chrono> // Required for std::chrono::milliseconds
#include "../gui/gamescene.h"
#include <set>
#include <functional>

// Heuristic functions
double PathPlanner::euclideanDistance(Hexagon* h, Hexagon* dest) {
    return (sqrt(pow(h->centerX - dest->centerX, 2) + pow(h->centerY - dest->centerY, 2)));
}

bool PathPlanner::compareDistances(Hexagon* h1, Hexagon* h2, Hexagon* dest) {
    return euclideanDistance(h1, dest) > euclideanDistance(h2, dest);
}
float PathPlanner::calculateDistance(float x1, float y1, float x2, float y2) {
    float distance = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
    if (std::isnan(distance)) {
        qDebug() << "Error: Distance calculation resulted in NaN. Target: (" 
                << x2 << ", " << y2 << "), Current: (" 
                << x1 << ", " << y1 << ")";
        distance = 0; // Fallback value
    }
    return distance;
}

PathPlanner::PathPlanner(WorldStateManager* wsm) {
    this->worldStateManager = wsm;
}

// Heuristic function to calculate straight-line distance between 2 hexagons
// float heuristic(const Hexagon* a, const Hexagon* b) {
//     float dx = a->centerX - b->cworldStateManagerenterX;
//     float dy = a->centerY - b->centerY;
//     return sqrt(dx * dx + dy * dy);
// }

/*QVector<Hexagon*> getNeighbors(Hexagon& hex, Grid *grid){
    QVector<Hexagon*> neighbors;
    
    const float horizSpacing = grid->getHorizSpacing();
    const float vertSpacing = grid->getVertSpacing();

    const int currentIndexX = hex.indX;
    const int currentIndexY = hex.indY;

    // Axial coordinate directions for neighbors
    QVector<std::pair<int, int>> directions = {
        {1, 0}, {-1, 0}, {0, 1}, {0, -1}, {1, -1}, {-1, 1}};

    // Loop through each direction to find neighbour
    for (auto [dq, dr] : directions) {
        float neighborX = hex.centerX + horizSpacing * dq;
        float neighborY = hex.centerY + vertSpacing * dr;

        int boundX = grid->getGrid().length();
        int boundY = grid->getGrid()[0].length();

        if (currentIndexX + dq < 0 || currentIndexY + dr < 0) continue; 
        if (currentIndexX + dq >= boundX || currentIndexY + dr >= boundY) continue;

        Hexagon* neighborHex = grid->getHexFromCoordinates(currentIndexX + dq, currentIndexY + dr);

        if (neighborHex == nullptr) {
          std::cout << "neighbor == null\n"; 
          continue;
        }
        // Hexagon* neighborHex = grid->getHexFromCoordinates(neighborX, neighborY);
        if (neighborHex && neighborHex->available == 0) { // Ensure it's traversable
            neighbors.push_back(neighborHex);
        }
    }

    return neighbors;
}*/

QVector<Hexagon *> PathPlanner::constructPath(Hexagon *start, Hexagon* goal) {
    QVector<Hexagon *> path;
    Hexagon* curHex = goal;
    while(curHex != start && curHex != nullptr) {
        path.push_back(curHex);
        curHex = curHex->cameFrom;
        if (path.size() > 1000) {
            std::cout<<"path size: "<<path.size()<<"\n" << "path size too large\n";
            break;
        }
    }
    path.push_back(start);
    std::reverse(path.begin(), path.end());
    return path;
}

QVector<Hexagon *> PathPlanner::GetPath(Grid& grid, int start_id, Hexagon* goal) {
    //grid.clearCameFrom();
    WorldState ws = worldStateManager->getCurrentState();
    Hexagon* start = nullptr;
    for (RobotData const& robot : ws.friendlyRobots())
    {
        if (robot.id == start_id)
        {
            start = grid.getHexFromCoordinates(robot.x, robot.y);
            break;
        }
    }

    if (!start)
    {
        std::cout << "start is null\n";
        return QVector<Hexagon*>();
    }
    if (!goal) {
        std::cout << "goal is null\n";
        return QVector<Hexagon*>();
    }
    if (goal->available != 0) {
        std::cout<<"goal not available\n";
        return QVector<Hexagon*>();
    }
    // Initialize Priority Queue
    auto cmp = [&goal](Hexagon* h1, Hexagon* h2) { return compareDistances(h1, h2, goal); };
    std::priority_queue<Hexagon*, std::vector<Hexagon*>, decltype(cmp)> openSet(cmp);
    Hexagon* pretend_start = nullptr;
    QVector<Hexagon*> start_neighbors = grid.getNeighbors(start->indX, start->indY);
    bool break_true = false;
    for(Hexagon* neighbor : start_neighbors) {
        QVector<Hexagon*> neighbor_neighbors = grid.getNeighbors(neighbor->indX, neighbor->indY);
        for(Hexagon* neighbor_neighbor : neighbor_neighbors){
            if(neighbor_neighbor->available==0){
                pretend_start = neighbor_neighbor;
                break_true = true;
                break;
            }
        }
        if(break_true){
            break;
        }
    
    }
    openSet.push(pretend_start);

    // Initalize scores
    std::unordered_map<Hexagon*, float> gScore; /* gScore[hex] is the current known cheapest path from start to hex */
    gScore.insert({pretend_start, 0.0f});
    std::unordered_map<Hexagon*, bool> inQueue; /* inQueue stores which hexagons have been placed in the queue */
    inQueue.insert({pretend_start, true});
    std::unordered_set<Hexagon*> visited;  // Add visited set
    visited.insert(pretend_start);

    while (!openSet.empty()) {
        Hexagon* current = openSet.top();
        openSet.pop();
        
        if (current == nullptr) continue;

        if (euclideanDistance(current, goal) == 0) {
            return constructPath(start, current);
        }

        QVector<Hexagon*> neighbors = grid.getNeighbors(current->indX, current->indY);

        for(Hexagon* neighbor : neighbors) {
            if(neighbor->available == 0 && visited.find(neighbor) == visited.end()) {  // Check if not visited
                float tentativeGScore = gScore[current] + euclideanDistance(current, neighbor);
                if (gScore.find(neighbor) == gScore.end() || tentativeGScore < gScore[neighbor]) {
                    neighbor->cameFrom = current;
                    gScore.insert_or_assign(neighbor, tentativeGScore);
                    if (inQueue.find(neighbor) == inQueue.end()) {
                        inQueue.insert({neighbor, true});
                        openSet.push(neighbor);
                        visited.insert(neighbor);  // Mark as visited
                    }
                }
            }
        }
    }

    // Failure case - no path could be found
    // returns empty vector
    std::cout << "pathfinding failure" << std::endl;
    return QVector<Hexagon*>();
}

QVector<Hexagon *> PathPlanner::GetPathClosest(Grid& grid, int start_id, Hexagon* goal) {
    //grid.clearCameFrom();
    WorldState ws = worldStateManager->getCurrentState();
    Hexagon* start = nullptr;
    for (RobotData const& robot : ws.friendlyRobots())
    {
        if (robot.id == start_id)
        {
            start = grid.getHexFromCoordinates(robot.x, robot.y);
            break;
        }
    }

    if (!start) {
        std::cout << "start is null\n";
        return QVector<Hexagon*>();
    }
    if (!goal) {
        std::cout << "goal is null\n";
        return QVector<Hexagon*>();
    }
    if (goal->available != 0) {
        Hexagon* alternateGoal = findClosestAvailableHexagon(grid, goal, start);
        if (alternateGoal) {
            goal = alternateGoal;
        } else {
            std::cout << "No available hexagons found near goal\n";
            return QVector<Hexagon*>();
        }
    }
    // std::cout<<start_id<<" start robot position: "<<start->centerX<<" "<<start->centerY<<"\n";
    // Initialize Priority Queue
    auto cmp = [&goal](Hexagon* h1, Hexagon* h2) { return compareDistances(h1, h2, goal); };
    std::priority_queue<Hexagon*, std::vector<Hexagon*>, decltype(cmp)> openSet(cmp);
    Hexagon* pretend_start = nullptr;
    QVector<Hexagon*> start_neighbors = grid.getNeighbors(start->indX, start->indY);
    bool break_true = false;
    for(Hexagon* neighbor : start_neighbors) {
        QVector<Hexagon*> neighbor_neighbors = grid.getNeighbors(neighbor->indX, neighbor->indY);
        for(Hexagon* neighbor_neighbor : neighbor_neighbors){
            if(neighbor_neighbor->available==0){
                pretend_start = neighbor_neighbor;
                break_true = true;
                break;
            }
        }
        if(break_true){
            break;
        }
    
    }
    openSet.push(pretend_start);

    // Initalize scores
    std::unordered_map<Hexagon*, float> gScore; /* gScore[hex] is the current known cheapest path from start to hex */
    gScore.insert({pretend_start, 0.0f});
    std::unordered_map<Hexagon*, bool> inQueue; /* inQueue stores which hexagons have been placed in the queue */
    inQueue.insert({pretend_start, true});
    std::unordered_set<Hexagon*> visited;  // Add visited set
    visited.insert(pretend_start);

    while (!openSet.empty()) {
        Hexagon* current = openSet.top();
        openSet.pop();
        
        if (current == nullptr) continue;

        if (euclideanDistance(current, goal) == 0) {
            return constructPath(start, current);
        }

        QVector<Hexagon*> neighbors = grid.getNeighbors(current->indX, current->indY);

        for(Hexagon* neighbor : neighbors) {
            if(neighbor->available == 0 && visited.find(neighbor) == visited.end()) {  // Check if not visited
                float tentativeGScore = gScore[current] + euclideanDistance(current, neighbor);
                if (gScore.find(neighbor) == gScore.end() || tentativeGScore < gScore[neighbor]) {
                    neighbor->cameFrom = current;
                    gScore.insert_or_assign(neighbor, tentativeGScore);
                    if (inQueue.find(neighbor) == inQueue.end()) {
                        inQueue.insert({neighbor, true});
                        openSet.push(neighbor);
                        visited.insert(neighbor);  // Mark as visited
                    }
                }
            }
        }
    }

    // Failure case - no path could be found
    // returns empty vector
    std::cout << "pathfinding failure" << std::endl;
    return QVector<Hexagon*>();
}

Hexagon* PathPlanner::findClosestAvailableHexagon(Grid& grid, Hexagon* target, Hexagon* start) {
    if (!target || !start) return nullptr;

    // Using priority queue instead of regular queue for weighted search
    std::priority_queue<PathPlanner::WeightedHexagon, 
                       std::vector<PathPlanner::WeightedHexagon>, 
                       std::greater<PathPlanner::WeightedHexagon>> pq;
    std::set<Hexagon*, std::less<Hexagon*>, std::allocator<Hexagon*>> visited;

    // Weight factors for balancing between target and start distances
    const double TARGET_WEIGHT = 0.7;  // Prioritize being close to target
    const double START_WEIGHT = 0.3;   // Secondary priority to start

    // Helper to calculate weighted score
    auto calculateScore = [&](Hexagon* hex) -> double {
        double distToTarget = PathPlanner::euclideanDistance(hex, target);
        double distToStart = PathPlanner::euclideanDistance(hex, start);
        return (TARGET_WEIGHT * distToTarget) + (START_WEIGHT * distToStart);
    };

    // Start with target's neighbors
    QVector<Hexagon*> initialNeighbors = grid.getNeighbors(target->indX, target->indY);
    for (Hexagon* neighbor : initialNeighbors) {
        if (!neighbor) continue;
        
        double score = calculateScore(neighbor);
        pq.push({neighbor, score});
        visited.insert(neighbor);
    }

    // Process hexagons in order of weighted distance
    while (!pq.empty()) {
        WeightedHexagon current = pq.top();
        pq.pop();

        // If this hexagon is available, it's our best choice so far
        if (current.hex->available == 0) {
            return current.hex;
        }

        // Add unvisited neighbors to queue
        QVector<Hexagon*> neighbors = grid.getNeighbors(current.hex->indX, current.hex->indY);
        for (Hexagon* neighbor : neighbors) {
            if (neighbor && visited.find(neighbor) == visited.end()) {
                double score = calculateScore(neighbor);
                pq.push({neighbor, score});
                visited.insert(neighbor);
            }
        }
    }

    return nullptr;
}

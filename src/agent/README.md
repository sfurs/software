## Overview: Agent

This folder includes core components of the Agent of the system, such as agent management, control, and master.

This folder is divided into three subfolders:
- `controls`: contains the `AgentControl` class, which handles the control of the robots.
- `pathplanner`: contains the `PathPlanner` class, which handles the path planning of the robots.
- `specifications`: contains the `NavigatorCommand` class, which handles the commands for the robots.

## Files

### 1. `Agent.cpp` & `Agent.h`
- **Agent Class**: 
  The `Agent` class represents a controllable entity within the system, inheriting from `QObject`. It interacts with `AgentControl` to execute behaviors such as moving, rotating, and determining possession of the ball.
  
  - **Functions**:
    - `Agent(QObject* parent, AgentControl* control)`: Constructor to initialize an `Agent` with control.
    - `~Agent()`: Destructor for cleanup.
    - `WorldState* getAgentState()`: Returns the current world state of the agent.
    - `void updateWorldState(WorldState* ws)`: Updates the agent's current world state.
    - `void start()`: Initializes and starts the agent's control loop, allowing periodic actions.
    - `bool isAgentControlManual()`: Checks if the agent is under manual control.
    - `bool hasPossession(const RobotData& robotData, const BallData& ballData)`: Determines if the agent currently has possession of the ball based on position and angle.
    - `void run()`: The main loop for the agent's actions, where behaviors such as moving towards goals or avoiding obstacles are handled.

### 2. `AgentMaster.cpp` & `AgentMaster.h`
- **AgentMaster Class**: 
  The `AgentMaster` class coordinates high-level control of the agents. It initiates the agents, manages manual and automatic control, and can interact with other system components such as the world state manager.
  
  - **Functions**:
    - `AgentMaster(AgentControl* control)`: Constructor that initializes `AgentMaster` with an `AgentControl` instance.
    - `~AgentMaster()`: Destructor that cleans up allocated resources.
    - `void start()`: Starts the `AgentMaster`, which in turn starts the agent control processes.
    - `void AgentActions()`: Placeholder function to be implemented for executing actions specific to the master control.

### 3. `CMakeLists.txt`
- This is the build configuration file for the project using CMake.
- It defines how the source files are compiled and includes dependencies such as `Qt6 Core`. 
- The `CMakeLists.txt` file includes all `.cpp` and `.h` files in the project as sources to build the `skynet` executable.

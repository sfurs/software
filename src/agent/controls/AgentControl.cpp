#include "AgentControl.h"
#include "network/netmaster.h"
#include "../worldstate/worldStateManager.h"
#include "RobotInstruction.h"
#include "ControlPID.h"
#include <QtMath>
#include <cmath>
#include <QDebug>
#include <QVector2D>
#include <algorithm>



AgentControl::AgentControl(QObject* parent, NetMaster* nm, WorldStateManager *wsm) : network(nm) {
    worldstate = wsm;
    pid_config = *nm->getPID();
    connect(this, &AgentControl::sendInstruction, nm, &NetMaster::SetRobotInstruction);
}

void AgentControl::start()
{
    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &AgentControl::tick);
    timer->start(1000 / 180); // Run at 180 Hz
}

void AgentControl::setManualControl(bool manual)
{
    std::cout << "Setting manual control to " << manual << std::endl;
    isManualControl = manual;

    if (isManualControl)
    {
        lock.lock();
        for (int rID : robots.keys())
        {
            emit sendInstruction({rID, 0, 0, 0, 0, 0, 0});
        }
        lock.unlock();
    }
}



bool AgentControl::getManualControl() {
    return isManualControl;
}

QMap<int, RobotData> AgentControl::getRobotData(){
    lock.lock();
    QMap<int, RobotData> copy_robots_data = robots_data;
    lock.unlock();
    return copy_robots_data;

}

void AgentControl::moveBy(int id, float forwards, float sideways, float maxspeed)
{
    WorldState field = worldstate->getCurrentState();
    QVector<RobotData> friends = field.friendlyRobots();

    bool foundRobot = false;
    for (RobotData const& friendData : friends)
    {
        if (id == friendData.id)
        {
            double angle = friendData.angle;
            double x = forwards * qCos(angle) - sideways * qSin(angle);
            double y = forwards * qSin(angle) + sideways * qCos(angle);
            moveTowardsPoint(id, friendData.x + x, friendData.y + y, maxspeed);
            foundRobot = true;
        }
    }
    if (!foundRobot)
    {
        qWarning() << "Unable to control robot " << id;
    }
}

void AgentControl::moveTowardsPoint(int id, float x, float y, float maxspeed)
{
    lock.lock();
    
    // Set target for the specific robot
    AgentControl::RobotControl& robot = robots[id];
    robot.pid.updateConfig(pid_config);
    robot.pid.x.setTarget(x);
    robot.pid.y.setTarget(y);
    robot.fixedGlobalPosition = true;
    robot.xSpeed = maxspeed;
    robot.ySpeed = maxspeed;
    robot.shouldSignalMoveFinish = true;
    
    lock.unlock();
}

void AgentControl::rotateBy(int id, double degrees, float maxSpeed)
{
    WorldState field = worldstate->getCurrentState();
    QVector<RobotData> friends = field.friendlyRobots();

    bool foundRobot = false;
    for (RobotData const& friendData : friends)
    {
        if (id == friendData.id)
        {
            rotateTo(id, friendData.angle + degrees, maxSpeed);
            foundRobot = true;
        }
    }

    if (!foundRobot)
    {
        qWarning() << "Unable to control robot " << id;
    }
}

void AgentControl::rotateTo(int id, double degrees, float maxSpeed)
{
    lock.lock();
    AgentControl::RobotControl &robot = robots[id];
    robot.pid.updateConfig(pid_config);
    robot.pid.rotate.setTarget(degrees);
    robot.fixedRotation = true;
    robot.rotationSpeed = maxSpeed;
    robot.shouldSignalRotateFinish = true;
    lock.unlock();
}

void AgentControl::moveForwards(int id, float maxSpeed)
{
    move(id, maxSpeed, qQNaN());
}

void AgentControl::moveSideways(int id, float maxSpeed)
{
    move(id, qQNaN(), maxSpeed);
}

void AgentControl::move(int id, float xSpeed, float ySpeed)
{
    lock.lock();
    AgentControl::RobotControl &robot = robots[id];
    // TODO add movement correction if one side is faster than another
    robot.xSpeed = xSpeed;
    robot.ySpeed = ySpeed;
    robot.fixedGlobalPosition = false;
    robot.shouldSignalMoveFinish = false;
    lock.unlock();
}

void AgentControl::haltRobot(int id)
{
    AgentControl::RobotControl &robot = robots[id];
    robot.fixedGlobalPosition = false;
    robot.fixedRotation = false;
    robot.xSpeed = 0;
    robot.ySpeed = 0;
    robot.rotationSpeed = 0;
    robot.shouldSignalMoveFinish = false;
    robot.shouldSignalRotateFinish = false;
    robot.kick = 0;
    robot.dribble = 0;
    robot.chipper = 0;
    emit sendInstruction({id, 0, 0, 0, 0, 0, 0});
}

void AgentControl::halt(int id)
{
    lock.lock();
    haltRobot(id);
    lock.unlock();
}

void AgentControl::haltAllRobots()
{
    lock.lock();
    for (int rID : robots.keys())
    {
        haltRobot(rID);
    }
    lock.unlock();
}

void AgentControl::stop(int id)
{
    lock.lock();
    AgentControl::RobotControl &robot = robots[id];
    robot.fixedGlobalPosition = false;
    robot.fixedRotation = false;
    robot.xSpeed = 0;
    robot.ySpeed = 0;
    robot.rotationSpeed = 0;
    robot.shouldSignalMoveFinish = true;
    robot.shouldSignalRotateFinish = true;
    robot.kick = 0;
    robot.dribble = 0;
    robot.chipper = 0;
    // emit sendInstruction(RobotInstruction(id, 0, 0, 0, false, false));
    lock.unlock();
    // TODO: tick() should stop requesting movement after this
}

void AgentControl::stopAllRobots()
{
    for (int rID : robots.keys())
    {
        stop(rID);
    }
    // std::cout << "Stopping all robots" << std::endl;
}

void AgentControl::spin(int id, double speed)
{
    lock.lock();
    AgentControl::RobotControl &robot = robots[id];
    robot.fixedRotation = false;
    robot.rotationSpeed = speed;
    robot.shouldSignalRotateFinish = false;
    lock.unlock();
}

void AgentControl::kick(int id, int power, int chipper)
{
    lock.lock();
    AgentControl::RobotControl &robot = robots[id];
    robot.kick = power;
    robot.chipper = chipper;
    robot.dribble = 0;
    lock.unlock();
}

void AgentControl::setDribble(int id, int dribble)
{
    lock.lock();
    robots[id].dribble = dribble;
    robots[id].kick = 0;
    robots[id].chipper = 0;
    lock.unlock();
}

double capSpeed(double maxSpeed, double b)
{
    if (qIsNaN(b) || b == 0)
    {
        return 0;
    }
    maxSpeed = qAbs(maxSpeed);
    if (maxSpeed > qAbs(b))
        return b;
    return maxSpeed * qAbs(b) / b;
}

int capPower(int maxPower, int power)
{
    return std::max(std::min(power, maxPower), 0);
}


void AgentControl::tick(){
    WorldState field = worldstate->getCurrentState();
    RefereeState state = worldstate->getGameState().getRefereeState();
    if (state == RefereeState::HALT)
    {
        haltAllRobots();
        return;
    }

    QVector<RobotData> friends = field.friendlyRobots();
    for (RobotData const& friendData : friends)
    {
        bool emitMoveFinish = false;
        bool emitRotateFinish = false;
        bool emitKickFinish = false;
        lock.lock();
        AgentControl::RobotControl& robot = robots[friendData.id];
        robots_data[friendData.id] = friendData;
        bool newCommand = false;
        RobotInstruction instruction;
        instruction.id = friendData.id;
        if (robot.fixedRotation)
        {
            double error = robot.pid.rotate.getTarget();
            error = fmod(error, M_PI * 2) - fmod(friendData.angle, M_PI * 2);
            if (error > M_PI)
            {
                error -= M_PI * 2;
            }
            else if (error < -M_PI)
            {
                error += M_PI * 2;
            }
            double v_phi = robot.pid.rotate.evaluate(error);
            instruction.v_phi = capSpeed(robot.rotationSpeed, v_phi);
            newCommand = true;
            if (robot.pid.rotate.reachedTargetError(error))
            {
                if (robot.shouldSignalRotateFinish)
                {
                    emitRotateFinish = true;
                    robot.shouldSignalRotateFinish = false;
                    // haltRobot(robotData.id);
                }
            }
        }
        else
        {
            if (!qIsNaN(robot.rotationSpeed) && (!robot.isStopped || robot.rotationSpeed != 0))
            {
                instruction.v_phi = robot.rotationSpeed;
                newCommand = true;
            }
        }
        if (robot.fixedGlobalPosition)
        {
            double error_x = robot.pid.x.getTarget() - friendData.x;
            double error_y = robot.pid.y.getTarget() - friendData.y;
            double angle = -friendData.angle;
            double local_error_x = error_x * qCos(angle) - error_y * qSin(angle);
            double local_error_y = error_x * qSin(angle) + error_y * qCos(angle);
            double v_x = robot.pid.x.evaluate(local_error_x);
            double v_y = robot.pid.y.evaluate(local_error_y);
            instruction.v_x = capSpeed(robot.xSpeed, v_x);
            instruction.v_y = capSpeed(robot.ySpeed, v_y);
            newCommand = true;
            if (robot.pid.x.reachedTargetError(local_error_x) && robot.pid.y.reachedTargetError(local_error_y))
            {
                if (robot.shouldSignalMoveFinish)
                {
                    emitMoveFinish = true;
                    robot.shouldSignalMoveFinish = false;
                }
            }
        }
        else
        {
            if (!qIsNaN(robot.xSpeed) && (!robot.isStopped || robot.xSpeed != 0))
            {
                instruction.v_x = robot.xSpeed;
                newCommand = true;
            }
            if (!qIsNaN(robot.ySpeed) && (!robot.isStopped || robot.ySpeed != 0))
            {
                instruction.v_y = robot.ySpeed;
                newCommand = true;
            }
        }
        if(robot.xSpeed > 1){
            instruction.v_x = capSpeed(1, instruction.v_x);
            newCommand = true;
        }
        if(robot.ySpeed > 1){
            instruction.v_y = capSpeed(1, instruction.v_y);
            newCommand = true;
        }
        if (robot.kick || robot.dribble || robot.chipper) {
            instruction.kick = capPower(15, robot.kick);
            instruction.dribble = capPower(3, robot.dribble);
            instruction.chipper = capPower(3, robot.chipper);
            newCommand = true;
        }
        else
        {
            instruction.kick = 0;
            instruction.dribble = 0;
            instruction.chipper = 0;
        }
        if (newCommand)
        {
            // std::cout << "Sending instruction to robot " << robotData.id << "with vx and vy" <<  instruction.v_x << " " << instruction.v_y << std::endl;
            emit sendInstruction(instruction);
            robot.kick = 0; // Reset kick after sending
            if (instruction.v_x == 0 && instruction.v_y == 0 && instruction.v_phi == 0)
            {
                robot.isStopped = true;
            }
            // Dribble remains until changed
        }
        lock.unlock();
        if (emitMoveFinish)
        {
            emit finishedMoving(friendData.id);
        }
        if (emitRotateFinish)
        {
            emit finishedRotating(friendData.id);
        }
    }
    // TODO also warn when an non existant robot is commanded

}


WorldState AgentControl::passWorldState(){
    return worldstate->getCurrentState();
}

#ifndef NAVIGATOR_H
#define NAVIGATOR_H

#include <QTimer>
#include <QMutex>
#include <QMap>
#include <deque>
#include <tuple>
#include <QObject>
#include <QString>
#include <QVector>
#include "AgentControl.h"
#include "../worldstate/worldStateManager.h"
#include "../worldstate/worldState.h"
#include "specifications/RotationSpec.h"
#include "specifications/NavigatorCommand.h"
#include "../pathplanner/PathPlanner.h"
#include "../pathplanner/Grid.h"

/**
 * @brief Wrapper class for AgentControl, handles navigation and pathfinding, plus other higher level logic.
 * Runs on what was the AgentControl thread, but now handles all navigation logic.
 * 
 */
class Navigator : public QObject {
    Q_OBJECT
public:
    Navigator(QObject* parent, WorldStateManager* wsm, AgentControl* agentControl);
    // Check if a robot is halted
    bool isHalted(int id);
    // Check if it is time to run a new command
    bool shouldRunCommand(int id);
    // Get the next command for a robot
    NavigatorCommand getNextCommand(int id);
    // Create a grid
    Grid CreateGrid();
    // Update the grid
    void UpdateGrid();
    // Check the grid
    void CheckGrid();
    // Get the grid
    Grid* getGrid();

public slots:
    // Start the navigator
    void start();
    // Plans and queues a path for robot to navigate to exact target coordinates using A* pathfinding
    void navigateTo(int id, float x, float y, RotationSpec rotationSpec);
    // Clears robot's command queue then plans new path to exact target coordinates using A* pathfinding
    void clearAndNavigateTo(int id, float x, float y, RotationSpec rotationSpec);
    // Plans and queues a path for robot to navigate to nearest reachable point to target using A* pathfinding
    void navigateToClosest(int id, float x, float y, RotationSpec rotationSpec);
    // Clears robot's command queue then plans new path to nearest reachable point to target using A* pathfinding
    void clearAndNavigateToClosest(int id, float x, float y, RotationSpec rotationSpec);
    // Grab the ball
    void findAndGrabBall(int id);
    // Add a command to kick the ball to a point
    void kickTo(int id, float x, float y, int power, int chipper);
    // Add a command to kick the ball towards the goal
    void kickToGoal(int id, int power);
    // Add a command to kick the ball in a trajectory
    void kickInTrajectory(int id, float angle, int power, int chipper);
    // Add a command to chip to the goal
    void chipToGoal(int id, int chipper);
    // Halt a robot
    void halt(int id);
    // Halt all robots
    void haltAll();
    // Unhalt a robot
    void unhalt(int id);
    // Unhalt all robots
    void unhaltAll();
    // Clear the queue of commands for a robot
    void clearQueue(int id); 
    // Clear the queue of commands for all robots
    void clearAllQueues();
    // Allow a robot to run next move command
    void allowNextMoveCommand(int id);
    // Allow a robot to run next rotate command
    void allowNextRotateCommand(int id);
    // Gets the id of the robot that is closest to the ball
    int findClosestToBall();
    // Moves all robots a certain distance away from the ball
    void moveAwayFromBall(float dist);

    // Convert a series of NavigatorCommands to a path from id
    QVector<Hexagon *> commandsToPaths(int id);
    // Convert a series of NavigatorCommands to a path from commands; ensure that the path is a copy
    QVector<Hexagon *> commandsToPaths(std::deque<NavigatorCommand> commands);

 signals:
     void plotPoints(int id, QVector<Hexagon *> path);

 private:
     void tick();
     // a tick subfunction to add new robots
     void addNewRobots();
     // find the robot data for a robot
     RobotData findRobot(const WorldState &ws, int id, bool &found);

     // tick subfunctions for each command type
     // tick command processor for moveTo
     void MOVE_TO(int id, const NavigatorCommand &command, const RobotData &robot);
     // tick command processor for moveTowardsPoint
     void MOVE_TOWARDS_POINT(int id, const NavigatorCommand &command, const RobotData &robot);
     // tick command processor for moveBy
     void MOVE_BY(int id, const NavigatorCommand &command, const RobotData &robot);
     // tick command processor for rotateTo
     void ROTATE_TO(int id, const NavigatorCommand &command, const RobotData &robot);
     // tick command processor for rotateBy
     void ROTATE_BY(int id, const NavigatorCommand &command, const RobotData &robot);
     // tick command processor for kick
     void KICK(int id, const NavigatorCommand &command, const RobotData &robot);
     // tick command processor for kickTo
     void KICK_TO(int id, const NavigatorCommand &command, const RobotData &robot);
     // tick command processor for kickInTrajectory
     void KICK_IN_TRAJECTORY(int id, const NavigatorCommand &command, const RobotData &robot);
     // tick command processor for dribble
     void DRIBBLE(int id, const NavigatorCommand &command, const RobotData &robot);
     // tick command processor for rotateToPoint
     void ROTATE_TO_POINT(int id, const NavigatorCommand &command, const RobotData &robot);
     // tick command processor for receiveFrom; robot1 is the robot receiving the ball, robot2 is the robot kicking the ball
     void RECEIVE_FROM(int id, const NavigatorCommand &command, const RobotData &robot1, const RobotData &robot2);
     // tick command processor for passToRobot; robot1 is the robot passing the ball, robot2 is the robot receiving the ball
     void PASS_TO_ROBOT(int id, const NavigatorCommand &command, const RobotData &robot1, const RobotData &robot2);

     float calculateAngle(float target_x, float target_y, float robot_x, float robot_y);
     AgentControl *agentControl;
     WorldStateManager *worldStateManager;
     QMutex lock;
     QMap<int, bool> newMoveCommand;
     QMap<int, bool> newRotateCommand;
     QMap<int, bool> haltState;
     QMap<int, std::deque<NavigatorCommand>> path;
     Grid grid = CreateGrid();
     PathPlanner pathPlanner;

     int MAX_SPEED = 4.0;
    

};

#endif
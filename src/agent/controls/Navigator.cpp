#include "Navigator.h"
#include <algorithm> // Add this for std::find

Navigator::Navigator(QObject *parent, WorldStateManager *wsm, AgentControl *agentControl)
    : QObject(parent), worldStateManager(wsm), agentControl(agentControl), pathPlanner(wsm)
{
    connect(agentControl, &AgentControl::finishedMoving, this, &Navigator::allowNextMoveCommand);
    connect(agentControl, &AgentControl::finishedRotating, this, &Navigator::allowNextRotateCommand);
}

void Navigator::start()
{
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Navigator::tick);
    timer->start(1000 / 180); // Run at 180 Hz
}

void Navigator::navigateTo(int id, float x, float y, RotationSpec rotationSpec)
{
    const double MAX_SPEED = 4.0;
    UpdateGrid();
    Hexagon *goal = grid.getHexFromCoordinates(x, y);

    QVector<Hexagon *> hex_path = pathPlanner.GetPath(grid, id, goal);
    emit plotPoints(id, hex_path);

    // Convert Hexagonal Vector to a series of NavigatorCommands

    QVector<NavigatorCommand> commands;

    for (Hexagon *hex : hex_path)
    {
        NavigatorCommand command;
        command.commandType = NavigatorCommandType::MOVE_TO;
        command.params.moveTo.x = static_cast<float>(hex->centerX);
        command.params.moveTo.y = static_cast<float>(hex->centerY);
        command.rotationSpec = rotationSpec;
        commands.append(command);
    }

    commands[commands.size() - 1].commandType = NavigatorCommandType::MOVE_TO;
    commands[commands.size() - 1].params.moveTo.x = static_cast<float>(x);
    commands[commands.size() - 1].params.moveTo.y = static_cast<float>(y);
    commands[commands.size() - 1].rotationSpec = rotationSpec;

    lock.lock();
    for (NavigatorCommand command : commands)
    {
        path[id].push_back(command);
    }
    lock.unlock();
}

void Navigator::clearAndNavigateTo(int id, float x, float y, RotationSpec rotationSpec)
{
    clearQueue(id);
    navigateTo(id, x, y, rotationSpec);
}

void Navigator::navigateToClosest(int id, float x, float y, RotationSpec rotationSpec)
{
    const double MAX_SPEED = 4.0;
    UpdateGrid();
    Hexagon *goal = grid.getHexFromCoordinates(x, y);

    QVector<Hexagon *> hex_path = pathPlanner.GetPathClosest(grid, id, goal);
    emit plotPoints(id, hex_path);

    // Convert Hexagonal Vector to a series of NavigatorCommands

    QVector<NavigatorCommand> commands;

    for (Hexagon *hex : hex_path)
    {
        NavigatorCommand command;
        command.commandType = NavigatorCommandType::MOVE_TO;
        command.params.moveTo.x = static_cast<float>(hex->centerX);
        command.params.moveTo.y = static_cast<float>(hex->centerY);
        command.rotationSpec = rotationSpec;
        commands.append(command);
    }

    lock.lock();
    for (NavigatorCommand command : commands)
    {
        path[id].push_back(command);
    }
    lock.unlock();
}

void Navigator::clearAndNavigateToClosest(int id, float x, float y, RotationSpec rotationSpec)
{
    clearQueue(id);
    navigateToClosest(id, x, y, rotationSpec);
}

void Navigator::findAndGrabBall(int id)
{
    // Get the ball's current position
    WorldState ws = worldStateManager->getCurrentState();
    BallData ball = ws.ballData;

    // Navigate to the ball, as close as possible
    navigateToClosest(id, ball.x, ball.y, RotationSpec::WITH_ROTATION);

    // Get ready to grab the ball
    NavigatorCommand grabCommand;
    grabCommand.commandType = NavigatorCommandType::DRIBBLE;
    grabCommand.params.dribble.power = 2;
    lock.lock();
    path[id].push_back(grabCommand);
    lock.unlock();

    // Move on top of the ball to grab it
    NavigatorCommand command;
    command.commandType = NavigatorCommandType::MOVE_TO;
    command.params.moveTo.x = ball.x;
    command.params.moveTo.y = ball.y;
    command.rotationSpec = RotationSpec::WRT_ROTATION;
    lock.lock();
    path[id].push_back(command);
    lock.unlock();
}

void Navigator::kickTo(int id, float x, float y, int power, int chipper)
{
    lock.lock();
    NavigatorCommand command;
    command.commandType = NavigatorCommandType::KICK_TO;
    command.params.kickTo.x = x;
    command.params.kickTo.y = y;
    command.params.kickTo.kick.power = power;
    command.params.kickTo.kick.chipper = chipper;
    path[id].push_back(command);
    lock.unlock();
}

void Navigator::kickToGoal(int id, int power)
{
    WorldState ws = worldStateManager->getCurrentState();
    int goalIndex = (ws.friendlyTeam == Team::YELLOW && ws.blueOnPositive) || (ws.friendlyTeam == Team::BLUE && !ws.blueOnPositive) ? 0 : 1;
    Rectangle goalArea = ws.fieldDimensions.goals[goalIndex];
    Point goalCenter = goalArea.center();
    kickTo(id, goalCenter.x, goalCenter.y, power, 0);
}

void Navigator::kickInTrajectory(int id, float angle, int power, int chipper)
{
    lock.lock();
    NavigatorCommand command;
    command.commandType = NavigatorCommandType::KICK_IN_TRAJECTORY;
    command.params.kickInTrajectory.kick.power = power;
    command.params.kickInTrajectory.kick.chipper = chipper;
    command.params.kickInTrajectory.theta = angle;
    path[id].push_back(command);
    lock.unlock();
}

void Navigator::chipToGoal(int id, int chipper)
{
    WorldState ws = worldStateManager->getCurrentState();
    int goalIndex = (ws.friendlyTeam == Team::YELLOW && ws.blueOnPositive) || (ws.friendlyTeam == Team::BLUE && !ws.blueOnPositive) ? 0 : 1;
    Rectangle goalArea = ws.fieldDimensions.goals[goalIndex];
    Point goalCenter = goalArea.center();
    kickTo(id, goalCenter.x, goalCenter.y, 100, chipper);
}

void Navigator::halt(int id)
{
    lock.lock();
    haltState[id] = true;
    lock.unlock();
}

void Navigator::haltAll()
{
    lock.lock();
    for (int id : haltState.keys())
    {
        haltState[id] = true;
    }
    lock.unlock();
}

void Navigator::unhalt(int id)
{
    lock.lock();
    haltState[id] = false;
    lock.unlock();
}

void Navigator::unhaltAll()
{
    lock.lock();
    for (int id : haltState.keys())
    {
        haltState[id] = false;
    }
    lock.unlock();
}

int Navigator::findClosestToBall()
{
    WorldState ws = worldStateManager->getCurrentState();
    BallData ball = ws.ballData;
    QMap<int, RobotData> robots = agentControl->getRobotData();

    float lowestDist = 100000;
    float dist;
    int lowestDistID = -1;

    lock.lock();
    for (int id : robots.keys())
    {
        float x = robots[id].x;
        float y = robots[id].y;
        dist = sqrt(((x - ball.x) * (x - ball.x)) + ((y - ball.y) * (y - ball.y)));

        if (dist < lowestDist)
        {
            lowestDist = dist;
            lowestDistID = id;
        }
    }
    lock.unlock();
    return lowestDistID;
}

void Navigator::moveAwayFromBall(float dist)
{
    WorldState ws = worldStateManager->getCurrentState();
    BallData ball = ws.ballData;
    QMap<int, RobotData> robots = agentControl->getRobotData();
    for (int id : robots.keys())
        {
            float x = robots[id].x;
            float y = robots[id].y;
            float distFromBall = sqrt(((x - ball.x) * (x - ball.x)) + ((y - ball.y) * (y - ball.y)));

            if (distFromBall <= dist)
            {
                // std::cout << "id " << id << " too close to ball" << std::endl;
                if(ball.x >= x)
                {
                    // move robot 500 units (0.5 metres) away from the ball
                    this->agentControl->moveTowardsPoint(id, x - dist, y, 1.0); //id, x , y, maxspeed
                }
                else if (ball.x < x)
                {
                    this->agentControl->moveTowardsPoint(id, x + dist, y, 1.0); //id, x , y, maxspeed
                }
                
            }
        }
}

bool Navigator::isHalted(int id)
{
    lock.lock();
    bool halted = haltState[id];
    lock.unlock();
    return halted;
}

bool Navigator::shouldRunCommand(int id)
{
    lock.lock();
    bool shouldRun = !haltState[id] && !path[id].empty() && newMoveCommand[id] && newRotateCommand[id];
    lock.unlock();
    return shouldRun;
}

NavigatorCommand Navigator::getNextCommand(int id)
{
    lock.lock();
    NavigatorCommand command = path[id].front();
    path[id].pop_front();
    lock.unlock();
    return command;
}

void Navigator::clearQueue(int id)
{
    lock.lock();
    path[id].clear();
    lock.unlock();
}

void Navigator::clearAllQueues()
{
    lock.lock();
    for (int id : path.keys())
    {
        path[id].clear();
    }
    lock.unlock();
}

void Navigator::tick()
{
    WorldState ws = worldStateManager->getCurrentState();
    addNewRobots(); // Add new robots to the path and haltState maps
    for (int id : path.keys())
    {
        if (haltState[id])
        {
            agentControl->halt(id);
            continue;
        }

        if (!path[id].empty() && shouldRunCommand(id))
        {
            UpdateGrid();
            bool found;
            RobotData robot = findRobot(ws, id, found);
            if (!found)
            {
                continue;
            }

            NavigatorCommand command;
            lock.lock();
            if (!path[id].empty())
            {
                command = path[id].front();
                path[id].pop_front();
            }
            else
            {
                lock.unlock();
                continue;
            }
            lock.unlock();

            switch (command.commandType)
            {
            case NavigatorCommandType::MOVE_TO:
                MOVE_TO(id, command, robot);
                break;
            case NavigatorCommandType::MOVE_TOWARDS_POINT:
                MOVE_TOWARDS_POINT(id, command, robot);
                break;
            case NavigatorCommandType::MOVE_BY:
                MOVE_BY(id, command, robot);
                break;
            case NavigatorCommandType::ROTATE_TO:
                ROTATE_TO(id, command, robot);
                break;
            case NavigatorCommandType::ROTATE_BY:
                ROTATE_BY(id, command, robot);
                break;
            case NavigatorCommandType::KICK:
                KICK(id, command, robot);
                break;
            case NavigatorCommandType::KICK_TO:
                KICK_TO(id, command, robot);
                break;
            case NavigatorCommandType::KICK_IN_TRAJECTORY:
                KICK_IN_TRAJECTORY(id, command, robot);
                break;
            case NavigatorCommandType::DRIBBLE:
                DRIBBLE(id, command, robot);
                break;
            case NavigatorCommandType::ROTATE_TO_POINT:
                ROTATE_TO_POINT(id, command, robot);
                break;
            case NavigatorCommandType::PASS_TO_ROBOT:
            {
                bool foundPTR;
                RobotData robotPTR = findRobot(ws, command.params.passToRobot.receiverId, foundPTR);
                if (foundPTR){
                    PASS_TO_ROBOT(id, command, robot, robotPTR);
                }
                break;
            }
            case NavigatorCommandType::RECEIVE_FROM:
            {
                bool foundRF;
                RobotData robotRF = findRobot(ws, command.params.receiveFrom.kickerId, foundRF);
                if (foundRF){
                    RECEIVE_FROM(id, command, robot, robotRF);
                }
                break;
            }
            default:
                break;
            }
        }
    }
}

Grid Navigator::CreateGrid()
{
    float s = 180.0 / sqrt(3);
    Grid grid(9300.0, 6300.0, s); // s = (180/2)/sqrt(3)
    return grid;
}

void Navigator::CheckGrid()
{
    Hexagon *hex = grid.getHexFromCoordinates(77.94, 0);
    std::cout << "--------\n";
    std::cout << hex->indX << "," << hex->indY << "\n";
    std::cout << "--------\n";
}

void Navigator::UpdateGrid()
{
    WorldState worldState = worldStateManager->getCurrentState();
    this->grid.updateGrid(worldState);
}

void Navigator::allowNextMoveCommand(int id)
{
    lock.lock();
    newMoveCommand[id] = true;
    lock.unlock();
}

void Navigator::allowNextRotateCommand(int id)
{
    lock.lock();
    newRotateCommand[id] = true;
    lock.unlock();
}

// Convert from -180 to +180 range to 0 to 360 range
float Navigator::calculateAngle(float target_x, float target_y, float robot_x, float robot_y)
{
    // Calculate angle between robot and target
    float angle = atan2(target_y - robot_y, target_x - robot_x);
    return angle;
}

// Command type handler functions
void Navigator::MOVE_TO(int id, const NavigatorCommand &command, const RobotData &robot)
{
    if (command.rotationSpec == RotationSpec::IN_CMD || command.rotationSpec == RotationSpec::NO_ROTATION)
    {
        agentControl->moveTowardsPoint(id, command.params.moveTo.x, command.params.moveTo.y, MAX_SPEED);
        lock.lock();
        newMoveCommand[id] = false;
        lock.unlock();
    }
    else if (command.rotationSpec == RotationSpec::WRT_ROTATION)
    {
        // push the command back into the queue with NO_ROTATION
        NavigatorCommand newCommand = command;
        newCommand.rotationSpec = RotationSpec::NO_ROTATION;
        lock.lock();
        path[id].push_front(newCommand);
        lock.unlock();

        float angle = calculateAngle(command.params.moveTo.x, command.params.moveTo.y, robot.x, robot.y);
        agentControl->rotateTo(id, angle, MAX_SPEED);
        lock.lock();
        newRotateCommand[id] = false;
        lock.unlock();
    }
    else if (command.rotationSpec == RotationSpec::WITH_ROTATION)
    {
        float angle = calculateAngle(command.params.moveTo.x, command.params.moveTo.y, robot.x, robot.y);
        agentControl->rotateTo(id, angle, MAX_SPEED);
        agentControl->moveTowardsPoint(id, command.params.moveTo.x, command.params.moveTo.y, MAX_SPEED);
        lock.lock();
        newMoveCommand[id] = false;
        newRotateCommand[id] = false;
        lock.unlock();
    }
}

void Navigator::MOVE_TOWARDS_POINT(int id, const NavigatorCommand &command, const RobotData &robot)
{
    if (command.rotationSpec == RotationSpec::IN_CMD || command.rotationSpec == RotationSpec::NO_ROTATION)
    {
        agentControl->moveTowardsPoint(id, command.params.moveTowardsPoint.x, command.params.moveTowardsPoint.y, MAX_SPEED);
        newMoveCommand[id] = false;
    }
    else if (command.rotationSpec == RotationSpec::WRT_ROTATION)
    {
        // push the command back into the queue with NO_ROTATION
        NavigatorCommand newCommand = command;
        newCommand.rotationSpec = RotationSpec::NO_ROTATION;
        lock.lock();
        path[id].push_front(newCommand);
        lock.unlock();

        float angle = calculateAngle(command.params.moveTowardsPoint.x, command.params.moveTowardsPoint.y, robot.x, robot.y);
        agentControl->rotateTo(id, angle, MAX_SPEED);
        lock.lock();
        newRotateCommand[id] = false;
        lock.unlock();
    }
    else if (command.rotationSpec == RotationSpec::WITH_ROTATION)
    {
        float angle = calculateAngle(command.params.moveTowardsPoint.x, command.params.moveTowardsPoint.y, robot.x, robot.y);
        agentControl->rotateTo(id, angle, MAX_SPEED);
        agentControl->moveTowardsPoint(id, command.params.moveTowardsPoint.x, command.params.moveTowardsPoint.y, MAX_SPEED);
        lock.lock();
        newMoveCommand[id] = false;
        newRotateCommand[id] = false;
        lock.unlock();
    }
}

void Navigator::MOVE_BY(int id, const NavigatorCommand &command, const RobotData &robot)
{
    if (command.rotationSpec == RotationSpec::IN_CMD || command.rotationSpec == RotationSpec::NO_ROTATION)
    {
        agentControl->moveBy(id, command.params.moveBy.deltaX, command.params.moveBy.deltaY, MAX_SPEED);
        lock.lock();
        newMoveCommand[id] = false;
        lock.unlock();
    }
    else if (command.rotationSpec == RotationSpec::WRT_ROTATION)
    {
        NavigatorCommand newCommand = command;
        newCommand.rotationSpec = RotationSpec::NO_ROTATION;
        lock.lock();
        path[id].push_front(newCommand);
        lock.unlock();

        float angle = calculateAngle(command.params.moveBy.deltaX, command.params.moveBy.deltaY, robot.x, robot.y);
        agentControl->rotateTo(id, angle, MAX_SPEED);
        lock.lock();
        newRotateCommand[id] = false;
        lock.unlock();
    }
    else if (command.rotationSpec == RotationSpec::WITH_ROTATION)
    {
        float angle = calculateAngle(command.params.moveBy.deltaX, command.params.moveBy.deltaY, robot.x, robot.y);
        agentControl->rotateTo(id, angle, MAX_SPEED);
        agentControl->moveBy(id, command.params.moveBy.deltaX, command.params.moveBy.deltaY, MAX_SPEED);
        lock.lock();
        newMoveCommand[id] = false;
        newRotateCommand[id] = false;
        lock.unlock();
    }
}

void Navigator::ROTATE_TO(int id, const NavigatorCommand &command, const RobotData &robot)
{
    agentControl->rotateTo(id, command.params.rotateTo.theta, MAX_SPEED);
    lock.lock();
    newRotateCommand[id] = false;
    lock.unlock();
}

void Navigator::ROTATE_TO_POINT(int id, const NavigatorCommand &command, const RobotData &robot)
{
    float angle = calculateAngle(command.params.rotateToPoint.x, command.params.rotateToPoint.y, robot.x, robot.y);
    agentControl->rotateTo(id, angle, MAX_SPEED);
    lock.lock();
    newRotateCommand[id] = false;
    lock.unlock();
}

void Navigator::ROTATE_BY(int id, const NavigatorCommand &command, const RobotData &robot)
{
    agentControl->rotateBy(id, command.params.rotateBy.deltaTheta, MAX_SPEED);
    lock.lock();
    newRotateCommand[id] = false;
    lock.unlock();
}

void Navigator::DRIBBLE(int id, const NavigatorCommand &command, const RobotData &robot)
{
    agentControl->setDribble(id, command.params.dribble.power);
}

void Navigator::KICK(int id, const NavigatorCommand &command, const RobotData &robot)
{
    // assumes the robot has the posession of the ball
    agentControl->kick(id, command.params.kick.power, command.params.kick.chipper);
}

void Navigator::KICK_TO(int id, const NavigatorCommand &command, const RobotData &robot)
{
    // assumes the robot has the posession of the ball

    NavigatorCommand kickCommand;
    kickCommand.commandType = NavigatorCommandType::KICK;
    kickCommand.params.kick.power = command.params.kickTo.kick.power;
    kickCommand.params.kick.chipper = command.params.kickTo.kick.chipper;
    lock.lock();
    path[id].push_front(kickCommand);
    lock.unlock();

    NavigatorCommand releaseCommand;
    releaseCommand.commandType = NavigatorCommandType::DRIBBLE;
    releaseCommand.params.dribble.power = 0;
    lock.lock();
    path[id].push_front(releaseCommand);
    lock.unlock();

    float angle = calculateAngle(command.params.kickTo.x, command.params.kickTo.y, robot.x, robot.y);
    NavigatorCommand rotateCommand;
    rotateCommand.commandType = NavigatorCommandType::ROTATE_TO;
    rotateCommand.params.rotateTo.theta = angle;
    rotateCommand.rotationSpec = RotationSpec::NO_ROTATION;
    lock.lock();
    path[id].push_front(rotateCommand);
    lock.unlock();

    NavigatorCommand grabCommand;
    grabCommand.commandType = NavigatorCommandType::DRIBBLE;
    grabCommand.params.dribble.power = 2;
    lock.lock();
    path[id].push_front(grabCommand);
    lock.unlock();
}

void Navigator::KICK_IN_TRAJECTORY(int id, const NavigatorCommand &command, const RobotData &robot)
{
    // assumes the robot has the posession of the ball

    NavigatorCommand kickCommand;
    kickCommand.commandType = NavigatorCommandType::KICK;
    kickCommand.params.kick.power = command.params.kickInTrajectory.kick.power;
    kickCommand.params.kick.chipper = command.params.kickInTrajectory.kick.chipper;
    lock.lock();
    path[id].push_front(kickCommand);
    lock.unlock();

    NavigatorCommand releaseCommand;
    releaseCommand.commandType = NavigatorCommandType::DRIBBLE;
    releaseCommand.params.dribble.power = 0;
    lock.lock();
    path[id].push_front(releaseCommand);
    lock.unlock();

    NavigatorCommand rotateCommand;
    rotateCommand.commandType = NavigatorCommandType::ROTATE_TO;
    rotateCommand.params.rotateTo.theta = command.params.kickInTrajectory.theta;
    rotateCommand.rotationSpec = RotationSpec::NO_ROTATION;
    lock.lock();
    path[id].push_front(rotateCommand);
    lock.unlock();

    NavigatorCommand grabCommand;
    grabCommand.commandType = NavigatorCommandType::DRIBBLE;
    grabCommand.params.dribble.power = 2;
    lock.lock();
    path[id].push_front(grabCommand);
    lock.unlock();
}

void Navigator::RECEIVE_FROM(int id, const NavigatorCommand& command, const RobotData& robot1, const RobotData& robot2){
    float angle = calculateAngle(robot2.x, robot2.y, robot1.x, robot1.y);
    //Rotate to the robot kicking the ball
    NavigatorCommand rotateCommand;
    rotateCommand.commandType = NavigatorCommandType::ROTATE_TO;
    rotateCommand.params.rotateTo.theta = angle;
    rotateCommand.rotationSpec = RotationSpec::NO_ROTATION;
    lock.lock();
    path[id].push_front(rotateCommand);
    lock.unlock();
    
    //Recieve the ball (trigger first so we can catch ball even if accidentally kicked early)
    NavigatorCommand dribbleCommand;
    dribbleCommand.commandType = NavigatorCommandType::DRIBBLE;
    dribbleCommand.params.dribble.power = 2;
    lock.lock();
    path[id].push_front(dribbleCommand);
    lock.unlock();
}

void Navigator::PASS_TO_ROBOT(int id, const NavigatorCommand &command, const RobotData &robot1, const RobotData &robot2){
    //Pass the ball
    NavigatorCommand kickToCommand;
    kickToCommand.commandType = NavigatorCommandType::KICK_TO;
    kickToCommand.params.kickTo.kick.power = command.params.passToRobot.kick.power;
    kickToCommand.params.kickTo.kick.chipper = command.params.passToRobot.kick.chipper;
    kickToCommand.params.kickTo.x = robot2.x;
    kickToCommand.params.kickTo.y = robot2.y;
    lock.lock();
    path[id].push_front(kickToCommand);
    lock.unlock();
}

void Navigator::addNewRobots()
{
    WorldState ws = worldStateManager->getCurrentState();
    for (RobotData robot : ws.blueRobotData)
    {
        if (!path.contains(robot.id))
        {
            path[robot.id] = std::deque<NavigatorCommand>();
        }
        if (!haltState.contains(robot.id))
        {
            haltState[robot.id] = false;
        }
        if (!newMoveCommand.contains(robot.id))
        {
            newMoveCommand[robot.id] = true;
        }
        if (!newRotateCommand.contains(robot.id))
        {
            newRotateCommand[robot.id] = true;
        }
    }
    for (RobotData robot : ws.yellowRobotData)
    {
        if (!path.contains(robot.id))
        {
            path[robot.id] = std::deque<NavigatorCommand>();
        }
        if (!haltState.contains(robot.id))
        {
            haltState[robot.id] = false;
        }
        if (!newMoveCommand.contains(robot.id))
        {
            newMoveCommand[robot.id] = true;
        }
        if (!newRotateCommand.contains(robot.id))
        {
            newRotateCommand[robot.id] = true;
        }
    }
}

RobotData Navigator::findRobot(const WorldState &ws, int id, bool &found)
{
    found = false;
    for (const RobotData &r : ws.friendlyRobots())
    {
        if (r.id == id)
        {
            found = true;
            return r;
        }
    }
    return RobotData(); // Return default-constructed RobotData if not found
    // TODO: consider a nullptr instead of a default struct for safety
}

Grid *Navigator::getGrid()
{
    return &grid;
}

QVector<Hexagon *> Navigator::commandsToPaths(int id)
{
    if (!path.contains(id) || path[id].empty())
    {
        return QVector<Hexagon *>();
    }
    lock.lock();
    std::deque<NavigatorCommand> commands = path[id];
    lock.unlock();
    return commandsToPaths(commands);
}

QVector<Hexagon *> Navigator::commandsToPaths(std::deque<NavigatorCommand> commands)
{
    QVector<Hexagon *> path;
    for (NavigatorCommand command : commands)
    {
        switch (command.commandType)
        {
        case NavigatorCommandType::MOVE_TO:
            path.append(grid.getHexFromCoordinates(command.params.moveTo.x, command.params.moveTo.y));
            break;
        case NavigatorCommandType::MOVE_TOWARDS_POINT:
            path.append(grid.getHexFromCoordinates(command.params.moveTowardsPoint.x, command.params.moveTowardsPoint.y));
            break;
        default:
            break;
        }
    }
    return path;
}

#ifndef NAVIGATOR_COMMAND_H
#define NAVIGATOR_COMMAND_H

#include <QString>
#include <QVector>

#include <cstring>

#include "RotationSpec.h"
#include "NavigatorCommandType.h"
#include "NavigatorCommandParams.h"
struct NavigatorCommand {
    // CMD Name
    NavigatorCommandType commandType;
    // CMD Values
    union { // we use union because it is easier to understand and modify
        InvalidParams invalid;
        MoveToParams moveTo;
        MoveTowardsPointParams moveTowardsPoint;
        MoveByParams moveBy;
        RotateToParams rotateTo;
        RotateByParams rotateBy;
        KickParams kick;
        KickToParams kickTo;
        KickInTrajectoryParams kickInTrajectory;
        DribbleParams dribble;
        RotateToPointParams rotateToPoint;
        ReceiveFromParams receiveFrom;
        PassToRobotParams passToRobot;
    } params;
    /* The values for the command and its order follow the command's declaration for
    robot control in the AgentControl API */
    // RotationSpec; determines how the robot should rotate to the target point
    RotationSpec rotationSpec;
    // Default constructor
    NavigatorCommand() : commandType(NavigatorCommandType::INVALID) {
        strcpy(params.invalid.reason, "Default construction of InvalidParams");
    }
};

#endif
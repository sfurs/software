#ifndef NAVIGATOR_COMMAND_PARAMS_H
#define NAVIGATOR_COMMAND_PARAMS_H
#include <QDateTime>

struct InvalidParams {
    char reason[128];
};
struct MoveToParams {
    float x;
    float y;
};

struct MoveTowardsPointParams {
    float x;
    float y;
};

struct MoveByParams {
    float deltaX;
    float deltaY;
};

struct RotateToParams {
    float theta;
};

struct RotateByParams {
    float deltaTheta;
};

struct KickParams {
    int power;
    int chipper;
};

struct KickToParams {
    KickParams kick;
    float x;
    float y;
};

struct KickInTrajectoryParams {
    KickParams kick;
    float theta;
};

struct DribbleParams {
    int power;
};

struct RotateToPointParams {
    float x;
    float y;
};

// struct PassToParams {
//     KickParams kick;
//     DribbleParams dribbler;
//     unsigned int receiverId;
//     int timeOut;
//     // QDateTime timestamp;
// };

struct PassToRobotParams {
    KickParams kick;
    unsigned int receiverId;
};

struct ReceiveFromParams {
    DribbleParams dribbler;
    unsigned int kickerId;
};

#endif
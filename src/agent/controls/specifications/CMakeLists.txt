find_package(Qt6 REQUIRED COMPONENTS Core)

# include this folder to the include dirs for all
target_include_directories(skynet PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

target_sources(skynet
    PUBLIC
        NavigatorCommand.h
        RotationSpec.h
        NavigatorCommandType.h
        NavigatorCommandParams.h
)

target_link_libraries(skynet
    PRIVATE
        Qt6::Core
)


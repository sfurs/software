#ifndef ROTATION_SPEC_H
#define ROTATION_SPEC_H

enum RotationSpec {
    IN_CMD = 0, // In command, do not rotate unless specified
    NO_ROTATION = 1, // Move to a point with no rotation
    WRT_ROTATION = 2, // Move to a point with respect to rotation, moving forward
    WITH_ROTATION = 3 // Move to a point with respect to rotation, moving with rotation
};

#endif
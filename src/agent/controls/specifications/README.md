## Overview: Navigator Enums and Structs

This folder contains key enumerations and data structures used by the **Navigator** component. These files define types related to navigation commands, rotation specifications, and command parameters used in robot movement.

## Files

### 1. `RotationSpec.h`

- **RotationSpec Enum**:  
  Defines how a robot should rotate when moving to a target location.

  - **Values**:
    - `IN_CMD = 0`:
      Rotation is determined explicitly within the command.
    - `NO_ROTATION = 1`:
      Move to a point without rotating.
    - `WRT_ROTATION = 2`:
      Move towards a point while maintaining the current rotation direction.
    - `WITH_ROTATION = 3`:
      Move towards a point while rotating to align with movement.

---

### 2. `NavigatorCommandType.h`

- **NavigatorCommandType Enum Class**:  
  Specifies the different types of navigation commands that can be issued to a robot.

  - **Values**:
    - `MOVE_TO`:
      Move directly to a specified point.
    - `MOVE_TOWARDS_POINT`:
      Move towards a given point without necessarily reaching it.
    - `MOVE_BY`:
      Move by a relative distance from the current position.
    - `ROTATE_TO`:
      Rotate to face a specified angle.
    - `ROTATE_BY`:
      Rotate by a specified amount from the current orientation.

---

### 3. `NavigatorCommandParams.h`

- **Command Parameter Structures**:  
  Contains structures defining the parameters used by different navigation commands.

  - **Structures**:
    - `MoveToParams`:
      - `float x`: Target X coordinate.
      - `float y`: Target Y coordinate.
    
    - `MoveTowardsPointParams`:
      - `float x`: Target X coordinate.
      - `float y`: Target Y coordinate.
    
    - `MoveByParams`:
      - `float deltaX`: X-axis displacement.
      - `float deltaY`: Y-axis displacement.
    
    - `RotateToParams`:
      - `float theta`: Target rotation angle in degrees.
    
    - `RotateByParams`:
      - `float deltaTheta`: Amount to rotate from the current angle.

---

### 4. `NavigatorCommand.h`

- **NavigatorCommand Struct**:  
  Represents a single navigation command issued to a robot, including its type, parameters, and rotation specifications.

  - **Fields**:
    - `NavigatorCommandType commandType`:
      The type of navigation command (e.g., `MOVE_TO`, `ROTATE_BY`).
    
    - **Union `params`**:
      Stores command-specific parameters to optimize memory usage.
      - `MoveToParams moveTo`
      - `MoveTowardsPointParams moveTowardsPoint`
      - `MoveByParams moveBy`
      - `RotateToParams rotateTo`
      - `RotateByParams rotateBy`
    
    - `RotationSpec rotationSpec`:
      Defines how the robot should rotate during execution.

---

This documentation provides an overview of the key enumerations and structs used within the **Navigator** module, ensuring clarity and maintainability.


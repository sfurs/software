#ifndef NAVIGATOR_COMMAND_TYPE_H
#define NAVIGATOR_COMMAND_TYPE_H

enum class NavigatorCommandType {
    INVALID, // Default value, used to indicate an invalidly initialized command
    MOVE_TO, // Move to a point
    MOVE_TOWARDS_POINT, // Move towards a point
    MOVE_BY, // Move by a certain distance
    ROTATE_TO, // Rotate to a certain angle
    ROTATE_BY, // Rotate by a certain angle
    KICK, // Kick the ball to the front
    KICK_TO, // Kick the ball to a point
    KICK_IN_TRAJECTORY, // Kick the ball in a certain trajectory
    DRIBBLE, // Dribble the ball
    ROTATE_TO_POINT, // Rotate to a point
    RECEIVE_FROM, //Receive the ball
    PASS_TO_ROBOT //Kick the ball to a robot
};

#endif
#ifndef AGENTCONTROL_H
#define AGENTCONTROL_H

#include <QObject>
#include <QMutex>
#include <QTimer>
#include <QMap>
#include "../../network/netmaster.h"
#include "../../worldstate/worldStateManager.h"
#include "RobotInstruction.h"
#include "ControlPID.h"

class NetMaster;
class WorldStateManager;

class AgentControl : public QObject
{
    Q_OBJECT
    /*
     * Provides Agent control functions and movement correction for agents
     * Control functions are directly called in the Agent Thread
     * The movement correction and communication with the network thread is done in Agent Control thread
     */
public:
    AgentControl(QObject *parent, NetMaster *nm, WorldStateManager *wsm);
    // move robot by a specific amount 
    void moveBy(int id, float x, float y, float maxspeed);
    // move robot towards a point
    void moveTowardsPoint(int id, float x, float y, float maxspeed);
    // rotate by degrees (from -pi to pi)
    void rotateBy(int id, double degrees, float maxspeed);
    // rotate to a specific degree
    void rotateTo(int id, double degrees, float maxspeed);

    // move robot forwards of where it is actively facing (negative maxSpeed is moving backwards)
    void moveForwards(int id, float maxSpeed);
    // move robot leftwards of where it is actively facing (positive is left)
    void moveSideways(int id, float maxSpeed);
    // move at speed
    void move(int id, float xSpeed, float ySpeed);
    // Spin at a given speed (Mainly for testing, i cant think of a situation where just spinning in one spot is a good strategy)
    void spin(int id, double speed);
    // halt the robot
    void halt(int id);
    // stop the robot
    void stop(int id);
    // halt all robots
    void haltAllRobots();
    // stop all robots
    void stopAllRobots();

    // reset AgentControl
    void reset();
    // kick the ball with power in [0, 15] and chipper in [0, 3], values outside this range will be clamped
    void kick(int id, int power, int chipper);
    // dribble the ball with power in [0, 3], values outside this range will be clamped
    void setDribble(int id, int dribble);
    
    QMap<int, RobotData> getRobotData();

    // pass world state to agent
    WorldState passWorldState();

public slots:
    void start();
    void setManualControl(bool manual);
    bool getManualControl();

signals:
    // send when a robot has reached its target position
    void finishedMoving(int id);
    // sent when a robot has reached its target rotation
    void finishedRotating(int id);
    // sends the instruction (internal do not use outside AgentControl)
    void sendInstruction(RobotInstruction instruction);

private:
    struct RobotControl
    {
        // An internal data type storing information on what commands are given to a robot

        // Whether AgentControl should aim to keep the robot with specific coords and angles
        bool fixedGlobalPosition = false;
        bool fixedRotation = false;

        // maximum directional speed
        double xSpeed = qQNaN();
        double ySpeed = qQNaN();
        double rotationSpeed = qQNaN();

        bool shouldSignalMoveFinish = false;
        bool shouldSignalRotateFinish = false;

        int kick = 0;  // Kick power ranges from 0 to 15
        int dribble = 0;  // Dribble power ranges from 0 to 3
        int chipper = 0;  // Chipper power ranges from 0 to 3

        bool isStopped = false;
        ControlPID pid;
    };
    // handles robot movement correction
    void tick();
    // halt a robot (does not lock)
    void haltRobot(int id);

    // placeholder for robot removal function
    void removeRobot(int id);

    NetMaster *network;
    QMutex lock;
    WorldStateManager *worldstate;
    ControlPID pid_config;
    
    QMap<int, RobotControl> robots;

    bool isManualControl = true;

    QMap<int, RobotData> robots_data;
};

#endif

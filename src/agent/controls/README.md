## Overview: Agent Control

This folder includes core components of the Agent Control of the system, such as agent control, navigator, and PID. Also includes the NavigatorCommand class, which handles the commands for the robots.

This folder has a subfolder called `specifications`, which contains the NavigatorCommand class, which handles the commands for the robots.

## Files

### 1. `AgentControl.cpp` & `AgentControl.h`
- **AgentControl Class**: 
  The `AgentControl` class provides functions for controlling robot agents. It handles movement, rotation, and other robot actions like kicking or dribbling. The control functions interact with the network to send instructions to the robots.
  
  - **Functions**:
    - `AgentControl(QObject* parent, NetMaster* nm)`: Constructor that initializes control with network and PID components.
    - `void moveBy(int id, float forwards, float sideways, float maxspeed)`: Moves the robot by specific amounts in the x and y directions.
    - `void moveTowardsPoint(int id, float x, float y, float maxspeed)`: Moves the robot towards a specific point on the field.
    - `void rotateBy(int id, double degrees, float maxSpeed)`: Rotates the robot by a specified number of degrees.
    - `void rotateTo(int id, double degrees, float maxSpeed)`: Rotates the robot to face a specific angle.
    - `void moveForwards(int id, float maxSpeed)`: Moves the robot forwards.
    - `void moveSideways(int id, float maxSpeed)`: Moves the robot sideways.
    - `void halt(int id)`: Stops the movement of a specific robot.
    - `void stopAllRobots()`: Stops all robots currently controlled by the system.
    - `void kick(int id)`: Initiates a kicking action for the robot.
    - `void setDribble(int id, bool dribble)`: Enables or disables the dribbling behavior for a robot.

### 2. `Navigator.cpp` & `Navigator.h`

- **Navigator Class**:  
  The `Navigator` class provides navigation capabilities for robot agents. It manages path planning, command execution, and state tracking for each robot.

  - **Functions**:
    - `Navigator(QObject* parent, WorldStateManager* wsm, AgentControl* agentControl)`:
      Constructor that initializes the navigator with references to the world state manager and agent control.
    
    - `bool isHalted(int id)`:
      Checks if a specific robot is halted.
    
    - `bool shouldRunCommand(int id)`:
      Determines if the next command should be executed for a given robot.
    
    - `NavigatorCommand getNextCommand(int id)`:
      Retrieves the next command in the queue for a specific robot.
    
    - `Grid CreateGrid()`:
      Initializes and returns a grid for path planning.
    
    - `void UpdateGrid()`:
      Updates the grid based on the current world state.
    
    - `void CheckGrid()`:
      Performs a diagnostic check on the grid.

  
  - **Public Slots**:
    - `void start()`:
      Starts the navigation process, running a timer loop for continuous navigation updates.
    
    - `void navigateTo(int id, float x, float y, RotationSpec rotationSpec)`:
      Plans a path and queues movement commands to navigate the robot to a target position using A* pathfinding.
    
    - `void clearAndNavigateTo(int id, float x, float y, RotationSpec rotationSpec)`:
      Clears the existing command queue before navigating to the specified destination.
    
    - `void halt(int id)`:
      Stops a specific robot from executing further commands.
    
    - `void haltAll()`:
      Stops all robots from executing further commands.
    
    - `void unhalt(int id)`:
      Resumes movement for a previously halted robot.
    
    - `void unhaltAll()`:
      Resumes movement for all previously halted robots.
    
    - `void clearQueue(int id)`:
      Clears the command queue for a specific robot.
    
    - `void clearAllQueues()`:
      Clears the command queues for all robots.
    
    - `void allowNextMoveCommand(int id)`:
      Allows a robot to execute the next movement command.
    
    - `void allowNextRotateCommand(int id)`:
      Allows a robot to execute the next rotation command.

    - `int findClosestToBall(int id)`:
      Returns the id of the robot that is closest to the ball.

    - `void moveAwayFromBall(float dist)`:
      Moves all robots a certain distance away from the ball.
    
  - **Signals**:
    - `void plotPoints(int id, QVector<Hexagon*> path)`:
      Emits an event with the planned path for visualization.
    
  - **Private Functions**:
    - `void tick()`:
      Executes periodic updates to process commands and control robot navigation.
    
    - `void addNewRobots()`:
      Ensures that newly detected robots are added to the navigation system.
    
    - `void MOVE_TO(int id, const NavigatorCommand& command, const RobotData& robot)`:
      Processes a **MoveTo** command, moving the robot to a target position while considering rotation specifications.
    
    - `void MOVE_TOWARDS_POINT(int id, const NavigatorCommand& command, const RobotData& robot)`:
      Processes a **MoveTowardsPoint** command, directing the robot to move towards a specified location.
    
    - `void MOVE_BY(int id, const NavigatorCommand& command, const RobotData& robot)`:
      Processes a **MoveBy** command, moving the robot by a relative offset.
    
    - `void ROTATE_TO(int id, const NavigatorCommand& command, const RobotData& robot)`:
      Processes a **RotateTo** command, rotating the robot to face a specific direction.
    
    - `void ROTATE_BY(int id, const NavigatorCommand& command, const RobotData& robot)`:
      Processes a **RotateBy** command, rotating the robot by a specified angle.
    
    - `float calculateAngle(float target_x, float target_y, float robot_x, float robot_y)`:
      Computes the angle between the robot’s current position and the target point.
    
  - **Private Attributes**:
    - `AgentControl* agentControl`:
      Reference to the agent control module.
    
    - `WorldStateManager* worldStateManager`:
      Reference to the world state manager.
    
    - `QMutex lock`:
      Mutex for thread-safe operations.
    
    - `QMap<int, bool> newMoveCommand`:
      Tracks whether each robot can execute a new move command.
    
    - `QMap<int, bool> newRotateCommand`:
      Tracks whether each robot can execute a new rotate command.
    
    - `QMap<int, bool> haltState`:
      Stores the halt state for each robot.
    
    - `QMap<int, std::deque<NavigatorCommand>> path`:
      Stores queued navigation commands for each robot.
    
    - `Grid grid = CreateGrid()`:
      The grid used for pathfinding.
    
    - `PathPlanner pathPlanner`:
      The A* path planning component.
    
    - `int MAX_SPEED = 4.0`:
      Maximum robot speed setting.

---

This documentation provides an overview of the **Navigator** class, detailing its responsibilities, methods, and interaction with other components such as `AgentControl` and `WorldStateManager`.


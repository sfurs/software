#include "Agent.h"
#include "../worldstate/worldState.h"
#include <QThread>
#include <QTimer>
#include <QVector2D>
#include <QtMath>
#include <cmath>
#include "controls/Navigator.h"

QMap<PlaySelection, std::string> playSelectionToString = {
    {NOT_IN_PLAY, "NOT_IN_PLAY"},
    {BUILD_UP, "BUILD_UP"},
    {MIDFIELD_PROGRESSION, "MIDFIELD_PROGRESSION"},
    {ATTACK_DEVELOPMENT, "ATTACK_DEVELOPMENT"},
    {SCORING, "SCORING"},
    {REBOUND_PRESSURE, "REBOUND_PRESSURE"},
    {HIGH_PRESS, "HIGH_PRESS"},
    {ZONAL_MARKING_ORGANIZATION, "ZONAL_MARKING_ORGANIZATION"},
    {TRAPS_PRESSURE_CONTAINMENT, "TRAPS_PRESSURE_CONTAINMENT"},
    {GOAL_DEFENSE_PROTECTION, "GOAL_DEFENSE_PROTECTION"},
    {BALL_RECOVERY, "BALL_RECOVERY"}
};

Agent::Agent(QObject* parent, Navigator* nav, AgentPython* agentPython, WorldStateManager* wsm, AgentControl* control, bool createTest) : QObject(parent), navigator(nav), agentPython(agentPython), worldStateManager(wsm), control(control), isTester(createTest), playSelectionFSM(nav, wsm, this) {
    connect(this, &Agent::navigateTo, navigator, &Navigator::navigateTo);
    connect(this, &Agent::clearAndNavigateTo, navigator, &Navigator::clearAndNavigateTo);
    connect(this, &Agent::halt, navigator, &Navigator::halt);
    connect(this, &Agent::haltAll, navigator, &Navigator::haltAll);
    connect(this, &Agent::unhalt, navigator, &Navigator::unhalt);
    connect(this, &Agent::unhaltAll, navigator, &Navigator::unhaltAll);
    connect(this, &Agent::clearQueue, navigator, &Navigator::clearQueue);
    connect(this, &Agent::clearAllQueues, navigator, &Navigator::clearAllQueues);
    agentWorldState = worldStateManager->getCurrentState();
    agentGameState = worldStateManager->getGameState();
}

Agent::~Agent() {
    // nothing to delete yet
}

void Agent::start() {
    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Agent::run);
    timer->start(1000 / 180); // Run at 180 Hz
    playSelectionFSM.start();
    if (isTester) {
        std::cout << "TESTING MODE\nStarting test in 5 seconds" << std::endl;
        QTimer::singleShot(5000, this, &Agent::runTest);
    }
}

bool Agent::isAgentControlManual() {
    return control->getManualControl();
}

StrategyDecision Agent::calculateStrategyDecision(WorldState worldState) {
    Point ballPosition = {worldState.ballData.x, worldState.ballData.y};

    bool ballInPositiveHome = worldState.fieldDimensions.positiveThird.contains(ballPosition) && (worldState.isPositiveHomeTeam()); // Ball is in our positive third and we are the home team
    bool ballInNegativeHome = worldState.fieldDimensions.negativeThird.contains(ballPosition) && (!worldState.isPositiveHomeTeam()); // Ball is in our negative third and we are the away team
    bool ballInPositiveAway = worldState.fieldDimensions.positiveThird.contains(ballPosition) && (!worldState.isPositiveHomeTeam()); // Ball is in the positive third and we are the away team
    bool ballInNegativeAway = worldState.fieldDimensions.negativeThird.contains(ballPosition) && (worldState.isPositiveHomeTeam());  // Ball is in the negative third and we are the home team

    bool ballInMidfield = worldState.fieldDimensions.midfield.contains(ballPosition); // Ball is in the midfield
    bool ballInHome = ballInPositiveHome || ballInNegativeHome; // Ball is in our third
    bool ballInAway = ballInPositiveAway || ballInNegativeAway; // Ball is in the opponent's third

    // Use the field dimension rectangles to determine ball position
    if (ballInMidfield) {
        //Check if either team has possession of the ball
        if (worldState.friendlyTeam == worldState.teamWithBall) { // If we have possession of the ball, we should attack
            return StrategyDecision::ATK;
        }
        else if (worldState.opponentTeam() == worldState.teamWithBall) { // If the opponent has possession of the ball, we should defend
            return StrategyDecision::DEF;
        }
        else { // If neither team has possession of the ball, we need to check which team is closer to the ball. Assume a 0.6 threshold for the ratio of distances, where we must be 60% closer to the ball than the opponent to switch to attack
            if (worldState.friendlyTeam == Team::INVALID) {
                return StrategyDecision::UNKNOWN;  // If neither team, return UNKNOWN
            }

            double friendly_dist_to_ball = -1;
            double opponent_dist_to_ball = -1;
            for (RobotData const& robot : worldState.friendlyRobots()) { // Get the closest robot to the ball for friendly
                friendly_dist_to_ball = friendly_dist_to_ball == -1 ? worldState.getDistanceToBall(robot) : std::min(friendly_dist_to_ball, worldState.getDistanceToBall(robot));
            }
            for (RobotData const& robot : worldState.opponentRobots()) { // Get the closest robot to the ball for enemy
                opponent_dist_to_ball = opponent_dist_to_ball == -1 ? worldState.getDistanceToBall(robot) : std::min(opponent_dist_to_ball, worldState.getDistanceToBall(robot));
            }

            // If we don't have valid distances, return UNKNOWN since we cannot infer a strategy decision
            if (friendly_dist_to_ball < 0 || opponent_dist_to_ball < 0) {
                return StrategyDecision::UNKNOWN;
            }

            // Otherwise, proceed with the strategy decision calculation
            double threshold = 0.6;
            // We should attack if we are closer to the ball than the opponent
            return friendly_dist_to_ball <= opponent_dist_to_ball * threshold ? StrategyDecision::ATK : StrategyDecision::DEF;
        }
    }
    else if (ballInHome) { // If the ball is in our defending third
        if (worldState.teamWithBall == worldState.friendlyTeam) { // If we have possession of the ball, we should attack (tuned aggression)
            return StrategyDecision::ATK;
        }
        // If we don't have possession of the ball, we should defend
        return StrategyDecision::DEF;
    }
    else if (ballInAway) { // If the ball is in our attacking third
        return StrategyDecision::ATK; // If the ball is in our attacking third, we should attack always
    }
    return StrategyDecision::UNKNOWN; // If the ball is not in any of these regions, we cannot infer a strategy decision and should not transition
}

void Agent::runTest() {
    std::cout << "TESTING MODE\nStarting test" << std::endl;
    // random robot id from worldState
    int size = std::min(agentWorldState.blueRobotData.size(), agentWorldState.yellowRobotData.size());
    if (size == 0) {
        std::cout << "No robots found" << std::endl;
        return;
    }
    // pick random int from size
    int randomIndex = rand() % size;
    int rId = agentWorldState.blueRobotData[randomIndex].id;
    std::cout << "TESTING MODE\nRobot ID: " << rId << std::endl;
    this->navigator->findAndGrabBall(rId);
    this->navigator->chipToGoal(rId, 3);
}

void Agent::run() {
    agentWorldState = worldStateManager->getCurrentState();
    agentGameState = worldStateManager->getGameState();
    StrategyDecision currentStrategyDecision = calculateStrategyDecision(agentWorldState);
    playSelectionFSM.processCurrentState(agentWorldState, agentGameState, currentStrategyDecision);
    playSelectionFSM.tick();
}

void Agent::transitionToPlaySelection(PlaySelection playSelection) {
    if (playSelection != currentPlaySelection) { //transition to new playSelection
        std::cout << "Play selection changed from " << playSelectionToString[currentPlaySelection] << " to " << playSelectionToString[playSelection] << std::endl;
        previousPlaySelection = currentPlaySelection;
        currentPlaySelection = playSelection;
    }
}

void Agent::tick() {
    // TODO: implement tick; propogation works
    return;
}

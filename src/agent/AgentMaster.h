#ifndef AGENT_MASTER_H
#define AGENT_MASTER_H

#include <QObject>
#include <QTimer>
#include <QThread>
#include <iostream>
#include "../worldstate/worldStateManager.h"
#include "Agent.h"
#include "controls/AgentControl.h"
#include "python/AgentPython.h"
#include "controls/Navigator.h"
class AgentMaster : public QObject
{
    Q_OBJECT
public:
    AgentMaster(Navigator *navigator, AgentPython *agentPython, WorldStateManager *wsm, AgentControl *control, bool createTest = false);
    ~AgentMaster();

    Q_INVOKABLE void start();
    // public slots:
    //     void setManualControl(bool manual);
    //     bool getManualControl();
    
    private:
        WorldStateManager* worldStateManager;
        QTimer* timer;
        Agent* agent;
        AgentControl* control;
};

#endif

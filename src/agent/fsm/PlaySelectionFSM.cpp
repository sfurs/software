#include "PlaySelectionFSM.h"
#include "AttackFSM.h"
#include "DefenseFSM.h"
#include "../controls/Navigator.h"
#include "../worldstate/worldStateManager.h"
#include "../worldstate/worldState.h"
#include <iostream>
#include <QMetaObject>
// PlaySelectionFSM implementation
PlaySelectionFSM::PlaySelectionFSM(Navigator* nav, WorldStateManager* wsm, Agent* agent) :
    navigator(nav), worldStateManager(wsm), agent(agent) {
    fsm = new PlayStateMachine(nav, wsm, agent);
}

PlaySelectionFSM::~PlaySelectionFSM() {
    delete fsm;
}

void PlaySelectionFSM::processCurrentState(const WorldState& ws, const GameState& gs, const StrategyDecision& sd) {
    worldState = ws;
    fsm->setWorldState(&worldState);  // Update the state machine's world state

    try {
        // First process state transitions
        switch(sd) {
            case StrategyDecision::ATK:
                fsm->process_event(AttackEvent());
                break;
            case StrategyDecision::DEF:
                fsm->process_event(DefendEvent());
                break;
            case StrategyDecision::KICKOFF:
                fsm->process_event(KickoffEvent());
                break;
            case StrategyDecision::LOOSE:
                fsm->process_event(LooseEvent());
                break;
            case StrategyDecision::PENALTY:
                fsm->process_event(PenaltyEvent());
                break;
            case StrategyDecision::UNKNOWN:
                // Don't transition on UNKNOWN state;
                break;
            default: // This should never happen, throw domain error
                throw std::domain_error("StrategyDecision is not valid");
                break;
        }
    } catch (const std::exception& e) {
        std::cerr << "Error processing state transition: " << e.what() << std::endl;
    }
}

const WorldState& PlaySelectionFSM::getWorldState() const {
    return worldState;
}

void PlaySelectionFSM::start() {
    fsm->initiate();
}

void PlaySelectionFSM::tick() {
    try {
        fsm->process_event(Tick());
    } catch (const std::exception& e) {
        std::cerr << "Error processing tick: " << e.what() << std::endl;
    }
}

// State implementations
Idle::Idle() {
    // std::cout << "Entering IDLE state\n";
}

sc::result Idle::react(const AttackEvent&) { return transit<Attack>(); }
sc::result Idle::react(const DefendEvent&) { return transit<Defend>(); }
sc::result Idle::react(const KickoffEvent&) { return transit<Kickoff>(); }
sc::result Idle::react(const LooseEvent&) { return transit<Loose>(); }
sc::result Idle::react(const PenaltyEvent&) { return transit<Penalty>(); }

Attack::Attack() : attackFSM(nullptr) {
    // std::cout << "Entering ATTACK state\n";
}

// Attack::~Attack() {
//     delete attackFSM;
// } now handled by std::unique_ptr

sc::result Attack::react(const Tick&) {
    if (!attackFSM) {
        attackFSM = std::make_unique<AttackStateMachine>(
            context<PlayStateMachine>().navigator,
            context<PlayStateMachine>().worldStateManager,
            context<PlayStateMachine>().agent
        );
    }

    if (attackFSM && !attackFSM->isInitialized) {
        QMetaObject::invokeMethod(context<PlayStateMachine>().agent, "transitionToPlaySelection", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::BUILD_UP));
        attackFSM->initiate();
        attackFSM->isInitialized = true;
    }
    if (attackFSM) {
        const WorldState* ws = context<PlayStateMachine>().getWorldState();
        attackFSM->process(ws);
    }
    return discard_event();
}

sc::result Attack::react(const DefendEvent&) { return transit<Defend>(); }
sc::result Attack::react(const KickoffEvent&) { return transit<Kickoff>(); }
sc::result Attack::react(const LooseEvent&) { return transit<Loose>(); }
sc::result Attack::react(const PenaltyEvent&) { return transit<Penalty>(); }

Defend::Defend() : defenseFSM(nullptr) {
    // std::cout << "Entering DEFEND state\n";
}

// Defend::~Defend() {
//     delete defenseFSM;
// } now handled by std::unique_ptr

sc::result Defend::react(const Tick&) {
    if (!defenseFSM) {
        defenseFSM = std::make_unique<DefenseStateMachine>(
            context<PlayStateMachine>().navigator,
            context<PlayStateMachine>().worldStateManager,
            context<PlayStateMachine>().agent
        );
    }
    if (defenseFSM && !defenseFSM->isInitialized) {
        QMetaObject::invokeMethod(context<PlayStateMachine>().agent, "transitionToPlaySelection", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::HIGH_PRESS));
        defenseFSM->initiate();
        defenseFSM->isInitialized = true;
    }
    if (defenseFSM) {
        const WorldState* ws = context<PlayStateMachine>().getWorldState();
        defenseFSM->process(ws);
    }
    return discard_event();
}

sc::result Defend::react(const AttackEvent&) { return transit<Attack>(); }
sc::result Defend::react(const KickoffEvent&) { return transit<Kickoff>(); }
sc::result Defend::react(const LooseEvent&) { return transit<Loose>(); }
sc::result Defend::react(const PenaltyEvent&) { return transit<Penalty>(); }

Kickoff::Kickoff() {
    std::cout << "Entering KICKOFF state\n";
}

sc::result Kickoff::react(const AttackEvent&) { return transit<Attack>(); }
sc::result Kickoff::react(const DefendEvent&) { return transit<Defend>(); }
sc::result Kickoff::react(const LooseEvent&) { return transit<Loose>(); }
sc::result Kickoff::react(const PenaltyEvent&) { return transit<Penalty>(); }

Loose::Loose() {
    std::cout << "Entering LOOSE state\n";
}

sc::result Loose::react(const AttackEvent&) { return transit<Attack>(); }
sc::result Loose::react(const DefendEvent&) { return transit<Defend>(); }
sc::result Loose::react(const KickoffEvent&) { return transit<Kickoff>(); }
sc::result Loose::react(const PenaltyEvent&) { return transit<Penalty>(); }

Penalty::Penalty() {
    std::cout << "Entering PENALTY state\n";
}

sc::result Penalty::react(const AttackEvent&) { return transit<Attack>(); }
sc::result Penalty::react(const DefendEvent&) { return transit<Defend>(); }
sc::result Penalty::react(const KickoffEvent&) { return transit<Kickoff>(); }
sc::result Penalty::react(const LooseEvent&) { return transit<Loose>(); }

// PlayStateMachine implementation
const WorldState* PlayStateMachine::getWorldState() const {
    return worldState;
}

void PlayStateMachine::setWorldState(const WorldState* ws) {
    worldState = ws;
}

PlayStateMachine::PlayStateMachine(Navigator* nav, WorldStateManager* wsm, Agent* agent) :
    navigator(nav), worldStateManager(wsm), agent(agent) {
    worldState = nullptr;
}

#ifndef DEFENSE_FSM_H
#define DEFENSE_FSM_H

#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/transition.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/mpl/list.hpp>
#include "../controls/Navigator.h"
#include "../worldstate/worldStateManager.h"
#include "../worldstate/worldState.h"
#include "FSMEvents.h"
#include "../Agent.h"

namespace sc = boost::statechart;
namespace mpl = boost::mpl;

// Forward declarations
struct DefenseStateMachine;
struct HighPress;
struct ZonalMarking;
struct TrapsPressureContainment;
struct GoalDefense;
struct BallRecovery;

struct DefenseStateMachine : sc::state_machine<DefenseStateMachine, HighPress> {
    DefenseStateMachine(Navigator* nav, WorldStateManager* wsm, Agent* agent);
    
    void process(const WorldState* ws);
    const WorldState* getWorldState() const { return currentWorldState; }
    void setWorldState(const WorldState* ws) { currentWorldState = ws; }
    
    Navigator* navigator;
    WorldStateManager* worldStateManager;
    const WorldState* currentWorldState;
    Agent* agent;
    
    bool isInitialized = false;
    
    // Transition conditions
    static bool isInDefensiveThird(const DefenseStateMachine&);
    static bool isOpponentInDefensiveHalf(const DefenseStateMachine&);
    static bool hasLostPossession(const DefenseStateMachine&);
    static bool isClearDanger(const DefenseStateMachine&);
    static bool regainedPossession(const DefenseStateMachine&);
};

// High Press State
struct HighPress : sc::simple_state<HighPress, DefenseStateMachine> {
    typedef sc::custom_reaction<DefenseTick> reactions;
    
    HighPress();
    sc::result react(const DefenseTick&);
};

// Zonal Marking State
struct ZonalMarking : sc::simple_state<ZonalMarking, DefenseStateMachine> {
    typedef sc::custom_reaction<DefenseTick> reactions;
    
    ZonalMarking();
    sc::result react(const DefenseTick&);
};

// Traps and Pressure State
struct TrapsPressureContainment : sc::simple_state<TrapsPressureContainment, DefenseStateMachine> {
    typedef sc::custom_reaction<DefenseTick> reactions;
    
    TrapsPressureContainment();
    sc::result react(const DefenseTick&);
};

// Goal Defense State
struct GoalDefense : sc::simple_state<GoalDefense, DefenseStateMachine> {
    typedef sc::custom_reaction<DefenseTick> reactions;
    
    GoalDefense();
    sc::result react(const DefenseTick&);
};

// Ball Recovery State
struct BallRecovery : sc::simple_state<BallRecovery, DefenseStateMachine> {
    typedef sc::custom_reaction<DefenseTick> reactions;
    
    BallRecovery();
    sc::result react(const DefenseTick&);
};

#endif // DEFENSE_FSM_H

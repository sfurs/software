# Finite State Machines (FSMs)

This folder contains hierarchical state machines for our game strategy, implemented using Boost.Statechart. The FSMs are organized into high-level and nested state machines to handle different aspects of gameplay (attack, defense, etc.). In addition, an event system is used to trigger state transitions.

## Key Components

### 1. PlaySelectionFSM
- **Files**: `PlaySelectionFSM.h`, `PlaySelectionFSM.cpp`
- **Description**: This is the root FSM that manages high-level game states. It acts as a controller for switching between overall strategies.
- **States**:
  - **Idle**: Default inactive state.
  - **Attack**: Activates offensive play mode and the nested AttackFSM.
  - **Defend**: Activates defensive play mode and the nested DefenseFSM.
  - **Kickoff**: Handles kickoff-specific behavior.
  - **Loose**: Activated when the ball is in contested situations.
  - **Penalty**: Manages penalty kick scenarios.

### 2. AttackFSM
- **Files**: `AttackFSM.h`, `AttackFSM.cpp`
- **Description**: A nested FSM that provides detailed control over offensive strategies when the team is in an attack phase.
- **States**:
  - **BuildUp**: Initial positioning to prepare an attack.
  - **MidfieldProgression**: Advances the ball through midfield.
  - **AttackDevelopment**: Executes final third maneuvers.
  - **Scoring**: (Work in Progress) Implements shot execution strategies.
  - **ReboundPressure**: Handles recovery after a missed shot.

### 3. DefenseFSM
- **Files**: `DefenseFSM.h`, `DefenseFSM.cpp`
- **Description**: A nested FSM that governs defensive strategies when the team is in the Defend state.
- **States**:
  - **HighPress**: Aggressively presses the opposing team.
  - **ZonalMarking**: Uses positional marking to defend.
  - **TrapsPressureContainment**: Implements tactics to channel and contain the attack.
  - **GoalDefense**: Protects the goal area.
  - **BallRecovery**: Activates actions to regain possession of the ball.

### 4. Event Definitions
- **File**: `FSMEvents.h`
- **Description**: This file defines the core events which trigger state transitions in the FSMs.
- **Events**:
  - **ProcessTick**: A base tick event used across FSMs.
  - **AttackTick**: A tick event dedicated to updating the AttackFSM.
  - **DefenseTick**: A tick event dedicated to updating the DefenseFSM.

---

## Hierarchical Architecture & Tick Flow

The overall operational flow is as follows:

```
Main Game Loop → PlaySelectionFSM::tick() →
   ├─ If in Attack state: AttackFSM::process(AttackTick) → Offensive state transitions
   └─ If in Defend state: DefenseFSM::process(DefenseTick) → Defensive state transitions
```

- The **PlaySelectionFSM** triggers state transitions based on a provided strategy (e.g., from world state calculations).
- When the **Attack** state is active, it delegates to **AttackFSM** with its own `AttackTick` updates.
- Similarly, if the **Defend** state is active, it delegates to **DefenseFSM** with `DefenseTick` events.

---

## Transition Logic & Conditions

### In DefenseFSM (see `DefenseFSM.cpp`):

- **isInDefensiveThird**:  
  Determines if the ball is inside the defensive third of the field by checking field boundaries.
  
- **isOpponentInOurHalf**:  
  Checks if the opponent team is present in our half of the field by assessing the ball’s location relative to team positioning.

- **isClearDanger**:  
  Evaluates if the defensive area is safe, typically by confirming the ball is within a secure boundary.

- **regainedPossession**:  
  Checks whether the team has regained control of the ball.

*Similar conditions exist in AttackFSM (`AttackFSM.cpp`) for transitioning between offensive states.*

---

## Event Separation and Design Principles

- **Event Separation**:  
  - **PlaySelectionFSM** uses a general `Tick` event.
  - **AttackFSM** updates are driven by `AttackTick` events.
  - **DefenseFSM** updates are driven by `DefenseTick` events.  
  This separation prevents event collisions between different FSM levels.

- **Hierarchical Activation**:  
  - Sub-FSMs (AttackFSM and DefenseFSM) are only activated when their parent state (Attack or Defend) is active.
  - For example, when the PlaySelectionFSM is in the Attack state:
    ```cpp
    sc::result Attack::react(const Tick&) {
      if (!attackFSM) {
        attackFSM = new AttackStateMachine(/* initialization */);
      }
      attackFSM->process(currentWorldState);
      return discard_event();
    }
    ```
- **State Isolation**:  
  Each state is responsible for its own logic and maintains its own transition conditions and context (accessible via the parent FSM).

- **Error Handling & Safety**:  
  All state transitions are wrapped in try-catch blocks to ensure that any errors in processing events do not crash the system.

---

## Dependencies

- **Boost.Statechart**:  
  Provides the core framework for building and managing the hierarchical state machines.
  
- **WorldStateManager & WorldState**:  
  Supply the current world and game state information to the FSMs.
  
- **Navigator**:  
  Interfaces with the robot control system for executing navigation commands.

#include "DefenseFSM.h"
#include "../worldstate/worldState.h"
#include <iostream>
// for propogating play state changes to agent
#include "PlaySelection.h"
#include <QMetaObject>

DefenseStateMachine::DefenseStateMachine(Navigator* nav, WorldStateManager* wsm, Agent* agent) 
    : navigator(nav), worldStateManager(wsm), currentWorldState(nullptr), agent(agent) {
    // Boost.Statechart automatically initializes the initial state as HighPress
}

void DefenseStateMachine::process(const WorldState* ws) {
    setWorldState(ws);
    process_event(DefenseTick());
}

// Transition condition implementations
bool DefenseStateMachine::isInDefensiveThird(const DefenseStateMachine& fsm) {
    const WorldState* ws = fsm.currentWorldState;
    Point ballPosition = {ws->ballData.x, ws->ballData.y};
    // If we are the home team, the defensive third is the positive third, otherwise it is the negative third
    const Rectangle &defensiveThird = ws->isPositiveHomeTeam() ? ws->fieldDimensions.positiveThird : ws->fieldDimensions.negativeThird;
    return defensiveThird.contains(ballPosition);
}

bool DefenseStateMachine::isOpponentInDefensiveHalf(const DefenseStateMachine& fsm) {
    const WorldState* ws = fsm.currentWorldState;
    Point ballPosition = {ws->ballData.x, ws->ballData.y};
    // If we are the home team, the defensive half is the positive half, otherwise it is the negative half
    const Rectangle &defensiveHalf = ws->isPositiveHomeTeam() ? ws->fieldDimensions.positiveHalf : ws->fieldDimensions.negativeHalf;
    return defensiveHalf.contains(ballPosition);
}

bool DefenseStateMachine::hasLostPossession(const DefenseStateMachine& fsm) { // TODO: check if this is correct
    const WorldState* ws = fsm.currentWorldState;
    return ws->teamWithBall != ws->friendlyTeam;
}

bool DefenseStateMachine::isClearDanger(const DefenseStateMachine& fsm) {
    const WorldState* ws = fsm.currentWorldState;
    int defenseAreaIndex = ws->isPositiveHomeTeam() ? 1 : 0; // 1 for blue, 0 for yellow. TODO: check if this is correct
    const auto &defenseArea = ws->fieldDimensions.defenseAreas[defenseAreaIndex];
    Point ballPosition = {ws->ballData.x, ws->ballData.y};
    return defenseArea.contains(ballPosition);
}

bool DefenseStateMachine::regainedPossession(const DefenseStateMachine& fsm) { // TODO: check if this is correct
    const WorldState* ws = fsm.currentWorldState;
    return ws->teamWithBall == ws->friendlyTeam;
}

// State implementations
HighPress::HighPress() {
    // std::cout << "Entering High Press state\n";
}

sc::result HighPress::react(const DefenseTick&) {
    const auto& fsm = context<DefenseStateMachine>();
    const WorldState* ws = fsm.getWorldState();
    if (!ws) return discard_event();
    
    if(DefenseStateMachine::isClearDanger(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::GOAL_DEFENSE_PROTECTION));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<GoalDefense>();
    }
    if(DefenseStateMachine::isOpponentInDefensiveHalf(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::ZONAL_MARKING_ORGANIZATION));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<ZonalMarking>();
    }
    if(DefenseStateMachine::isInDefensiveThird(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::TRAPS_PRESSURE_CONTAINMENT));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<TrapsPressureContainment>();
    }
    QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
    return discard_event();
}

ZonalMarking::ZonalMarking() {
    // std::cout << "Entering Zonal Marking state\n";
}

sc::result ZonalMarking::react(const DefenseTick&) {
    const auto& fsm = context<DefenseStateMachine>();
    const WorldState* ws = fsm.getWorldState();
    if (!ws) return discard_event();
    
    if(DefenseStateMachine::isClearDanger(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::GOAL_DEFENSE_PROTECTION));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<GoalDefense>();
    }
    if(DefenseStateMachine::isInDefensiveThird(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::TRAPS_PRESSURE_CONTAINMENT));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<TrapsPressureContainment>();
    }
    QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
    return discard_event();
}

TrapsPressureContainment::TrapsPressureContainment() {
    // std::cout << "Entering Traps/Pressure state\n";
}

sc::result TrapsPressureContainment::react(const DefenseTick&) {
    const auto& fsm = context<DefenseStateMachine>();
    const WorldState* ws = fsm.getWorldState();
    if (!ws) return discard_event();
    
    if(DefenseStateMachine::isClearDanger(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::GOAL_DEFENSE_PROTECTION));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<GoalDefense>();
    }
    if(DefenseStateMachine::regainedPossession(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::BALL_RECOVERY));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<BallRecovery>();
    }
    QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
    return discard_event();
}

GoalDefense::GoalDefense() {
    // std::cout << "Entering Goal Defense state\n";
}

sc::result GoalDefense::react(const DefenseTick&) {
    const auto& fsm = context<DefenseStateMachine>();
    const WorldState* ws = fsm.getWorldState();
    if (!ws) return discard_event();

    if(!DefenseStateMachine::isClearDanger(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::BALL_RECOVERY));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<BallRecovery>();
    }
    QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
    return discard_event();
}

BallRecovery::BallRecovery() {
    // std::cout << "Entering Ball Recovery state\n";
}

sc::result BallRecovery::react(const DefenseTick&) {
    const auto& fsm = context<DefenseStateMachine>();
    const WorldState* ws = fsm.getWorldState();
    if (!ws) return discard_event();
    
    if(DefenseStateMachine::regainedPossession(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::HIGH_PRESS));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<HighPress>();
    }
    if(DefenseStateMachine::isClearDanger(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::GOAL_DEFENSE_PROTECTION));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<GoalDefense>();
    }
    QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
    return discard_event();
}

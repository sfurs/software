#ifndef PLAY_SELECTION_FSM_H
#define PLAY_SELECTION_FSM_H

#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/transition.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include "shared/StrategyDecision.h"
#include "worldstate/worldState.h"
#include "../controls/Navigator.h"
#include "../worldstate/worldStateManager.h"
// #include "agent/Agent.h"

// Namespace alias for boost::statechart
namespace sc = boost::statechart;

// Forward declarations of all state structs
struct Idle;
struct Attack;
struct Defend;
struct Kickoff;
struct Loose;
struct Penalty;
// Forward declarations of all state machines
struct PlayStateMachine;
struct AttackStateMachine;
struct DefenseStateMachine;
// agent Forward declarations
class Agent;

/**
 * Event classes that trigger state transitions
 * Each inherits from sc::event to enable state machine functionality
 */
struct AttackEvent : sc::event<AttackEvent> {};
struct DefendEvent : sc::event<DefendEvent> {};
struct KickoffEvent : sc::event<KickoffEvent> {};
struct LooseEvent : sc::event<LooseEvent> {};
struct PenaltyEvent : sc::event<PenaltyEvent> {};
struct Tick : sc::event<Tick> {};

/**
 * Main Finite State Machine class that manages game state transitions
 * Heavily based on UBC Thunderbots FSM
 */
class PlaySelectionFSM {
public:
    PlaySelectionFSM(Navigator* nav, WorldStateManager* wsm, Agent* agent);
    ~PlaySelectionFSM();

    /**
     * Updates the FSM based on new world state data
     * @param ws Current world state
     */
    void processCurrentState(const WorldState& ws, const GameState& gs, const StrategyDecision& sd);

    /**
     * Gets the current world state
     * @return Current world state
     */
    const WorldState& getWorldState() const;

    /**
     * Starts the state machine
     */
    void start();

    void tick();

private:
    Agent* agent;
    PlayStateMachine* fsm;
    Navigator* navigator;
    WorldStateManager* worldStateManager;
    WorldState worldState;
};

/**
 * Core state machine definition that uses Idle as initial state
 */
struct PlayStateMachine : sc::state_machine<PlayStateMachine, Idle> {
    PlayStateMachine(Navigator* nav, WorldStateManager* wsm, Agent* agent);

    Navigator* navigator;
    WorldStateManager* worldStateManager;
    Agent* agent;

    /**
     * Gets the current world state
     * @return Pointer to current world state
     */
    const WorldState* getWorldState() const;

    /**
     * Updates the stored world state
     * @param ws Pointer to new world state
     */
    void setWorldState(const WorldState* ws);
private:
    const WorldState* worldState;
};

/**
 * Idle state definition
 * Default state when no specific game state is active
 */
struct Idle : sc::simple_state<Idle, PlayStateMachine> {
    Idle();
    typedef boost::mpl::list<
        sc::custom_reaction<AttackEvent>,
        sc::custom_reaction<DefendEvent>,
        sc::custom_reaction<KickoffEvent>,
        sc::custom_reaction<LooseEvent>,
        sc::custom_reaction<PenaltyEvent>
    > reactions;

    sc::result react(const AttackEvent&);
    sc::result react(const DefendEvent&);
    sc::result react(const KickoffEvent&);
    sc::result react(const LooseEvent&);
    sc::result react(const PenaltyEvent&);
};

/**
 * Attack state definition
 * Active when team is in offensive play
 */
struct Attack : sc::simple_state<Attack, PlayStateMachine> {
    Attack();
    // ~Attack();

    typedef boost::mpl::list<
        sc::custom_reaction<DefendEvent>,
        sc::custom_reaction<KickoffEvent>,
        sc::custom_reaction<LooseEvent>,
        sc::custom_reaction<PenaltyEvent>,
        sc::custom_reaction<Tick>
    > reactions;

    sc::result react(const DefendEvent&);
    sc::result react(const KickoffEvent&);
    sc::result react(const LooseEvent&);
    sc::result react(const PenaltyEvent&);
    sc::result react(const Tick&);

private:
   std::unique_ptr<AttackStateMachine> attackFSM;
};

/**
 * Defend state definition
 * Active when team is in defensive play
 */
struct Defend : sc::simple_state<Defend, PlayStateMachine> {
    Defend();
    // ~Defend();
    typedef boost::mpl::list<
        sc::custom_reaction<AttackEvent>,
        sc::custom_reaction<KickoffEvent>,
        sc::custom_reaction<LooseEvent>,
        sc::custom_reaction<PenaltyEvent>,
        sc::custom_reaction<Tick>
    > reactions;

    sc::result react(const AttackEvent&);
    sc::result react(const KickoffEvent&);
    sc::result react(const LooseEvent&);
    sc::result react(const PenaltyEvent&);
    sc::result react(const Tick&);

private:
    std::unique_ptr<DefenseStateMachine> defenseFSM;
};

/**
 * Kickoff state definition
 * Active during kickoff situations
 */
struct Kickoff : sc::simple_state<Kickoff, PlayStateMachine> {
    Kickoff();
    typedef boost::mpl::list<
        sc::custom_reaction<AttackEvent>,
        sc::custom_reaction<DefendEvent>,
        sc::custom_reaction<LooseEvent>,
        sc::custom_reaction<PenaltyEvent>
    > reactions;

    sc::result react(const AttackEvent&);
    sc::result react(const DefendEvent&);
    sc::result react(const LooseEvent&);
    sc::result react(const PenaltyEvent&);
};

/**
 * Loose state definition
 * Active when ball is in neutral/contested play
 */
struct Loose : sc::simple_state<Loose, PlayStateMachine> {
    Loose();
    typedef boost::mpl::list<
        sc::custom_reaction<AttackEvent>,
        sc::custom_reaction<DefendEvent>,
        sc::custom_reaction<KickoffEvent>,
        sc::custom_reaction<PenaltyEvent>
    > reactions;

    sc::result react(const AttackEvent&);
    sc::result react(const DefendEvent&);
    sc::result react(const KickoffEvent&);
    sc::result react(const PenaltyEvent&);
};

/**
 * Penalty state definition
 * Active during penalty situations
 */
struct Penalty : sc::simple_state<Penalty, PlayStateMachine> {
    Penalty();
    typedef boost::mpl::list<
        sc::custom_reaction<AttackEvent>,
        sc::custom_reaction<DefendEvent>,
        sc::custom_reaction<KickoffEvent>,
        sc::custom_reaction<LooseEvent>
    > reactions;

    sc::result react(const AttackEvent&);
    sc::result react(const DefendEvent&);
    sc::result react(const KickoffEvent&);
    sc::result react(const LooseEvent&);
};

#endif // PLAY_SELECTION_FSM_H

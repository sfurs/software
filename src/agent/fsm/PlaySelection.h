#ifndef PLAY_SELECTION_H
#define PLAY_SELECTION_H

enum PlaySelection
{
    NOT_IN_PLAY = 0, // used for when the ball is not in play (ie. setup)
    BUILD_UP = 1,
    MIDFIELD_PROGRESSION = 2,
    ATTACK_DEVELOPMENT = 3,
    SCORING = 4,
    REBOUND_PRESSURE = 5,
    HIGH_PRESS = 6,
    ZONAL_MARKING_ORGANIZATION = 7,
    TRAPS_PRESSURE_CONTAINMENT = 8,
    GOAL_DEFENSE_PROTECTION = 9,
    BALL_RECOVERY = 10
};

#endif // PLAY_SELECTION_H
#ifndef ATTACK_FSM_H
#define ATTACK_FSM_H

#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/transition.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/mpl/list.hpp>
#include "../controls/Navigator.h"
#include "../worldstate/worldStateManager.h"
#include "../worldstate/worldState.h"
#include "FSMEvents.h"
#include "../Agent.h"

namespace sc = boost::statechart;
namespace mpl = boost::mpl;

// Forward declarations
struct AttackStateMachine;
struct BuildUp;
struct MidfieldProgression;
struct AttackDevelopment;
struct Scoring;
struct ReboundPressure;

struct AttackStateMachine : sc::state_machine<AttackStateMachine, BuildUp> {
    AttackStateMachine(Navigator* nav, WorldStateManager* wsm, Agent* agent);
    
    void process(const WorldState* ws);
    const WorldState* getWorldState() const { return currentWorldState; }
    void setWorldState(const WorldState* ws) { currentWorldState = ws; }
    
    Navigator* navigator;
    WorldStateManager* worldStateManager;
    const WorldState* currentWorldState;
    Agent* agent;
    bool isInitialized = false;  // Replace the initialized() method
    
    // Transition conditions - need to be static for boost::statechart
    static bool isInMidfield(const AttackStateMachine&);
    static bool isInAttackingThird(const AttackStateMachine&);
    static bool isInDefendingThird(const AttackStateMachine&);
    static bool hasClearShot(const AttackStateMachine&);
    static bool isShotMissed(const AttackStateMachine&);
    static bool regainedPossession(const AttackStateMachine&);
};

struct BuildUp : sc::simple_state<BuildUp, AttackStateMachine> {
    typedef sc::custom_reaction<AttackTick> reactions;
    
    BuildUp();
    sc::result react(const AttackTick&);
};

struct MidfieldProgression : sc::simple_state<MidfieldProgression, AttackStateMachine> {
    typedef sc::custom_reaction<AttackTick> reactions;
    
    MidfieldProgression();
    sc::result react(const AttackTick&);
};

struct AttackDevelopment : sc::simple_state<AttackDevelopment, AttackStateMachine> {
    typedef sc::custom_reaction<AttackTick> reactions;
    
    AttackDevelopment();
    sc::result react(const AttackTick&);
};

struct Scoring : sc::simple_state<Scoring, AttackStateMachine> {
    typedef sc::custom_reaction<AttackTick> reactions;
    
    Scoring();
    sc::result react(const AttackTick&);
};

struct ReboundPressure : sc::simple_state<ReboundPressure, AttackStateMachine> {
    typedef sc::custom_reaction<AttackTick> reactions;
    
    ReboundPressure();
    sc::result react(const AttackTick&);
};

#endif // ATTACK_FSM_H

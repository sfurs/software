find_package(Boost REQUIRED)

# include this folder to the include dirs for all
target_include_directories(skynet PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

target_sources(skynet
    PUBLIC
        PlaySelectionFSM.h
        AttackFSM.h
        DefenseFSM.h
        FSMEvents.h
        PlaySelection.h
    PRIVATE
        PlaySelectionFSM.cpp
        AttackFSM.cpp
        DefenseFSM.cpp
)


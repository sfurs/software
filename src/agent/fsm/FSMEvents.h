#ifndef FSM_EVENTS_H
#define FSM_EVENTS_H

#include <boost/statechart/event.hpp>

namespace sc = boost::statechart;

// Tick events for the FSMs: these are used to trigger state transitions and run the FSMs during the agent's main loop
struct ProcessTick : sc::event<ProcessTick> {};
struct AttackTick : sc::event<AttackTick> {}; 
struct DefenseTick : sc::event<DefenseTick> {};

#endif // FSM_EVENTS_H

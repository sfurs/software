#include "AttackFSM.h"
#include "../worldstate/worldState.h"
#include <iostream>
// for propogating play state changes to agent
#include "PlaySelection.h"
#include <QMetaObject>

AttackStateMachine::AttackStateMachine(Navigator* nav, WorldStateManager* wsm, Agent* agent) 
    : navigator(nav), worldStateManager(wsm), currentWorldState(nullptr), agent(agent) {
    // Boost.Statechart automatically initializes the initial state as BuildUp
}

void AttackStateMachine::process(const WorldState* ws) {
    setWorldState(ws);
    process_event(AttackTick());
}

// Static transition condition implementations
bool AttackStateMachine::isInMidfield(const AttackStateMachine& fsm) {
    const WorldState* ws = fsm.currentWorldState;
    const Rectangle &midfield = ws->fieldDimensions.midfield;
    Point ballPosition = {ws->ballData.x, ws->ballData.y};
    return midfield.contains(ballPosition);
}

bool AttackStateMachine::isInAttackingThird(const AttackStateMachine& fsm) {
    const WorldState* ws = fsm.currentWorldState;
    // If we are the home team, the attacking third is the negative third, otherwise it is the positive third
    const Rectangle &attackingThird = ws->isPositiveHomeTeam() ? ws->fieldDimensions.negativeThird : ws->fieldDimensions.positiveThird;
    Point ballPosition = {ws->ballData.x, ws->ballData.y};
    return attackingThird.contains(ballPosition);
}

bool AttackStateMachine::isInDefendingThird(const AttackStateMachine& fsm) {
    return !isInMidfield(fsm) && !isInAttackingThird(fsm);
}

bool AttackStateMachine::hasClearShot(const AttackStateMachine& fsm) { // TODO: implement if we can transition to Scoring in method
    const WorldState* ws = fsm.currentWorldState;
    return ws->teamWithBall == ws->friendlyTeam;
}

bool AttackStateMachine::isShotMissed(const AttackStateMachine& fsm) { // TODO: implement if we can transition to ReboundPressure in method by checking if we missed the goal
    return false;
}

bool AttackStateMachine::regainedPossession(const AttackStateMachine& fsm) { // TODO: implement if we can transition to BuildUp in method by checking if we regained possession
    const WorldState* ws = fsm.currentWorldState;
    return ws->teamWithBall == ws->friendlyTeam;
}

BuildUp::BuildUp() {
    //  std::cout << "Entering BuildUp state\n";
}

sc::result BuildUp::react(const AttackTick&) {
    const auto& fsm = context<AttackStateMachine>();
    const WorldState* ws = fsm.getWorldState();
    if (!ws) return discard_event();

    if (AttackStateMachine::isInMidfield(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::MIDFIELD_PROGRESSION));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<MidfieldProgression>();
    }
    else if (AttackStateMachine::isInAttackingThird(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::ATTACK_DEVELOPMENT));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<AttackDevelopment>();
    }
    QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
    return discard_event();
}

MidfieldProgression::MidfieldProgression() {
    // std::cout << "Entering MidfieldProgression state\n";
}

sc::result MidfieldProgression::react(const AttackTick&) {
    const auto& fsm = context<AttackStateMachine>();
    const WorldState* ws = fsm.getWorldState();
    if (!ws) return discard_event();

    if (AttackStateMachine::isInAttackingThird(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::ATTACK_DEVELOPMENT));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<AttackDevelopment>();
    }
    else if (AttackStateMachine::isInDefendingThird(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::BUILD_UP));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<BuildUp>();
    }
    QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
    return discard_event();
}

AttackDevelopment::AttackDevelopment() {
    // std::cout << "Entering AttackDevelopment state\n";
}

sc::result AttackDevelopment::react(const AttackTick&) {
    const auto& fsm = context<AttackStateMachine>();
    const WorldState* ws = fsm.getWorldState();
    if (!ws) return discard_event();

    if (AttackStateMachine::hasClearShot(fsm)) { // TODO: implement if we can transition to scoring in method
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::SCORING));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<Scoring>();
    }
    else if (AttackStateMachine::isInMidfield(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::MIDFIELD_PROGRESSION));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<MidfieldProgression>();
    }
    else if (AttackStateMachine::isInDefendingThird(fsm)) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::BUILD_UP));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<BuildUp>();
    }
    QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
    return discard_event();
}

Scoring::Scoring() {
    // std::cout << "Entering Scoring state\n";
}

sc::result Scoring::react(const AttackTick&) {
    // TODO: Implement Scoring State
    const auto& fsm = context<AttackStateMachine>();
    const WorldState* ws = fsm.getWorldState();
    if (!ws) return discard_event();

    if (ws->teamWithBall != ws->friendlyTeam) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::REBOUND_PRESSURE));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<ReboundPressure>();
    }
    QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
    return discard_event();
}

ReboundPressure::ReboundPressure() {
    // std::cout << "Entering ReboundPressure state\n";
}

sc::result ReboundPressure::react(const AttackTick&) {
    const auto& fsm = context<AttackStateMachine>();
    const WorldState* ws = fsm.getWorldState();
    if (!ws) return discard_event();

    if (ws->teamWithBall == ws->friendlyTeam) {
        QMetaObject::invokeMethod(fsm.agent, "transitionToPlayState", Qt::QueuedConnection, Q_ARG(PlaySelection, PlaySelection::BUILD_UP));
        QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
        return transit<BuildUp>();
    }
    QMetaObject::invokeMethod(fsm.agent, "tick", Qt::QueuedConnection);
    return discard_event();
}

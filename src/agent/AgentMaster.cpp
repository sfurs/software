#include "AgentMaster.h"
#include <QMetaObject>

AgentMaster::AgentMaster(Navigator *navigator, AgentPython *agentPython, WorldStateManager *wsm, AgentControl *control, bool createTest) : timer(new QTimer(this))
{
    worldStateManager = wsm;
    // agent = new Agent(this, navigator, agentPython, wsm, control);
    if (!createTest) {
        agent = new Agent(this, navigator, agentPython, wsm, control, false);
    } else {
        agent = new Agent(this, navigator, agentPython, wsm, control, true);
    }
}

AgentMaster::~AgentMaster()
{
    delete timer;
    delete agent;
}

void AgentMaster::start()
{
    agent->start();
}

// void AgentMaster::setManualControl(bool manual) {
//     // std::cout << "Setting manual control to " << manual << std::endl;
//     QMetaObject::invokeMethod(control, "setManualControl", Qt::QueuedConnection, Q_ARG(bool, manual));
// }

// bool AgentMaster::getManualControl() {
//     return control->getManualControl();
// }
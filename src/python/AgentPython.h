#ifndef AGENTPYTHON_H
#define AGENTPYTHON_H

#include "../worldstate/worldStateManager.h"
#include <QObject>

// Handle Qt slots macro
#pragma push_macro("slots")
#undef slots
#include <pybind11/embed.h>
#pragma pop_macro("slots")

/**
 * @class AgentPython
 * @brief Facilitates the integration of Python functionality within the C++ environment,
 * utilizing WorldStateManager and pybind11.
 */
class AgentPython : public QObject
{
    Q_OBJECT

private:
    /**
     * @brief Pointer to the singleton instance of WorldStateManager.
     * Declared as const to ensure it cannot be reassigned.
     */
    WorldStateManager *pWorldStateManager;

    /** @brief Flag to indicate if the Python environment is set up. */
    bool isSetup = false;

    /**
     * @brief Sets up the Python environment, including locating and loading required modules.
     */
    void moduleSetup();

    pybind11::scoped_interpreter *interpreter;

public:
    /**
     * @brief Default constructor for AgentPython.
     */
    AgentPython(pybind11::scoped_interpreter *guard, WorldStateManager *wsm);

public slots:
    /**
     * @brief Invokes Python functions to interact with WorldState.
     */
    void cppToPythonWorldState();

    /**
     * @brief Invokes Python functions to run the classifier.
     */
    void runClassifier();
};

#endif

# README

## Prerequisites
Before building and running the project, ensure the following dependencies are installed:

### macOS
1. **Python**: Install Python 3 using Homebrew:
   ```bash
   brew install python
   ```

2. **pybind11**: Install pybind11 using Homebrew:
   ```bash
   brew install pybind11
   ```

### Ubuntu
1. **Python**: Install Python 3 and its development headers:
   ```bash
   sudo apt update
   sudo apt install python3 python3-pip python3-dev python3-pybind11
   ```

---

## Build Instructions
Once the prerequisites are installed, follow these steps:

1. Navigate to the project directory where the `CMakeLists.txt` file is located.
2. Run `cmake` to configure and build the project.

The required Python packages listed in `requirements.txt` will be installed automatically during the build process.

---

## Notes
- If additional Python packages are needed, add them to the `requirements.txt` file located at:
  ```
  sfurs/software/src/pythonSetup/python_scripts/requirements.txt
  ```
- Ensure that the `cmake` command is executed within the build directory, as per your project's standard workflow.

---

## **Overview: AgentPython**

The `AgentPython` module facilitates interaction between the C++ world and Python scripts using the `pybind11` library. It embeds Python functionalities, enabling the agent to leverage Python-based logic for operations such as decision-making and world state analysis.

---

## **Files**

### 1. `AgentPython.cpp` & `AgentPython.h`
- **AgentPython Class**:  
  The `AgentPython` class enables seamless integration between C++ and Python for executing Python scripts and interacting with Python-defined modules. It uses `pybind11` for creating bindings and embedding Python functionality.  

  - **Functions**:
    - **`AgentPython()`**:  
      Constructor to initialize the `AgentPython` object, setting up required flags and configurations for Python interaction.  
    - **`void cppToPythonWorldState()`**:  
      Passes the `WorldStateManager` instance to a Python script, enabling Python-based world state analysis.  
    - **`void moduleSetup()`**:  
      Configures Python's `sys.path` to include directories for locating Python modules required by the agent. Ensures that modules can be dynamically imported during runtime.

  - **Key Features**:
    - **Python-C++ Interaction**:  
      Uses `pybind11` to expose the `WorldStateManager`, `WorldState`, `BallData`, `RobotData`, and `Team` to Python.  
    - **Dynamic Python Path Setup**:  
      Adjusts `sys.path` at runtime to locate the required Python modules based on relative paths and CMake configuration.  
    - **Error Handling**:  
      Wraps key operations in `try-catch` blocks to handle Python exceptions gracefully and provide clear error messages.

---

### 2. **Embedded Python Bindings**
- The `AgentPython.cpp` file contains a `pybind11` embedded module (`worldStateManager_module`) that binds the following classes and enums for use in Python:
  - **`WorldStateManager`**:  
    Exposes functions like `getCurrentState()` and `printWorldState()` to Python.
  - **`WorldState`**:  
    Includes data such as `capture_time`, `ballData`, and `friendlyTeam`, with methods to access robot data (`blueRobotData`, `yellowRobotData`).
  - **`BallData` & `RobotData`**:  
    Simplified representations of ball and robot state data for Python-based analysis.
  - **`Team` Enum**:  
    Defines the `BLUE` and `YELLOW` team constants.

---

### 3. **CMakeLists.txt**
- This file configures the build process for the `AgentPython` module:
  - Links the `pybind11` library for embedding Python.
  - Ensures that required source files (`AgentPython.cpp` and `AgentPython.h`) are included in the build.

---

## **Usage Workflow**

1. **Initialization**:  
   - Instantiate the `AgentPython` class in your main application.  

2. **Module Setup**:  
   - Call `moduleSetup()` to configure Python paths and load required modules.

3. **Python Interaction**:  
   - Use `cppToPythonWorldState()` to pass the current `WorldStateManager` instance to a Python function. This enables Python to process and analyze the world state dynamically.

4. **Error Handling**:  
   - Any Python-related issues during runtime are captured and logged for debugging.


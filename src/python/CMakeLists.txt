# Pybind11 and Python integration
find_package(Python3 REQUIRED COMPONENTS Interpreter Development)
find_package(pybind11 REQUIRED)

# Include the directory containing this file in the include paths

add_library(skynet_python STATIC)

# Link Python libraries to the target
target_link_libraries(skynet_python
    PUBLIC
        skynet_worldstate
        skynet_shared
        ${Python3_LIBRARIES}
        ${pybind11_LIBRARIES}
        Qt6::Core
)


target_include_directories(skynet_python PUBLIC 
    ${CMAKE_CURRENT_SOURCE_DIR}
    # Add Python include directories to the target
    ${Python3_INCLUDE_DIRS}
    ${pybind11_INCLUDE_DIRS}
)

# Add the Python setup files to the target
target_sources(skynet_python
    PUBLIC
        AgentPython.h
    PRIVATE
        AgentPython.cpp
)

target_compile_options(skynet_python PUBLIC
    -fvisibility=hidden
)

# Set the target directory for Python package installations
set(PYTHON_PACKAGE_INSTALL_DIR ${CMAKE_BINARY_DIR}/python_libs)
set(PYTHON_SCRIPTS_DIR ${CMAKE_BINARY_DIR}/python_scripts)

# Create custom command to copy Python scripts
add_custom_command(
    OUTPUT ${PYTHON_SCRIPTS_DIR}
    COMMAND ${CMAKE_COMMAND} -E copy_directory
        ${CMAKE_CURRENT_SOURCE_DIR}/python_scripts
        ${PYTHON_SCRIPTS_DIR}
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/python_scripts
    COMMENT "Copying Python scripts to build directory"
)

# Create custom target for Python scripts
add_custom_target(copy_python_scripts
    DEPENDS ${PYTHON_SCRIPTS_DIR}
)

# Custom target to install python packages
add_custom_target(install_python_packages
    COMMAND ${Python3_EXECUTABLE} -m pip install --upgrade -r ${CMAKE_CURRENT_SOURCE_DIR}/python_scripts/requirements.txt -t ${PYTHON_PACKAGE_INSTALL_DIR}
    COMMENT "Installing Python packages in ${PYTHON_PACKAGE_INSTALL_DIR} from requirements.txt"
    VERBATIM
)

# Ensure both python packages and scripts are ready before building skynet
add_dependencies(skynet_python install_python_packages copy_python_scripts)

target_link_libraries(skynet
    PUBLIC
        skynet_python
)

#include "AgentPython.h"

#include <exception>
#include <iostream>
#include <vector>
#include <QCoreApplication>
#include <QFileInfo>
#include <QDir>
#include <QDebug>

namespace py = pybind11;

// seperate bindings in different file later
PYBIND11_EMBEDDED_MODULE(worldStateManager_module, m)
{
    // Bind WorldStateManager (already in your module)
    py::class_<WorldStateManager>(m, "WorldStateManager")
        //.def(py::init<>())
        .def("getCurrentState", &WorldStateManager::getCurrentState)
        .def("printWorldState", &WorldStateManager::printWorldState);

    // Bind WorldState
    py::class_<WorldState>(m, "WorldState")
        //.def(py::init<>()) // Default constructor
        //.def(py::init<BallData, QList<RobotData>, QList<RobotData>, double, Team>()) // Parameterized constructor
        .def_readonly("capture_time", &WorldState::capture_time)
        .def_readonly("ballData", &WorldState::ballData)
        .def_readonly("friendlyTeam", &WorldState::friendlyTeam)
        .def_readonly("teamWithBall", &WorldState::teamWithBall)
        .def_readonly("idWithBall", &WorldState::idWithBall)
        .def("blueRobotData", [](const WorldState &data)
             { return std::vector<RobotData>(data.blueRobotData.begin(), data.blueRobotData.end()); })
        .def("yellowRobotData", [](const WorldState &data)
             { return std::vector<RobotData>(data.yellowRobotData.begin(), data.yellowRobotData.end()); });

    // Bind BallData
    py::class_<BallData>(m, "BallData")
        //.def(py::init<>())
        .def_readonly("x", &BallData::x)
        .def_readonly("y", &BallData::y)
        .def_readonly("z", &BallData::z)
        .def_readonly("velocity_x", &BallData::velocity_x)
        .def_readonly("velocity_y", &BallData::velocity_y)
        .def_readonly("velocity_z", &BallData::velocity_z);

    // Bind RobotData
    py::class_<RobotData>(m, "RobotData")
        //.def(py::init<>())
        .def_readonly("id", &RobotData::id)
        .def_readonly("x", &RobotData::x)
        .def_readonly("y", &RobotData::y)
        .def_readonly("angle", &RobotData::angle)
        .def_readonly("velocity_x", &RobotData::velocity_x)
        .def_readonly("velocity_y", &RobotData::velocity_y)
        .def_readonly("velocity_angle", &RobotData::velocity_angle);

    // Bind Team enum
    py::enum_<Team>(m, "Team")
        .value("INVALID", Team::INVALID)
        .value("BLUE", Team::BLUE)
        .value("YELLOW", Team::YELLOW)
        .export_values();
}

// Constructor
AgentPython::AgentPython(pybind11::scoped_interpreter *guard, WorldStateManager *wsm)
    : isSetup(false), interpreter(guard)
{
    pWorldStateManager = wsm;
}

/**
 * @brief Configures Python module search paths.
 *
 * Adds paths dynamically based on the location of the current file and the
 * Python package directory defined in CMake.
 */
void AgentPython::moduleSetup()
{
    try
    {
        QDir path = QDir(QCoreApplication::applicationDirPath());

// if we are using macos, this is needed because linux and macos makes exe differently
#ifdef __APPLE__
        path.cdUp();
        path.cdUp();
        path.cdUp();
#endif

        QString q_module_dir = path.absolutePath() + "/python_scripts";
        QString q_python_package_dir = path.absolutePath() + "/python_libs";

        // std::cout << "Looking for Python files in: " << q_module_dir.toStdString() << std::endl;
        // std::cout << "Looking for Python packages in: " << q_python_package_dir.toStdString() << std::endl;

        // Import sys and os modules
        py::module_ sys = py::module_::import("sys");
        py::module_ os = py::module_::import("os");

        // Use the Python module path defined in CMake
        py::object python_module_path = py::str(q_module_dir.toStdString());

        // Append the Python module path to sys.path
        sys.attr("path").attr("append")(python_module_path);

        // Also append the package install directory
        py::object python_package_dir = py::str(q_python_package_dir.toStdString());
        sys.attr("path").attr("append")(python_package_dir);

        // Optional: Print paths for debugging
        py::print("Python Module Path:", python_module_path);
        py::print("Python Package Directory:", python_package_dir);

        py::object os_path = os.attr("path");

        std::cout << "test1.py exists: " << os_path.attr("exists")(q_module_dir.toStdString() + "/test1.py").cast<bool>() << std::endl;
        std::cout << "classify.py exists: " << os_path.attr("exists")(q_module_dir.toStdString() + "/classify.py").cast<bool>() << std::endl;
    }
    catch (const py::error_already_set &e)
    {
        std::cerr << "Error during module setup: " << e.what() << std::endl;
        throw;
    }
}

/**
 * @brief Calls the Python function to interact with the WorldState.
 */
void AgentPython::cppToPythonWorldState()
{
    try
    {
        // Ensure the module is set up only once
        if (!isSetup)
        {
            moduleSetup();
            isSetup = true;
        }
        // return;

        // Import the Python test module
        py::module_ test_module = py::module_::import("test1");

        // Pass the WorldStateManager instance to the Python function
        if (pWorldStateManager != nullptr)
        {
            test_module.attr("test_module")(py::cast(pWorldStateManager));
        }
    }
    catch (const py::error_already_set &e)
    {
        std::cerr << "Error during Python interaction: " << e.what() << std::endl;
    }
}

void AgentPython::runClassifier()
{
    try
    {
        // Ensure the module is set up only once
        if (!isSetup)
        {
            moduleSetup();
            isSetup = true;
        }

        // Import the Python test module
        py::module_ classifier_module = py::module_::import("classify");

        // Pass the WorldStateManager instance to the Python function
        if (pWorldStateManager != nullptr)
        {
            classifier_module.attr("main")(py::cast(pWorldStateManager));
        }
    }
    catch (const py::error_already_set &e)
    {
        std::cerr << "Error during Python interaction: " << e.what() << std::endl;
    }
}

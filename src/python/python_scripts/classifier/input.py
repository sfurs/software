import numpy as np
from worldStateManager_module import Team
class Input:
    def __init__(self, worldState, gameState = None):
        # Create game state vector
        header_game_vector = np.array([
                worldState.capture_time,
                0 if worldState.friendlyTeam == Team.BLUE else 1 if worldState.friendlyTeam == Team.YELLOW else 2,
                0 if worldState.teamWithBall == Team.BLUE else 1 if worldState.teamWithBall == Team.YELLOW else 2,
                -1,
                -1,
                -1,
                0 #this is an unused field
            ])

        if gameState is not None:
            header_game_vector = np.array([
                worldState.capture_time,
                worldState.friendlyTeam,
                worldState.teamWithBall,
                gameState.team_state,
                gameState.ref_cmd,
                gameState.ref_state
            ])
        
        num_blue_bots = len(worldState.blueRobotData())
        num_yellow_bots = len(worldState.yellowRobotData())
        
        # Create tensor with shape (12, 7) for robot data
        robot_tensor = np.zeros((12, 7))
        
        # Fill blue robot data (first 6 rows)
        blue_data = worldState.blueRobotData()
        for i in range(6):
            if i < num_blue_bots:
                robot_has_ball = 1 if blue_data[i].id == worldState.idWithBall and worldState.teamWithBall == Team.BLUE else 0
                robot_tensor[i] = [
                    blue_data[i].x, 
                    blue_data[i].y, 
                    blue_data[i].angle, 
                    blue_data[i].velocity_x, 
                    blue_data[i].velocity_y, 
                    blue_data[i].velocity_angle, 
                    robot_has_ball,
                ]
            else:
                robot_tensor[i] = [
                    -99999, 
                    -99999, 
                    -99999, 
                    -99999, 
                    -99999, 
                    -99999, 
                    -99999
                ] # Invalid vector for non-existent robots
                
        # Fill yellow robot data (last 6 rows)
        yellow_data = worldState.yellowRobotData()
        for i in range(6):
            if i < num_yellow_bots:
                robot_has_ball = 1 if yellow_data[i].id == worldState.idWithBall and worldState.teamWithBall == Team.YELLOW else 0
                robot_tensor[i+6] = [
                    yellow_data[i].x, 
                    yellow_data[i].y, 
                    yellow_data[i].angle, 
                    yellow_data[i].velocity_x, 
                    yellow_data[i].velocity_y, 
                    yellow_data[i].velocity_angle, 
                    robot_has_ball,
                ]
            else:
                robot_tensor[i+6] = [
                    -99999,
                    -99999, 
                    -99999, 
                    -99999, 
                    -99999, 
                    -99999, 
                    -99999, 
                    -99999
                ] # Invalid vector for non-existent robots
        
        # Create ball feature vector
        ball_data = worldState.ballData
        ball_vector = np.array([
            ball_data.x, 
            ball_data.y, 
            ball_data.z, 
            ball_data.velocity_x, 
            ball_data.velocity_y, 
            ball_data.velocity_z,
            0 #this is an unused field
        ])
        
        # Combine all features into final tensor
        self.tensor = np.concatenate([
            header_game_vector,
            robot_tensor.flatten(),
            ball_vector
        ])

    def print_tensor(self):
        print(self.tensor)

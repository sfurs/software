# Classifier Module

The Classifier module is responsible for classifying the game state based on the referee data and the current world state. It provides a method to classify the game state and emit the result.

## TODO:
- Add classifier logic
- Link classifier to the world state manager
- Write this README
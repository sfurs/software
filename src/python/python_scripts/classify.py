import sys

import numpy as np
import worldStateManager_module
from classifier.input import Input

def main(wsm):
    worldState = wsm.getCurrentState()
    input = Input(worldState)
    input.print_tensor()

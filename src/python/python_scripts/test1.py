import sys

print(sys.path)

import numpy as np
# print(np.__file__)
# print(np.__version__)
import worldStateManager_module

def test_module(wsm):
    arr = np.array([1,2,3])
    worldState = wsm.getCurrentState()
    blue_robot_data = worldState.blueRobotData()
    yellow_robot_data = worldState.yellowRobotData()
    ball_data = worldState.ballData
    capture_time = worldState.capture_time
    print(arr)
    
    print("blue team: ")
    for bot in blue_robot_data:
        print("id: ", bot.id, ", x: ", bot.x, ", y: ", bot.y, ", angle: ", bot.angle)
    
    print("yellow team: ")
    for bot in yellow_robot_data:
        print("id: ", bot.id, ", x: ", bot.x, ", y: ", bot.y, ", angle: ", bot.angle)
    
    print("ball_x: ", ball_data.x, "ball_y: ", ball_data.y)
    print("capture_time: ", capture_time)

#include "GameState.h"
#include "Logging.h"

void GameState::updateRefereeState(RefereeData data) {
    if (data.blue_team.score != refereeData.blue_team.score) {
        data.command = RefereeCommand::STOP_GOAL_BLUE;
    } else if (data.yellow_team.score != refereeData.yellow_team.score) {
        data.command = RefereeCommand::STOP_GOAL_YELLOW;
    }
    if (data.has_is_blue_on_positive) {
        is_blue_on_positive = data.is_blue_on_positive;
    }
    refereeData = data;
    updateGameState();
}

void GameState::updateWorldState(BallData data, Team friendlyTeam) {
    ball = data;
    team = friendlyTeam;
    updateGameState();
}

RefereeState GameState::getRefereeState() {
    return gameState;
}

RefereeData GameState::getRefereeData() {
    return refereeData;
}

void GameState::printState() {
    std::cout << "Game State: " << StateNames[gameState].toStdString() << std::endl;
    std::cout << "  Last Referee Command: " << CommandNames[refereeData.command].toStdString()
              << " was issued at " << refereeData.command_time << " ms." << std::endl;
    std::cout << "  Next Referee Command: " << CommandNames[refereeData.next_command].toStdString() << std::endl;
    std::cout << "  Game Stage: " << StageNames[refereeData.stage].toStdString() << std::endl;
    std::cout << "  Ball Position: (" << ball.x << ", " << ball.y << ")" << std::endl;
    std::cout << "  Team: " << TeamNames[team].toStdString() << std::endl;
    std::cout << "  State Start Time: " << state_start << std::endl;
    std::cout << "  State Ball Position: (" << state_ball_x << ", " << state_ball_y << ")" << std::endl;
}

QDebug operator<<(QDebug os, const GameState& data) {
    os << "Game State: " << StateNames[data.gameState] << "\n";
    os << "  Last Referee Command: " << CommandNames[data.refereeData.command] << " was issued at " << data.refereeData.command_time << " ms.\n";
    os << "  Next Referee Command: " << CommandNames[data.refereeData.next_command] << "\n";
    os << "  Game Stage: " << StageNames[data.refereeData.stage] << "\n";
    os << "  Ball Position: (" << data.ball.x << ", " << data.ball.y << ")\n";
    os << "  Team: " << TeamNames[data.team] << "\n";
    os << "  State Start Time: " << data.state_start << "\n";
    os << "  State Ball Position: (" << data.state_ball_x << ", " << data.state_ball_y << ")\n";
    return os;
}

void GameState::updateGameState() {
    RefereeData referee = refereeData;
    RefereeState oldState = gameState;

    if (referee.command == RefereeCommand::HALT) {
        gameState = RefereeState::HALT;
    } else if (referee.command == RefereeCommand::TIMEOUT_YELLOW ||
        referee.command == RefereeCommand::TIMEOUT_BLUE) {
        //TODO is this per team?
        gameState = RefereeState::TIMEOUT;
    } else if (referee.command == RefereeCommand::STOP_GOAL_YELLOW) {
        blue_score = refereeData.blue_team.score; 
        yellow_score = refereeData.yellow_team.score;
    } else if (referee.command == RefereeCommand::STOP_GOAL_BLUE) {
        blue_score = refereeData.blue_team.score; 
        yellow_score = refereeData.yellow_team.score;
    } else if (referee.command == RefereeCommand::STOP) {
        gameState = RefereeState::STOP;
        // only from BALL_PLACEMENT, HALT, TIMEOUT, RUN?
    } else if (referee.command == RefereeCommand::PREPARE_KICKOFF_YELLOW ||
        referee.command == RefereeCommand::PREPARE_KICKOFF_BLUE) {
        if (isAttacker(referee.command)) {
            gameState = RefereeState::PREPARE_KICKOFF_ATTACK;
        } else {
            gameState = RefereeState::PREPARE_KICKOFF_DEFEND;
        }
    } else if (referee.command == RefereeCommand::PREPARE_PENALTY_YELLOW ||
        referee.command == RefereeCommand::PREPARE_PENALTY_BLUE) {
        if (isAttacker(referee.command)) {
            gameState = RefereeState::PREPARE_PENALTY_ATTACK;
        } else {
            gameState = RefereeState::PREPARE_PENALTY_DEFEND;
        }
    } else if (referee.command == RefereeCommand::BALL_PLACEMENT_YELLOW ||
        referee.command == RefereeCommand::BALL_PLACEMENT_BLUE) {
        if (isAttacker(referee.command)) {
            if(referee.next_command == RefereeCommand::DIRECT_FREE_YELLOW ||
                referee.next_command == RefereeCommand::DIRECT_FREE_BLUE) {
                gameState = RefereeState::BALL_PLACEMENT_FREEKICK;
            } else {
                gameState = RefereeState::BALL_PLACEMENT_FORCE_PLAY;
            }
        } else {
            gameState = RefereeState::BALL_PLACEMENT_POSITION;
        }
    } else if (referee.command == RefereeCommand::DIRECT_FREE_YELLOW ||
        referee.command == RefereeCommand::DIRECT_FREE_BLUE) {
        if (isAttacker(referee.command)) {
            gameState = RefereeState::FREEKICK;
        } else {
            gameState = RefereeState::WAIT_FOR_BALL_IN_PLAY;
        }
    } else if (referee.command == RefereeCommand::FORCE_START) {
        gameState = RefereeState::RUN;
    } else {
        if (gameState == RefereeState::PREPARE_KICKOFF_ATTACK) {
            if (referee.command == RefereeCommand::NORMAL_START) {
                gameState = RefereeState::KICKOFF;
            }
        } else if (gameState == RefereeState::PREPARE_KICKOFF_DEFEND) {
            if (referee.command == RefereeCommand::NORMAL_START) {
                gameState = RefereeState::WAIT_FOR_BALL_IN_PLAY;
            }
        } else if (gameState == RefereeState::PREPARE_PENALTY_ATTACK) {
            if(referee.command == RefereeCommand::NORMAL_START) {
                gameState = RefereeState::PENALTY_ATTACK;
            }
        } else if (gameState == RefereeState::PREPARE_PENALTY_DEFEND) {
            if(referee.command == RefereeCommand::NORMAL_START) {
                gameState = RefereeState::PENALTY_DEFEND;
            }
        } else if (gameState == RefereeState::KICKOFF || 
            gameState == RefereeState::FREEKICK ||
            gameState == RefereeState::WAIT_FOR_BALL_IN_PLAY) {
            if (ballInPlay()) {
                gameState = RefereeState::RUN;
            }
        } 
    }
    if(oldState != gameState) {
        qDebug() << "State change from" << StateNames[oldState] << "to" << StateNames[gameState];
        state_start = referee.command_time / 1000000.0;
        state_ball_x = ball.x;
        state_ball_y = ball.y;
    }
}

bool GameState::ballInPlay() {
    double time_diff = (refereeData.command_time / 1000000.0) - state_start;
    double distance_diff = std::pow(ball.x - state_ball_x,2) + std::pow(ball.y - state_ball_y,2);
    return time_diff > 15 || distance_diff > 25;
}

unsigned int GameState::getBlueScore() {
    return blue_score;
}

unsigned int GameState::getYellowScore() {
    return yellow_score;
}

bool GameState::isAttacker(RefereeCommand cmd) {
    Team attacker = BLUE;
    if (cmd == RefereeCommand::PREPARE_PENALTY_YELLOW ||
        cmd == RefereeCommand::PREPARE_KICKOFF_YELLOW ||
        cmd == RefereeCommand::DIRECT_FREE_YELLOW ||
        cmd == RefereeCommand::BALL_PLACEMENT_YELLOW) {
        attacker = YELLOW;
    }
    return attacker == team;
}

bool GameState::isBlueOnPositive() {
    return is_blue_on_positive;
}

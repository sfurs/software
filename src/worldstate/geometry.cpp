#include <stdexcept>
#include "geometry.h"


Point Point::operator+(const Point& other) const {
    return Point{x + other.x, y + other.y};
}

Point Point::operator-(const Point& other) const {
    return Point{x - other.x, y - other.y};
}

Point Point::operator*(const float& scalar) const {
    return Point{x * scalar, y * scalar};
}

Point Point::operator/(const float& scalar) const {
    if (scalar == 0) {
        throw std::invalid_argument("Division by zero");
    }
    return Point{x / scalar, y / scalar};
}

std::ostream& operator<<(std::ostream& os, const Point& point) {
    os << "(x=" << point.x << ", y=" << point.y << ")";
    return os;
}

QDebug operator<<(QDebug os, const Point& point) {
    os << "( x =" << point.x << ", y =" << point.y << ")";
    return os;
}

float Line::length() const {
    return std::sqrt(std::pow(end.x - start.x, 2) + std::pow(end.y - start.y, 2));
}

std::ostream& operator<<(std::ostream& os, const Line& line) {
    os << "Start:" << line.start << ", End: " << line.end << ", Width: " << line.width;
    return os;
}

QDebug operator<<(QDebug os, const Line& line) {
    os << "Start:" << line.start << ", End:" << line.end << ", Width:" << line.width;
    return os;
}

float Circle::area() const {
    return M_PI * std::pow(radius, 2);
}

bool Circle::contains(float x, float y) const {
    return std::pow(x - center.x, 2) + std::pow(y - center.y, 2) <= std::pow(radius, 2);
}

bool Circle::contains(const Point& point) const {
    return std::pow(point.x - center.x, 2) + std::pow(point.y - center.y, 2) <= std::pow(radius, 2);
}

std::ostream& operator<<(std::ostream& os, const Circle& circle) {
    os << "Center: " << circle.center << ", Radius: " << circle.radius;
    return os;
}

QDebug operator<<(QDebug os, const Circle& circle) {
    os << "Center:" << circle.center << ", Radius:" << circle.radius;
    return os;
}

Rectangle::Rectangle(Point topLeft, Point bottomRight): topLeft(topLeft), bottomRight(bottomRight) {
    if (topLeft.x > bottomRight.x || topLeft.y > bottomRight.y) {
        throw std::invalid_argument("Top left point must be above and to the left of the bottom right point");
    }
}

float Rectangle::area() const {
    return (bottomRight.x - topLeft.x) * (bottomRight.y - topLeft.y);
}

Point Rectangle::center() const {
    return (topLeft + bottomRight) / 2;
}

bool Rectangle::contains(float x, float y) const {
    return x >= topLeft.x && x <= bottomRight.x && y >= topLeft.y && y <= bottomRight.y;
}

bool Rectangle::contains(const Point& point) const {
    return point.x >= topLeft.x && point.x <= bottomRight.x && 
           point.y >= topLeft.y && point.y <= bottomRight.y;
}

std::ostream& operator<<(std::ostream& os, const Rectangle& rectangle) {
    os << "Top Left: " << rectangle.topLeft << ", Bottom Right: " << rectangle.bottomRight;
    return os;
}

QDebug operator<<(QDebug os, const Rectangle& rectangle) {
    os << "Top Left:" << rectangle.topLeft << ", Bottom Right:" << rectangle.bottomRight;
    return os;
}


#ifndef GEOMETRY_H

#define GEOMETRY_H

#include <iostream>
#include "Logging.h"

struct Point {
    float x;
    float y;

public:
    Point operator+(const Point& other) const;
    Point operator-(const Point& other) const;
    Point operator*(const float& scalar) const;
    Point operator/(const float& scalar) const;


    friend std::ostream& operator<<(std::ostream& os, const Point& point);
    friend QDebug operator<<(QDebug os, const Point& point);
};

struct Line {
    Point start;
    Point end;
    float width;

public:
    // Compute the length of line in 2D cartesian space
    float length() const;

    friend std::ostream& operator<<(std::ostream& os, const Line& line);
    friend QDebug operator<<(QDebug os, const Line& line);
};

struct Circle {
    Point center;
    float radius;

public:
    // Compute the area of the circle
    float area() const;

    // Check if a point is inside the circle
    bool contains(float x, float y) const;
    bool contains(const Point& point) const;

    friend std::ostream& operator<<(std::ostream& os, const Circle& circle);
    friend QDebug operator<<(QDebug os, const Circle& circle);
};;

struct Rectangle {
    Point topLeft;
    Point bottomRight;

public:
    Rectangle(Point topLeft, Point bottomRight);

    // Compute the area of the rectangle
    float area() const;
    // Compute the center of the rectangle
    Point center() const;

    // Check if a point is inside the rectangle
    bool contains(float x, float y) const;
    bool contains(const Point& point) const;

    friend std::ostream& operator<<(std::ostream& os, const Rectangle& rectangle);
    friend QDebug operator<<(QDebug os, const Rectangle& rectangle);
};

#endif

## Overview: WorldState

This file contains the definition and implementation of the `WorldState` class, which stores and manages the state of the field, including data about the ball and the robots (blue and yellow teams) at a specific capture time. It provides functions to print and output the current state of the world in both text and debug formats.

## Files

### 1. `worldState.cpp` & `worldState.h`
- **WorldState class**: 
    The `WorldState` class holds crucial data regarding the ball, blue robots, and yellow robots on a soccer field at a specific capture time. It provides methods to print this state and stream it for debugging purposes.

    - **Functions**: 
        - `WorldState()`:
        Constructor to initialize `WorldState` with default values
        - `WorldState(BallData ballData, QList<RobotData> blueRobotData, QList<RobotData> yellowRobotData, double capture_time)`:
        Parameterized constructor to initialize the world state with specific data for the ball, blue team robots, yellow team robots, and the capture time.
        - `void printState()`:
        Outputs the current world state, including the capture time, ball data, and robot data, to the standard output using `cout`
        - `friend QDebug operator<<(QDebug os, const WorldState& data)`: Overloads the `<<` operator to allow the world state to be output using the QDebug stream.

### 2. `GameState.cpp` & `GameState.h`
- **GameState class**
    The `GameState` class holds the game stage and referee data, including current referee command, game stage, and team information (such as score, cards).

    - **Functions**:
        - `GameState()`:
        Constructor to initialize `GameState` (this is implicitly defined)
        - `void updateWorldState(BallData data)`
        Update the current game state based on the world state
        - `void updateRefereeState(RefereeData data)`
        Update the current game state based on the referee state
        - `RefereeState getRefereeState()`
        Returns the overall game state of the game
        - `RefereeData getRefereeData()`
        Returns referee specific data such as game stage and team info
        - `void updateGameState()`
        (private) Update the current state of the game
        - `bool ballInPlay()`
        (private) returns whether the ball is in play

- **RefereeState Enum**
    - Represents a abstracted form of the current game state
        - `HALT`: Robots are not allowed to move
        - `STOP`: Both teams have to keep distance to the ball
        - `TIMEOUT`: Both teams can do what they want
        - `BALL_PLACEMENT_FORCE_PLAY`: Place the ball for a force play
        - `BALL_PLACEMENT_FREEKICK`: Place the ball for a free kick
        - `BALL_PLACEMENT_POSITION`: Wait for the opponent to place the ball
        - `PREPARE_PENALTY_ATTACK`: Keeper moves to goal line, attacker move behind ball, other robots move to legal positions
        - `PREPARE_PENALTY_DEFEND`: Move to legal position for penalty kicks
        - `PREPARE_KICKOFF_ATTACK`: Teams have to move to their sides for kickoff
        - `PREPARE_KICKOFF_DEFEND`: Teams have to move to their sides for kickoff
        - `KICKOFF`: One team may kick the ball in 10 seconds
        - `PENALTY_ATTACK`: Preform a penalty kick within 10 seconds
        - `PENALTY_DEFEND`: Defend against a penalty kick
        - `FREEKICK`: One team may kick the ball wihtin 10 seconds
        - `WAIT_FOR_BALL_IN_PLAY`: Do not touch ball until it is in play
        - `RUN`: Both teams may manipulate the ball

### 3. `listWorldStates.cpp` & `listWorldStates.h`:
- **listWorldStates struct**: 
    The `ListWorldStates` struct is designed to manage a linked list of `WorldState` objects, representing the progression of states over time. It stores pointers to the `prevState` and `nextState` in the list, allowing traversal through a sequence of `WorldState` objects.

    - **Functions**:
        - `ListWorldStates()`:
        Default constructor that initializes the `struct` by setting the pointers `prevState` and `nextState` to `nullptr`.
        - `~ListWorldStates()`:
        Destructor that cleans up the dynamically allocated memory associated with the linked list.

### 4. `worldStateManager.cpp` & `worldStateManager.h`:
- **WorldStateManager Class**: 
    The `WorldStateManager` class manages the state of the robotic soccer game, including the ball, robots, and game rules (such as referee commands and game state). It provides methods for creating new world states, handling referee data, and processing game logic.

    - **Functions**:
        - `void lock() const`: Locks the world state for thread-safe access.
        - `void unlock() const`: Unlocks the world state after the data is accessed or modified.
        - `void uploadRefereeData(RefereeData referee)` : Updates the current referee data with new instructions.
        - `RefereeData getRefereeData()` : Returns the current referee data.
        - `void createWorldState(BallData ballData, QList<RobotData> blueRobotData, QList<RobotData> yellowRobotData, double capture_time)`: Creates a new world state based on the provided ball and robot data, and updates the capture time.
        - `WorldState getCurrentState()` : Returns the current world state.
        - `void setGameState(GameState gS)` : Sets the current game state to the specified state (e.g., First_Half, Timeout, etc.).
        - `void processRefereeCommand(RefereeCommand rC)`: Processes referee commands, such as starting the game, placing the ball, or handling penalties.
        - `static inline WorldStateManager* getInstance()` : Singleton pattern implementation to ensure that only one instance of WorldStateManager is created.
        - `void printWorldState()` : Prints the current world state, including ball and robot data, to the console for debugging.


### 4. `CMakeLists.txt`
- This is the build configuration file for the project using CMake.
- It defines how the source files are compiled and includes dependencies such as `Qt6 Core`. 
- The `CMakeLists.txt` file includes all `.cpp` and `.h` files in the project as sources to build the `skynet` executable.

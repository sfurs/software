#include "fieldDimensions.h"

// Constants for field dimensions
const float FIELD_WIDTH = 10400.0f;
const float FIELD_HEIGHT = 7400.0f;
const float FIELD_MARGIN = 300.0f;
const float FIELD_RUNOFF = 700.0f;
const float FIELD_LINE_WIDTH = 10.0f;
const float INBOUND_WIDTH = 9000.0f;
const float INBOUND_HEIGHT = 6000.0f;
const float CENTER_CIRCLE_DIAMETER = 1000.0f;
const float DEFENSE_AREA_WIDTH = 1000.0f;
const float DEFENSE_AREA_HEIGHT = 2000.0f;
const float GOAL_AREA_WIDTH = 180.0f;
const float GOAL_AREA_HEIGHT = 1000.0f;
const float PENALTY_MARK_DISTANCE = 1000.0f;
const float THIRD_WIDTH = INBOUND_WIDTH / 3.0f;
const float HALF_WIDTH = INBOUND_WIDTH / 2.0f;

// Offset field dimensions to the centre of the field
const float FIELD_DIMENSIONS_X_OFFSET = FIELD_WIDTH / 2;
const float FIELD_DIMENSIONS_Y_OFFSET = FIELD_HEIGHT / 2;

namespace {
    // Helper to create wall boundaries based on FIELD_RUNOFF and FIELD_MARGIN
    Rectangle calculateWall() {
        float adjustedRunoff = FIELD_RUNOFF - FIELD_MARGIN;
        return Rectangle{
            Point{adjustedRunoff, adjustedRunoff} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_WIDTH, FIELD_HEIGHT} - Point{adjustedRunoff, adjustedRunoff} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
        };
    }

    // Helper to create in-bounds area based on FIELD_RUNOFF and FIELD_LINE_WIDTH
    Rectangle calculateInBounds() {
        float adjustedRunoff = FIELD_RUNOFF + FIELD_LINE_WIDTH;
        return Rectangle{
            Point{adjustedRunoff, adjustedRunoff} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{adjustedRunoff, adjustedRunoff} + Point{INBOUND_WIDTH, INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
        };
    }

    // Helper to define defense areas
    std::array<Rectangle, 2> calculateDefenseAreas() {
        float centerY = FIELD_HEIGHT / 2;
        float halfDefenseAreaHeight = DEFENSE_AREA_HEIGHT / 2;
        return {
            Rectangle{
                Point{FIELD_RUNOFF, centerY - halfDefenseAreaHeight} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
                Point{FIELD_RUNOFF + DEFENSE_AREA_WIDTH, centerY + halfDefenseAreaHeight} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
            },
            Rectangle{
                Point{FIELD_RUNOFF + INBOUND_WIDTH - DEFENSE_AREA_WIDTH, centerY - halfDefenseAreaHeight} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
                Point{FIELD_RUNOFF + INBOUND_WIDTH, centerY + halfDefenseAreaHeight} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
            }
        };
    }

    // Helper to define penalty mark points
    std::array<Point, 2> calculatePenaltyMarks() {
        float centerY = FIELD_RUNOFF + INBOUND_HEIGHT / 2;
        return {
            Point{FIELD_RUNOFF + PENALTY_MARK_DISTANCE, centerY} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + INBOUND_WIDTH - PENALTY_MARK_DISTANCE, centerY} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
        };
    }

    // Helper to create goal areas
    std::array<Rectangle, 2> calculateGoals() {
        float centerY = FIELD_HEIGHT / 2;
        float halfGoalAreaHeight = GOAL_AREA_HEIGHT / 2;
        return {
            Rectangle{
                Point{FIELD_RUNOFF - GOAL_AREA_WIDTH, centerY - halfGoalAreaHeight} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
                Point{FIELD_RUNOFF, centerY + halfGoalAreaHeight} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
            },
            Rectangle{
                Point{FIELD_RUNOFF + INBOUND_WIDTH, centerY - halfGoalAreaHeight} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
                Point{FIELD_RUNOFF + INBOUND_WIDTH + GOAL_AREA_WIDTH, centerY + halfGoalAreaHeight} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
            }
        };
    }

    // Helper to create midfield boundary
    Rectangle calculateMidfield() {
        return Rectangle{
            Point{FIELD_RUNOFF + THIRD_WIDTH, FIELD_RUNOFF} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + (2 * THIRD_WIDTH), FIELD_RUNOFF + INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
        };
    }

    // Helper to create negative third (top side of field in GUI)
    Rectangle calculateNegativeThird() {
        return Rectangle{
            Point{FIELD_RUNOFF, FIELD_RUNOFF} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + THIRD_WIDTH, FIELD_RUNOFF + INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
        };
    }

    // Helper to create positive third (bottom side of field in GUI)
    Rectangle calculatePositiveThird() {
        return Rectangle{
            Point{FIELD_RUNOFF + (2 * THIRD_WIDTH), FIELD_RUNOFF} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + INBOUND_WIDTH, FIELD_RUNOFF + INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
        };
    }

    // Helper to create negative half (left side of field in GUI)
    Rectangle calculateNegativeHalf() {
        return Rectangle{
            Point{FIELD_RUNOFF, FIELD_RUNOFF} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + HALF_WIDTH, FIELD_RUNOFF + INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
        };
    }

    // Helper to create positive half (right side of field in GUI)
    Rectangle calculatePositiveHalf() {
        return Rectangle{
            Point{FIELD_RUNOFF + HALF_WIDTH, FIELD_RUNOFF} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + INBOUND_WIDTH, FIELD_RUNOFF + INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
        };
    }
}

FieldDimensions::FieldDimensions() :
    field(
        Rectangle{
            Point{0, 0} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_WIDTH, FIELD_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET}
        }
    ),
    fieldMargin(FIELD_MARGIN),
    wall(calculateWall()),
    inBounds(calculateInBounds()),
    fieldLineWidth(FIELD_LINE_WIDTH),
    touchLines({
        Line{
            Point{FIELD_RUNOFF, FIELD_RUNOFF} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + INBOUND_WIDTH, FIELD_RUNOFF} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            FIELD_LINE_WIDTH
        },
        Line{
            Point{FIELD_RUNOFF, FIELD_RUNOFF + INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + INBOUND_WIDTH, FIELD_RUNOFF + INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            FIELD_LINE_WIDTH
        }
    }),
    goalLines({
        Line{
            Point{FIELD_RUNOFF, FIELD_RUNOFF} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF, FIELD_RUNOFF + INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            FIELD_LINE_WIDTH
        },
        Line{
            Point{FIELD_RUNOFF + INBOUND_WIDTH, FIELD_RUNOFF} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + INBOUND_WIDTH, FIELD_RUNOFF + INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            FIELD_LINE_WIDTH
        }
    }),
    halfWayLine(
        Line{
            Point{FIELD_RUNOFF + INBOUND_WIDTH / 2, FIELD_RUNOFF} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + INBOUND_WIDTH / 2, FIELD_RUNOFF + INBOUND_HEIGHT} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            FIELD_LINE_WIDTH
        }
    ),
    goalToGoalLine(
        Line{
            Point{FIELD_RUNOFF, FIELD_RUNOFF + INBOUND_HEIGHT / 2} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            Point{FIELD_RUNOFF + INBOUND_WIDTH, FIELD_RUNOFF + INBOUND_HEIGHT / 2} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            FIELD_LINE_WIDTH
        }
    ),
    centerCircle(
        Circle{
            Point{FIELD_RUNOFF + INBOUND_WIDTH / 2, FIELD_RUNOFF + INBOUND_HEIGHT / 2} - Point{FIELD_DIMENSIONS_X_OFFSET, FIELD_DIMENSIONS_Y_OFFSET},
            CENTER_CIRCLE_DIAMETER / 2
        }
    ),
    defenseAreas(calculateDefenseAreas()),
    penaltyMarks(calculatePenaltyMarks()),
    goals(calculateGoals()),
    midfield(calculateMidfield()),
    negativeThird(calculateNegativeThird()),
    positiveThird(calculatePositiveThird()),
    negativeHalf(calculateNegativeHalf()),
    positiveHalf(calculatePositiveHalf())
{}

std::ostream& operator<<(std::ostream& os, const FieldDimensions& fd){
    os << "Field Dimensions:\n";
    os << "Field:\n  " << fd.field << "\n";

    os << "Field Margin: " << fd.fieldMargin << "\n";

    os << "Wall:\n  " << fd.wall << "\n";

    os << "In Bounds:\n  " << fd.inBounds << "\n";

    os << "Field Line Width: " << fd.fieldLineWidth << "\n";

    os << "Touch Lines (size: " << fd.touchLines.size() << "\n";
    for (const Line& line : fd.touchLines) {
        os << "  " << line << "\n";
    }

    os << "Goal Lines:\n";
    for (const Line& line : fd.goalLines) {
        os << "  " << line << "\n";
    }

    os << "Halfway Line:\n  " << fd.halfWayLine << "\n";

    os << "Goal to Goal Line:\n  " << fd.goalToGoalLine << "\n";

    os << "Center Circle:\n  " << fd.centerCircle << "\n";

    os << "Defense Areas:\n";
    for (const Rectangle& area : fd.defenseAreas) {
        os << "  " << area << "\n";
    }

    os << "Penalty Marks:\n";
    for (const Point& mark : fd.penaltyMarks) {
        os << "  " << mark << "\n";
    }

    os << "Goals:\n";
    for (const Rectangle& goal : fd.goals) {
        os << "  " << goal << "\n";
    }

    return os;
}

QDebug operator<<(QDebug os, const FieldDimensions& fd){
    os << "Field Dimensions:\n";
    os << "Field:\n  " << fd.field << "\n";

    os << "Field Margin: " << fd.fieldMargin << "\n";

    os << "Wall:\n  " << fd.wall << "\n";

    os << "In Bounds:\n  " << fd.inBounds << "\n";

    os << "Field Line Width: " << fd.fieldLineWidth << "\n";

    os << "Touch Lines:\n";
    for (const Line& line : fd.touchLines) {
        os << "  " << line << "\n";
    }

    os << "Goal Lines:\n";
    for (const Line& line : fd.goalLines) {
        os << "  " << line << "\n";
    }

    os << "Halfway Line:\n  " << fd.halfWayLine << "\n";

    os << "Goal to Goal Line:\n  " << fd.goalToGoalLine << "\n";

    os << "Center Circle:\n  " << fd.centerCircle << "\n";

    os << "Defense Areas:\n";
    for (const Rectangle& area : fd.defenseAreas) {
        os << "  " << area << "\n";
    }

    os << "Penalty Marks:\n";
    for (const Point& mark : fd.penaltyMarks) {
        os << "  " << mark << "\n";
    }

    os << "Goals:\n";
    for (const Rectangle& goal : fd.goals) {
        os << "  " << goal << "\n";
    }

    return os;
}


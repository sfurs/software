#include "worldStateManager.h"
#include "worldState.h"
#include "FieldData.h"
#include <QMutex>
#include <QVector>
#include <ctime>
#include <exception>
#include <iostream>
#include "../shared/Team.h"
#include "../shared/RefereeData.h"

WorldStateManager::WorldStateManager() {}

void WorldStateManager::lock() const
{
    worldStateMutex.lock();
}

void WorldStateManager::unlock() const
{
    worldStateMutex.unlock();
}

void WorldStateManager::uploadRefereeData(RefereeData referee)
{
    lock();
    gameState.updateRefereeState(referee);

    if (
        gameState.getRefereeData().command == RefereeCommand::STOP_GOAL_BLUE
        || gameState.getRefereeData().command == RefereeCommand::STOP_GOAL_YELLOW) {
        updateScore();
    } 
    
    blueOnPositive = gameState.isBlueOnPositive();
    std::cout << "Blue on positive: " << blueOnPositive << std::endl;

    emit gameStateReflex(gameState);
    unlock();
}

void WorldStateManager::createWorldState(BallData ballData, QList<RobotData> blueRobotData, QList<RobotData> yellowRobotData, double capture_time)
{
    lock();
    currentState = WorldState(ballData, blueRobotData, yellowRobotData, capture_time, friendlyTeam, blueOnPositive);
    worldStates[worldStateIndex] = currentState;
    worldStateIndex++;
    worldStateIndex = worldStateIndex % worldStateHistory;

    gameState.updateWorldState(ballData, friendlyTeam);
    emit worldStateReflex(currentState);
    unlock();
}

WorldState WorldStateManager::getInternalState()
{
    if (worldStateIndex != 0)
        state = worldStates[worldStateIndex - 1];
    else
        state = worldStates[worldStateHistory - 1];
    return state;
}

WorldState WorldStateManager::getCurrentState()
{
    lock();
    WorldState state = getInternalState();
    unlock();
    return state;
}

GameState WorldStateManager::getGameState()
{
    lock();
    GameState data = gameState;
    unlock();
    return data;
}

void WorldStateManager::printWorldState()
{
    std::cout << "Ball: " << state.ballData.x << " " << state.ballData.y << std::endl;
    for (int i = 0; i < state.blueRobotData.size(); i++)
    {
        std::cout << "Blue: " << state.blueRobotData[i].x << " " << state.blueRobotData[i].y << " " << state.blueRobotData[i].angle << std::endl;
    }
    for (int i = 0; i < state.yellowRobotData.size(); i++)
    {
        std::cout << "Yellow: " << state.yellowRobotData[i].x << " " << state.yellowRobotData[i].y << " " << state.yellowRobotData[i].angle << std::endl;
    }
}

void WorldStateManager::setFriendlyTeam(Team newTeam)
{
    lock();
    friendlyTeam = newTeam;
    unlock();
}

void WorldStateManager::updateScore(){
    blueScore = gameState.getBlueScore();
    yellowScore = gameState.getYellowScore();

    emit emitScores(blueScore, yellowScore);
}

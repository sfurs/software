#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "RefereeData.h"
#include "FieldData.h"
#include "Team.h"
#include <QMap>
#include <iostream>
/** \brief Enum for different states of play*/
enum class RefereeState {
    // Robots are not allowed to move
    HALT, // 5.1.2
    // Both teams have to keep distance to the ball
    STOP, // 5.1.1
    // Both teams can do what ever they want
    TIMEOUT, // 4.4.2 
    

    // One team places the ball, the other keeps distance to ball

    //Ball placement positions differ
    BALL_PLACEMENT_FORCE_PLAY, // 5.2
    BALL_PLACEMENT_FREEKICK, // 5.2
    
    // Team must move robots away from line between ball and placement position
    BALL_PLACEMENT_POSITION, // 5.2 and 8.4.3


    // Keeper on goal line, attacker behind ball
    // other robots on legal positions
    PREPARE_PENALTY_ATTACK, //5.3.5
    PREPARE_PENALTY_DEFEND,
    // Execute a penalty kick within 10 seconds
    PENALTY_ATTACK, //5.3.5
    PENALTY_DEFEND,
    
    // Teams have to move to their sides
    
    // Only 1 robot allowed in the center circle
    PREPARE_KICKOFF_ATTACK, // 5.3.2
    // No robots allowed in the center circle
    PREPARE_KICKOFF_DEFEND, // 5.3.2
    
    // Team may kick the ball within 10 seconds
    KICKOFF, // 5.3.2

    // One team may kick the ball within 10 seconds
    FREEKICK, // 5.3.3

    // Defence for both kickoff and freekick
    WAIT_FOR_BALL_IN_PLAY,
    // Both teams may manipulate the ball
    RUN
    //TODO figure out where to place the double-kick rule

    //TODO shootout?
};
// based on sections 5.3 , and appendix B in the rules

// Names of overall current state of the game for ui and debugging
const QMap<RefereeState, QString> StateNames = {
    {RefereeState::HALT, "Halt"},
    {RefereeState::TIMEOUT, "Timeout"},
    {RefereeState::STOP, "Stop"},
    {RefereeState::PREPARE_KICKOFF_ATTACK, "Prepare Kickoff Attacker"},
    {RefereeState::PREPARE_KICKOFF_DEFEND, "Prepare Kickoff Defender"},
    {RefereeState::BALL_PLACEMENT_FORCE_PLAY, "Ball Placement Force Play"},
    {RefereeState::BALL_PLACEMENT_FREEKICK, "Ball Placement Freekick"},
    {RefereeState::BALL_PLACEMENT_POSITION, "Opponent Ball Placement"},
    {RefereeState::PREPARE_PENALTY_ATTACK, "Prepare Penalty Attacker"},
    {RefereeState::PREPARE_PENALTY_DEFEND, "Prepare Penalty Defender"},
    {RefereeState::KICKOFF, "Kickoff"},
    {RefereeState::FREEKICK, "Freekick"},
    {RefereeState::PENALTY_ATTACK, "Penalty Attacker"},
    {RefereeState::PENALTY_DEFEND, "Penalty Defender"},
    {RefereeState::WAIT_FOR_BALL_IN_PLAY, "Wait for ball in play"},
    {RefereeState::RUN, "Run"}
};

// Names of referee commands for ui and debugging
const QMap<RefereeCommand, QString> CommandNames = {
    {RefereeCommand::NO_COMMAND, "No Command"},
    {RefereeCommand::HALT, "Halt"},
    {RefereeCommand::STOP, "Stop"},
    {RefereeCommand::STOP_GOAL_YELLOW, "Stop Goal Yellow"},
    {RefereeCommand::STOP_GOAL_BLUE, "Stop Goal Blue"},
    {RefereeCommand::NORMAL_START, "Normal Start"},
    {RefereeCommand::FORCE_START, "Force Start"},
    {RefereeCommand::PREPARE_KICKOFF_YELLOW, "Prepare Kickoff Yellow"},
    {RefereeCommand::PREPARE_KICKOFF_BLUE, "Prepare Kickoff Blue"},
    {RefereeCommand::PREPARE_PENALTY_YELLOW, "Prepare Penalty Yellow"},
    {RefereeCommand::PREPARE_PENALTY_BLUE, "Prepare Penalty Blue"},
    {RefereeCommand::DIRECT_FREE_YELLOW, "Direct Free Yellow"},
    {RefereeCommand::DIRECT_FREE_BLUE, "Direct Free Blue"},
    {RefereeCommand::TIMEOUT_YELLOW, "Timeout Yellow"},
    {RefereeCommand::TIMEOUT_BLUE, "Timeout Blue"},
    {RefereeCommand::BALL_PLACEMENT_YELLOW, "Ball Placement Yellow"},
    {RefereeCommand::BALL_PLACEMENT_BLUE, "Ball Placement Blue"}
};

// Names of game stages for ui and debugging
const QMap<GameStage, QString> StageNames = {
    {GameStage::NO_STAGE, "No Stage"},
    {GameStage::NORMAL_FIRST_HALF_PRE, "Normal First Half Pre"},
    {GameStage::NORMAL_FIRST_HALF, "Normal First Half"},
    {GameStage::NORMAL_HALF_TIME, "Normal Half Time"},
    {GameStage::NORMAL_SECOND_HALF, "Normal Second Half"},
    {GameStage::EXTRA_TIME_BREAK, "Extra Time Break"},
    {GameStage::EXTRA_FIRST_HALF_PRE, "Extra First Half Pre"},
    {GameStage::EXTRA_FIRST_HALF, "Extra First Half"},
    {GameStage::EXTRA_HALF_TIME, "Extra Half Time"},
    {GameStage::EXTRA_SECOND_HALF_PRE, "Extra Second Half Pre"},
    {GameStage::EXTRA_SECOND_HALF, "Extra Second Half"},
    {GameStage::PENALTY_SHOOTOUT_BREAK, "Penalty Shootout Break"},
    {GameStage::PENALTY_SHOOTOUT, "Penalty Shootout"},
    {GameStage::POST_GAME, "Post Game"}
};

const QMap<Team, QString> TeamNames = {
    {Team::BLUE, "Blue"},
    {Team::YELLOW, "Yellow"},
    {Team::INVALID, "Invalid"}
};

class GameState {

    /*
     * The Game State
     *
     * The game state is the state of the game as specified by the referee
    */
public:
    /* 
     * Update the internal state based on referee data
     *
     * @param data The referee data
    */
    void updateRefereeState(RefereeData data);
    /*
     * Update internal state based on world state (mainly for detecting game starts)
     *
     * @param ballData ball data
     * @param friendlyTeam the current team
    */
    void updateWorldState(BallData balldata, Team friendlyTeam);

    // Agent API
    /*
     * Returns the overall current state of the game
     *
     * @return An enum representing current state of the game
    */
    RefereeState getRefereeState();
    /*
     * Returns referee specific data such as game stage and team info
     *
     * @return The referee specific data
    */
    RefereeData getRefereeData();
    /*
     * Prints the current state of the game
    */
    /** \brief Print the current game state to stdout */
    void printState();

    /** \brief Stream operator for QDebug output */
    friend QDebug operator<<(QDebug os, const GameState& data);

    // void printState();

    /*
     * Returns the current amount of blue goals
     *
     * @return Blue team's score
     */
    unsigned int getBlueScore();

    /*
     * Returns the current amount of yellow goals
     *
     * @return Yellow team's score
     */
    unsigned int getYellowScore();
    /*
     *
     *
     *
    */
    bool isBlueOnPositive();

private:
    /*
     * update the current state of the game
    */
    void updateGameState();
    /*
     * check if the ball is in play
     *
     * @return whether the ball is in play
    */
    bool ballInPlay();

    /*
     * check if the referee command states that we are the attacker
     *
     * @param cmd the referee command
     *
     * @return whether the command specifies us as the attacker
    */
    bool isAttacker(RefereeCommand cmd);

    /** \brief The current state of the game*/
    RefereeState gameState = RefereeState::RUN;//TODO for games, this should be set to RefereeState::HALT
    RefereeData refereeData;

    // for updating the ball state in updateGameState
    BallData ball;

    // to simplify commands
    Team team;

    // for ballInPlay
    double state_start;
    double state_ball_x;
    double state_ball_y;

    unsigned int blue_score=0;
    unsigned int yellow_score=0;

    bool is_blue_on_positive = false;
};

#endif

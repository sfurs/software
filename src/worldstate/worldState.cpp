#include "worldState.h"
#include <iostream>
// #include "stateData/robotData.h"
// #include "stateData/ballData.h"
#include "FieldData.h"
#include "Team.h"

#include <QVector>
#include <QVector2D>
#include <QDebug>
#include <qcolor.h>
#include <qcontainerfwd.h>
#include <stdexcept>

// Because we cannot have a nullptr list. 
// Also don't want to re-construct these lists all the time.
QVector<RobotData> const WorldState::INVALID_ROBOTS = QVector<RobotData>(0u);

WorldState::WorldState()
{
    this->capture_time = -1;
    this->ballData = BallData();
    this->blueRobotData = INVALID_ROBOTS;
    this->yellowRobotData = INVALID_ROBOTS;
    this->friendlyTeam = Team::INVALID;
    this->teamWithBall = Team::INVALID;
    this->idWithBall = 0;
    this->blueOnPositive = false;
}

WorldState::WorldState(
    BallData ballData,
    QList<RobotData> blueRobotData,
    QList<RobotData> yellowRobotData,
    double capture_time,
    Team friendlyTeam,
    bool blueOnPositive
):
    ballData(ballData),
    blueRobotData(blueRobotData),
    yellowRobotData(yellowRobotData),
    capture_time(capture_time),
    friendlyTeam(friendlyTeam),
    blueOnPositive(blueOnPositive)
{
    if (ballData.z != 0) {
        std::cout << "Ball Data: " << ballData.x << ", " << ballData.y << ", " << ballData.z << std::endl;
    }
    this->teamWithBall = updatePossession();
}

WorldState::WorldState(
    BallData ballData,
    QList<RobotData> blueRobotData,
    QList<RobotData> yellowRobotData,
    double capture_time,
    Team friendlyTeam,
    std::optional<Team> teamWithBall,
    bool blueOnPositive
) :
    ballData(ballData),
    blueRobotData(blueRobotData),
    yellowRobotData(yellowRobotData),
    capture_time(capture_time),
    friendlyTeam(friendlyTeam),
    teamWithBall(teamWithBall),
    blueOnPositive(blueOnPositive)
{
    if (ballData.z != 0) {
        std::cout << "Ball Data: " << ballData.x << ", " << ballData.y << ", " << ballData.z << std::endl;
    }
}

const double POSSESSION_DISTANCE = 150.0;
const double POSSESSION_ANGLE = 30.0 * M_PI / 180.0;

bool WorldState::ballInYellowNet() const {
    return 0;
}

bool WorldState::ballInBlueNet() const {
    return 0;
}

// Loop through all current robots and update 'hasBall'
std::optional<Team> WorldState::updatePossession()
{
    for (size_t i = 0; i < blueRobotData.size(); i++)
    {
        if (hasPossession(blueRobotData[i]))
        {
            teamWithBall = BLUE;
            idWithBall = blueRobotData[i].id;
            return Team::BLUE;
        }
    }
    for (size_t i = 0; i < yellowRobotData.size(); i++)
    {
        if (hasPossession(yellowRobotData[i]))
        {
            teamWithBall = YELLOW;
            idWithBall = yellowRobotData[i].id;
            return Team::YELLOW;
        }
    }
    return std::nullopt;
}

// Check if a single robot has possession
bool WorldState::hasPossession(const RobotData &robot) const
{
    QVector2D toBall(ballData.x - robot.x, ballData.y - robot.y);
    double distance = toBall.length();
    if (distance > POSSESSION_DISTANCE)
    {
        return false;
    }

    double angleToBall = std::atan2(toBall.y(), toBall.x());
    double angleError = angleToBall - robot.angle;
    if (std::abs(angleError) > POSSESSION_ANGLE)
    {
        return false;
    }
    return true;
}

Team WorldState::opponentTeam() const {
    switch (friendlyTeam) {
        case Team::BLUE:
            return Team::YELLOW;
        case Team::YELLOW:
            return Team::BLUE;
        default:
            return INVALID;
    }
}

QVector<RobotData> WorldState::opponentRobots() const {
    switch (opponentTeam()) {
        case Team::BLUE:
            return blueRobotData;
        case Team::YELLOW:
            return yellowRobotData;
        default:
            return INVALID_ROBOTS;
    }
}

QVector<RobotData> WorldState::friendlyRobots() const {
    switch (friendlyTeam) {
        case Team::BLUE:
            return blueRobotData;
        case Team::YELLOW:
            return yellowRobotData;
        default:
            return INVALID_ROBOTS;
    }
}

double WorldState::getDistanceToBall(const RobotData& robot) const {
    return std::sqrt(std::pow(robot.x - ballData.x, 2) + std::pow(robot.y - ballData.y, 2));
}

bool WorldState::isPositiveHomeTeam() const {
    return (friendlyTeam == Team::BLUE && blueOnPositive) || (friendlyTeam == Team::YELLOW && !blueOnPositive);
}

void WorldState::printState() const
{
    std::cout << "Capture Time: " << this->capture_time << "\n";
    std::cout << "Ball Data:" << "\n";
    std::cout << "  " << "Position (x,y,z): " << "(" << this->ballData.x << "," << this->ballData.y << "," << this->ballData.z << ")" << "  ";
    std::cout << "  " << "Velocity (Vx, Vy, Vz): " << "(" << this->ballData.velocity_x << "," << this->ballData.velocity_y << "," << this->ballData.velocity_z << ")" << "\n";
    std::cout << "  " << "Current Team: " << this->friendlyTeam << "\n";

    std::cout << "  " << "Ball possession: ";
    if (this->teamWithBall.has_value()) {
        std::cout << this->teamWithBall.value();
    }
    else {
        std::cout << "No team";
    }
    std::cout << "\n";

    std::cout << "Robot with ball: " << this->idWithBall << "\n";

    for (RobotData const& robot : this->blueRobotData)
    {
        std::cout << "Blue Robot #" << robot.id << "\n";
        std::cout << "  " << "Position (x,y, angle): " << "(" << robot.x << "," << robot.y << "," << robot.angle << ")" << "    ";
        std::cout << "Velocity (Vx, Vy, angle): " << "(" << robot.velocity_x << "," << robot.velocity_y << "," << robot.velocity_angle << ")" << "\n";
    }
    for (RobotData const& robot : this->yellowRobotData)
    {
        std::cout << "Yellow Robot #" << robot.id << "\n";
        std::cout << "  " << "Position (x,y, angle): " << "(" << robot.x << "," << robot.y << "," << robot.angle << ")" << "    ";
        std::cout << "Velocity (Vx, Vy, angle): " << "(" << robot.velocity_x << "," << robot.velocity_y << "," << robot.velocity_angle << ")" << "\n";
    }
    std::cout << "Field Dimensions:\n";
    std::cout << this->fieldDimensions;

    std::cout << std::endl;
}

QDebug operator<<(QDebug os, const WorldState &data)
{
    os << "Capture Time: " << data.capture_time << "\n";
    os << "Ball Data:" << "\n";
    os << "  " << "Position (x,y,z): " << "(" << data.ballData.x << "," << data.ballData.y << "," << data.ballData.z << ")" << "  ";
    os << "  " << "Velocity (Vx, Vy, Vz): " << "(" << data.ballData.velocity_x << "," << data.ballData.velocity_y << "," << data.ballData.velocity_z << ")" << "\n";
    for (RobotData const& robot : data.blueRobotData)
    {
        os << "Blue Robot #" << robot.id << "\n";
        os << "  " << "Position (x,y, angle): " << "(" << robot.x << "," << robot.y << "," << robot.angle << ")" << "    ";
        os << "Velocity (Vx, Vy, angle): " << "(" << robot.velocity_x << "," << robot.velocity_y << "," << robot.velocity_angle << ")" << "\n";
    }
    for (RobotData const& robot : data.yellowRobotData)
    {
        os << "Yellow Robot #" << robot.id << "\n";
        os << "  " << "Position (x,y, angle): " << "(" << robot.x << "," << robot.y << "," << robot.angle << ")" << "    ";
        os << "Velocity (Vx, Vy, angle): " << "(" << robot.velocity_x << "," << robot.velocity_y << "," << robot.velocity_angle << ")" << "\n";
    }

    os << "Robot with ball: " << data.idWithBall << "\n";

    os << "Field Dimensions:\n";
    os << data.fieldDimensions;
    return os;
}


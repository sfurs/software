#ifndef WORLD_STATE_H
#define WORLD_STATE_H

#include <ctime>
// #include "stateData/ballData.h"
// #include "stateData/robotData.h"
#include "FieldData.h"
#include <Team.h>
#include <QVector>
#include <QDebug>
#include <optional>
#include "fieldDimensions.h"
// #include "StrategyDecision.h"

/** \brief  The WorldState struct, holding the crucial data to be manipulated*/
class WorldState
{
public:
    /** \brief The time at which the data was captured */
    double capture_time;
    /** \brief The data of the ball */
    BallData ballData;
    /** \brief The data of the robots */
    QVector<RobotData> blueRobotData;
    /** \brief The data of the robots */
    QVector<RobotData> yellowRobotData;

    // Identification of our Team
    Team friendlyTeam;

    // Flag indicating which team has possession of the ball.
    std::optional<Team> teamWithBall;

    // The id of the robot with possession of the ball
    int idWithBall = -1;

    // Field dimensions
    FieldDimensions fieldDimensions = FieldDimensions();

    /** \brief The default constructor of the WorldState struct */
    WorldState();

    /** \brief The parameterized constructor of the WorldState struct */
    WorldState(BallData ballData, QList<RobotData> blueRobotData, QList<RobotData> yellowRobotData, double capture_time, Team friendlyTeam, bool blueOnPositive);

    /** \brief The fully parameterized constructor of the WorldState struct */
    WorldState(BallData ballData, QList<RobotData> blueRobotData, QList<RobotData> yellowRobotData, double capture_time, Team friendlyTeam, std::optional<Team> teamWithBall, bool blueOnPositive);

    // Loop through all current robots and update 'hasBall'
    std::optional<Team> updatePossession();

    // Check if a single robot has possession
    bool hasPossession(const RobotData &robot) const;

    /** \brief COUT All of the WorldState vars*/
    void printState() const;

    /** \brief COUT All of the WorldState vars as an operator*/
    friend QDebug operator<<(QDebug os, const WorldState &data);

    Team opponentTeam() const;
    QVector<RobotData> opponentRobots() const;
    QVector<RobotData> friendlyRobots() const;

    double getDistanceToBall(const RobotData &robot) const;
    bool ballInYellowNet() const;
    bool ballInBlueNet() const;

    bool isPositiveHomeTeam() const;
    // Loop through all current robots and update 'hasBall'

    /** \brief Whether blue is on the positive side of the field*/
    bool blueOnPositive = false;

    /** \brief To avoid creating an empty QVector<RobotData>() for default-initialized WorldStates */
    static QVector<RobotData> const INVALID_ROBOTS;
};
#endif // WORLD_STATE_H

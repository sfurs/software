#ifndef LIST_WORLD_STATES_H
#define LIST_WORLD_STATES_H

#include "worldState.h"

struct ListWorldStates{
    WorldState *prevState;
    WorldState *nextState;

    ListWorldStates();
    ~ListWorldStates();
};

#endif //LIST_WORLD_STATES_H
#ifndef WORLD_STATE_MANAGER_H
#define WORLD_STATE_MANAGER_H

#include "worldState.h"
#include "GameState.h"
#include <QMutex>
#include <QVector>
#include <QString>
#include <QMap>
#include <QObject>
#include <ctime>
#include <exception>
#include "../shared/Team.h"
/** \brief The WorldStateManager class, holding the crucial data to be manipulated*/
// class WorldStateManager {
class WorldStateManager : public QObject // TODO: might not need to inherit from QObject
{
    Q_OBJECT
private:
    /** \brief The mutex to lock the world state */
    mutable QMutex worldStateMutex;
    // this is mutable as it does not hold any state, only temporarly controls access

    /** \brief The current state of the world */
    WorldState currentState;

    /** \brief The current game state */
    GameState gameState;

    /** \brief The vector of world states*/
    QVector<WorldState> worldStates = QVector<WorldState>(100);

    /** \brief The index of the current world state*/
    int worldStateIndex = 1;

    /** \brief The current state of the world*/
    WorldState state;

    /** \brief The referee data*/
    RefereeData refereeData;

    WorldState getInternalState();
    // update the game state (does not lock)
    void updateRefereeState();

    /** \brief The current team*/
    Team friendlyTeam = Team::INVALID;

    // Reflex* reflex = nullptr;

    /** \brief Whether blue is on the positive side of the field*/
    bool blueOnPositive = false;

    int blueScore = 0;
    int yellowScore = 0;

public:
    WorldStateManager();

    const int worldStateHistory = 100;
    /** \brief The default constructor of the WorldStateManager class */
    // WorldStateManager() : currentState() {};

    /** \brief The method to lock the world state */
    void lock() const;

    /** \brief The method to unlock the world state */
    void unlock() const;

    /** \brief The method to upload the referee data*/
    void uploadRefereeData(RefereeData referee);

    /** \brief The method to get the referee data*/
    RefereeData getRefereeData();

    /** \brief The method to create the world state */
    void createWorldState(BallData ballData, QList<RobotData> blueRobotData, QList<RobotData> yellowRobotData, double capture_time);

    /** \brief The method to get the current state of the world */
    WorldState getCurrentState();

    /** \brief The method to set the current team*/
    void setFriendlyTeam(Team newTeam);

    /** \brief The method to set the game state*/
    void setGameState(GameState gS);

    GameState getGameState();

    /** \brief The method to handle referee commands*/
    // void processRefereeCommand(RefereeCommand rC);

    /**
     * @brief Prints the current world state
     */
    void printWorldState();

    // void setReflex(Reflex* reflex);

    //updates the score
    void updateScore();

signals:
    /**
     * @brief Emits the current world state
     *
     * @param ws The current world state
     */
    void worldStateReflex(WorldState ws);
    /**
     * @brief Emits the current game state
     *
     * @param gs The current game state
     */
    void gameStateReflex(GameState gs);
    
    void emitScores(int blueScores, int yellowScores);
};

#endif // WORLD_STATE_MANAGER_H

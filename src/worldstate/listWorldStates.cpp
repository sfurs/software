#include "listWorldStates.h"
#include "worldState.h"

ListWorldStates::ListWorldStates(){
    prevState = nullptr;
    nextState = nullptr;
}

ListWorldStates::~ListWorldStates(){
    delete prevState;
    delete nextState;
}
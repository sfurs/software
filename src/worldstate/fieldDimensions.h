#ifndef FIELD_DIMENSIONS_H

#define FIELD_DIMENSIONS_H

#include <array>
#include <iostream>

#include "geometry.h"
#include "Logging.h"

/** \brief
    Struct to hold the field dimensions.
    Dimensions measured in millimeters.
    Areas of the field are defined by the inner edges of the lines.
    (0, 0) is defined as the centre of the field.
    +--------------------------+
    |                          |
    |                          |
    |          (0, 0) -> x     |
    |           |              |
    |           v              |
    |           y              |
    +--------------------------+
*/
struct FieldDimensions {
    // Area of the full field
    Rectangle field;

    // Distance from the field edge to the field walls
    float fieldMargin;

    // Area inside the field walls
    Rectangle wall;

    // Playable area of the field
    Rectangle inBounds;

    // Field line width
    float fieldLineWidth;

    // The longer field lines
    std::array<Line, 2> touchLines;

    // The shorter field lines
    std::array<Line, 2> goalLines;

    // Runs along the width of the field and through the center of the field
    Line halfWayLine;

    // Runs along the lenght of the field, pass through the center of two goals and field
    Line goalToGoalLine;

    // At the center of the field
    Circle centerCircle;

    // The rectangular area touching the goal lines centrally in front of both goals
    std::array<Rectangle, 2> defenseAreas;

    // The penalty mark defines the point from which a team executes a penalty kick against the opponent goal.
    std::array<Point, 2> penaltyMarks;

    // The inner area of the goal
    std::array<Rectangle, 2> goals;

    // Rectangle defining the midfield
    Rectangle midfield;

    // Rectangle defining the negative third
    Rectangle negativeThird;

    // Rectangle defining the positive third
    Rectangle positiveThird;

    // Rectangle defining the negative half
    Rectangle negativeHalf;

    // Rectangle defining the positive half
    Rectangle positiveHalf;
    
public:
    FieldDimensions();
    friend std::ostream& operator<<(std::ostream& os, const FieldDimensions& fc);
    friend QDebug operator<<(QDebug os, const FieldDimensions& fc);
};

#endif

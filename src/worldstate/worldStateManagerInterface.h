#ifndef WORLD_STATE_MANAGER_INTERFACE_H
#define WORLD_STATE_MANAGER_INTERFACE_H

#include "worldState.h"
#include "FieldData.h"
#include <QList>
#include <QMutex>

class WorldStateManagerInterface
{
public:
    virtual ~WorldStateManagerInterface() = default;

    virtual void lock() const = 0;
    virtual void unlock() const = 0;

    virtual void uploadRefereeData(RefereeData referee) = 0;
    virtual void createWorldState(BallData ballData, QList<RobotData> blueRobotData, QList<RobotData> yellowRobotData, double capture_time) = 0;
    virtual WorldState getInternalState() = 0;
    virtual WorldState getCurrentState() = 0;
    virtual GameState getGameState() = 0;
    virtual void printWorldState() = 0;
    virtual void setFriendlyTeam(Team newTeam) = 0;
};

#endif // WORLD_STATE_MANAGER_INTERFACE_H
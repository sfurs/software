#ifndef CONTROLPID_H
#define CONTROLPID_H

#include <QtGlobal>


class PID {
    /*
     * A Proportional-Integral-Derivative controller
    */
public:
    /*
     * All functions except the constructor and updateParameters are expected to run in AgentControl
     * updateParameters runs in the network thread so each control system has seperate calibration settings
    */
    PID();
    PID(double kp, double ki, double kd);

    /*
     * Update the pid config based on another pid
    */
    void updateConfig(const PID);
    /*
     * Update the target for the PID controller
     *
     * @param target The target for the PID controller
     *
    */
    void setTarget(double target);
    /*
     * Update the target for the PID controller
     *
     * @param target The target for the PID controller
     * @param time The inital time in ms
     *
    */
    void setTarget(double target, qint64 time);
    /*
     * Get the output of the PID controller
    */
    double update(double input);
    double update(double current, qint64 time);
    double evaluate(double input);
    double evaluate(double error, qint64 time);

    bool reachedTarget(double input);
    bool reachedTargetError(double error);

    void updateParameters(double kp, double ki, double kd, double deadband, qint64 tolerance);
    double getTarget();
    qint64 first_stable=0;
    qint64 reachedTolerance = 0;
private:
    double target;
    double kp;
    double ki;
    double kd;
    double deadband;
    double previous_output = 0;
    double total_error;
    double previous_error;
    qint64 previous_call;
};

struct ControlPID {
    /*
     * The PID controllers for a robot
    */
    PID x;
    PID y;
    PID rotate;
    void updateConfig(const ControlPID ref);
};

#endif

## Overview: Shared Module Overview
The shared module contains some template class and struct definitions shared by multiple modules. 

## Files
### 1. `TeamInfo.h`
 - **`TeamInfo` struct**
 Contains information about a certain team.
    - **Fields**
        - `int score`
        - `int red_cards`
        - `int current_yellow_cards`
        - `int max_robots`
        - `int remaining_timeouts`
        - `int remaining_timeout_time`
        - `int goalkeeper`

### 2. `FieldData.h`
- **FieldData Class**
  - **Functions**
    - `FieldData()`: Default constructor, which initialized an empty FieldData object
    - `FieldData(QVector<RobotData> blue, QVector<RobotData> yellow, BallData ball, double capture_time)`: Parameterized constructor that initializes the FieldData object with robot data for both teams
    - `const QVector<RobotData>& getRobotsRef(Team t)`: Returns a constant referece to the vector of robots for a specified team.
    - `QVector<RobotData> getRobots(Team t)`: Returns a copy of the vector of robots for the specified team
    - `const BallData getBall()`: Returns the current ball data, including its position and velocity.
    - `const double getCaptureTime()`: Returns the capture timestamp representing when the field data was captured.

### 3. `RefereeData.h`
 - **`RefereeData` struct**
 Contains information about a command sent from the referee
    - **Fields**
        - `RefereeCommand command`
        - `long long command_time`
        - `float command_x`
        - `float command_y`
        - `GameStage stage`
        - `TeamInfo yellow_team`
        - `TeamInfo red_team`

- **GameStage Enum**: 
    - Represents the different stages of a game:
        - `First_Half`: First half of the game.
        - `Half_Time`: Halftime break.
        - `Second_Half`: Second half of the game.
        - `OT_Break`: Break between regular time and overtime.
        - `OT_First_Half`: First half of overtime.
        - `OT_Half_Time`: Halftime in overtime.
        - `OT_Second_Half`: Second half of overtime.
        - `Pre_Shoot_Out_Break`: Break before a shoot-out.
        - `Shoot_Out`: Shoot-out phase.
        - `Timeout`: Game is paused for a timeout.

- **RefereeCommand Enum**: 
    - Represents commands from the referee:
        - `NO_COMMAND`: No previous command has been sent
        - `HALT`: The game is halted, likely due to a major event.
        - `STOP`: The game is stopped.
        - `STOP_GOAL_YELLOW`: The game is stopped.
        - `STOP_GOAL_BLUE`: The game is stopped.
        - `NORMAL_START`: Resume normal play.
        - `FORCE_START`: Forces the start of the game.
        - `PREPARE_KICKOFF_YELLOW`: Robots get into position for a kickoff
        - `PREPARE_KICKOFF_BLUE`: Robots get into position for a kickoff
        - `PREPARE_PENALTY_YELLOW`: Robots get into position for a kickoff
        - `PREPARE_PENALTY_BLUE`: Robots get into position for a kicko
        - `DIRECT_FREE_YELLOW`: The yellow team my take a direct free kick 
        - `DIRECT_FREE_BLUE`: The blue team my take a direct free kick 
        - `TIMEOUT_YELLOW`: The yellow team is currently in a timeout
        - `TIMEOUT_BLUE`: The blue team is currently in a timeout
        - `BALL_PLACEMENT_YELLOW`:Equivalent to STOP but the yellow team must place the ball
        - `BALL_PLACEMENT_BLUE`:Equivalent to STOP but the blue team must place the ball

### 4. `Team.h`
- **Team enumeration**
  - **Values**
    - `INVALID = 0`: Represents an invalid or unspecified team. Serves as a default value when there is no team
    - `BLUE = 1`: Represents the blue team
    - `YELLOW = 2`: Represents the yellow team.

### 5. `ControlPID.h`

### 6. `RobotInstruction.h`

- **RobotInstruction Struct**:  
  The `RobotInstruction` struct represents a set of movements the robot can perform.

  - **Members**:
  
    - `int id`:  
      An identification for the set of movements.

    - `float v_x`:  
      The velocity in the x-direction (forwards is positive, backwards is negative).

    - `float v_y`:  
      The velocity in the y-direction (left is positive, right is negative).

    - `float v_phi`:  
      The angular velocity (counterclockwise is positive, clockwise is negative).

    - `bool kick`:  
      A boolean value that indicates whether the robot kicks (`true` for kick, `false` for no kick).

    - `bool dribble`:  
      A boolean value that indicates whether the robot dribbles (`true` for dribble, `false` for no dribble).

- **QDebug `<<` Overload**:  
  Provides movement and angular movement information for debugging purposes. It prints the robot's control instructions in a readable format.
  This overload allows the `RobotInstruction` struct to be output using `QDebug`, showing the robot’s ID, velocity in both the x and y directions, and its angular velocity.


#include "FieldData.h"

FieldData::FieldData(){}

FieldData::FieldData(QVector<RobotData> blue, QVector<RobotData> yellow, BallData ball, double capture_time) 
    : blue_robots(blue), yellow_robots(yellow), ball(ball), capture_time(capture_time) {}

const QVector<RobotData>& FieldData::getRobotsRef(Team t) {
    if (t == BLUE)
        return blue_robots;

    if (t == YELLOW)
        return yellow_robots;

    throw std::invalid_argument("Team not specified");
}

QVector<RobotData> FieldData::getRobots(Team t) {
    if (t == BLUE)
        return blue_robots;

    if (t == YELLOW)
        return yellow_robots;

    throw std::invalid_argument("Team not specified");
}

const BallData FieldData::getBall() {
    return ball;
}

const double FieldData::getCaptureTime() {
    return capture_time;
}
#ifndef TEAM_H
#define TEAM_H

enum Team {
    BLUE = 0,
    YELLOW = 1,
    INVALID = 2,
};

#endif // TEAM_H

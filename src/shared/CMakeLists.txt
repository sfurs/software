find_package(Qt6 REQUIRED COMPONENTS Core)

add_library(skynet_shared STATIC)

# include this folder to the include dirs for all
target_include_directories(skynet PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(skynet_shared PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(skynet_shared
    PUBLIC
        Qt6::Core
)

target_sources(skynet_shared
    PUBLIC
        RefereeData.h
        TeamInfo.h
        Team.h
        Logging.h
        FieldData.h
        ControlPID.h #TODO redesign ControlPID
    PRIVATE
        LogCategories.cpp
        FieldData.cpp
)

target_link_libraries(skynet
    PUBLIC
        skynet_shared
)

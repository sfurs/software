#ifndef SHARED_LOGMANIP_H
#define SHARED_LOGMANIP_H

#include <QTextStream>

namespace LogIO {
    QTextStream &fixed(QTextStream &stream);
}

#endif

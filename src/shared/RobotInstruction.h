#ifndef ROBOT_INSTRUCTION_H
#define ROBOT_INSTRUCTION_H

#include <QDebug>
//TODO replace this include with `Logging.h` once debug logs are merged

struct RobotInstruction {
    int id;
    /*
     * Forwards is positive and Backwards is negative
    */
    float v_x=0;
    /*
     * Left is positive and Right is negative
    */
    float v_y=0;
    /*
     * Counterclockwise is positive Clockwise is negative
    */
    float v_phi=0;
    /*
     * Kick power ranges from 0 to 15
    */
    int kick=0;
    /*
     * Dribble power ranges from 0 to 3
    */
    int dribble=0;
    /*
     * Chipper power ranges from 0 to 3
    */
    int chipper=0;
};

// TODO move this so it does not break the ODR
inline QDebug operator<<(QDebug dbg, const RobotInstruction in) {
    dbg << "Control, id:" << in.id << "\n" 
        << "x: "<< in.v_x << ", y: " << in.v_y << "\n"
        << "angle: " << in.v_phi << "\n"
        << "kick: " << in.kick << "\n"
        << "dribble: " << in.dribble << "\n"
        << "chipper: " << in.chipper;
    return dbg;
}

#endif

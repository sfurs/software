#include <QLoggingCategory>

// All Q_LOGGING_CATEGORY macros are defined here to prevent any conflicts



Q_LOGGING_CATEGORY(LOG_AGENT, "agent")
Q_LOGGING_CATEGORY(LOG_NETWORK, "network")
Q_LOGGING_CATEGORY(LOG_AGENT_CONTROL, "agent.control")
Q_LOGGING_CATEGORY(LOG_GUI, "gui")

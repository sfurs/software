#ifndef FIELDDATA_H

#define FIELDDATA_H

#include "Team.h"
#include <QVector>

struct RobotData {
    // robot id
    unsigned int id;

    // contain current coordinates and angle of robot
    float x;
    float y;
    float angle;

    // current computed velocity of robot
    float velocity_x;
    float velocity_y;
    float velocity_angle;
};

struct BallData {
    // current position of ball in 3d space
    float x;
    float y;
    float z;

    // computes velocity of ball
    float velocity_x;
    float velocity_y;
    float velocity_z;
};

class ObjectTracker;

class FieldData {
    public:
        FieldData();
        FieldData(QVector<RobotData>, QVector<RobotData>, BallData, double);
        const QVector<RobotData>& getRobotsRef(Team);
        QVector<RobotData> getRobots(Team);
        const BallData getBall();
        const double getCaptureTime();
    friend ObjectTracker;
    private:
        QVector<RobotData> yellow_robots;
        QVector<RobotData> blue_robots;
        BallData ball;
        double capture_time;
};

#endif

#ifndef STRATEGY_DECISION_H
#define STRATEGY_DECISION_H

enum StrategyDecision {
    UNKNOWN = 0,
    ATK = 1,
    DEF = 2,
    KICKOFF = 3,
    LOOSE = 4,
    PENALTY = 5
};

#endif
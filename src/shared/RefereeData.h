#ifndef REFEREEDATA_H
#define REFEREEDATA_H
#include "TeamInfo.h"

enum class GameStage {
        // No game stage has been recieved yet.
        NO_STAGE = -1,
        // The first half is about to start.
        // A kickoff is called within this stage.
        // This stage ends with the NORMAL_START.
        NORMAL_FIRST_HALF_PRE = 0,
        // The first half of the normal game, before half time.
        NORMAL_FIRST_HALF = 1,
        // Half time between first and second halves.
        NORMAL_HALF_TIME = 2,
        // The second half is about to start.
        // A kickoff is called within this stage.
        // This stage ends with the NORMAL_START.
        NORMAL_SECOND_HALF_PRE = 3,
        // The second half of the normal game, after half time.
        NORMAL_SECOND_HALF = 4,
        // The break before extra time.
        EXTRA_TIME_BREAK = 5,
        // The first half of extra time is about to start.
        // A kickoff is called within this stage.
        // This stage ends with the NORMAL_START.
        EXTRA_FIRST_HALF_PRE = 6,
        // The first half of extra time.
        EXTRA_FIRST_HALF = 7,
        // Half time between first and second extra halves.
        EXTRA_HALF_TIME = 8,
        // The second half of extra time is about to start.
        // A kickoff is called within this stage.
        // This stage ends with the NORMAL_START.
        EXTRA_SECOND_HALF_PRE = 9,
        // The second half of extra time.
        EXTRA_SECOND_HALF = 10,
        // The break before penalty shootout.
        PENALTY_SHOOTOUT_BREAK = 11,
        // The penalty shootout.
        PENALTY_SHOOTOUT = 12,
        // The game is over.
        POST_GAME = 13

};

enum class RefereeCommand {
        // No referee command has been recieved yet.
        NO_COMMAND = -1,
        // All robots should completely stop moving.
        HALT = 0,
        // Robots must keep 50 cm from the ball.
        STOP = 1,
        // The old GOAL_YELLOW and GOAL_BLUE commands are equivalent to STOP
        STOP_GOAL_YELLOW = 14,
        STOP_GOAL_BLUE = 15,
        // A prepared kickoff or penalty may now be taken.
        NORMAL_START = 2,
        // The ball is dropped and free for either team.
        FORCE_START = 3,
        // The yellow team may move into kickoff position.
        PREPARE_KICKOFF_YELLOW = 4,
        // The blue team may move into kickoff position.
        PREPARE_KICKOFF_BLUE = 5,
        // The yellow team may move into penalty position.
        PREPARE_PENALTY_YELLOW = 6,
        // The blue team may move into penalty position.
        PREPARE_PENALTY_BLUE = 7,
        // The yellow team may take a direct free kick.
        DIRECT_FREE_YELLOW = 8,
        // The blue team may take a direct free kick.
        DIRECT_FREE_BLUE = 9,
        // The yellow team is currently in a timeout.
        TIMEOUT_YELLOW = 12,
        // The blue team is currently in a timeout.
        TIMEOUT_BLUE = 13,
        // Equivalent to STOP, but the yellow team must pick up the ball and
        // drop it in the Designated Position.
        BALL_PLACEMENT_YELLOW = 16,
        // Equivalent to STOP, but the blue team must pick up the ball and drop
        // it in the Designated Position.
        BALL_PLACEMENT_BLUE = 17,
};

struct RefereeData
{
    // Last referee command
    RefereeCommand command = RefereeCommand::NO_COMMAND;
    // Time last referee command was sent in microseconds
    long long command_time;
    // next command the referee plans to send
    RefereeCommand next_command = RefereeCommand::NO_COMMAND;
    // Position for a ball placement command
    float command_x;
    float command_y;
    // Current stage of the game
    GameStage stage = GameStage::NO_STAGE;
    // Team info from the referee
    TeamInfo yellow_team;
    TeamInfo blue_team;
    // Whether blue team is on positive x
    bool is_blue_on_positive = true;

    // In case two auto-refs are used and one does not provide is_blue_on_positive
    bool has_is_blue_on_positive = false;
};


#endif // REFEREEDATA_H

#ifndef TEAMINFO_H
#define TEAMINFO_H

struct TeamInfo 
{
    int score;
    int red_cards;
    int current_yellow_cards;
    int max_robots;
    // excludes current timeout if active
    int remaining_timeouts;
    // amount of timeout this team can use in microseconds
    int remaining_timeout_time;
    // The id of the robot that is the goalkeeper
    int goalkeeper;

};

#endif // TEAMINFO_H

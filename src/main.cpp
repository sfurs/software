// #include "gui/application.h"

#include "gui/mainwindow.h"
#include "network/netmaster.h"
#include "agent/AgentMaster.h"
#include "agent/controls/AgentControl.h"
#include "reflex/reflex.h"
#include "python/AgentPython.h"
#include "agent/controls/Navigator.h"
#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QThread>
#include <QMetaObject>
#include "Logging.h"
#include <signal.h>

// Meta Types
#include "shared/RobotInstruction.h"
#include "shared/Team.h"
#include "agent/fsm/PlaySelection.h"

// Handle Qt slots macro
#pragma push_macro("slots")
#undef slots
#include <pybind11/embed.h>
#pragma pop_macro("slots")

namespace py = pybind11;

int main(int argc, char *argv[])
{
    py::scoped_interpreter guard{}; // Initialize Python interpreter
    QApplication app(argc, argv);
    app.setApplicationName("Skynet");
    app.setApplicationVersion("0.1.0");

    // CLI options
    QCommandLineParser parser;
    parser.setApplicationDescription("Skynet description (placeholder)");
    parser.addHelpOption();
    parser.addVersionOption();
    QCommandLineOption testOption("test", "Run in test mode");
    parser.addOption(testOption);
    parser.process(app);

    // Check for test flag
    bool isTestMode = parser.isSet(testOption);

    // Register RobotInstruction so it can be used in QMetaObject::InvokeMethod in Qt<6.5
    qRegisterMetaType<RobotInstruction>("RobotInstruction");
    qRegisterMetaType<Team>("Team");
    qRegisterMetaType<PlaySelection>("PlaySelection");

    signal(SIGINT, [](int)
           { QApplication::quit(); });
    signal(SIGTERM, [](int)
           { QApplication::quit(); });

    WorldStateManager *worldStateManager = new WorldStateManager();
    NetMaster *nm = new NetMaster(worldStateManager);


    AgentPython *agentPython = new AgentPython(&guard, worldStateManager);
//     Do NOT move AgentPython to another thread - it must be in the main thread or else GIL will be lost and Python will crash


    QThread nmThread;
    nm->moveToThread(&nmThread);
    QObject::connect(&app, &QApplication::aboutToQuit, &nmThread, &QThread::quit);
    QObject::connect(&nmThread, &QThread::finished, nm, &QObject::deleteLater);
    nmThread.start();

    QThread worldStateManagerThread;
    worldStateManager->moveToThread(&worldStateManagerThread);
    QObject::connect(&app, &QApplication::aboutToQuit, &worldStateManagerThread, &QThread::quit);
    worldStateManager->setFriendlyTeam(nm->getFriendlyTeam());
    worldStateManagerThread.start();

    AgentControl *control = new AgentControl(nullptr, nm, worldStateManager);
    QThread controlThread;
    control->moveToThread(&controlThread);
    QObject::connect(&app, &QApplication::aboutToQuit, &controlThread, &QThread::quit);
    QObject::connect(&controlThread, &QThread::finished, control, &QObject::deleteLater);
    controlThread.start();
    QMetaObject::invokeMethod(control, &AgentControl::start, Qt::QueuedConnection);

    Navigator *navigator = new Navigator(nullptr,worldStateManager, control);
    QThread navigatorThread;
    navigator->moveToThread(&navigatorThread);
    QObject::connect(&app, &QApplication::aboutToQuit, &navigatorThread, &QThread::quit);
    QObject::connect(&navigatorThread, &QThread::finished, navigator, &QObject::deleteLater);
    navigatorThread.start();
    QMetaObject::invokeMethod(navigator, &Navigator::start, Qt::QueuedConnection);

    AgentMaster *agent = new AgentMaster(navigator, agentPython, worldStateManager, control, isTestMode);
    QThread agentThread;
    agent->moveToThread(&agentThread);
    QObject::connect(&app, &QApplication::aboutToQuit, &agentThread, &QThread::quit);
    QObject::connect(&agentThread, &QThread::finished, agent, &QObject::deleteLater);
    agentThread.start();
    QMetaObject::invokeMethod(agent, &AgentMaster::start, Qt::QueuedConnection);

    Reflex *reflex = new Reflex(control, navigator);
    QThread reflexThread;
    reflex->moveToThread(&reflexThread);
    QObject::connect(&app, &QApplication::aboutToQuit, &reflexThread, &QThread::quit);
    QObject::connect(&reflexThread, &QThread::finished, reflex, &QObject::deleteLater);
    reflex->setWorldStateManager(worldStateManager);
    reflexThread.start();

    MainWindow w = MainWindow(worldStateManager, nm, agent, control, navigator);
    w.show();

    int ret = app.exec();

    nmThread.wait();
    worldStateManagerThread.wait();
    controlThread.wait();
    navigatorThread.wait();
    agentThread.wait();
    reflexThread.wait();

    return ret;
}

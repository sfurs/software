#ifndef NETMASTER_H
#define NETMASTER_H

#include <QObject>
#include <QTimer>
#include <unistd.h>
#include <QString>
#include <QHostAddress>

#include "../worldstate/worldStateManager.h"
#include "RawFieldData.h"
#include "RobotInstruction.h"
#include "Team.h"

struct ControlPID;
class UdpConnection;
class UdpConnection;
class TcpConnection;
class SerialConnection;
class VisionProtobuf;
class ControlWrapper;
class TeamProtobuf;
class RefereeProtobuf;
class InputSanitizer;
class ObjectTracker;

class NetMaster : public QObject
{
    Q_OBJECT
public:
    NetMaster(WorldStateManager *wsm);
    NetMaster(int t);
    ~NetMaster();
    ControlPID *getPID();

public slots:
    void updateFieldPositions(RawFieldData raw_data);
    void updateField();
    void updateRefereeData(RefereeData data);
    void stop();
    void SetRobotInstruction(RobotInstruction instruction);
    void setRefereeIP(const QString &ip);
    void setRefereePort(quint16 port);
    void setVisionIP(const QString &ip);
    void setVisionPort(quint16 port);
    void setControlIP(const QString &ip);
    void setControlBluePort(quint16 port);
    void setControlYellowPort(quint16 port);
    void setRobotControlPort(const QString &port);
    void setRobotControlBaudRate(int baudRate);
    void setFriendlyTeam(Team team);
    QString getRefereeIP();
    quint16 getRefereePort();
    QString getVisionIP();
    quint16 getVisionPort();
    QString getControlIP();
    quint16 getControlBluePort();
    quint16 getControlYellowPort();
    QString getRobotControlPort();
    int getRobotControlBaudRate();
    Team getFriendlyTeam();
    bool getIsSerial();
    void setIsSerial(bool isSerial);
    bool getSerialEnabled();

private:
    void initNetworks();

    QTimer timer;
    UdpConnection *network;
    UdpConnection *refereeNetwork;
    TcpConnection *gameControllerNetwork;
    SerialConnection *robotControlNetwork;
    VisionProtobuf *vision;
    ControlWrapper *control;
    TeamProtobuf *gameController;
    RefereeProtobuf *referee;
    int teamSize;
    WorldStateManager *worldStateManager;
    bool isSerial = false;
    bool serialEnabled = false;

    ObjectTracker *objectTracker;
    InputSanitizer *fieldVisionFilter;
    QString m_refereeIP = "224.5.23.1";
    quint16 m_refereePort = 10003;
    QString m_visionIP = "224.5.23.2";
    quint16 m_visionPort = 10020;
    QString m_controlIP = "127.0.0.1";
    quint16 m_controlBluePort = 10301;
    quint16 m_controlYellowPort = 10302;
    QString m_robotControlPort = "ttyUSB0";
    int m_robotControlBaudRate = 250000;

    Team friendlyTeam = Team::BLUE;
};

#endif // NETMASTER_H

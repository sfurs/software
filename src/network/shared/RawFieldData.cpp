#include "RawFieldData.h"
#include <stdexcept>
#include "Logging.h"

RawFieldData::RawFieldData() {
}

RawFieldData::RawFieldData(QVector<RawRobot> blue, QVector<RawRobot> yellow, QVector<RawBall> balls, 
                uint32_t camera_id, double t_sent, double t_capture, uint32_t frame_number) 
                : blue_robots(blue), yellow_robots(yellow), balls(balls), time(t_capture), camera_id(camera_id) {}

QVector<RawRobot> RawFieldData::getRobots(Team team) {
    if (team == BLUE) {
        return blue_robots;
    }
    if (team == YELLOW) {
        return yellow_robots;
    }
    throw std::invalid_argument("Team not specified");
}

QVector<RawRobot>& RawFieldData::getRobotsRef(Team t) {
    if (t == BLUE)
        return blue_robots;

    if (t == YELLOW)
        return yellow_robots;

    throw std::invalid_argument("Team not specified");
}

QVector<RawBall> RawFieldData::getBalls() {
    return balls;
}

double RawFieldData::getCaptureTime() {
    return time;
}

unsigned int RawFieldData::getCameraID() {
    return camera_id;
}

QDebug operator<<(QDebug os, const RawFieldData& data) {
    os << "Time: " << Qt::fixed << qSetRealNumberPrecision(2) << data.time << "\n";
    for (int i = 0; i < data.blue_robots.size(); ++i) {
        os << "Blue robot " <<
            "id: " << data.blue_robots[i].id <<
            " x: " << Qt::fixed << qSetRealNumberPrecision(2) << data.blue_robots[i].x <<
            " y: " << Qt::fixed << qSetRealNumberPrecision(2) << data.blue_robots[i].y <<
            " angle: " << Qt::fixed << qSetRealNumberPrecision(2) << data.blue_robots[i].angle << "\n";
    }
    for (int i = 0; i < data.yellow_robots.size(); ++i) {
        os << "Yellow robot " <<
            "id: " << data.yellow_robots[i].id <<
            " x: " << Qt::fixed << qSetRealNumberPrecision(2) << data.yellow_robots[i].x <<
            " y: " << Qt::fixed << qSetRealNumberPrecision(2) << data.yellow_robots[i].y <<
            " angle: " << Qt::fixed << qSetRealNumberPrecision(2) << data.yellow_robots[i].angle << "\n";
    }
    for (int i = 0; i < data.balls.size(); ++i) {
        os << "Ball " <<
            "x: " << Qt::fixed << qSetRealNumberPrecision(2) << data.balls[i].x <<
            " y: " << Qt::fixed << qSetRealNumberPrecision(2) << data.balls[i].y <<
            " z: " << Qt::fixed << qSetRealNumberPrecision(2) << data.balls[i].z << "\n";
    }
    return os;
}

## Overview: Network Shared Overview
This folder contains the RawFieldData which is the output of the vision protobuf and is consumed by the input sanitizer and object tracker.

## Files

### 1. `RawFieldData.cpp` & `RawFieldData.h`
- **RawFieldData class**
  - **Functions**
    - `RawFieldData(QVector<RawRobot> blue,QVector<RawRobot> yellow, QVector<RawBall> balls, uint32_t camera_id, double t_sent, double t_capture, uint32_t frame_number)`:
    Initilizes the class with vectors of blue and yello robots, balls, and time-related data.
    - `QVector<RawRobot> getRobots(Team team)`:
    Returns a vector of robots for the specified team
    - `QVector<RawRobot>& getRobotsRef(Team t)`:
    Returns a reference to the vector of robots for the specified team
    - `QVector<RawBall> getBalls()`: Returns the vector of balls detected on the field
    - `double getCaptureTime()`: Returns the capture time of the field data

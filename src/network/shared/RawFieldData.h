#ifndef RAWFIELDDATA_H
#define RAWFIELDDATA_H
#include <QVector>
#include "Logging.h"
#include "Team.h"

// RawRobot and RawBall are included here as they will only be used in conjunction with RawFieldData
struct FieldInfo {
    unsigned int camera_id;
    double capture_time;
};

struct RawRobot {
    /** \brief The id of the robot */
    unsigned int id;
    /** \brief The x position in mm of the robot */
    float x;
    /** \brief The y position in mm of the robot */
    float y;
    /** \brief The angle in radians of the robot */
    float angle;
};

struct RawBall {
    /** \brief The x position in mm of the ball */
    float x;
    /** \brief The y position in mm of the ball */
    float y;
    /** \brief The z position in mm of the ball */
    float z;
};

class RawFieldData {
    public:
        RawFieldData();
        // RawFieldData(QVector<RawRobot> blue, QVector<RawRobot> yellow, QVector<RawBall> balls);
        RawFieldData(QVector<RawRobot> blue, QVector<RawRobot> yellow, QVector<RawBall> balls, 
                        uint32_t camera_id, double t_sent, double t_capture, uint32_t frame_number);
        /*
         * Get the robots of a specific team
         *
         * @param team The team to get robots from
        */
        QVector<RawRobot> getRobots(Team team);
        // const QVector<RawRobot>& getRobotsRef(Team);
        QVector<RawRobot>& getRobotsRef(Team);
        /*
         * Get all balls on the field
        */
        QVector<RawBall> getBalls();
        /*
         * Get the time when the SSL-Vision captured the field data
        */
        double getCaptureTime();
        /*
         * Get the camera id of the camera that captured the field data
        */
        unsigned int getCameraID();
        /*
         * Enable this class to be printed in a log message
        */
        friend QDebug operator<<(QDebug os, const RawFieldData& data);
    private:
        unsigned int camera_id;
        double time;
        QVector<RawRobot> blue_robots;
        QVector<RawRobot> yellow_robots;
        QVector<RawBall> balls;
};

#endif // RAWFIELDDATA_H


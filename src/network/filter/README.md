## Overview: Filter
The `skynet_filter` library contains the implmentation of the vision filtering and the Kalman Filter.

## Files

### 1. `KalmanFilter.h` & `KalmanFilter.cpp`

- **KalmanFilter Class**
    A kalman filter implementation
    - **Functions**:
      - `KalmanFilter(int observation_dimensions, int mean_dimensions)`
      - `update(const double* observation)`:
      Update the kalman filter with a new observation
      - `predict()`:
      Predict the mean and state covariance
      - `predict(const double* control_input)`:
      Predict the mean and state covariance with a control input
      - `getPrediction()`:
      - `getObservationDimension()`:
      - `getMeanDimension()`

- **KalmanFilterMatrix Class**
    This class gives access to the data in a matrix used by the kalman filter
    - **Functions**:
      - `get(int i)`:
      Returns the value at index i
      - `get(int i, int j)`:
      Returns the value at index (i,j) assuming the matrix is 2D
      - `set(int i)`:
      Returns a modifiable reference of the value at index i
      - `set(int i, int j)`:
      Returns a modifiable reference of the value at index (i,j) assuming the matrix is 2D
      - `data()`:
      Returns a pointer to the data

### 2. `InputSanitizer.cpp` & `InputSanitizer.h`
- **InputSanitizer class**  
    - **Functions**:
      - `InputSanitizer(ObjectTracker* tracker) : tracker(tracker)`:
      Constructor to initialize an InputSanitizer with a reference to an ObjectTracker class 
      - `filterField(RawFieldData& field)`:
      Filters the field data at current capture to sanitize and then update the positions of robots and balls.

### 3. `ObjectTracker.cpp` & `ObjectTracker.h`
- **ObjectTracker class**
    - **Functions**:
    - `ObjectTracker()`:
    Constructor that initializes an ObjectTracker instance
    - `updateRobot(Team team, rawobot data, double capture)`:
    Updates the state of a robot for specified teams with new data and capture timestamp
      - `Initial Robot State`: If the robot is seen for the first time, it's initialized with a new ID
      - `Old capture handling`: ignore the capture timestamp if it is older than the previous timestamp.
      - `Velocity Calculation`: the robot's velocity is computed based on the difference in position and angle over time
      - `State update`: Updates the robot's current position and stores the capture timestamp.
    - `updateBall(RawBall data, double capture)`: Updates the state of the ball using the new data and capture timestamp.

### 3. `BallTracker.cpp` & `BallTracker.h`
- **BallTracker class**
  - **Functions**
    - `BallTracker()`: Create a new BallTracker
    - `update(RawBall data, FieldInfo field)`: Update the ball tracker with a new RawBall and FieldInfo
    - `predict(double time)`: Predict the current ball status at the time specified by time
    - `getPredict()`: Get the prediction as a BallData
    - `reset()`: Reset the ball tracker
    - `getLastUpdate()`: Returns the time of the last update

### 4. `RobotTracker.cpp` & `RobotTracker.h`
- **RobotTracker class**
  - **Functions**
    - `RobotTracker(int id)`: Create a new RobotTracker for a robot with id id
    - `update(RawRobot data, FieldInfo field)`: Update the robot tracker with a new RawRobot and FieldInfo
    - `predict(double time)`: Predict the current robot status at the time specified by time
    - `getPredict()`: Get the prediction as a RobotData
    - `reset()`: Reset the robot tracker
    - `getLastUpdate()`: Returns the time of the last update

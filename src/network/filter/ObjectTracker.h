#ifndef OBJECTTRACKER_H
#define OBJECTTRACKER_H

#include <QVector>
#include <QMap>
#include "RawFieldData.h"
#include "FieldData.h"
#include "RobotTracker.h"
#include "BallTracker.h"


class ObjectTracker {
    /*
     * Tracks balls and robots, providing a smoother prediction of where objects are
     * along side predictions of the current velocity
    */

    public:
        ObjectTracker();
        ObjectTracker(RawFieldData);
        ~ObjectTracker();

        /*
         * Update the predictions based on new field data
         *
         * @param field The new field data
        */
        FieldData filterField(RawFieldData field);

        /*
         * Update the predictions of a specific robot based on field data
        */
        void updateRobot(Team team, RawRobot data, FieldInfo field);

        /*
         * Update the predictions of the ball based on field data
        */
        void updateBall(RawBall data, FieldInfo field);

        /*
         * Update the internal predictions of the robots
        */
        void updatePrediction();

        /*
         * Return the predicted field data
        */
        FieldData getFieldData();
        //TODO also need to add robot data reset/removal functions
    private:
        QMap<int, RobotTracker*> blueTeam;
        QMap<int, RobotTracker*> yellowTeam;
        BallTracker ball;
        // TODO add config
        bool resetBall = true;
        double previousUpdate = 0;


};

#endif

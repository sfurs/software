#include "RobotTracker.h"
#include <QtMath>

RobotTracker::RobotTracker() : RobotTracker(-1) {}

RobotTracker::RobotTracker(unsigned int id) : id(id), filter(3,6),
    mean(filter.state_matrix()), transition(filter.transition_matrix()),
    observation_covar(filter.observe_covar_matrix())
{
    KalmanFilterMatrix matrix = filter.observe_matrix();
    matrix.set(0,0) = 1;
    matrix.set(1,1) = 1;
    matrix.set(2,2) = 1;

    KalmanFilterMatrix process = filter.process_covar_matrix();
    process.set(0,0) = 0.5;
    process.set(1,1) = 0.5;
    process.set(2,2) = 0.5;
    process.set(3,3) = 0.5;
    process.set(4,4) = 0.5;
    process.set(5,5) = 0.5;

}

void RobotTracker::update(RawRobot data, FieldInfo field) 
{
    double update_data[] = {data.x, data.y, boundAngle(data.angle)};
    if (qIsNaN(previous_update)) {
        mean.set(0) = data.x;
        mean.set(1) = data.y;
        mean.set(2) = boundAngle(data.angle);
        previous_predict = qQNaN();
        transition.set(0,3) = 0;
        transition.set(1,4) = 0;
        transition.set(2,5) = 0;
    } else {
        double new_angle = mean.get(2) + boundAngle(data.angle - mean.get(2));
        update_data[2] = new_angle;
    }
    if (field.camera_id != prev_cam_id) {
        // more noise if camera changed
        prev_cam_id = field.camera_id;
        observation_covar.set(0,0) = 4;
        observation_covar.set(1,1) = 4;
        observation_covar.set(2,2) = 4;
    } else {
        observation_covar.set(0,0) = 1;
        observation_covar.set(1,1) = 1;
        observation_covar.set(2,2) = 1;
    }
    previous_update = field.capture_time;
    filter.update(update_data);
}

void RobotTracker::predict(double time) {
    if(!qIsNaN(previous_predict)) {
        double time_delta = time - previous_predict;
        transition.set(0,3) = time_delta;
        transition.set(1,4) = time_delta;
        transition.set(2,5) = time_delta;
    }
    filter.predict();
    mean.set(2) = boundAngle(mean.get(2));
    previous_predict = time;
}

RobotData RobotTracker::getPredict() {
    double* val = mean.data();
    return {id, (float)val[0], (float)val[1], (float)val[2],
             (float)val[3], (float)val[4], (float)val[5]};
}

void RobotTracker::reset() {
    previous_update = qQNaN();
}

double RobotTracker::getLastUpdate() {
    return previous_update;
}

double RobotTracker::boundAngle(double angle) {
    while (angle > M_PI) {
        angle -= 2 * M_PI;
    }
    while (angle < -M_PI) {
        angle += 2 * M_PI;
    }
    return angle;
}

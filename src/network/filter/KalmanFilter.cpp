#include "KalmanFilter.h"
#include <Eigen/Dense>

typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> MatrixXd;
typedef Eigen::Map<const MatrixXd,Eigen::RowMajor> MapXd;

// The definition is here as Eigen is slow to compile
struct KalmanFilterInternal
{
    MatrixXd transition_model;
    MatrixXd jacobian_model;
    MatrixXd observation_model;
    MatrixXd process_covariance;
    MatrixXd observation_covariance;
    //optional
    bool has_control_model;
    MatrixXd control_model;

    //internal
    MatrixXd estimate_mean;
    MatrixXd estimate_covariance;
    
    KalmanFilterInternal(int observ_dim, int state_dim) {
        transition_model = MatrixXd::Identity(state_dim, state_dim);
        observation_model = MatrixXd::Zero(observ_dim, state_dim);
        process_covariance = MatrixXd::Zero(state_dim, state_dim);
        observation_covariance = MatrixXd::Zero(observ_dim, observ_dim);

        estimate_mean = MatrixXd::Zero(state_dim, 1);
        estimate_covariance = MatrixXd::Identity(state_dim, state_dim);
        
    }
};

typedef struct KalmanFilterMatrix::KFPtr {
    MatrixXd* ptr;
} KFPtr;

KalmanFilterMatrix::KalmanFilterMatrix(int rows, int cols, KFPtr* data): rows(rows), cols(cols), internal_data(data)
{} 

KalmanFilterMatrix::KalmanFilterMatrix(const KalmanFilterMatrix& KFM) : rows(KFM.rows), cols()
{
    internal_data = new KalmanFilterMatrix::KFPtr({KFM.internal_data->ptr});
}

KalmanFilterMatrix::~KalmanFilterMatrix() 
{
    delete internal_data;
}

double KalmanFilterMatrix::get(int i) const
{
    return internal_data->ptr->coeff(i);
}

double KalmanFilterMatrix::get(int row, int col) const
{
    if (row >= rows || col >= cols) {
        return 0; //TODO throw instead
    }
    return internal_data->ptr->coeff(row, col);
}

double& KalmanFilterMatrix::set(int i)
{
    return internal_data->ptr->coeffRef(i);
}

double& KalmanFilterMatrix::set(int row, int col)
{
    return internal_data->ptr->coeffRef(row, col);
}

double* KalmanFilterMatrix::data()
{
    return internal_data->ptr->data();
}

KalmanFilter::KalmanFilter(int input_dimensions, int output_dimensions) : input_dimension(input_dimensions), output_dimension(output_dimensions)
{
    internal = new KalmanFilterInternal(input_dimensions, output_dimensions);
}

KalmanFilter::~KalmanFilter()
{
    delete internal;
}

void KalmanFilter::update(const double* observation)
{

    MatrixXd observation_matrix = MapXd(observation, input_dimension, 1);

    MatrixXd gain = internal->estimate_covariance * internal->observation_model.transpose() *
        (internal->observation_model * internal->estimate_covariance *
        internal->observation_model.transpose() + internal->observation_covariance).inverse();

    internal->estimate_mean = internal->estimate_mean + gain * (observation_matrix -
            (internal->observation_model * internal->estimate_mean));

    internal->estimate_covariance = (MatrixXd::Identity(output_dimension, output_dimension) -
        (gain * internal->observation_model))* internal->estimate_covariance;
}

void KalmanFilter::predict()
{
    predict(nullptr);
}

void KalmanFilter::predict(const double* input)
{
    if(internal->has_control_model && input != nullptr) {
        MatrixXd input_matrix = MapXd(input, input_dimension, 1);
        internal->estimate_mean = internal->transition_model * internal->estimate_mean +
            internal->control_model * input_matrix;
    } else {
        internal->estimate_mean = internal->transition_model * internal->estimate_mean;
    }
    internal->estimate_covariance = internal->transition_model * internal->estimate_covariance *
        internal->transition_model.transpose() + internal->process_covariance;
}

int KalmanFilter::getObservationDimension() {
    return input_dimension;
}

int KalmanFilter::getMeanDimension() {
    return output_dimension;
}

KalmanFilterMatrix KalmanFilter::state_matrix() {
    return KalmanFilterMatrix(output_dimension, 1, new KFPtr({&internal->estimate_mean}));
}

KalmanFilterMatrix KalmanFilter::covariance_matrix() {
    return KalmanFilterMatrix(output_dimension, output_dimension, new KFPtr({&internal->estimate_covariance}));
}

KalmanFilterMatrix KalmanFilter::transition_matrix() {
    return KalmanFilterMatrix(output_dimension, output_dimension, new KFPtr({&internal->transition_model}));
}

KalmanFilterMatrix KalmanFilter::process_covar_matrix() {
    return KalmanFilterMatrix(output_dimension, output_dimension, new KFPtr({&internal->process_covariance}));
}

KalmanFilterMatrix KalmanFilter::observe_matrix() {
    return KalmanFilterMatrix(input_dimension, output_dimension, new KFPtr({&internal->observation_model}));
}

KalmanFilterMatrix KalmanFilter::observe_covar_matrix() {
    return KalmanFilterMatrix(input_dimension, input_dimension, new KFPtr({&internal->observation_covariance}));
}

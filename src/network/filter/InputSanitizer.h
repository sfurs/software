#ifndef INPUTSANITIZER_H
#define INPUTSANITIZER_H

#include "RawFieldData.h"
#include <QVector>

class ObjectTracker;

class InputSanitizer {
    public
        /*
            Responsible for detecting when robots are no longer found
            and resetting the object tracker data when needed;
        */:
        InputSanitizer(ObjectTracker* tracker);
        /*
         * Applies caching and filtering on the vision data
         *
         * @param field Vision data
        */
        void filterField(RawFieldData& field);
        // RawFieldData filterField(RawFieldData& field);
    private:
        ObjectTracker* tracker;
};

#endif // INPUTSANITIZER_H

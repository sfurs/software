#include "ObjectTracker.h"
#include "Team.h"
#include <QDateTime>

#include "Logging.h"

QVector<RobotData> create_robot_data(RawFieldData curr_field, RawFieldData previous_field, Team team);
BallData create_ball_data(RawFieldData curr_field, RawFieldData previous_field);


ObjectTracker::ObjectTracker() : ball() {

}

ObjectTracker::~ObjectTracker() {
    for (RobotTracker* filter: yellowTeam.values()) {
        delete filter;
    }
    for (RobotTracker* filter: blueTeam.values()) {
        delete filter;
    }
}

void ObjectTracker::updateRobot(Team team, RawRobot data, FieldInfo field) {
    QMap<int, RobotTracker*>* teamData = &blueTeam;
    
    if (team == YELLOW) {
        teamData = &yellowTeam;
    }
    
    if (teamData->find(data.id) == teamData->end()) {
        // create robot tracker
        RobotTracker* tracker = new RobotTracker(data.id);
        teamData->insert(data.id, tracker);
    }
    (*teamData)[data.id]->update(data, field);
}

void ObjectTracker::updateBall(RawBall data, FieldInfo field) {
    ball.update(data, field);
} 

void ObjectTracker::updatePrediction() {
    previousUpdate = ((double)QDateTime::currentMSecsSinceEpoch()) / 1000.0;
    for (RobotTracker* filter: yellowTeam.values()) {
        filter->predict(previousUpdate);
    }
    for (RobotTracker* filter: blueTeam.values()) {
        filter->predict(previousUpdate);
    }
    ball.predict(previousUpdate);
}

FieldData ObjectTracker::getFieldData() {
    FieldData field;
    for (QMap<int, RobotTracker*>::key_value_iterator iter = yellowTeam.keyValueBegin(); 
            iter != yellowTeam.keyValueEnd(); ++iter) {
        RobotTracker* filter = iter->second;
        field.yellow_robots.append(filter->getPredict());
    }
    for (QMap<int, RobotTracker*>::key_value_iterator iter = blueTeam.keyValueBegin(); 
            iter != blueTeam.keyValueEnd(); ++iter) {
        RobotTracker* filter = iter->second;
        field.blue_robots.append(filter->getPredict());
    }
    field.ball = ball.getPredict();
    field.capture_time = previousUpdate;
    return field;
}

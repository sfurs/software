#ifndef ROBOTTRACKER_H
#define ROBOTTRACKER_H

#include "RawFieldData.h"
#include "FieldData.h"
#include "../filter/KalmanFilter.h"
#include <QtGlobal>

class RobotTracker {
    public:
        RobotTracker();
        RobotTracker(unsigned int id);
        void update(RawRobot data, FieldInfo field);
        void predict(double time);
        RobotData getPredict();
        void reset();
        double getLastUpdate();
    private:
        double boundAngle(double angle);
        unsigned int id;
        unsigned int prev_cam_id;
        double previous_update = qQNaN();
        double previous_predict = qQNaN();
        KalmanFilter filter;
        KalmanFilterMatrix mean;
        KalmanFilterMatrix transition;
        KalmanFilterMatrix observation_covar;
};

#endif /* ROBOTTRACKER_H */

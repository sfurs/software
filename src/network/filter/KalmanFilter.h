#ifndef KALMANFILTER_H

#define KALMANFILTER_H

/*
 * The internal data of the kalman filter
 *  this is defined in KalmanFilter.cpp
*/
struct KalmanFilterInternal;

class KalmanFilter;
class KalmanFilterMatrix
{
    /*
     * A matrix used to access and change the kalman filter data
    */
    public:        
        /*
         * Access an element in a KalmanFilterMatrix using 1d coordinates
        */
        double get(int i) const;
        /*
         * Access an element in a KalmanFilterMatrix using 2d coordinates
        */
        double get(int i, int j) const;
        /*
         * Access an element in a KalmanFilterMatrix using 1d coordinates
        */
        double& set(int i);
        /*
         * Access an element in using 2d coordinates
        */
        double& set(int row, int col);
        /*
         * Get the full data
        */
        double* data();
        friend KalmanFilter;
        KalmanFilterMatrix(const KalmanFilterMatrix& KFM);
        ~KalmanFilterMatrix();
    private:
        struct KFPtr;
        KalmanFilterMatrix(int rows, int cols, KFPtr* data);
        int rows;
        int cols;
        KFPtr* internal_data;
};

class KalmanFilter
{
    /*
     * A Kalman Filter implementation
    */
    public:
        KalmanFilter(int observation_dimensions, int mean_dimensions);
        KalmanFilter(const KalmanFilter&) = delete;
        ~KalmanFilter();
        /*
         * Update the kalman filter with a new observation
         *
         * @param observation an array of size observation_dimension
        */
        void update(const double* observation);
        /*
         * Predict the mean over a time period
        */
        void predict();
        /*
         * Predict the mean with a control input
         *
         * @param control_input an array of size observation_dimension
        */
        void predict(const double* control_input);
        /*
         * Returns the dimension of an observation
         *
         * @return the dimension of observations
        */
        int getObservationDimension();
        /*
         * Returns the dimension of the mean
         *
         * @return the dimension of the mean
        */
        int getMeanDimension();
        /*
         * Returns the mean state matrix
        */
        KalmanFilterMatrix state_matrix();
        /*
         * Returns the covariance state matrix
        */
        KalmanFilterMatrix covariance_matrix();
        /*
         * Returns the state transition matrix
        */
        KalmanFilterMatrix transition_matrix();
        /*
         * Returns the process covariance matrix
        */
        KalmanFilterMatrix process_covar_matrix();
        /*
         * Returns the observation matrix
        */
        KalmanFilterMatrix observe_matrix();
        /*
         * Returns the observation covariance matrix
        */
        KalmanFilterMatrix observe_covar_matrix();
    private:
        int input_dimension;
        int output_dimension;
        KalmanFilterInternal* internal;
};
#endif

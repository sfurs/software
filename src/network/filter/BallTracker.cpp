#include "BallTracker.h"



BallTracker::BallTracker() : filter(3, 6), mean(filter.state_matrix()),
    transition(filter.transition_matrix()),
    observation_covar(filter.observe_covar_matrix())
{

    KalmanFilterMatrix matrix = filter.observe_matrix();
    matrix.set(0,0) = 1;
    matrix.set(1,1) = 1;
    matrix.set(2,2) = 1;

    KalmanFilterMatrix process = filter.process_covar_matrix();
    process.set(0,0) = 0.1;
    process.set(1,1) = 0.1;
    process.set(2,2) = 0.1;
    process.set(3,3) = 0.1;
    process.set(4,4) = 0.1;
    process.set(5,5) = 0.1;

}


void BallTracker::update(RawBall data, FieldInfo field) {
    double update_data[] = {data.x, data.y, data.z};
    if (qIsNaN(previous_update)) {
        mean.set(0) = data.x;
        mean.set(1) = data.y;
        mean.set(2) = data.z;
        previous_predict = qQNaN();
        transition.set(0,3) = 0;
        transition.set(1,4) = 0;
        transition.set(2,5) = 0;
    }
    if (field.camera_id != prev_cam_id) {
        // more noise if camera changed
        prev_cam_id = field.camera_id;
        observation_covar.set(0,0) = 4;
        observation_covar.set(1,1) = 4;
        observation_covar.set(2,2) = 4;
    } else {
        observation_covar.set(0,0) = 1;
        observation_covar.set(1,1) = 1;
        observation_covar.set(2,2) = 1;
    }
    previous_update = field.capture_time;
    filter.update(update_data);
}

void BallTracker::predict(double time) {
    if(!qIsNaN(previous_predict)) {
        double time_delta = time - previous_predict;
        transition.set(0,3) = time_delta;
        transition.set(1,4) = time_delta;
        transition.set(2,5) = time_delta;
    }
    filter.predict();
    previous_predict = time;
}

BallData BallTracker::getPredict() {
    double* val = mean.data();
    return {(float)val[0], (float)val[1], (float)val[2], (float)val[3],
        (float)val[4], (float)val[5]};
}

void BallTracker::reset() {
    previous_update = qQNaN();
}

double BallTracker::getLastUpdate() {
    return previous_update;
}

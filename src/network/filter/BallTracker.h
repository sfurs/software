#ifndef BALLTRACKER_H
#define BALLTRACKER_H

#include "RawFieldData.h"
#include "FieldData.h"
#include "../filter/KalmanFilter.h"

class BallTracker {
    public:
        BallTracker();
        void update(RawBall data, FieldInfo field);
        void predict(double time);
        BallData getPredict();
        void reset();
        double getLastUpdate();
    private:
        unsigned int prev_cam_id;
        double previous_update = qQNaN();
        double previous_predict = qQNaN();
        KalmanFilter filter;
        KalmanFilterMatrix mean;
        KalmanFilterMatrix transition;
        KalmanFilterMatrix observation_covar;
};

#endif /* BALLTRACKER_H */

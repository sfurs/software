#include "InputSanitizer.h"
#include "ObjectTracker.h"
#include "RawFieldData.h"

#include "Logging.h"

InputSanitizer::InputSanitizer(ObjectTracker* tracker) : tracker(tracker) {
}

void InputSanitizer::filterField(RawFieldData& field) {
    FieldInfo field_info = {field.getCameraID(), field.getCaptureTime()};
    float thresh = 0.1;
    QVector<RawRobot>& blueBots =  field.getRobotsRef(BLUE);
    for (RawRobot& bot : blueBots) {
        //TODO not sure why we implement a deadzone in the middle of the field
        if (bot.x < thresh && bot.x > -thresh) {
            bot.x = 0;
        }
        if(bot.y < thresh && bot.y > -thresh) {
            bot.y = 0;
        }
        tracker->updateRobot(BLUE, bot, field_info);
    }
    QVector<RawRobot>& yellowBots =  field.getRobotsRef(YELLOW);
    for (RawRobot& bot : yellowBots) {
        if (bot.x < thresh && bot.x > -thresh) {
            bot.x = 0;
        }
        if(bot.y < thresh && bot.y > -thresh) {
            bot.y = 0;
        }
        tracker->updateRobot(YELLOW, bot, field_info);
    }
    for (RawBall ball : field.getBalls()) {
        tracker->updateBall(ball, field_info);
    }
}


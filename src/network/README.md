## Overview: Network Module Overview
The network module provides a way to handle UDP network communication in skynet. It will communcate using specified ports and IP addresses with UDPMulticast libraries provided by Qt. It uses the QUdpSocket class to send and receive UDP packets, and provides an interface for connecting to a multicast group and sending and receiving data.

## Files

### 1. `NetMaster.cpp` & `NetMaster.h`

- **NetMaster Class**:  
  The `NetMaster` class is responsible for managing network communication and controlling robots on the field. It interacts with multiple systems, including UDP, TCP, and serial communication, and coordinates with field data, referees, and robot instructions.

  - **Functions**:

    - `NetMaster()`  
      **Constructor**: Default constructor that initializes the `NetMaster` with a default team size of 6.

    - `NetMaster(int t)`  
      **Constructor**: Initializes the `NetMaster` with a specified team size `t`.

    - `~NetMaster()`  
      **Destructor**: Cleans up and releases all resources used by the `NetMaster`.

    - `ControlPID* getPID()`  
      **Function**: Retrieves the PID controller from the `ControlWrapper`, which manages the robots' movement and control algorithms.

    - `void updateFieldPositions(RawFieldData raw_data)`  
      **Slot**: Updates the field positions based on the raw data received from the vision system.

    - `void updateRefereeData(RefereeData data)`  
      **Slot**: Updates the referee data, which includes game status and instructions from the referee system.

    - `void stop()`  
      **Slot**: Stops all robots and halts the current game actions.

    - `void SetRobotInstruction(RobotInstruction instruction)`  
      **Slot**: Sends a set of instructions to a robot, specifying movement and actions like kicking or dribbling.

    - `void setRefereeIP(const QString& ip)`
      **Slot**: Sets IP address for the referee system and establishes a connection to recieved data.

    - `void setRefereePort(quint16 port)`
     **Slot**: Sets the port for the referee system and connects to it.

    - `void setVisionIP(const QString& ip)`
     **Slot**: Sets the IP address for vision system and establishes a connection to receieve data.

    - `void setVisionPort(quint16 port)`
     **Slot**: Set the port for the vision system and connects to it.

    - `void setControlIP(const QString& ip)`
     **Slot**: Sets IP address for controlling robots. Based on the current team, it connects to either the blue or yellow team port.

    - `void setControlBluePort(quint16 port)`
     **Slot**: Sets the control port for the blue team. If the current team is blue, it connects using this port.

    - `void setControlYellowPort(quint16 port)`
     **Slot**: Sets the control port for the yellpw team. If the current team is yellow, it connects using this port.

    - `void setRobotControlPort(const QString& port)`
     **Slot**: Sets the serial port for robot control and establishes a connection with specified baud rate.

    - `void setRobotControlBaudRate(int baudRate)`
     **Slot**: Sets the baud rate for robot control and reconnects using the specified settings.

    - `void setFriendlyTeam(Team team)`
     **Slot**: Sets the current team to either blue or yellow and updates the control port connection accordingly.

    - `QString getRefereeIP()`
     **Slot**: Retrieves IP address used for referee system.

    - `quint16 getRefereePort()`
     **Slot**: Retrieves the port used for referee system.

    - `QString getVisionIP()`
     **Slot**: Retrieves IP address used for vision system.

    - `quint16 getVisionPort()`
     **Slot**: Retrieves the port used for the vision system.

    - `QString getControlIP()`
     **Slot**: Retrieves the IP addressed used for controlloing the robots.

    - `quint16 getControlBluePort()`
     **Slot**: Retrieves the port used for controlling the blue team.

    - `quint16 getControlYellowPort()`
     **Slot**: Retrieves the port used for controlling the yellow team.

    - `QString getRobotControlPort()`
     **Slot**: Retrieves the serial port used for robot control.

    - `int getRobotControlBaudRate()`
     **Slot**: Returns the baud rate used for robot control.

    - `Team getFriendlyTeam()`
     **Slot**: Retrieves the current team, either blue or yellow, being controlled.


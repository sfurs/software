#include "ControlProtobuf.h"

ControlProtobuf::ControlProtobuf(UdpConnection* connection) : Protobuf(connection) {
    // kickStrength = 0;
    calibration.x.updateParameters(0.035, 0.0003, 0.02, 15, 50);
    calibration.y.updateParameters(0.035, 0.0003, 0.02, 15, 50);
    calibration.rotate.updateParameters(2.0, 0.0004, 0.04, 0.02, 50);
}


void ControlProtobuf::setTeamSize(int robots) {
    if(robots > team.size()) {
//        int oldSize = team.size();
        team.resize(robots);
//        for (int under = oldSize; under < team.size(); under++) {
//            halt(under);
//        }
    } else if(robots < team.size()) {
        for (int over = team.size(); over > robots; over++) {
            removeRobotFromCommand(over);
        }
        team.resize(robots);
    }
}

void ControlProtobuf::setAsync(int id) {
    if (!team[id].isAsync) {
        addRobotToCommand(id);
        team[id].isAsync = true;
    }
}

void ControlProtobuf::setSync(int id) {
    if (team[id].isAsync) {
        removeRobotFromCommand(id);
        team[id].isAsync = false;
    }
}

void ControlProtobuf::setRobotId(int id, int robotID) {
    team[id].control.set_id(robotID);
}

// void ControlProtobuf::setRobotPosSpeed(int id, Agent bot) {


void ControlProtobuf::setRobotPosSpeed(RobotInstruction instruction) {
    int id  = instruction.id;
    if (id < 0 || id >= team.size()) {
        qWarning() << "Invalid id " << id << ". Have you configured the simulator to 6 robots in Division B?";
        return;
    }
    auto movement = team[id].control.mutable_move_command();
    auto local_velocity = movement->mutable_local_velocity();
    local_velocity->set_forward(static_cast<float>(instruction.v_x));
    local_velocity->set_left(static_cast<float>(instruction.v_y));
    local_velocity->set_angular(static_cast<float>(instruction.v_phi));
    if (instruction.chipper > 0 && instruction.kick == 0) {
        team[id].control.set_kick_speed(instruction.chipper*2);
        team[id].control.set_kick_angle(45);
    }
    else {
        team[id].control.set_kick_speed(instruction.kick);
        team[id].control.set_kick_angle(0);
    }
    team[id].control.set_dribbler_speed(instruction.dribble);
    sendAsync();
}

void ControlProtobuf::halt(int id) {
    auto movement = team[id].control.mutable_move_command();
    auto local_velocity = movement->mutable_local_velocity();
    local_velocity->set_forward(0);
    local_velocity->set_left(0);
    local_velocity->set_angular(0);
    team[id].control.set_kick_speed(0);
    team[id].control.set_kick_angle(0);
    team[id].control.set_dribbler_speed(0);
}

void ControlProtobuf::setForwardSpeed(int id, float speed) {
    auto movement = team[id].control.mutable_move_command();
    auto velocity = movement->mutable_local_velocity();
    velocity->set_forward(speed);
}

void ControlProtobuf::setLeftSpeed(int id, float speed) {
    auto movement = team[id].control.mutable_move_command();
    auto velocity = movement->mutable_local_velocity();
    velocity->set_left(speed);
}

void ControlProtobuf::setAngular(int id, float velocity) {
    auto movement = team[id].control.mutable_move_command();
    auto local_velocity = movement->mutable_local_velocity();
    local_velocity->set_angular(velocity);
}

void ControlProtobuf::setKickSpeed(int id, float speed) {
    team[id].control.set_kick_speed(speed);
}

void ControlProtobuf::setKickAngle(int id, float angle) {
    team[id].control.set_kick_angle(angle);
}

void ControlProtobuf::setDribblerSpeed(int id, float speed) {
    team[id].control.set_dribbler_speed(speed);
}

void ControlProtobuf::send(int id) {
    addRobotToCommand(id);
    sendAsync();
    removeRobotFromCommand(id);
}

void ControlProtobuf::sendAsync() {
    if (activeCommands)
    sendMsg();
}

void ControlProtobuf::addRobotToCommand(int id) {
    if (team[id].commandIndex < 0) {
        if (!send_msg) {
            createSendMsg<RobotControl>();
        }
        auto command_msg = static_cast<RobotControl*> (send_msg);
        team[id].commandIndex = activeCommands;
        // Avoid an unnessary copy from the team array to the 
        // this is fine as when ControlProtobuf is deconstructed both the
        //  command_msg and ControlProtobuf robot are destructed at the same time
        command_msg->mutable_robot_commands()->UnsafeArenaAddAllocated(&(team[id].control));
        activeCommands++;
    }
}

void ControlProtobuf::removeRobotFromCommand(int id) {
    if (send_msg && team[id].commandIndex >= 0) {
        auto command_msg = static_cast<RobotControl*> (send_msg);
        command_msg->mutable_robot_commands()->UnsafeArenaExtractSubrange(team[id].commandIndex, 1, nullptr);
        for (int shift = team[id].commandIndex; shift < team.size(); shift++) {
            team[shift].commandIndex--;
        }
        team[id].commandIndex = -1;
        activeCommands--;
    }
}

ControlPID* ControlProtobuf::getPID() {
    return &calibration; 
}

void ControlProtobuf::recvSignal() {}
void ControlProtobuf::recvMsg(const QByteArray) {}

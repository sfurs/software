#include "ControlPID.h"

#include <QDateTime>

PID::PID() : PID (0,0,0) {}

PID::PID(double kp, double ki, double kd) : kp(kp), ki(ki), kd(kd) {}

void PID::updateConfig(const PID ref) {
    kp = ref.kp;
    ki = ref.ki;
    kd = ref.kd;
    deadband = ref.deadband;
    reachedTolerance = ref.reachedTolerance;
}

void PID::setTarget(double target) {
    setTarget(target, QDateTime::currentMSecsSinceEpoch());
}

void PID::setTarget(double target, qint64 time) {
    this->target = target;
    previous_call = time;
    total_error = 0;
    previous_error = 0;
}

double PID::update(double current_position) {
    return update(current_position, QDateTime::currentMSecsSinceEpoch());
}

double PID::update(double input, qint64 current_time) {
    double error = target - input;
    return evaluate(error, current_time);
}

double PID::evaluate(double error) {
    return evaluate(error, QDateTime::currentMSecsSinceEpoch());
}

double PID::evaluate(double error, qint64 current_time) {
    if (qAbs(error) <= deadband) {
        error = 0;
        total_error = 0;  // Reset integral term
        if (first_stable == 0) {
            first_stable = current_time;
        }
    } else {
        if (first_stable > 0) {
            first_stable = 0;
        }
    }
    if (current_time == previous_call) {
        return previous_output;
    }
    double time = ((double)(current_time - previous_call))/1000.0f;
    total_error += error * time;
    double diff_error = (error - previous_error) / time;
    previous_call = current_time;
    previous_error = error;
    previous_output = kp * error + ki * total_error + kd * diff_error;
    return previous_output;
}

bool PID::reachedTarget(double input) {
    double error = target - input;
    return reachedTargetError(error);
}

bool PID::reachedTargetError(double error) {
    if (qAbs(error) <= deadband) {
        if (previous_call - first_stable > reachedTolerance) {
            return true;
        }
    }
    return false;
}

void PID::updateParameters(double kp, double ki, double kd, double deadband, qint64 tolerance) {
    this->kp = kp;
    this->ki = ki;
    this->kd = kd;
    this->deadband = deadband;
    reachedTolerance = tolerance;
}

double PID::getTarget() {
    return target;
}

void ControlPID::updateConfig(const ControlPID ref) {
   x.updateConfig(ref.x);
   y.updateConfig(ref.y);
   rotate.updateConfig(ref.rotate);
}

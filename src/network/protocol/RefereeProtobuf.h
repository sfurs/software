#ifndef REFEREEPROTOBUF_H
#define REFEREEPROTOBUF_H
#include "Protobuf.h"
#include "TcpConnection.h"
#include "UdpConnection.h"
#include "RefereeData.h"

class RefereeProtobuf : public Protobuf
{
Q_OBJECT
public:
    /*
     * Protobuf Message for robot commands sent by the referee
    */
    RefereeProtobuf(UdpConnection* connection);
    
signals:

    /*
     * Sent when the referee sends a new message
    */
    void updateRefereeData(RefereeData referee);
    /*
     * Sent when a new command is sent
    */
    void newCommand(int command, uint64_t time, float x, float y);

protected:
    void recvSignal();
    void recvMsg(const QByteArray);
private:
    uint64_t lastCommandSent;
    uint64_t lastPacketSent;
    uint32_t lastStage;
}; 


#endif // REFEREEPROTOBUF_H

#ifndef PROTOBUF_H
#define PROTOBUF_H

#include "Connection.h"
#include <QObject>
#include <QByteArray>
#include <google/protobuf/message.h>
#include <google/protobuf/arena.h>
class Protobuf : public QObject
{
    // The base protobuf object, this header file should only be included in
    //  files that define a new protobuf message
Q_OBJECT
public:
    Protobuf(Connection*);
protected:
    google::protobuf::Message* recv_msg = nullptr;
    google::protobuf::Message* send_msg = nullptr;
    
    google::protobuf::Arena recv_arena;
    google::protobuf::Arena send_arena;
    
    bool deliminate = false;
    
    void sendMsg();
    // Templates are used because Arena::CreateMessage needs the type of the Message 
    /* 
     * Create the message object to send
     *
     * @tparam ProtoMsg the protobuf message class
    */
    template<class ProtoMsg>
    void createSendMsg()
    {
        if(!send_msg)
        {
            send_arena.Reset();
        }
        send_msg = google::protobuf::Arena::CreateMessage<ProtoMsg>(&send_arena);
    }
    
    /* 
     * Parse a QByteArray encoded Protobuf message for further use
     * the resulting message is stored in recv_msg
     *
     * @tparam ProtoMsg the protobuf message class
     *
     * @param binary the encoded message
    */
    template<class ProtoMsg>
    void parse(const QByteArray binary)
    {
        if(!recv_msg)
        {
            recv_msg = google::protobuf::Arena::CreateMessage<ProtoMsg>(&recv_arena);
        }
        parseMsg(binary); 
    }

    /* 
     * Called when a message has been deserialized and ready to send signals
    */
    virtual void recvSignal() = 0;
    /* 
     * Called when a message has been received to be deserialized
    */
    virtual void recvMsg(const QByteArray) = 0;
private:
    /* 
     * On receiving a message, calls recvMsg to deserialize and recvSignal to signal
    */
    void parseMsg(const QByteArray binary);
    Connection* connection;
};
#endif // PROTOBUF_H

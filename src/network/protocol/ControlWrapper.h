#ifndef CONTROLWRAPPER_H
#define CONTROLWRAPPER_H


#include "ControlProtobuf.h"
#include "ControlWire.h"
#include "ControlBase.h"
#include "UdpConnection.h"
#include "SerialConnection.h"
#include "ControlPID.h"

class ControlWrapper
{
public:
    /*
     * A wrapper to allow switch between protobuf and serial communications
     * for controls
    */
    ControlWrapper(UdpConnection* proto_net, SerialConnection* serial_net);
    ~ControlWrapper();
    /*
     * Set the team size
     *
     * @param robots number of robots
    */
    void setTeamSize(int robots);

    /*
     * 
     *  Set a robot to always be sent its command when any command is sent
     * 
     * @param id
    */
    void setAsync(int id);

    /**
     * @brief Sets speed of a robot before sending it
     * @param id ID of the robot
     */
    void setRobotInstruction(RobotInstruction instruction);

    /*
     * Set the command to halt the robot
    */
    void halt(int id);

    /*
     * Set robot id of a given command id
    */
    void setRobotId(int id, int robotID);

    /*
     * Sets the active control
    */
    void setControl(bool isSerial);

    /*
     * Get the pid controller for this output
    */
    ControlPID* getPID();
private:
    ControlWire* serial;
    ControlProtobuf* proto;
    ControlBase* active = nullptr;
};
#endif // CONTROLWRAPPER_H

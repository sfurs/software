#ifndef CONTROLPROTOBUF_H
#define CONTROLPROTOBUF_H
#include "ControlBase.h"
#include "Protobuf.h"
#include "ssl_simulation_robot_control.pb.h"
#include "UdpConnection.h"
#include <QVector>

class ControlProtobuf : public Protobuf, public ControlBase
{
    /*
     * Protobuf Message for robot commands sent to the simulator
    */
public:
    ControlProtobuf(UdpConnection* connection);

    /*
     * Set the team size
     * 
     * @param robots number of robots
    */
    void setTeamSize(int robots) override;

    /*
     * Set a robot to always be sent its command when any command is sent
     * 
     * @param id
    */
    void setAsync(int id);
    /*
     * Set a robot to only be sent its command when send is called 
    */
    void setSync(int id);


    /**
     * @brief Sets speed of a robot before sending it
     * @param id ID of the robot
     */
    void setRobotPosSpeed(RobotInstruction instruction) override;

    /*
     * Set the command to halt the robot
    */
    void halt(int id) override;
    
    /*
     * Set robot id of a given command id
    */
    void setRobotId(int id, int robotID) override;

    /*
     * Sets the maximum forward speed(m/s)
    */
    void setForwardSpeed(int id, float speed);

    /*
     * Sets the maximum leftward speed(m/s)
    */
    void setLeftSpeed(int id, float speed);

    /*
     * Sets the maximum angular speed
    */
    void setAngular(int id, float velocity);

    /*
     * Sets the kick speed
    */
    void setKickSpeed(int id, float speed);

    /*
     * Sets the kick angle
    */
    void setKickAngle(int id, float angle);

    /*
     * Sets the dribbler speed
    */
    void setDribblerSpeed(int id, float speed);

    /*
     * Send any changes to the command to a robot
    */
    void send(int id);
    
    /*
     * Send any commands in the packet
    */
    void sendAsync();

    ControlPID* getPID() override;
protected:
    /*
     * NOP
    */
    void recvSignal() override;
    /*
     * NOP
    */
    void recvMsg(const QByteArray) override;
private:
    /*
     * Add a command to the packet
    */
    void addRobotToCommand(int id);
    /*
     * Remove a command from the packet
    */
    void removeRobotFromCommand(int id);
    struct Robot {
        int commandIndex = -1;
        bool isAsync = false;
        RobotCommand control;
    };
    QVector<Robot> team;
    int activeCommands = 0;
    // int kickStrength;
    ControlPID calibration;
}; 


#endif // CONTROLPROTOBUF_H

#ifndef TEAMPROTOBUF_H
#define TEAMPROTOBUF_H
#include "Protobuf.h"
#include "TcpConnection.h"
#include <QString>
#include <string>

class TeamProtobuf : public Protobuf
{
    /*
     * Protobuf Message for robot commands sent to the simulator (functions subject to change)
    */
Q_OBJECT
public:
    TeamProtobuf(TcpConnection* connection);
    
    /*
     * Initalize a batch command, reseting any existing command
    */
    void teamRegister(std::string name);
    void teamRegister(std::string name, uint32_t team);
    void changeKeeper(uint32_t newKeeper);
    /*
     * Respond to an advantage choice request
     *
     * @params continue_game keep the game running
    */
    void choice(bool continue_game);
    void requestBotSub();
    void ping();
signals:
    void teamReply(int status_code, QString reason);
protected:
    void recvSignal();
    void recvMsg(const QByteArray);
private:
    bool sendRequest;
    std::string token;
    QByteArray signature;
}; 


#endif // TEAMPROTOBUF_H

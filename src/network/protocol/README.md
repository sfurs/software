## Overview: Network Protocol Overview
This folder contains the protocol handling.

## Files

### 1. `VisionProtobuf.cpp` & `VisionProtobuf.h`
- **VisionProtobuf Class**:
    The VisionProtobuf class processes data received over the network connection, specifically for vision-related data. The class inherits from `Protobuf` and processes messages for robot and ball detection.

    - **Functions**:
      - `VisionProtobuf(UdpConnection* connection)`:
      Constructor to initialize a VisionProtobud instance with a UDP connection.
      - `recvMsg(const QByteArray binary)`:
      Parse incoming binary message into an SSL_WrapperPacket format for processing
      - `recvSignal()`:
      Processes the received message, extracts data for both team robots (yellow and blue) and balls, and emits a signal to update field positions using `updateFieldPositions(RawFieldData data).`

### 2. `RefereeProtobuf.cpp` & `RefereeProtobuf.h`

  - **Referee Protobuf Class**:
  The `RefereeProtobuf` class manages communication with a referee. It inherits from the Protobuf class. It is used to listen for incoming signals from the referee and parse them for information. Emits the `updateRefereeData` and `newCommand` signals when new game information is received.

    - **Constructor**:

      - `RefereeProtobuf(UdpConnection* Connection)`
        **Constructor**: Establishes a connection through the provided `UdpConnection` object and initializes lastCommandSent to 0.

    - **Protected Functions**:
      - `void recvSignal()`
      Listens for signals from the referee and parses the binary data received. Emits the updateRefereeData nad newCommand signals when new information is received.

      - `void recvMessage(const QByteArray)`
      Parses received binary data into a Referee type.
        - **Parameter**:
          - `QByteArray binary`: Binary data to parse

      - **Setting Up with SSL-Game-Controller**
      To set up the referee with the SSL-Game-Controller, pass the UDP connection of the controller to the constructor of this class. By deafult, SSL-Game-Controller runs the auto-referee on `224.5.23.1:10003` - initializing the Referee Protobuf on this address by passing it to the constructor will initialize it to listen for updates by the auto referee. 

### 3. `ControlWrapper.cpp` & `ControlWrapper.h`

- **ControlWrapper Class**:  
  The `ControlWrapper` class serves as a wrapper to switch between protobuf and serial communications for controlling robots. It allows for flexible communication methods and manages the team size, robot instructions, and control modes.

  - **Functions**:

    - `ControlWrapper(UdpConnection* proto_net, SerialConnection* serial_net)`  
      **Constructor**: Initializes the `ControlWrapper` with `UdpConnection` for protobuf communication and `SerialConnection` for serial communication.

    - `~ControlWrapper()`  
      **Destructor**: Cleans up resources and deletes the `ControlWrapper` object when it is no longer needed.

    - `void setTeamSize(int robots)`  
      **Function**: Sets the size of the team, i.e., the number of robots to be controlled.

    - `void setAsync(int id)`  
      **Function**: Marks a robot (by its `id`) to always receive its command whenever any command is sent.

    - `void setRobotInstruction(RobotInstruction instruction)`  
      **Function**: Sets the movement and action instructions for a specific robot before sending it.

    - `void halt(int id)`  
      **Function**: Halts the movement of the robot with the specified `id`.

    - `void setRobotId(int id, int robotID)`  
      **Function**: Associates a command `id` with a specific robot's `robotID`.

    - `void setControl(bool isSerial)`  
      **Function**: Sets the active control method, either serial or protobuf communication, based on the `isSerial` flag.

    - `ControlPID* getPID()`  
      **Function**: Retrieves the PID controller for the current control output, used to manage precise robot movements.

---

### 4. `ControlProtobuf.cpp` & `ControlProtobuf.h`

- **ControlProtobuf Class**:  
  The `ControlProtobuf` class is responsible for sending robot control commands using protobuf messages. It communicates with a simulator via UDP and allows setting various parameters related to robot movement and control.

  - **Inheritance**:
    - Inherits from `Protobuf` and `ControlBase` classes.

  - **Functions**:

    - `ControlProtobuf(UdpConnection* connection)`  
      **Constructor**: Initializes the `ControlProtobuf` object with a UDP connection to handle network communication.

    - `void setTeamSize(int robots)`  
      **Override**: Sets the size of the team by defining the number of robots.

    - `void setAsync(int id)`  
      Marks a robot with the given `id` to always receive commands asynchronously (automatically when any command is sent).

    - `void setSync(int id)`  
      Sets a robot to only receive commands when the `send()` function is explicitly called.

    - `void setRobotPosSpeed(RobotInstruction instruction)`  
      **Override**: Sets the speed and position for the robot before sending the command.

    - `void halt(int id)`  
      **Override**: Sends a command to halt the movement of the robot with the given `id`.

    - `void setRobotId(int id, int robotID)`  
      **Override**: Maps the `id` of the command to the `robotID`.

    - `void setForwardSpeed(int id, float speed)`  
      Sets the maximum forward speed (in meters per second) for the robot with the given `id`.

    - `void setLeftSpeed(int id, float speed)`  
      Sets the maximum leftward speed (in meters per second) for the robot with the given `id`.

    - `void setAngular(int id, float velocity)`  
      Sets the maximum angular speed (in radians per second) for the robot with the given `id`.

    - `void setKickSpeed(int id, float speed)`  
      Defines the kick speed for the robot with the given `id`.

    - `void setKickAngle(int id, float angle)`  
      Sets the kick angle for the robot with the given `id`.

    - `void setDribblerSpeed(int id, float speed)`  
      Sets the dribbler speed for the robot with the given `id`.

    - `void send(int id)`  
      Sends any updated commands to the robot with the specified `id`.

    - `void sendAsync()`  
      Sends any asynchronous commands to all robots.

    - `ControlPID* getPID()`  
      **Override**: Retrieves the PID controller for robot movement adjustments.

  - **Protected Functions**:

    - `void recvSignal()`  
      **Override**: A no-operation function (NOP), intended for receiving signals.

    - `void recvMsg(const QByteArray)`  
      **Override**: A no-operation function (NOP), intended for receiving messages.

  - **Private Functions**:

    - `void addRobotToCommand(int id)`  
      Adds a robot's command to the current command packet.

    - `void removeRobotFromCommand(int id)`  
      Removes a robot's command from the command packet.

### 5. `ControlWire.cpp` & `ControlWire.h`

- **ControlWire Class**:  
  The `ControlWire` class is responsible for controlling robots over a serial connection. It extends the `ControlBase` class and interacts with the hardware to send commands to robots.

  - **Constructor & Destructor**:
  
    - `ControlWire(SerialConnection* connection)`  
      **Constructor**: Initializes the `ControlWire` object with a `SerialConnection` object for serial communication.

    - `~ControlWire()`  
      **Destructor**: Cleans up resources when the object is destroyed.

  - **Functions**:

    - `void setTeamSize(int robots) override`  
      Sets the size of the team by defining the number of robots.

    - `void setRobotId(int id, int robotID) override`  
      Sets the `robotID` for a given command `id`. The `setID` function inside `ControlFormat` updates a bit field where the robot's ID is stored.

    - `void setRobotPosSpeed(RobotInstruction instruction) override`  
      Controls robot movement by setting position and speed based on the provided `RobotInstruction` data.

    - `void halt(int id) override`  
      Stops the movement of the robot with the given `id`.

    - `ControlPID* getPID() override`  
      Retrieves the PID controller for robot control adjustments.

    - `void sendAsync()`  
      Sends commands asynchronously over the serial connection. It prepares the control data in raw bytes and sends it via the serial connection to the robots.

  - **Private Functions**:

    - `int boundVelocity(int vel)`  
      A helper function that limits or adjusts the velocity to ensure values are within the robot's physical capabilities.



# use the protobuf find_protobuf rather than cmake
find_package(Protobuf CONFIG QUIET)
if (NOT Protobuf_FOUND)
    # for older protobuf versions
    find_package(Protobuf REQUIRED)
else()
    # in case the protobuf headers were installed outside /usr/include
    get_target_property(Protobuf_INCLUDE_DIRS protobuf::libprotobuf
    INTERFACE_INCLUDE_DIRECTORIES)
    set(Protobuf_LIBRARIES protobuf::libprotobuf) # cmake-lint: disable=C0103
endif()

set(PROTO_SRCS
    vision_detection.proto
    vision_geometry.proto
    vision_wrapper.proto
    ssl_simulation_robot_control.proto
    ssl_gc_common.proto
    ssl_gc_rcon.proto
    ssl_gc_game_event.proto
    ssl_gc_geometry.proto
    ssl_gc_rcon_team.proto
    ssl_gc_referee_message.proto)

# Isolate the deprecated warnings from the pb.cc files
add_library(skynet_proto STATIC)
target_include_directories(skynet_proto PUBLIC ${Protobuf_INCLUDE_DIRS})

#get the pb.h files
target_include_directories(skynet_proto PUBLIC ${CMAKE_CURRENT_BINARY_DIR})


set_target_properties(skynet_proto PROPERTIES
    AUTOMOC OFF
    AUTOUIC OFF
)

protobuf_generate(
    LANGUAGE cpp
    TARGET skynet_proto
    PROTOS ${PROTO_SRCS}
)

if (ENABLE_PCH)
    target_precompile_headers(skynet_proto PUBLIC ${CMAKE_SOURCE_DIR}/src/pch/protobuf.pch.hh)
endif ()
# Disable the deprecation warning when compiling the proto files alone
target_compile_options(skynet_proto PRIVATE -Wno-deprecated-declarations)
target_link_libraries(skynet_proto ${Protobuf_LIBRARIES})
target_link_libraries(skynet_protocol PUBLIC skynet_proto)
target_link_libraries(skynet_network PUBLIC skynet_proto)


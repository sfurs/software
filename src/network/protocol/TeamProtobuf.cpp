#include "TeamProtobuf.h"
#include "ssl_gc_rcon_team.pb.h"

TeamProtobuf::TeamProtobuf(TcpConnection* connection) : Protobuf(connection)
{
    deliminate = true;
}

void TeamProtobuf::teamRegister(std::string name)
{
    createSendMsg<TeamRegistration>();
    sendRequest = false;
    auto msg = static_cast<TeamRegistration*>(send_msg);
    msg->set_team_name(name);
    sendMsg();
}

void TeamProtobuf::changeKeeper(uint32_t newKeeper)
{
    if(!sendRequest) {
        createSendMsg<TeamToController>();
        sendRequest = true;
    }
    TeamToController* msg = static_cast<TeamToController*>(send_msg);
    msg->set_desired_keeper(newKeeper);
    sendMsg();
}

void TeamProtobuf::choice(bool continue_game)
{
    if(!sendRequest) {
        createSendMsg<TeamToController>();
        sendRequest = true;
    }
    TeamToController* msg = static_cast<TeamToController*>(send_msg);
    AdvantageChoice choice = STOP;
    if (continue_game) {
        choice = CONTINUE;
    }
    msg->set_advantage_choice(choice);
    sendMsg();
}

void TeamProtobuf::requestBotSub()
{
    if(!sendRequest) {
        createSendMsg<TeamToController>();
        sendRequest = true;
    }
    TeamToController* msg = static_cast<TeamToController*>(send_msg);
    msg->set_substitute_bot(true);
    sendMsg();
}

void TeamProtobuf::ping()
{
    if(!sendRequest) {
        createSendMsg<TeamToController>();
        sendRequest = true;
    }
    TeamToController* msg = static_cast<TeamToController*>(send_msg);
    msg->set_ping(true);
    sendMsg();
}

void TeamProtobuf::recvMsg(const QByteArray binary)
{
    parse<ControllerToTeam>(binary);
}

void TeamProtobuf::recvSignal()
{
    ControllerToTeam* reply_msg = static_cast<ControllerToTeam*>(recv_msg);
    if(reply_msg->has_controller_reply()) {
        auto reply = reply_msg->controller_reply();
        int code = 0;
        QString reason = "";
        if(reply.has_reason()) {
            reason = QString::fromStdString(reply.reason());
        }
        if (reply.has_status_code()) {
            code = reply.status_code();
        }
        teamReply(code, reason);
    }

}

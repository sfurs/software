#ifndef CONTROLWIRE_H
#define CONTROLWIRE_H

#include "ControlBase.h"
#include "SerialConnection.h"
#include <QMap>

struct ControlFormat;

class ControlWire : public ControlBase
{
public:
    ControlWire(SerialConnection* connection);
    ~ControlWire();

    void setTeamSize(int robots) override;

    void setRobotId(int id, int robotID) override;

    void setRobotPosSpeed(RobotInstruction instruction) override;

    void halt(int id) override;
    
    ControlPID* getPID() override;

    void send(int id);

    void sendAsync();

private:
    int boundVelocity(int vel);
    int boundDribbler(int dribbler);
    int boundChipper(int chipper);
    int boundKicker(int kicker);
    int boundID(int id);
    SerialConnection* connection;
    QMap<int, int> indexes;
    int msgsSize = 0;
    struct ControlFormat* msgs = nullptr;
    ControlPID calibration;
};

#endif // CONTROLWIRE_H

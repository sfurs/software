#include "Protobuf.h"
#include <google/protobuf/io/zero_copy_stream.h>
#include <google/protobuf/util/delimited_message_util.h>

Protobuf::Protobuf(Connection* connection):connection(connection)
{
    connect(connection, &Connection::recvMsg, this, &Protobuf::recvMsg);
}

void Protobuf::sendMsg()
{
    QByteArray binary;
    auto size = send_msg->ByteSizeLong();
    if (deliminate) {
        // uvarint32 will never be larger than 5 bytes
        // protobuf messages are limited to 2GB
        size += 5;
    }
    binary.resize(size);
    if (deliminate) {
        google::protobuf::io::ArrayOutputStream array(binary.data(), size);
        google::protobuf::io::CodedOutputStream output(&array);
        google::protobuf::util::SerializeDelimitedToCodedStream(*send_msg, &output);
        binary.truncate(output.ByteCount());
    } else {
        send_msg->SerializeToArray(binary.data(), binary.size());
    }
    connection->sendMsg()(binary);
}

void Protobuf::parseMsg(const QByteArray binary)
{
    if(binary.size())
    {
        if (deliminate) {
            google::protobuf::io::ArrayInputStream array(binary.data(), binary.size());
            google::protobuf::io::CodedInputStream input(&array);
            google::protobuf::util::ParseDelimitedFromCodedStream(recv_msg, &input, nullptr);
        } else {
            recv_msg->ParseFromArray(binary.data(), binary.size());
        }
        recvSignal();
    }
}

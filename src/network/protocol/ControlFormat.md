# Control Format

This file is a documentation of the control format used within the wire protocol. It is intended to be used as a reference for the control format for the firmware team.

## Control Format Fields

The control format is a struct that contains the following fields:

- `int id`: The ID of the robot (0 to 15)
- `int v_x`: The velocity in the x direction (-7 to 7)
- `int v_y`: The velocity in the y direction (-7 to 7)
- `int v_phi`: The angular velocity (-7 to 7)
- `int kick`: The kick speed (0 to 15)
- `int chipper`: The chipper speed (0 to 3)
- `int dribbler`: The dribbler speed (0 to 3)

## Bit layout

From the code snippet below, we can see that the control format is a struct that contains the following fields:
```cpp
    char id_forwardVelocity; // id 4 bits, forward velocity 4 bits
    char leftVelocity_rotationalVelocity; // left velocity 4 bits, rotational velocity 4 bits
    char dribbler_chipper_kicker; // dribbler 2 bits, chipper 2 bits, kicker 4 bits
```

The bit layout of the packet is as follows:
- Byte 1: id 4 bits, forward velocity 4 bits
- Byte 2: left velocity 4 bits, rotational velocity 4 bits
- Byte 3: dribbler 2 bits, chipper 2 bits, kicker 4 bits

## Determining Velocities

Velocities are bounded to the range of -7 to 7. All velocities are then masked via `velocity & 0x0F` to ensure that the velocities are within 4 bits.

A velocity of 0 is represented by 0, a velocity of 1 is represented by 1, a velocity of -1 is represented by 15, a velocity of 7 is represented by 7, and a velocity of -7 is represented by 9. Here is complete mapping for all velocity values from -7 to 7 in 4 bits binary:

| Velocity | Binary |
|----------|--------|
| -7       | 1001   |
| -6       | 1010   |
| -5       | 1011   |
| -4       | 1100   |
| -3       | 1101   |
| -2       | 1110   |
| -1       | 1111   |
| 0        | 0000   |
| 1        | 0001   |
| 2        | 0010   |
| 3        | 0011   |
| 4        | 0100   |
| 5        | 0101   |
| 6        | 0110   |
| 7        | 0111   |

As per RobotInstruction.h, the velocities are interpreted as follows:
- `v_x` is the forward velocity (negative is backwards)
- `v_y` is the left velocity (negative is right)
- `v_phi` is the clockwise rotational velocity (negative is counterclockwise)


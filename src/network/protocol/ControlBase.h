#ifndef CONTROLBASE_H
#define CONTROLBASE_H
#include "RobotInstruction.h"
#include "ControlPID.h"

class ControlBase
{
public:

    /*
     * Set the team size
     *
     * @param robots number of robots
    */
    virtual void setTeamSize(int robots) = 0;

    /**
     * @brief Sets speed of a robot before sending it
     * @param id ID of the robot
     */
    virtual void setRobotPosSpeed(RobotInstruction instruction) = 0;

    /*
     * Set the command to halt the robot
    */
    virtual void halt(int id) = 0;

    /*
     * Set robot id of a given command id
    */
    virtual void setRobotId(int id, int robotID) = 0;

    virtual ControlPID* getPID() = 0;
};

#endif // CONTROLBASE_H

#include "ControlWrapper.h"
ControlWrapper::ControlWrapper(UdpConnection* proto_net, SerialConnection* serial_net) {
    proto = new ControlProtobuf(proto_net);
    serial = new ControlWire(serial_net);
}

ControlWrapper::~ControlWrapper() {
    delete serial;
    delete proto;
}

void ControlWrapper::setAsync(int id) {
    if (active == proto) {
        proto->setAsync(id);
    }
}

void ControlWrapper::setTeamSize(int size) {
    active->setTeamSize(size);
}

void ControlWrapper::setRobotInstruction(RobotInstruction instruction) {
    active->setRobotPosSpeed(instruction);
}

void ControlWrapper::halt(int id) {
    active->halt(id);
}

void ControlWrapper::setRobotId(int id, int robotID) {
    active->setRobotId(id, robotID);
}

void ControlWrapper::setControl(bool isSerial) {
    if(isSerial) {
        active = serial;
    } else {
        active = proto;
    }
}

ControlPID* ControlWrapper::getPID() {
    return active->getPID();
}

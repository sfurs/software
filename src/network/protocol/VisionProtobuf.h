#ifndef VISIONPROTOBUF_H
#define VISIONPROTOBUF_H
#include "Protobuf.h"
#include "vision_wrapper.pb.h"
#include "UdpConnection.h"
#include <QVector>
#include "RawFieldData.h"
#include <cmath>

class VisionProtobuf : public Protobuf
{
    /*
     * Protobuf Message recieved from the vision server
    */
Q_OBJECT
public:
    VisionProtobuf(UdpConnection* connection);
signals:
    /*
     * Sent on recieving new field positions
     *
     * @param field Unfiltered field position data
     */
    void updateFieldPositions(RawFieldData field);

protected:
    void recvMsg(const QByteArray);
    void recvSignal();
}; 


#endif // VISIONPROTOBUF_H

#include "RefereeProtobuf.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include "ssl_gc_referee_message.pb.h"
#pragma GCC diagnostic pop

RefereeProtobuf::RefereeProtobuf(UdpConnection* connection) : Protobuf(connection), lastCommandSent(0)
{
}

void RefereeProtobuf::recvMsg(const QByteArray binary)
{
    parse<Referee>(binary);
}

static TeamInfo parseTeamInfo(Referee::TeamInfo& info) {
    TeamInfo result;
    result.score = info.score();
    result.red_cards = info.red_cards();
    result.current_yellow_cards = info.yellow_card_times_size();
    if (info.has_max_allowed_bots()) {
        result.max_robots = info.max_allowed_bots();
    } else {
        result.max_robots = 6 - result.red_cards - result.current_yellow_cards;
    }
    result.remaining_timeouts = info.timeouts();
    result.remaining_timeout_time = info.timeout_time();
    result.goalkeeper = info.goalkeeper();
    return result;
}

void RefereeProtobuf::recvSignal()
{
    Referee* msg = static_cast<Referee*>(recv_msg);
    uint64_t packet_sent = msg->packet_timestamp();
    if (packet_sent > lastPacketSent) {
        lastPacketSent = packet_sent;
        uint64_t command_sent = msg->command_timestamp();
        int command = msg->command();
        float x = 0;
        float y = 0;
        if (msg->has_designated_position()) { // for ball placement commands
            x = msg->designated_position().x();
            y = msg->designated_position().y();
        }
        if (command_sent > lastCommandSent) {
            newCommand(command, command_sent, x, y);
        }
        int stage = msg->stage();
        Referee::TeamInfo yellow = msg->yellow();
        Referee::TeamInfo blue = msg->blue();
        
        RefereeData packet;
        packet.command = static_cast<RefereeCommand>(command);
        if(packet.command == RefereeCommand::STOP_GOAL_YELLOW ||
            packet.command == RefereeCommand::STOP_GOAL_BLUE) {
            packet.command = RefereeCommand::STOP;
        }
        if(msg->has_next_command()) {
            packet.next_command = static_cast<RefereeCommand>(msg->next_command());
        } else {
            packet.next_command = RefereeCommand::NO_COMMAND;
        }
        if(msg->has_blue_team_on_positive_half()) {
            packet.is_blue_on_positive = msg->blue_team_on_positive_half();
            packet.has_is_blue_on_positive = true;
        }
        packet.command_time = command_sent;
        packet.command_x = x;
        packet.command_y = y;
        packet.stage = static_cast<GameStage>(stage);
        packet.yellow_team = parseTeamInfo(yellow);
        packet.blue_team = parseTeamInfo(blue);
        
        updateRefereeData(packet);
    }
}

#include "VisionProtobuf.h"
#include "vision_wrapper.pb.h"
#include "RawFieldData.h"
#include <QVector>
#include "Logging.h"


VisionProtobuf::VisionProtobuf(UdpConnection* connection) : 
Protobuf(connection)
{
}
void VisionProtobuf::recvMsg(const QByteArray binary)
{
    parse<SSL_WrapperPacket>(binary); //TODO TF does it do?
}

void VisionProtobuf::recvSignal()
{
    // qCDebug(LOG_NETWORK) << "VisionProtobuf::recvSignal called";
    auto frame = static_cast<SSL_WrapperPacket*>(recv_msg);

    if (frame == nullptr) {
        qCWarning(LOG_NETWORK) << "Empty message";

    } else if (frame->has_detection()){
        QVector<RawRobot> blue_robots;
        QVector<RawRobot> yellow_robots;
        QVector<RawBall> balls;
        auto detect = frame->detection();
        for (int i = 0; i < detect.balls_size(); i++)
        {
            auto ball = detect.balls(i);
            balls.append({ball.x(), ball.y()});
        }

        for (int i = 0; i < detect.robots_yellow_size();i++)
        {
            auto robot = detect.robots_yellow(i);
            yellow_robots.append({robot.robot_id(), robot.x(), robot.y(), robot.orientation()});
        }

        for (int i = 0; i < detect.robots_blue_size();i++)
        {
            auto robot = detect.robots_blue(i);
            blue_robots.append({robot.robot_id(), robot.x(), robot.y(), robot.orientation()});
        }


        RawFieldData field(blue_robots, yellow_robots, balls, 
                    detect.camera_id(), detect.t_sent(), detect.t_capture(), detect.frame_number());
        // qCDebug(LOG_NETWORK) << detect.camera_id() << detect.t_sent() << detect.t_capture() << detect.frame_number();
        // qCDebug(LOG_NETWORK) << "camera id" << detect.camera_id() << "\ntime sent" << detect.t_sent() << "\ntime capture" << detect.t_capture() << "\nframe number" << detect.frame_number();
        qCDebug(LOG_NETWORK) << field;
        emit updateFieldPositions(field);
    }
//    if(frame->has_geometry()){}

}

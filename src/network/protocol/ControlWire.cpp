#include "ControlWire.h"
#include <cstdint>
#include <QByteArray>

// Remove unwanted bits that the compiler may put in
#pragma pack(push, 1)

struct ControlFormat {
    // This is a char and not a bitfield as the compiler may rearrange the order
    // of the elements
    char id_forwardVelocity; // id 4 bits, forward velocity 4 bits
    char leftVelocity_rotationalVelocity; // left velocity 4 bits, rotational velocity 4 bits
    char dribbler_chipper_kicker; // dribbler 2 bits, chipper 2 bits, kicker 4 bits

    // Functions regarding id_forwardVelocity
    void setID(int id) {
        char newValue = id_forwardVelocity & 0xF0; // Clear bits 0-3 (id bits)
        newValue = newValue | (id & 0x0F); // Set new id value (bits 0–3)
        id_forwardVelocity = newValue;
    }
    void setForwardVelocity(int forwardVelocity){
        char newValue = id_forwardVelocity & 0x0F; // Clear bits 4–7 (forward_velocity bits)
        newValue = newValue | ((forwardVelocity & 0x0F) << 4); // Set new forward_velocity value (bits 4-7)
        id_forwardVelocity = newValue;
    }
    void setID_ForwardVelocity(int id, int forwardVelocity){
        id_forwardVelocity = (id & 0x0F) | ((forwardVelocity & 0x0F) << 4);
    }

    // Functions regarding leftVelocity_rotationalVelocity
    void setLeftVelocity(int leftVelocity) {
        char newValue = leftVelocity_rotationalVelocity & 0xF0; // Clear bits 0-3 (left_velocity bits)
        newValue = newValue | (leftVelocity & 0x0F); // Set new left_velocity value (bits 0–3)
        leftVelocity_rotationalVelocity = newValue;
    }
    void setRotationalVelocity(int rotationalVelocity){
        char newValue = leftVelocity_rotationalVelocity & 0x0F; // Clear bits 4–7 (rotational_velocity bits)
        newValue = newValue | ((rotationalVelocity & 0x0F) << 4); // Set new rotational_velocity value (bits 4-7)
        leftVelocity_rotationalVelocity = newValue;
    }
    void setLeftVelocityRotationalVelocity(int leftVelocity, int rotationalVelocity){
        leftVelocity_rotationalVelocity = (leftVelocity & 0x0F) | ((rotationalVelocity & 0x0F) << 4);
    }

    // Functions regarding dribbler_chipper_kicker
    void setDribbler(int dribbler) {
        char newValue = dribbler_chipper_kicker & 0xFC; // Clear bits 0–1 (dribbler bits)
        newValue = newValue | (dribbler & 0x03);        // Set new dribbler value (bits 0–1)
        dribbler_chipper_kicker = newValue;
    }

    void setChipper(int chipper) {
        char newValue = dribbler_chipper_kicker & 0xF3; // Clear bits 2–3 (chipper bits)
        newValue = newValue | ((chipper & 0x03) << 2);  // Set new chipper value (bits 2–3)
        dribbler_chipper_kicker = newValue;
    }

    void setKicker(int kicker) {
        char newValue = dribbler_chipper_kicker & 0x0F; // Clear bits 4–7 (kicker bits)
        newValue = newValue | ((kicker & 0x0F) << 4);   // Set new kicker value (bits 4–7)
        dribbler_chipper_kicker = newValue;
    }

    void setDribblerChipperKicker(int dribbler, int chipper, int kicker) {
        dribbler_chipper_kicker = (dribbler & 0x03) | ((chipper & 0x03) << 2) | ((kicker & 0x0F) << 4);
    }
};

#pragma pack(pop)


ControlWire::ControlWire(SerialConnection* connection) : connection(connection) {
    calibration.x.updateParameters(0,0,0,0,0);
    calibration.y.updateParameters(0,0,0,0,0);
    calibration.rotate.updateParameters(0,0,0,0,0);
}

ControlWire::~ControlWire() {
    delete[] msgs;
}

void ControlWire::setTeamSize(int robots) {
    // This is hardcoded into the format
    robots = 6;
    msgs = new struct ControlFormat[robots];
    msgsSize = robots;
    for (int i = 0;i<robots;i++) {
        //TODO this may cause an unexpected failure mode if setRobotId is not called on all robots
        msgs[i].id_forwardVelocity = 0;
        msgs[i].leftVelocity_rotationalVelocity = 0;
        msgs[i].dribbler_chipper_kicker = 0;
    }
}

void ControlWire::setRobotId(int id, int robotID) {
    msgs[id].setID(robotID);
}

int ControlWire::boundVelocity(int velocity) {
    if (velocity > 7) {
        return 7;
    }
    if (velocity < -7) {
        return -7;
    }
    return velocity;
}

int ControlWire::boundID(int id) {
    if (id > 15) {
        return 15;
    }
    if (id < 0) {
        return 0;
    }
    return id;
}

int ControlWire::boundDribbler(int dribbler) {
    if (dribbler > 3) {
        return 3;
    }
    if (dribbler < 0) {
        return 0;
    }
    return dribbler;
}

int ControlWire::boundChipper(int chipper) {
    if (chipper > 3) {
        return 3;
    }
    if (chipper < 0) {
        return 0;
    }
    return chipper;
}

int ControlWire::boundKicker(int kicker) {
    if (kicker > 15) {
        return 15;
    }
    if (kicker < 0) {
        return 0;
    }
    return kicker;
}

void ControlWire::setRobotPosSpeed(RobotInstruction instruction) {
    int index = instruction.id;
    if (index >= msgsSize) {
        qWarning("Invalid robot id");
    }
    ControlFormat* packet = &(msgs[index]);

    int dribbler = instruction.dribble;
    int chipper = instruction.chipper;
    int kicker = instruction.kick;
    int forward_velocity = static_cast<int>(instruction.v_x/2.f);
    int left_velocity = static_cast<int>(instruction.v_y/2.f);
    int rotate_velocity = static_cast<int>(instruction.v_phi/2.f);

    packet->setForwardVelocity(boundVelocity(forward_velocity));
    packet->setLeftVelocityRotationalVelocity(boundVelocity(left_velocity), boundVelocity(rotate_velocity));
    packet->setDribblerChipperKicker(boundDribbler(dribbler), boundChipper(chipper), boundKicker(kicker));

    sendAsync();
}

void ControlWire::halt(int id) {
    RobotInstruction instruction;
    instruction.id = id;
    instruction.v_x = 0;
    instruction.v_y = 0;
    instruction.v_phi = 0;
    instruction.kick = 0;
    instruction.dribble = 0;
    instruction.chipper = 0;
    setRobotPosSpeed(instruction);
}

void ControlWire::sendAsync() {
    QByteArray msg (reinterpret_cast<char*>(msgs), sizeof(ControlFormat)*msgsSize);
    connection->sendSerial(msg);
}

ControlPID* ControlWire::getPID() {
    return &calibration;
}

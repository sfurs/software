#include "netmaster.h"
#include "../worldstate/worldStateManager.h"
#include "ControlPID.h"
#include "Team.h"
#include "protocol/ControlWrapper.h"
#include "protocol/VisionProtobuf.h"
#include "protocol/TeamProtobuf.h"
#include "protocol/RefereeProtobuf.h"
#include "filter/ObjectTracker.h"
#include "filter/InputSanitizer.h"
#include <QThread>
#include <qloggingcategory.h>
#include "Logging.h"

NetMaster::NetMaster(WorldStateManager *wsm) : NetMaster(6)
{
    worldStateManager = wsm;
}

NetMaster::NetMaster(int t) : timer(this), network(new UdpConnection(this)), refereeNetwork(new UdpConnection(this)), gameControllerNetwork(new TcpConnection(this)),
                              robotControlNetwork(new SerialConnection(this)), objectTracker(new ObjectTracker()), fieldVisionFilter(new InputSanitizer(objectTracker))
{
    qCDebug(LOG_NETWORK) << "NetMaster constructor start";

    vision = new VisionProtobuf(network);
    qCDebug(LOG_NETWORK) << "VisionProtobuf created";

    control = new ControlWrapper(network, robotControlNetwork);
    qCDebug(LOG_NETWORK) << "ControlWrapper created";

    gameController = new TeamProtobuf(gameControllerNetwork);
    qCDebug(LOG_NETWORK) << "TeamProtobuf created";

    referee = new RefereeProtobuf(refereeNetwork);
    qCDebug(LOG_NETWORK) << "RefereeProtobuf created";

    teamSize = t;
    qCDebug(LOG_NETWORK) << "Team size set to" << teamSize;

    // Set up simulator first
    control->setControl(false);
    isSerial = false;
    control->setTeamSize(teamSize);
    for (int i = 0; i < t; i++)
    {
        control->setAsync(i);
        control->setRobotId(i, i);
    }

    // if serial exists and is enabled, set it up
#ifdef ENABLE_SERIAL
    control->setControl(true);
    isSerial = true;
    control->setTeamSize(teamSize);
    for (int i = 0; i < t; i++)
    {
        control->setAsync(i);
        control->setRobotId(i, i);
    }
    serialEnabled = true;
#endif

    qCDebug(LOG_NETWORK) << "Control setup complete";

    connect(vision, &VisionProtobuf::updateFieldPositions, this, &NetMaster::updateFieldPositions);
    connect(referee, &RefereeProtobuf::updateRefereeData, this, &NetMaster::updateRefereeData);
    qCDebug(LOG_NETWORK) << "Connections set up";
    connect(&timer, &QTimer::timeout, this, &NetMaster::updateField);
    timer.start(1000 / 180);

    initNetworks();

    qCDebug(LOG_NETWORK) << "NetMaster constructor complete";
}

void NetMaster::initNetworks()
{
    qCDebug(LOG_NETWORK) << "Initializing networks";
    qCDebug(LOG_NETWORK) << "Referee IP:" << m_refereeIP << "Port:" << m_refereePort;
    qCDebug(LOG_NETWORK) << "Vision IP:" << m_visionIP << "Port:" << m_visionPort;
    qCDebug(LOG_NETWORK) << "Control IP:" << m_controlIP << "Blue Port:" << m_controlBluePort << "Yellow Port:" << m_controlYellowPort;
#ifdef OS_NAME
    qCDebug(LOG_NETWORK) << "OS Name:" << OS_NAME;
    if (OS_NAME == "Darwin")
    {
        m_robotControlPort = "tty.usbserial-B0044HVQ"; // TODO: check if this is the correct port for the DSD TECH SH-U09C5 USB to TTL UART Converter in all instances
    }
    else if (OS_NAME == "Linux")
    {
        m_robotControlPort = "ttyUSB0";
    }
    else
    {
        qWarning() << "Unsupported OS:" << OS_NAME;
    }
#else
    qCDebug(LOG_NETWORK) << "OS Name not defined";
#endif
    qCDebug(LOG_NETWORK) << "Robot Control Port:" << m_robotControlPort << "Baud Rate:" << m_robotControlBaudRate;

    refereeNetwork->connectRecv(QHostAddress(m_refereeIP), m_refereePort);
    robotControlNetwork->connect(m_robotControlPort, m_robotControlBaudRate);
    network->connectRecv(QHostAddress(m_visionIP), m_visionPort);
    if (friendlyTeam == BLUE)
    {
        network->connectSend(QHostAddress(m_controlIP), m_controlBluePort);
    }
    else if (friendlyTeam == YELLOW)
    {
        network->connectSend(QHostAddress(m_controlIP), m_controlYellowPort);
    }
    else {
        qCDebug(LOG_NETWORK) << "Cannot connect to a team's port: friendlyTeam is INVALID";
    }

    qCDebug(LOG_NETWORK) << "Network initialization complete";
}

NetMaster::~NetMaster()
{
    delete vision;
    delete control;
    delete gameController;
}

ControlPID *NetMaster::getPID()
{
    return control->getPID();
}

void NetMaster::SetRobotInstruction(RobotInstruction instruction)
{
    control->setRobotInstruction(instruction);
}

void NetMaster::updateFieldPositions(RawFieldData raw_data)
{
    fieldVisionFilter->filterField(raw_data);
}

void NetMaster::updateField()
{
    objectTracker->updatePrediction();
    FieldData field_positions = objectTracker->getFieldData();
    worldStateManager->createWorldState(
        field_positions.getBall(),
        field_positions.getRobots(BLUE),
        field_positions.getRobots(YELLOW),
        field_positions.getCaptureTime());
    // qCDebug(LOG_NETWORK) << "Field positions updated";
}

void NetMaster::updateRefereeData(RefereeData data)
{
    worldStateManager->uploadRefereeData(data);
}

void NetMaster::stop()
{
    timer.stop();
    QThread::currentThread()->exit();
}

void NetMaster::setRefereeIP(const QString &ip)
{
    m_refereeIP = ip;
    refereeNetwork->connectRecv(QHostAddress(m_refereeIP), m_refereePort);
}

void NetMaster::setRefereePort(quint16 port)
{
    m_refereePort = port;
    refereeNetwork->connectRecv(QHostAddress(m_refereeIP), m_refereePort);
}

void NetMaster::setVisionIP(const QString &ip)
{
    m_visionIP = ip;
    network->connectRecv(QHostAddress(m_visionIP), m_visionPort);
}

void NetMaster::setVisionPort(quint16 port)
{
    m_visionPort = port;
    network->connectRecv(QHostAddress(m_visionIP), m_visionPort);
}

void NetMaster::setControlIP(const QString &ip)
{
    m_controlIP = ip;
    if (friendlyTeam == BLUE)
    {
        network->connectSend(QHostAddress(m_controlIP), m_controlBluePort);
    }
    else
    {
        network->connectSend(QHostAddress(m_controlIP), m_controlYellowPort);
    }
}

void NetMaster::setControlBluePort(quint16 port)
{
    m_controlBluePort = port;
    if (friendlyTeam == BLUE)
    {
        network->connectSend(QHostAddress(m_controlIP), m_controlBluePort);
    }
}

void NetMaster::setControlYellowPort(quint16 port)
{
    m_controlYellowPort = port;
    if (friendlyTeam == YELLOW)
    {
        network->connectSend(QHostAddress(m_controlIP), m_controlYellowPort);
    }
}

void NetMaster::setRobotControlPort(const QString &port)
{
    m_robotControlPort = port;
    robotControlNetwork->connect(m_robotControlPort, m_robotControlBaudRate);
}

void NetMaster::setRobotControlBaudRate(int baudRate)
{
    m_robotControlBaudRate = baudRate;
    robotControlNetwork->connect(m_robotControlPort, m_robotControlBaudRate);
}

void NetMaster::setFriendlyTeam(Team team)
{
    friendlyTeam = team;
    if (friendlyTeam == BLUE)
    {
        worldStateManager->setFriendlyTeam(Team::BLUE);
        network->connectSend(QHostAddress(m_controlIP), m_controlBluePort);
    }
    else
    {
        worldStateManager->setFriendlyTeam(Team::YELLOW);
        network->connectSend(QHostAddress(m_controlIP), m_controlYellowPort);
    }
}

QString NetMaster::getRefereeIP()
{
    return m_refereeIP;
}

quint16 NetMaster::getRefereePort()
{
    return m_refereePort;
}

QString NetMaster::getVisionIP()
{
    return m_visionIP;
}

quint16 NetMaster::getVisionPort()
{
    return m_visionPort;
}

QString NetMaster::getControlIP()
{
    return m_controlIP;
}

// quint16 NetMaster::getControlPort() {
//     return m_controlPort;
// }

quint16 NetMaster::getControlBluePort()
{
    return m_controlBluePort;
}

quint16 NetMaster::getControlYellowPort()
{
    return m_controlYellowPort;
}

QString NetMaster::getRobotControlPort()
{
    return m_robotControlPort;
}

int NetMaster::getRobotControlBaudRate()
{
    return m_robotControlBaudRate;
}

Team NetMaster::getFriendlyTeam()
{
    return friendlyTeam;
}

bool NetMaster::getIsSerial()
{
    return isSerial;
}

void NetMaster::setIsSerial(bool isSerial)
{
    this->isSerial = isSerial;
    control->setControl(isSerial);
}

bool NetMaster::getSerialEnabled()
{
    return serialEnabled;
}

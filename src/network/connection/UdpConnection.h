#ifndef UDPCONNECTION_H
#define UDPCONNECTION_H

#include "Connection.h"
#include <QObject>
#include <QUdpSocket>
#include <QNetworkDatagram>

class UdpConnection : public Connection
{
Q_OBJECT
public:
    /*
     * Creates an UdpConnection with two unconnected udp sockets, one for receiving and another for sending datagrams.
    */
    UdpConnection(QObject *parent);
    ~UdpConnection();
    /*
     * Set or update the address and port of the receiving socket
     *
     * @param addr the address to connect to receiving socket
     * @param port the port to connect to receiving socket 
    */
    void connectRecv(QHostAddress addr, quint16 port);
    /*
     * Set or update the address and port of the sending socket
     *
     * @param addr the address to connect to sending socket
     * @param port the port to connect to sending socket 
    */
    void connectSend(QHostAddress addr, quint16 port);
    /*
     * Send a msg using the sending socket
     *
     * @param msg the data to send to the socket
     * @return true if msg was sent successfully, false otherwise
    */
    bool sendUdp(QByteArray msg);
    Connection * getConnection();
/*signals:
    / 
     * Sent on error with the receiving socket
     *
     * @param error the error that occurred
    /
    void recvErrorOccurred(QAbstractSocket::SocketError error);
    /
     * Sent on error with the sending socket
     *
     * @param error the error that occurred
    /
    void sendErrorOccurred(QAbstractSocket::SocketError error);
*/
//private slots:
    /*
     * handleRecv - handles the receiving socket data, buffers the data, then calls the recvMsg signal
    */
    void handleRecv();

private:
    QUdpSocket * recvSocket;
    QUdpSocket * sendSocket;
    bool sendConnected;
    QHostAddress sendAddr;
    quint16 sendPort;
};

#endif // UDPCONNECTION_H

#ifndef CONNECTION_H
#define CONNECTION_H
#include <QObject>
#include <QAbstractSocket>
#include <functional>

class Connection : public QObject
{
Q_OBJECT
public:
    Connection(QObject* parent);

    /*
     * Returns the current send function
    */
    const std::function<void(QByteArray)> sendMsg();

signals:
    void recvMsg(const QByteArray msg);
    /* 
     * Sent on error with the receiving socket
    */
    void recvErrorOccurred(QAbstractSocket::SocketError error);
    /*
     * Sent on error with the sending socket
    */
    void sendErrorOccurred(QAbstractSocket::SocketError error);
    /*
     * Sent on error with the sending socket
    */
    void errorOccurred(QAbstractSocket::SocketError error);

protected:
    /*
     * Update the send function for this connection type
     *
     * @param func the new send function 
    */
    void setSend(std::function<void(QByteArray)> func);

private:
    std::function<void(QByteArray)> sendFunc;

};

#endif // CONNECTION_H

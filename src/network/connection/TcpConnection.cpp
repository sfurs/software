#include "TcpConnection.h"

TcpConnection::TcpConnection(QObject *parent) : Connection(parent), connected(false)
{
    setSend([this](QByteArray msg){sendTcp(msg);});
    socket = new QTcpSocket(this);
}

void TcpConnection::connect(QHostAddress addr, quint16 port)
{
    if (socket->state() != QAbstractSocket::UnconnectedState) {
        delete socket;
        socket = new QTcpSocket(this);
    }
    QObject::connect(socket, &QTcpSocket::errorOccurred, this, &Connection::recvErrorOccurred);
    this->addr = addr;
    this->port = port;
    socket->connectToHost(addr, port);
    QObject::connect(socket, &QTcpSocket::readyRead, [this]{handleRecv();});
    connected = true;
}

bool TcpConnection::sendTcp(QByteArray msg)
{
    if (!connected)
    {
        return false;
    }
    qint64 bytesWritten = socket->write(msg);
    if (bytesWritten < 0)
    {
        return false;
    }
    return true;
}

void TcpConnection::handleRecv()
{
    QByteArray data = socket->readAll();
    recvMsg(data);
}

TcpConnection::~TcpConnection()
{
    delete socket;
}

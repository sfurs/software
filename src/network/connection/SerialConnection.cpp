#include "SerialConnection.h"
#include <QIODevice>

SerialConnection::SerialConnection(QObject* parent) : Connection(parent), connected(false)
{
    setSend([this](QByteArray msg) {sendSerial(msg);});
#if ENABLE_SERIAL
    port = new QSerialPort(this);
#endif // ENABLE_SERIAL
}

SerialConnection::~SerialConnection() {
#if ENABLE_SERIAL
    delete port;
#endif // ENABLE_SERIAL
}

bool SerialConnection::connect(QString name, quint32 baud)
{
    if (connected) {
        //TODO add code for reconnecting
    }
#if ENABLE_SERIAL
    port->setPortName(name);
    port->setBaudRate(baud);
    if(port->open(QIODeviceBase::ReadWrite)) {
        connected = true;
        return true;
    }
#else
    // TODO warn that serial connection support was not compiled in
#endif // ENABLE_SERIAL
    return false;
}

bool SerialConnection::sendSerial(QByteArray msg) {
    if (!connected)
    {
        return false;
    }
    qint64 bytesWritten = 0;
#if ENABLE_SERIAL
    bytesWritten = port->write(msg);
#endif // ENABLE_SERIAL
    if (bytesWritten < 0)
    {
        return false;
    }
    return true;
} 

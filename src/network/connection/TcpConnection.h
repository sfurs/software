#ifndef TCPCONNECTION_H
#define TCPCONNECTION_H

#include "Connection.h"
#include <QObject>
#include <QTcpSocket>
#include <QNetworkDatagram>

class TcpConnection : public Connection
{
Q_OBJECT
public:
    /*
     * Creates an TcpConnection with a unconnected tcp socket.
    */
    TcpConnection(QObject *parent);
    ~TcpConnection();
    /*
     * Set or update the address and port of the socket
     *
     * @param addr the address to connect to socket
     * @param port the port to connect to socket 
    */
    void connect(QHostAddress addr, quint16 port);
    /*
     * Send a msg using the sending socket
     *
     * @param msg the msg to send
     * @return true if msg was sent successfully, false otherwise
    */
    bool sendTcp(QByteArray msg);
private:
    void handleRecv();
    QTcpSocket * socket;
    bool connected;
    QHostAddress addr;
    quint16 port;
};

#endif // TCPCONNECTION_H

#include "Connection.h"

Connection::Connection(QObject *parent) : QObject(parent), sendFunc(nullptr)
{}

void Connection::setSend(std::function<void(QByteArray)> func) {
    sendFunc = func;
}

const std::function<void(QByteArray)> Connection::sendMsg() {
    return sendFunc;
}


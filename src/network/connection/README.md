

## Files

### 1. `UdpConnection.cpp` & `UdpConnection.h`

- **UdpConnection Class**:  
  The `UdpConnection` class manages UDP connections for both sending and receiving data through multicast groups. It handles creating sockets, binding them to addresses and ports, and processing incoming and outgoing messages.

  - **Functions**:

    - `UdpConnection(QObject* parent)`  
      **Constructor**: Initializes the `UdpConnection` class by setting up the `recvConnected` and `sendConnected` flags, and creating two `QUdpSocket` instances for receiving and sending data.

    - `void connectRecv(QHostAddress addr, quint16 port)`  
      **Function**: Connects the receiving socket to a multicast group at the specified IP address and port. It binds the `UdpConnection` object to the provided address and port, allowing it to receive datagrams from clients like `GrSim` or the SSL networking client. A signal-slot connection is established between the socket's `readyRead()` signal and the `handleRecv()` slot to process incoming data.

    - `void connectSend(QHostAddress addr, quint16 port)`  
      **Function**: Connects the sending socket to a multicast group at the specified IP address and port. Similar to `connectRecv()`, but this function sets the socket up for sending data instead of receiving.

    - `void sendMsg(QByteArray msg)`  
      **Function**: Sends the provided `QByteArray` message through the sending socket. The message should be formatted according to the communication protocol used by the SSL client (TODO: Add further documentation regarding the protocol).

    - `void handleRecv()`  
      **Function**: Reads incoming data from the receiving socket and emits the `recvMsg()` signal with the buffered data.

    - **Signals**:

      - `recvMsg(QByteArray msg)`  
        **Signal**: Emitted whenever the receiving socket gets data. The buffered data is passed along with this signal, which can be connected to a slot in another class to handle the incoming data and perform necessary actions.

  - **Basic Usage**:

    1. **Initialization**: Create an instance of the `UdpConnection` class with the desired IP address and port.

    2. **Signal-Slot Connection**: Use Qt's `connect` function to link signals from `UdpConnection` to slots in other classes.

    Example:
    ```cpp
    connect(network, &UdpConnection::recvMsg, this, &MainWindow::RobotRecvMsg);
    ```

    - **Explanation**: In this example, the `recvMsg` signal from the `UdpConnection` object (`network`) is connected to the `RobotRecvMsg` slot in the `MainWindow` object (`this`). When a message is received by the `UdpConnection`, the `recvMsg` signal is emitted, triggering the `RobotRecvMsg` slot with the received message as an argument.

### 2. `SerialConnection.cpp` & `SerialConnection.h`

- **SerialConnection Class**:  
  The `SerialConnection` class manages communication over a serial port. It inherits from the `Connection` class and uses Qt's `QObject` for signal-slot mechanisms. 

  - **Constructor & Destructor**:
  
    - `SerialConnection(QObject* parent)`  
      **Constructor**: Initializes an unconnected `SerialConnection` object, optionally specifying a parent QObject.

    - `~SerialConnection()`  
      **Destructor**: Cleans up and closes the serial connection when the object is destroyed.

  - **Functions**:

    - `bool connect(QString name, quint32 baud)`  
      Sets or updates the serial device and baud rate.  
      - **Parameters**:
        - `name`: The name of the serial device (e.g., `COM1`, `/dev/ttyUSB0`).
        - `baud`: The baud rate for the serial connection.
      - **Returns**:  
        `true` if the connection was successfully established, `false` otherwise.

    - `bool sendSerial(QByteArray msg)`  
      Sends a message over the serial connection.  
      - **Parameters**:  
        - `msg`: The data to send.
      - **Returns**:  
        `true` if the message was successfully sent, `false` otherwise.

  - **Private Functions**:

    - `void handleRecv()`  
      Handles receiving data from the serial connection. This function is used internally to process incoming messages.




#ifndef SERIALCONNECTION_H
#define SERIALCONNECTION_H

#include "Connection.h"
#include <QObject>
#include <QString>
#include <QByteArray>
#if ENABLE_SERIAL
#include <QSerialPort>
#endif // ENABLE_SERIAL

class SerialConnection : public Connection
{
Q_OBJECT
public:
    /*
     * Creates an unconnected SerialConnection
    */
    SerialConnection(QObject * parent);
    ~SerialConnection();

    /*
     * Set or updates the serial device and baud
     *
     * @param name the name of the serial device
     * @param baud the baud of the device
    */
    bool connect(QString name, quint32 baud);
    
    /*
     * Send a msg to the serial connection
     *
     * @param msg the data to send
     * @return true if the msg was sent successfully, false otherwise
    */
    bool sendSerial(QByteArray msg);
private:
    void handleRecv();
    QString name;
#if ENABLE_SERIAL
    QSerialPort * port;
#endif // ENABLE_SERIAL
    bool connected;
};

#endif /* SERIALCONNECTION_H */

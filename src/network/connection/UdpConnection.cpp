#include "UdpConnection.h"

UdpConnection::UdpConnection(QObject *parent) : Connection(parent), sendConnected(false)
{
    setSend([this](QByteArray msg) {sendUdp(msg);});
    recvSocket = new QUdpSocket(this);
    sendSocket = new QUdpSocket(this);
}

void UdpConnection::connectRecv(QHostAddress addr, quint16 port)
{
    if (recvSocket->state() != QAbstractSocket::UnconnectedState) {
        delete recvSocket;
        recvSocket = new QUdpSocket(this);
    }
    QObject::connect(recvSocket, &QUdpSocket::errorOccurred, this, &Connection::recvErrorOccurred);
    recvSocket->bind(addr, port, QAbstractSocket::ShareAddress | QAbstractSocket::ReuseAddressHint);
    recvSocket->joinMulticastGroup(addr);
    QObject::connect(recvSocket, &QUdpSocket::readyRead, [this]{handleRecv();});
}

void UdpConnection::connectSend(QHostAddress addr, quint16 port)
{
    QObject::connect(sendSocket, &QUdpSocket::errorOccurred, this, &Connection::sendErrorOccurred);
    sendAddr = addr;
    sendPort = port;
    sendConnected = true;
}

bool UdpConnection::sendUdp(QByteArray msg)
{
    if (!sendConnected)
    {
        return false;
    }
    qint64 bytesWritten = sendSocket->writeDatagram(msg, sendAddr, sendPort);
    if (bytesWritten < 0)
    {
        return false;
    }
    return true;
}

void UdpConnection::handleRecv()
{
    QByteArray data;
    while (recvSocket->hasPendingDatagrams())
    {
        QNetworkDatagram datagram = recvSocket->receiveDatagram();
        data.append(datagram.data());
    }
    recvMsg(data);
}

UdpConnection::~UdpConnection()
{
    delete recvSocket;
    delete sendSocket;
}

## Overview: GUI

This folder includes core visual components of the system, such as the graphical rendering of robots, ball, and the game field.

## Files

### 1. `VisualBall.cpp` & `VisualBall.h`
- **VisualBall Class**:  
  The `VisualBall` class represents the visual rendering of the ball within the game. It manages the ball’s position, appearance, and visibility based on its location on the field.

  - **Functions**:
    - `VisualBall()`: Default constructor initializing the ball at position `(0, 0, 0)` with a red color.
    - `VisualBall(qreal x, qreal y, qreal z)`: Constructor that initializes the ball at a specified position.
    - `QRectF boundingRect() const`: Defines the bounding rectangle of the ball for collision detection.
    - `QPainterPath shape() const`: Specifies the shape of the ball as a circle.
    - `void paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)`: Renders the ball using the specified color and radius.
    - `void updateVisuals(float x, float y)`: Updates the ball’s position and visibility based on its location within the field.

### 2. `VisualRobot.cpp` & `VisualRobot.h`
- **VisualRobot Class**:  
  The `VisualRobot` class is responsible for rendering the robots on the field. It updates their positions, angles, and visual appearance, including a label showing each robot’s role and action.
  - **Extra Function**:
    - qreal normalizeAngle(qreal angle) : Normalizes the angle using two pi and returns angle.
  - **Functions**:
    - `VisualRobot()`: Default constructor initializing the robot with a random color and orientation.
    - `VisualRobot(Team team, int numRobot, qreal x, qreal y, qreal angle, QString role, QString action)`: Constructor that initializes a robot with team, robot number, position, angle, role, and action.
    - `QRectF boundingRect() const`: Defines the bounding rectangle of the robot for collision detection.
    - `QPainterPath shape() const`: Specifies the shape of the robot, including its circular body and flat front.
    - `void paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)`: Renders the robot’s body and nose, depending on its team color.
    - `void updateVisualRobot(float x, float y, float angle)`: Updates the robot’s position and orientation.

- **RobotLabel Class**:  
  The `RobotLabel` class provides a text label to display each robot’s role and action above it.

  - **Functions**:
    - `RobotLabel()`: Default constructor initializing the label with default text values.
    - `RobotLabel(QString r, QString a, qreal x, qreal y)`: Constructor to set the label’s role, action, and position.
    - `QRectF boundingRect() const`: Defines the bounding rectangle for the label.
    - `void paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)`: Renders the robot’s role and action as text.

### 3. `GameScene.cpp` & `GameScene.h`
- **GameScene Class**:  
  The `GameScene` class represents the overall game field, including visual elements like robots, balls, and field lines. It updates these elements based on the world state data and handles rendering the background, including the field lines and goals.

  - **Functions**:
    - `GameScene(QGraphicsView *gameView, QLCDNumber *gameTime)`: Constructor that initializes the scene with a view for the game and a clock display for the game time.
    - `~GameScene()`: Destructor that cleans up the dynamically allocated visual components (robots and ball).
    - `void drawBackground(QPainter *painter, const QRectF &rect) override`: Draws the game field’s background, including the turf, field lines, and goals.
    - `void updateVisualRobots()`: Updates the positions of the visual robots and the ball based on the current world state data.
    - `VisualRobot* getVisualRobot(int id)`: Retrieves a specific `VisualRobot` object by its ID, used to update or manipulate individual robots on the field.

### 4. `CMakeLists.txt`
- This is the build configuration file for the GUI components using CMake.
- It includes GUI-related `.cpp` and `.h` files and links dependencies such as `Qt6 Widgets` and `Qt6 Core`.
- The `CMakeLists.txt` ensures that the GUI components are compiled as part of the overall project and integrated into the `skynet` executable.
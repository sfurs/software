#ifndef VISUALPOINT_H
#define VISUALPOINT_H

#include <QGraphicsItem>

class VisualPoint : public QGraphicsItem
{
public:
    /**
     * @brief Visual representation of the ball[s]
     */
    VisualPoint();
    VisualPoint(qreal x,qreal y,qreal z);

    /*
     * Estimate painting area of the ball
     * @return a QRectF with x,y,width, and height starting at the top left corner
    */
    QRectF boundingRect() const override;

    /*
     * Specify ball hit box used for detecting collisions
    */
    QPainterPath shape() const override;

    /*
     * Specify what a ball looks like
    */
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    void updateVisuals(float x, float y, float z, bool isFlipped);

protected:

private:
    qreal x;
    qreal y;
    qreal z = 0;
    QColor color;
};

#endif // VISUALROBOT_H

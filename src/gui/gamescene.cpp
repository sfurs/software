#include <QGraphicsScene>
#include <QPainter>
#include <QRandomGenerator>
#include <QStyleOption>
#include <QtMath>
#include <QTime>
#include <iostream>
#include "gamescene.h"
#include "QtWidgets/qgraphicsview.h"
#include "visualrobot.h"
#include "FieldData.h"
#include "Team.h"
#include "Logging.h"
#include "../agent/pathplanner/Grid.h"
#include "../agent/AgentMaster.h"

GameScene::GameScene(WorldStateManager *wsm, QGraphicsView *gameView, QLCDNumber *gameTimePtr, Navigator *navigator,const QString &mode) 
    : QGraphicsScene(gameView), currentMode(mode), grid(navigator->getGrid()->getGrid())
{
    worldStateManager = wsm;
    this->navigator = navigator;
    connect(navigator, &Navigator::plotPoints, this, &GameScene::plotPoints);
    gameTime = gameTimePtr;
    gameTime->setDigitCount(9); // 9 digits (3 for seconds, 3 for milliseconds, 3 for microseconds
    gameTime->setSmallDecimalPoint(true);
    // cache the scaled turf as QImage::scaled is a very expensive operation
    loadTurfImage();
    loadHexImage();
    // Swap width and height for the rotated scene
    this->setSceneRect(0, 0, height, width);           // origin is placed at top left corner for scene coordinates
    this->setItemIndexMethod(QGraphicsScene::NoIndex); // alternatively use BspTreeIndex which may run slower for updating robots
    int n = teamSize;
    for (Team team = BLUE; team != INVALID;)
    {
        for (int i = 0; i < n; i++)
        {
            int x = (width - ccDiameter) / 2;
            int y = (height - ccDiameter) / 2;

            if (team == BLUE)
            {
                x += 1000;
                y += (i + 1) * 200 - 1000;
            }
            else if (team == YELLOW)
            {
                x -= 1000;
                y += (i + 1 + n) * 200 - 1000;
            }

            VisualRobot *robot = new VisualRobot(team, i, x, y, 0, "tempRole", "tempAction");
            if (team == BLUE)
            {
                blueVisualRobots.append(robot);
            }
            else
            {
                yellowVisualRobots.append(robot);
            }

            robot->setFlag(QGraphicsItem::ItemIsMovable);

            this->addItem(robot);
            qCDebug(LOG_GUI) << "Robot added to the game screen: " << i;

            RobotLabel *label = robot->getRobotLabel();
            this->addItem(label); // adding robot label to game screen
        }
        if (team == BLUE)
        {
            team = YELLOW;
        }
        else
        {
            team = INVALID;
        }
    }
    int x = (width - ccDiameter) / 2;
    int y = (height - ccDiameter) / 2;
    visualBall = new VisualBall(x, y, 0);
    this->addItem(visualBall);
    qCDebug(LOG_GUI) << "All robots should be visible";
}

GameScene::~GameScene()
{
    delete visualBall;
    for (VisualRobot *robot : blueVisualRobots)
    {
        delete robot;
    }
    for (VisualRobot *robot : yellowVisualRobots)
    {
        delete robot;
    }
}

// Add this new method
void GameScene::loadTurfImage()
{
    QString turfPath = currentMode == "dark" ? ":/turf-dark.jpg" : ":/turf-light.jpg";
    turf = QImage(turfPath);
    // Swap width and height for rotation
    turf = turf.scaled(height, width, Qt::IgnoreAspectRatio);
    update();
}

// Add this new method
void GameScene::loadHexImage()
{
    hexImage = QImage(":/hex_no_space.jpg");
    update();
}

void GameScene::setDisplayHexGrid(bool display)
{
    displayHexGrid = display;

    update(); // Trigger a repaint to apply changes
}

// Add this new method
void GameScene::updateMode(const QString &mode)
{
    if (currentMode != mode)
    {
        currentMode = mode;
        loadTurfImage();
    }
}

void GameScene::updateVisualRobots()
{
    WorldState ws = worldStateManager->getCurrentState();

    // max 3 digits for seconds, 3 digits for milliseconds, 3 digits for microseconds
    gameTime->display(QString::number(ws.capture_time - (((int)ws.capture_time) / 1000) * 1000, 'f', 6));

    // qCDebug(LOG_GUI) << "BlueBotPos: " << ws.blueRobotData.size() << '\t';
    if (ws.capture_time >= 0)
    {
        for (int i = 0; i < ws.blueRobotData.size() && i < teamSize; i++)
        {
            blueVisualRobots[i]->updateVisualRobot(ws.blueRobotData[i].x, ws.blueRobotData[i].y, ws.blueRobotData[i].angle, isFlipped);
        }

        // qCDebug(LOG_GUI) << "YellowBotPos: " << ws.yellowRobotData.size() << '\t';
        for (int i = 0; i < ws.yellowRobotData.size() && i < teamSize; i++)
        {

            if (i >= teamSize)
            {
                break;
            }
            // Remove coordinate swapping here
            yellowVisualRobots[i]->updateVisualRobot(ws.yellowRobotData[i].x, ws.yellowRobotData[i].y, ws.yellowRobotData[i].angle, isFlipped);
            update();
        }
        // Remove coordinate swapping for the ball
        visualBall->updateVisuals(ws.ballData.x, ws.ballData.y, ws.ballData.z, isFlipped);
        // qCDebug(LOG_GUI) << "updateVisualRobots called";
        for (int id : visualPoints.keys()) {
            for (int i = 0; i < visualPoints[id].size(); i++) {
                if (i < path[id].size() && displayHexGrid) {
                    visualPoints[id][i]->updateVisuals(path[id][i]->centerX, path[id][i]->centerY, 0, isFlipped);
                } else {
                    visualPoints[id][i]->updateVisuals(-10400, -7400, 0, isFlipped);
                }
            }
        }
    }
    else
    {
        for (int i = 0; i < teamSize; i++)
        {
            blueVisualRobots[i]->updateVisualRobot(-10400, -7400, 0, isFlipped);
            yellowVisualRobots[i]->updateVisualRobot(-10400, -7400, 0, isFlipped);
        }
        visualBall->updateVisuals(-10400, -7400, 0, isFlipped);
    }
}

void GameScene::drawBackground(QPainter *painter, const QRectF &rect)
{
    // Save the current painter state
    painter->save();

    // Translate and rotate the painter
    painter->translate(height, 0);
    painter->rotate(90);

    // Draw the turf
    painter->fillRect(QRectF(0, 0, width, height), turf);

    // Conditionally draw the hex grid
    if (displayHexGrid)
    {
        for (int row = 0; row < grid.size() - 1; ++row)
        {
            for (int col = 0; col < grid[row].size(); ++col)
            {
                const Hexagon &hex = grid[row][col];

                // Optionally, output the information for debugging (if needed)
                // std::cout << "row: " << hex.indX << ", col: " << hex.indY << " - ";
                // std::cout << "x: " << hex.centerX << ", y: " << hex.centerY << " | ";

                float scaleFactor = 2.5; // Example: Increase the size by 50%

                // Update the QRectF to scale the image
                QRectF hexRect(
                    width / 2 + hex.centerX,
                    height / 2 + hex.centerY + (hexImage.height() * scaleFactor) / 2,
                    hexImage.width() * scaleFactor,
                    hexImage.height() * scaleFactor);

                painter->drawImage(hexRect, hexImage);
            }

            // std::cout << "\n"; // Move to the next row
        }
    }

    QPen linePen(lineColor, lineWidth, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin); // sharp precise lines
    painter->setPen(linePen);

    // touch lines
    painter->drawLine(margin, margin, margin + playWidth, margin);
    painter->drawLine(margin, height - margin, margin + playWidth, height - margin);
    // goal to goal line
    painter->drawLine(margin, height / 2, margin + playWidth, height / 2);

    // goal lines
    painter->drawLine(margin, margin, margin, margin + playHeight);
    painter->drawLine(width - margin, margin, width - margin, margin + playHeight);
    // half way line
    painter->drawLine(width / 2, margin, width / 2, margin + playHeight);

    // center circle
    painter->drawEllipse((width - ccDiameter) / 2, (height - ccDiameter) / 2, ccDiameter, ccDiameter);

    // penalty kick marks
    painter->drawLine(margin + pkDist, (height - pkWidth) / 2, margin + pkDist, (height + pkWidth) / 2);
    painter->drawLine(width - margin - pkDist, (height - pkWidth) / 2, width - margin - pkDist, (height + pkWidth) / 2);

    // Defense Area
    painter->drawRect(margin, (height - defAreaHeight) / 2, defAreaWidth, defAreaHeight);
    painter->drawRect(width - margin - defAreaWidth, (height - defAreaHeight) / 2, defAreaWidth, defAreaHeight);

    // Net Area
    QPen wallPen(wallColor, wallWidth, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin); // sharp precise lines
    painter->setPen(wallPen);
    // left net
    painter->drawLine(margin, (height - netHeight) / 2, margin - netWidth, (height - netHeight) / 2);
    painter->drawLine(margin - netWidth, (height - netHeight) / 2, margin - netWidth, (height + netHeight) / 2);
    painter->drawLine(margin - netWidth, (height + netHeight) / 2, margin, (height + netHeight) / 2);
    // right net
    painter->drawLine(width - margin, (height - netHeight) / 2, width - margin + netWidth, (height - netHeight) / 2);
    painter->drawLine(width - margin + netWidth, (height - netHeight) / 2, width - margin + netWidth, (height + netHeight) / 2);
    painter->drawLine(width - margin + netWidth, (height + netHeight) / 2, width - margin, (height + netHeight) / 2);

    // Wall Boundary
    painter->drawRect(wallMargin, wallMargin, width - 2 * wallMargin, height - 2 * wallMargin);

    // Restore the painter state
    painter->restore();
}

void GameScene::flipView()
{
    isFlipped = !isFlipped;
}

void GameScene::plotPoints(int id, QVector<Hexagon *> path)
{
    if (visualPoints.find(id) == visualPoints.end()) { // if the id is not in the map, give it a set of visual points
        for(int i = 0; i < 1000; i++) {
            VisualPoint *point = new VisualPoint(-10400, -7400, 0);
            visualPoints[id].append(point);
            this->addItem(point);
        }
    }
    this->path[id] = path;
}
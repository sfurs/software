#ifndef VISUALROBOT_H
#define VISUALROBOT_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QGraphicsItem>
#include "Team.h"

class VisualRobot; 

//RobotLabel class for text
class RobotLabel : public QGraphicsItem
{
public:
    RobotLabel();
    RobotLabel(QString r, QString a, qreal x, qreal y);

    void setRole(const QString& r) { 
        role = r; 
        update(); //this is required to update QGraphicsItem
    }
    void setAction(const QString& a) { 
        action = a; 
        update(); //this is required to update QGraphicsItem
    }

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    QString role;
    QString action;
};

//VisualRobot class declaration
class VisualRobot : public QGraphicsItem
{
public:
    /*
     * @brief VisualRobot constructor initialized with a randomly selected color and orientation
     */
    VisualRobot();
    VisualRobot(Team team, int numRobot,qreal x,qreal y,qreal angle, QString role, QString action);

    /*
     * Estimate painting area of the ball
     * @return a QRectF with x,y,width, and height starting at the top left corner
    */
    QRectF boundingRect() const override;

    /*
     * Specify robot hit box used for detecting collisions
    */
    QPainterPath shape() const override;

    /*
     * Specify what a robot looks like: ears turn red when in a collision
    */
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    //methods for text class
    void setRole(const QString& role) { robotLabel.setRole(role); }
    void setAction(const QString& action) { robotLabel.setAction(action); }
    RobotLabel* getRobotLabel() { return &robotLabel; }
    // Update the declaration to include angle
    void updateVisualRobot(float x, float y, float angle, bool isFlipped);


protected:

private:
    qreal x;
    qreal y;
    qreal angle = 0;
    QColor color;
    int number;
    Team team;
    bool isFlipped = false;
    //class and values for text display
    RobotLabel robotLabel;  
    //    int delay_counter;
    //    advance(int phase)
    //    virtual QRectF	boundingRect() const = 0
};

#endif // VISUALROBOT_H

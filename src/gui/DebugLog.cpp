#include "DebugLog.h"
#include <iostream>
#include <QMetaObject>

// Convert environment variable values to string
inline DebugLog::LogLevel getLevelFromString(QString level) {
    level = level.toLower(); 
    if (level == "-1" || level == "none" || level == "n") {
        return DebugLog::LogLevel::None;
    }
    if (level == "0" || level == "debug" || level == "d") {
        return DebugLog::LogLevel::Debug;
    }
    if (level == "1" || level == "info" || level == "i") {
        return DebugLog::LogLevel::Info;
    }
    if (level == "2" || level == "warn" || level == "warning" || level == "w") {
        return DebugLog::LogLevel::Warning;
    }
    if (level == "3" || level == "error" || level == "critical" || level == "e") {
        return DebugLog::LogLevel::Critical;
    }
    if (level == "4" || level == "fatal" || level == "f") {
        return DebugLog::LogLevel::Fatal;
    }
    return DebugLog::LogLevel::Invalid;
}


DebugLog::DebugLog() {
    qSetMessagePattern("[%{time process}]["
        "%{if-debug}Debug%{endif}"
        "%{if-info}Info%{endif}"
        "%{if-warning}Warning%{endif}"
        "%{if-critical}Critical%{endif}"
        "%{if-fatal}Fatal%{endif}]"
        "%{if-category}[%{category}]%{endif}"
#ifdef QT_MESSAGELOGCONTEXT
        "{%{file}:%{line}}"
#endif
        ":%{message}\n");
    qInstallMessageHandler(DebugLog::handleLog);
    QString file_path = qEnvironmentVariable("LOG_FILE");
    LogLevel file_level = getLevelFromString(qEnvironmentVariable("LOG_FILE_LEVEL"));
    LogLevel env_stderr_level = getLevelFromString(qEnvironmentVariable("LOG_STDERR_LEVEL"));
    if (env_stderr_level != DebugLog::LogLevel::Invalid) {
        stderr_level = env_stderr_level;
    }
    LogLevel env_stdout_level = getLevelFromString(qEnvironmentVariable("LOG_STDOUT_LEVEL"));
    if (env_stdout_level != DebugLog::LogLevel::Invalid) {
        stdout_level = env_stdout_level;
    }
    LogLevel env_preview_level = getLevelFromString(qEnvironmentVariable("LOG_LOG_PREVIEW_LEVEL"));
    if (env_preview_level != DebugLog::LogLevel::Invalid) {
        preview_level = env_preview_level;
    }
    openFile(file_path, file_level);

}

void DebugLog::cleanup() {
    DebugLog self = getLog();
    if (self.file) {
        delete self.file;
        self.file = nullptr;
    }
}

void DebugLog::openFile(QString path, LogLevel level) {
    if (file) {
        delete file;
        file = nullptr;
        delete file_stream;
        file_stream = nullptr;
    }
    if(path.length() > 0) {
        if (level != LogLevel::Invalid) {
            file_level = level;
        }
        file = new QFile(path);
        if (!file->open(QIODevice::ReadWrite)) {
            qWarning() << "Could not open log file:" << path;
            delete file;
            delete file_stream;
            file = nullptr;
            file_stream = nullptr;
        } else {
            file->setPermissions(QFileDevice::WriteOwner | QFileDevice::ReadOwner);
        }
        file_stream = new QTextStream(file);
    }
}

void DebugLog::setLogPreview(QTextBrowser* preview) {
    log_preview = preview;
}

inline DebugLog::LogLevel convertMsgTypeToLogLevel(QtMsgType type) {
    if (type == QtDebugMsg) {
        return DebugLog::LogLevel::Debug;
    }
    if (type == QtInfoMsg) {
        return DebugLog::LogLevel::Info;
    }
    if (type == QtWarningMsg) {
        return DebugLog::LogLevel::Warning;
    }
    if (type == QtCriticalMsg) {
        return DebugLog::LogLevel::Critical;
    }
    if (type == QtFatalMsg) {
        return DebugLog::LogLevel::Fatal;
    }
    return DebugLog::LogLevel::Invalid;
}

void DebugLog::handleLog(QtMsgType type, const QMessageLogContext& ctx, const QString& msg) {
    DebugLog& self = getLog();
    LogLevel level = convertMsgTypeToLogLevel(type);
    QString log_msg = qFormatLogMessage(type, ctx, msg);
    if (level == LogLevel::Invalid | level == LogLevel::None) {
        level = LogLevel::Fatal;
    }
    if (self.file) {
        if (self.file_level <= level) {
            *(self.file_stream) << log_msg;
            self.file_stream->flush();
        }
    }
    if (self.stdout_level <= level) {
        std::cout << log_msg.toStdString() << std::flush;
    }
    if (self.stderr_level <= level) {
        std::cerr << log_msg.toStdString() << std::flush;
    }
    if (self.log_preview) {
        if (self.preview_level <= level) {
            //QMetaObject::invokeMethod(self.log_preview, &QTextBrowser::append, Q_ARG(QString, log_msg));
            QMetaObject::invokeMethod(self.log_preview, "append", Q_ARG(QString, log_msg));
        }
    }
}

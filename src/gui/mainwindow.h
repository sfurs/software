#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QTextBrowser>
#include <QGraphicsView>
#include <QString>
#include <QLineEdit>
#include <QString>
#include <QHostAddress>
#include <QTextEdit>
#include <QGroupBox>
#include <QRadioButton>
#include <QSpinBox>
#include <QVector>
#include <QKeyEvent>
#include <QLCDNumber>
#include <iostream>

#include "visualrobot.h"
#include "gamescene.h"
#include "DebugLog.h"
#include "../network/netmaster.h"
#include "../agent/AgentMaster.h"
#include "../agent/controls/AgentControl.h"
#include "../worldstate/worldStateManager.h"
#include "Team.h"
#include "../agent/controls/Navigator.h"
using namespace std;

namespace Ui
{
    class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(WorldStateManager *wsm, NetMaster *nm, AgentMaster *am, AgentControl *ac, Navigator *nav, QWidget *parent = nullptr);
    ~MainWindow();
    void setNetMaster(NetMaster *nm);
    void setAgentMaster(AgentMaster *am);
    void setAgentControl(AgentControl *ac);

    Ui::MainWindow *ui;
signals:
    void close();
    void navigateTo(int id, float x, float y, RotationSpec rotationSpec);
    void clearAndNavigateTo(int id, float x, float y, RotationSpec rotationSpec);
public slots:
    void updateScore(int blueScore, int yellowScore);

private slots:
    // void handleMoveButton();
    //void initRobotControls();
    void initScore();
    void initGameView();
    void initRobotStatus();
    void initLogo();
    void initBall();
    void initConfigWidgets();

    //    void initNetwork();
    void RobotNumberChange(int robot_number);
    void RobotRightpushButton();
    //    void RobotRecvMsg(float x, float y, float z, float conf);
    //    void RobotRecvMsg2(bool b, int robot_id, float x, float y, float orientation, float conf);
    void RobotLeftpushButton();
    void RobotForwardpushButton();
    void RobotBackpushButton();
    void RobotRotateRightpushButton();
    void RobotRotateLeftpushButton();
    void RobotKickpushButton();
    void RobotDribblepushButton();
    void incrementElapsedTime();
    void initLogBox();
    void initRadioButtons();
    void initAstarInputs(); // Astar box in AI tab
    void initGridDisplayCheckbox();
    void AutoModeRadioButton();
    void ManualModeRadioButton();
    // void RobotTeamSelectSpinBox();
    // void RobotSelectSpinBox();
    void vector_update(int);
    void onDarkModeSelected();
    void onLightModeSelected();
    void onFlipView();
    void applyStyleSheet(const QString &mode);
    void onSimulatorRadioButton();
    void onControlWireRadioButton();
    void handleRefereeIPChange();
    void handleSSLVisionIPChange();
    void handleSimulatorIPChange();
    void handleRefereePortChange();
    void handleSSLVisionPortChange();
    void handleSimulatorPortBlueChange();
    void handleSimulatorPortYellowChange();
    void handleWirePortChange();
    void handleWireBaudRateChange();
    void onBlueTeamRadioButton();
    void onYellowTeamRadioButton();

    //Astar input handlers
    void AstarRobotIDSelectSpinBox();
    void AstarRobotIsBlueTeam();
    void AstarGoalXSelectSpinBox();
    void AstarGoalYSelectSpinBox();
    void AstarRunButton();

protected:
    void resizeEvent(QResizeEvent *event) override;
    void showEvent(QShowEvent *event) override;

private:
    NetMaster *netMaster;
    AgentMaster *agentMaster;
    AgentControl *agentControl;
    Navigator *navigator;
    WorldStateManager *worldStateManager;

    int robInsID;
    QSet<int> activeKeys;
    void processCommand();
    void updateGameState();
    void updateAllRobotStatus();
    void updateRobotStatus(int robot_number, WorldState ws, vector<int> &notFoundRobots);
    void updateBallStatus(WorldState ws);
    void closeEvent(QCloseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

    int elapsedTime = 0;
    bool shouldPrintRobotNotFoundWarning = true;

    QPushButton *moveRight_button;
    QPushButton *moveLeft_button;
    QPushButton *moveForward_button;
    QPushButton *moveBack_button;
    QPushButton *rotateRight_button;
    QPushButton *rotateLeft_button;
    QPushButton *kick_button;
    QPushButton *dribble_button;

    QSpinBox *team_number;
    QSpinBox *robot_number;

    // QString NetworkSSLVisionIIPlineEdit_text_box;
    // QString NetworkSimulatoIPlineEdit_text_box;
    // QString NetworkSSLVisionIPortlineEdit_text_box;
    // QString NetworkSimulatorPortlineEdit_text_box;

    // int NetworkSSLVisionIIPlineEdit_text_box_size;
    // int NetworkSimulatoIPlineEdit_text_box_size;
    // int NetworkSSLVisionIPortlineEdit_text_box_size;
    // int NetworkSimulatorPortlineEdit_text_box_size;

    QVector<int> current_command_vector = {0, 0, 0};
    Team team = INVALID;
    int robot_number_int = 1;
    QTextBrowser *logBrowser;

    QLCDNumber *gameTimePtr;

    QGraphicsView *gameView;
    GameScene *gameScene;
    QTimer *timer;
    typedef enum
    {
        modeSwitch = 0,
        left = 1,
        right = 2,
        forward = 3,
        backwards = 4,
        rotateLeft = 5,
        rotateRight = 6,
        kick = 7,
        dribble = 8
    } commands;

    QGroupBox *groupBox;
    QRadioButton *autoModeButton;
    QRadioButton *manualModeButton;
    //Aster box in AI tab
    int astar_robo_id = 0;
    bool astar_is_blue_team = true;
    double astar_x_cordinate = 0.0;
    double astar_y_cordinate = 0.0;
    
    QSpinBox *astar_robot_id;
    QRadioButton *astarIsBlueTeam;
    QDoubleSpinBox *astar_goal_x_cordinate;
    QDoubleSpinBox *astar_goal_y_cordinate;
    QPushButton *astar_enter_button;
};
#endif // MAINWINDOW_H

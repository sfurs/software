#ifndef DEBUGLOG_H
#define DEBUGLOG_H
#include <QTextBrowser>
#include <QFile>

class DebugLog {
public:
    static DebugLog& getLog() {
        // only need one log ever
        static DebugLog INSTANCE;
        return INSTANCE;
    };

    enum class LogLevel {
        Invalid = -1,
        Debug = 0,
        Info = 1,
        Warning = 2,
        Critical = 3,
        Fatal = 4,
        None = 5
    };
    static void handleLog(QtMsgType type, const QMessageLogContext& ctx, const QString& msg);
    static void cleanup();
    void openFile(QString path, LogLevel level);
    void setLogPreview(QTextBrowser* preview);
private:
    DebugLog();

    static constexpr char const* LevelNames[] = {"DEBUG", "INFO", "WARNING", "CRITICAL", "FATAL"};
    LogLevel stdout_level = LogLevel::None;
    LogLevel stderr_level = LogLevel::Warning;
    LogLevel file_level = LogLevel::Debug;
    LogLevel preview_level = LogLevel::Info;
    QFile* file = nullptr;
    QTextStream* file_stream = nullptr;
    QTextBrowser * log_preview = nullptr;
};


#endif // DEBUGLOG_H

#include "visualrobot.h"
#include <QGraphicsScene>
#include <QPainter>
#include <QRandomGenerator>
#include <QStyleOption>
#include <QtMath>
#include <iostream>

constexpr qreal Pi = M_PI;
constexpr qreal TwoPi = 2 * M_PI;

static qreal normalizeAngle(qreal angle)
{
    while (angle < 0)
        angle += TwoPi;
    while (angle > TwoPi)
        angle -= TwoPi;
    return angle;
}

//constructor for robotText
RobotLabel::RobotLabel()
{
    this->role = "DefaultRole";
    this->action = "DefaultAction";
}

RobotLabel::RobotLabel(QString r, QString a, qreal x, qreal y) 
{
    this->role = r;
    this->action = a;
    this->setPos(x, y); 
}

VisualRobot::VisualRobot() : color(QRandomGenerator::global()->bounded(256),
                                QRandomGenerator::global()->bounded(256),
                                QRandomGenerator::global()->bounded(256))
{
    setRotation(QRandomGenerator::global()->bounded(360 * 16));
}

// constructor that initializes robots based on team and robot number
VisualRobot::VisualRobot(Team team, int numRobot,qreal x,qreal y,qreal angle, QString initRole, QString initAction)
    : team(team), robotLabel(initRole, initAction, x, y)
{
    
    QColor yellow = Qt::yellow;
    QColor blue = Qt::blue;
    this->x = x;
    this->y = y;
    this->number = numRobot;


    this->setRotation(angle);
    this->setPos(mapToParent(x,y));

    if (team == YELLOW) {
        this->color = yellow;
    } else {
        this->color = blue;
    }

}

QRectF VisualRobot::boundingRect() const
{
    qreal adjust = 2;
    qreal p = 90.0;

    QPoint topLeft;
    topLeft.setX(-p - adjust);
    topLeft.setY(-p - adjust);
    QPoint bottomRight;
    bottomRight.setX(p + adjust);
    bottomRight.setY(p + adjust);
    return QRect(topLeft, bottomRight);//QRectF(-180.0 - adjust, -180.0 - adjust, 180.0 + adjust, 180.0 + adjust);
}


QPainterPath VisualRobot::shape() const
{
    QPainterPath path;
    QPointF center;
    center.setX(0);
    center.setY(0);
    path.addEllipse(center,90,90);//.addRect(-90, -90, 90, 90);
    return path;
}

void VisualRobot::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    // Body
    int radius = 90;
    int flatSectionWidth = 60;  // Width of the flat front

    // Create a path for the body with a flat front
    QPainterPath robotBody;
    robotBody.moveTo(-radius, -radius);  // Top-left point of the circle

    // Draw the circular part of the robot (arc)
    QRectF bodyRect(-radius, -radius, radius * 2, radius * 2);
    robotBody.arcTo(bodyRect, 30, 300);  // 300-degree arc leaves the flat front

    // Add flat front (straight line connecting the two arc ends)
    robotBody.lineTo(radius * cos(qDegreesToRadians(30.0)), radius * sin(qDegreesToRadians(30.0)));
    robotBody.lineTo(radius * cos(qDegreesToRadians(-30.0)), radius * sin(qDegreesToRadians(-30.0)));

    if (!isFlipped) {
        painter->rotate(90);
        painter->translate(0, 0);
    } else {
        painter->rotate(270);
        painter->translate(0, 0);
    }

    painter->setBrush(color);
    painter->drawPath(robotBody);

    // Nose
    QColor noseColor = Qt::black;
    if (color == Qt::blue) {
        noseColor = Qt::white;
    }
    painter->setPen(QPen(noseColor, 25));  // Set line color and thickness

    QPointF noseEnd = QPointF(radius * 0.5, 0);

    painter->drawLine(QPointF(0, 0), noseEnd);  // Draw line from (0,0) to noseEnd
}


void VisualRobot::updateVisualRobot(float x, float y, float angle, bool iF)
{
    // Update position
    if (!iF) {
        this->setPos(y + 3700, x + 5200);
        if (isFlipped) {
            isFlipped = false;
        }
    } else {
        this->setPos(-y + 3700, -x + 5200);
        if (!isFlipped) {
            isFlipped = true;
        }
    }

    // Convert angle from radians to degrees (if necessary)
    // Adjust the angle sign depending on your coordinate system
    this->setRotation(-angle * 180 / M_PI);

    // std::cout << "Robot position: " << x << ", " << y << std::endl;

    if (x > 5200 || x < -5200 || y > 3700 || y < -3700) {
        this->setVisible(false);
    } else {
        this->setVisible(true);
    }

    // Update the graphics item
    update();
}

QRectF RobotLabel::boundingRect() const
{   //get font to calculate text height and width
    QFont font;
    font.setPointSize(100);
    QFontMetrics fm(font);
    int textHeight = fm.height();
    //determine the longest string
    QString longerString = (role.length() > action.length()) ? role : action;
    int textWidth = fm.horizontalAdvance(longerString);
    
    //set rectangle size
    qreal adjust = 50;
    QPoint topLeft;
    topLeft.setX(0); 
    topLeft.setY(- textHeight - adjust);  // Adjusted for two lines of text
    QPoint bottomRight;
    bottomRight.setX(90 + adjust + textWidth); 
    bottomRight.setY(textHeight);

    return QRect(topLeft, bottomRight);
}

void RobotLabel::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QFont font = painter->font();
    font.setPointSize(100);
    painter->setFont(font);
    painter->setPen(Qt::black);

    QFontMetrics fm(font);
    int textHeight = fm.height();

    QString role = this->role;
    QString action = this->action;

    // BUG: The text doesn't follow each robot, 
    //      it stays in the same place regardless of robot position
    // TODO: Find a way to connect the robot's text its robot body robot
    // painter->drawText(QPointF(0, -textHeight / 2), role); 
    // painter->drawText(QPointF(0, textHeight/2), action);  
}

#ifndef GAMESCENE_H
#define GAMESCENE_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QLCDNumber>
#include "visualrobot.h"
#include "VisualBall.h"
#include "FieldData.h"
#include "../worldstate/worldStateManager.h"
#include <QApplication>
#include "../agent/pathplanner/Grid.h"
#include "../agent/AgentMaster.h"
#include "VisualPoint.h"
#include "../agent/controls/Navigator.h"
#include <algorithm>

//class GameField : public QGraphicsScene
class GameScene : public QGraphicsScene
{
public:
signals:
    void updateGUIClock(QString time);
    GameScene(WorldStateManager *wsm, QGraphicsView *gameView, QLCDNumber *gameTime, Navigator *navigator, const QString &mode = "dark");
    ~GameScene();

    void drawBackground(QPainter *opainter, const QRectF &rect) override;
    //    QRectF boundingRect() const override;

    //    QPainterPath shape() const override;
    //    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
    //               QWidget *widget) override;

    VisualRobot *getVisualRobot(int id);
    // void updateVisualRobots(QList<RobotData> robotData);
    void updateVisualRobots();

    void updateMode(const QString &mode);

    void flipView();

    void setDisplayHexGrid(bool display);

public slots:
    void plotPoints(int id, QVector<Hexagon *> path);

private:
    QVector<VisualRobot*> blueVisualRobots;
    QVector<VisualRobot*> yellowVisualRobots;
    QMap<int, QVector<VisualPoint*>> visualPoints;
    VisualBall * visualBall;
    WorldStateManager* worldStateManager;
    bool displayHexGrid = false; // Initially, the hex grid is hidden
    Navigator *navigator;

    QImage turf;
    QImage hexImage;
    const QVector<QVector<Hexagon>> &grid;
    const float width = 10400;
    const float height = 7400;
    //    All measurements are in millimeters
    //    robot max size 180 mm diameter and 150 cm tall
    //    ball is exactly orange golf ball = 42.7 mm
    //    field is 9 * 6 meters
    const float robotHeight = 150;
    const float robotRadius = 90;
    const float ballRadius = 42.7;
    const QColor ballColor = QColorConstants::Svg::orange;

    const int teamSize = 6;
    const int numberOfTeams = 2;

    // Field dimensions and markings
    const float fieldPrecision = 0.10; // The acceptable fractional error of the field markings in a physical arena
    const float margin = 700;          // space surounding the playing field includes referee walking space
    const float wallMargin = 400;      // referree walking space between the boundary wall
    const float playWidth = 9000;      // goal line length
    const float playHeight = 6000;     // touch line length
    const float wallHeight = 100;      // height of the wall that seperates referee from field
    const QColor wallColor = Qt::black;
    const float wallWidth = 20;

    // Field Lines
    const QColor lineColor = Qt::white;
    const float lineWidth = 10;

    // Defense Area Dimensions
    const float defAreaHeight = 2000;
    const float defAreaWidth = 1000;

    // Net Dimensioins the net will be centered on the goal line
    // absolute spacing lines are drawn outside the area
    const float netPostThickness = 200;
    //    const float netHeight = 160; vertical height
    const float netWidth = 180;
    const float netHeight = 1000;

    // Center Circle
    const float ccDiameter = 500;

    //    advance(int phase)
    //    virtual QRectF	boundingRect() const = 0

    // Extra option markings
    // penalty kick markings
    const float pkDist = 6000; // marks penalty kick location from goal along center line
    const float pkWidth = 100;

    QLCDNumber *gameTime;
    QString currentMode;
    void loadTurfImage();
    void loadHexImage();
    bool isFlipped = false;

    QMap<int, QVector<Hexagon*>> path;
};

#endif // GAMESCENE_H

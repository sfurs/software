#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "FieldData.h"
#include "visualrobot.h"
#include "RobotInstruction.h"
#include <QTimer>
#include <QSplitter>
#include "Logging.h"
#include <QFile>
#include <QRadioButton>
#include <QLabel>
#include <QPixmap>
#include <QString>
#include "gamescene.h"
#include <QFrame>
#include <QLabel>
#include <QVBoxLayout>
#include <cmath>
#include <qcontainerfwd.h>
#include <sstream>
#include "Team.h"

MainWindow::MainWindow(WorldStateManager *wsm, NetMaster *nm, AgentMaster *am, AgentControl *ac, Navigator *nav, QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    worldStateManager = wsm;
    navigator = nav;
    qCDebug(LOG_GUI)
        << "MainWindow constructor start";
    ui->setupUi(this);
    // qCDebug(LOG_GUI) << "UI setup complete";

    this->resize(1600, 1000); // Adjust these values as needed

    this->setNetMaster(nm);
    this->setAgentMaster(am);
    this->setAgentControl(ac);

    initScore();
    initLogBox();
    initRadioButtons();
    initAstarInputs();
    initGridDisplayCheckbox();
    initGameView();
    initLogo();
    initBall();
    initRobotStatus();
    initConfigWidgets();

    // Connect the radio buttons
    connect(ui->DarkModeRadioButton, &QRadioButton::clicked, this, &MainWindow::onDarkModeSelected);
    connect(ui->LightModeRadioButton, &QRadioButton::clicked, this, &MainWindow::onLightModeSelected);
    connect(ui->FlipViewButton, &QRadioButton::clicked, this, &MainWindow::onFlipView);
    connect(this, &MainWindow::navigateTo, navigator, &Navigator::navigateTo);
    connect(this, &MainWindow::clearAndNavigateTo, navigator, &Navigator::clearAndNavigateTo);
    // Set initial style (dark mode by default)
    applyStyleSheet("dark");

    qCDebug(LOG_GUI) << "MainWindow constructor complete";
}

void MainWindow::initScore(){
    connect(worldStateManager, &WorldStateManager::emitScores, this, &MainWindow::updateScore);
    ui->ScoreLabel->setText("Blue Score: " + QString::number(0) + ", Yellow Score: " + QString::number(0));
}

void MainWindow::initConfigWidgets() {
    // qCDebug(LOG_GUI) << "Initializing config widgets";
    connect(ui->SimulatorRadioButton, &QRadioButton::clicked, this, &MainWindow::onSimulatorRadioButton);
    connect(ui->ControlWireRadioButton, &QRadioButton::clicked, this, &MainWindow::onControlWireRadioButton);

    if (netMaster != nullptr)
    {
        ui->RefereeIPInput->setText(netMaster->getRefereeIP());
        ui->VisionIPInput->setText(netMaster->getVisionIP());
        ui->RobotControlIPInput->setText(netMaster->getControlIP());
        ui->RefereePortInput->setText(QString::number(netMaster->getRefereePort()));
        ui->VisionPortInput->setText(QString::number(netMaster->getVisionPort()));
        ui->RobotControlBluePortInput->setText(QString::number(netMaster->getControlBluePort()));
        ui->RobotControlYellowPortInput->setText(QString::number(netMaster->getControlYellowPort()));
        ui->RobotWirePortInput->setText(netMaster->getRobotControlPort());
        ui->RobotWireBaudRateInput->setText(QString::number(netMaster->getRobotControlBaudRate()));
    }
    else
    {
        qWarning() << "netMaster is not set!";
    }

    // qCDebug(LOG_GUI) << "Connecting network input signals";
    connect(ui->RefereeIPInput, &QLineEdit::textChanged, this, &MainWindow::handleRefereeIPChange, Qt::QueuedConnection);
    connect(ui->VisionIPInput, &QLineEdit::textChanged, this, &MainWindow::handleSSLVisionIPChange, Qt::QueuedConnection);
    connect(ui->RobotControlIPInput, &QLineEdit::textChanged, this, &MainWindow::handleSimulatorIPChange, Qt::QueuedConnection);
    connect(ui->RefereePortInput, &QLineEdit::textChanged, this, &MainWindow::handleRefereePortChange, Qt::QueuedConnection);
    connect(ui->VisionPortInput, &QLineEdit::textChanged, this, &MainWindow::handleSSLVisionPortChange, Qt::QueuedConnection);
    connect(ui->RobotControlBluePortInput, &QLineEdit::textChanged, this, &MainWindow::handleSimulatorPortBlueChange, Qt::QueuedConnection);
    connect(ui->RobotControlYellowPortInput, &QLineEdit::textChanged, this, &MainWindow::handleSimulatorPortYellowChange, Qt::QueuedConnection);
    connect(ui->RobotWirePortInput, &QLineEdit::textChanged, this, &MainWindow::handleWirePortChange, Qt::QueuedConnection);
    connect(ui->RobotWireBaudRateInput, &QLineEdit::textChanged, this, &MainWindow::handleWireBaudRateChange, Qt::QueuedConnection);

    if (netMaster->getIsSerial())
    {
        ui->SimulatorRadioButton->setChecked(false);
        ui->ControlWireRadioButton->setChecked(true);
    }
    else
    {
        ui->SimulatorRadioButton->setChecked(true);
        ui->ControlWireRadioButton->setChecked(false);
    }
    if (!netMaster->getSerialEnabled())
    {
        ui->ControlWireRadioButton->setDisabled(true);
    }
    if (worldStateManager->getCurrentState().friendlyTeam == BLUE)
    {
        ui->BlueTeamRadioButton->setChecked(true);
    }
    else
    {
        ui->YellowTeamRadioButton->setChecked(true);
    }

    connect(ui->BlueTeamRadioButton, &QRadioButton::clicked, this, &MainWindow::onBlueTeamRadioButton);

    connect(ui->YellowTeamRadioButton, &QRadioButton::clicked, this, &MainWindow::onYellowTeamRadioButton);
    // qCDebug(LOG_GUI) << "Config widgets initialization complete";
}

void MainWindow::initLogBox()
{
    logBrowser = ui->LogBrowser;
    DebugLog &logger = DebugLog::getLog();
    logger.setLogPreview(logBrowser);
}

void MainWindow::initRadioButtons()
{
    groupBox = ui->ModegroupBox;
    autoModeButton = ui->ModeAutoradioButton;
    manualModeButton = ui->ModeManualradioButton;
    
    connect(autoModeButton, &QRadioButton::pressed, this, &MainWindow::AutoModeRadioButton);
    connect(manualModeButton, &QRadioButton::pressed, this, &MainWindow::ManualModeRadioButton);

    manualModeButton->setChecked(true);
}

void MainWindow::initAstarInputs()
{
    astar_robot_id = ui->ID;
    astarIsBlueTeam = ui->isBlueTeam;
    astar_goal_x_cordinate = ui->x_cordinate;
    astar_goal_y_cordinate = ui->y_cordinate;
    astar_enter_button = ui->Enter;
    connect(astar_robot_id,&QSpinBox::valueChanged, this, &MainWindow::AstarRobotIDSelectSpinBox);
    connect(astar_robot_id,&QSpinBox::valueChanged, this, &MainWindow::AstarRobotIsBlueTeam);
    connect(astar_goal_x_cordinate,&QDoubleSpinBox::valueChanged,this, &MainWindow::AstarGoalXSelectSpinBox);
    connect(astar_goal_y_cordinate,&QDoubleSpinBox::valueChanged,this, &MainWindow::AstarGoalYSelectSpinBox);
    connect(astar_enter_button,&QPushButton::pressed, this, &MainWindow::AstarRunButton);
}

void MainWindow::initGridDisplayCheckbox()
{
    //QCheckBox* gridDisplayCheckbox = ui->HexDisplay;
    //connect(gridDisplayCheckbox, &QCheckBox::toggled, gameScene, &GameScene::setDisplayHexGrid);
    connect(ui->HexDisplay, &QCheckBox::toggled, [this](bool checked) {
        //std::cout << "HexDisplay toggled: " << checked << std::endl;
        if (gameScene) {
            gameScene->setDisplayHexGrid(checked);
        } else {
            std::cerr << "Error: gameScene is nullptr!" << std::endl;
        }

    });
}

/*void MainWindow::initRobotControls()
{
    moveRight_button = ui->RobotRightpushButton;
    moveLeft_button = ui->RobotLeftpushButton;
    moveForward_button = ui->RobotForwardpushButton;
    moveBack_button = ui->RobotBackpushButton;
    rotateLeft_button = ui->RobotRotateLeftpushButton;
    rotateRight_button = ui->RobotRotateRightpushButton;
    kick_button = ui->RobotKickpushButton;
    // dribble_button = ui->RobotDribblepushButton;
    team_number = ui-> RobotTeamSelectSpinBox;
    robot_number= ui-> RobotSelectSpinBox;

    connect(moveLeft_button, &QPushButton::pressed, this, &MainWindow::RobotLeftpushButton);
    connect(moveRight_button, &QPushButton::pressed, this, &MainWindow::RobotRightpushButton);
    connect(moveForward_button, &QPushButton::pressed, this, &MainWindow::RobotForwardpushButton);
    connect(moveBack_button, &QPushButton::pressed, this, &MainWindow::RobotBackpushButton);
    connect(rotateLeft_button, &QPushButton::pressed, this, &MainWindow::RobotRotateLeftpushButton);
    connect(rotateRight_button, &QPushButton::pressed, this, &MainWindow::RobotRotateRightpushButton);
    connect(kick_button, &QPushButton::pressed, this, &MainWindow::RobotKickpushButton);
    //connect(dribble_button, &QPushButton::pressed, this, &MainWindow::RobotDribblepushButton);
    connect(team_number,&QSpinBox::valueChanged, this, &MainWindow::RobotTeamSelectSpinBox);
    connect(robot_number,&QSpinBox::valueChanged,this, &MainWindow::RobotSelectSpinBox);
}*/

 /**
 * @brief MainWindow::initGameView initialize the game scene and connect the scene to the game window
 */


// void MainWindow::initGameView()
// {
//     gameView = ui->GameMapgraphicsView;
//     gameTimePtr = ui->GameTime;

//     // Set the size policy to make the GameMapgraphicsView flexible
//     gameView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

//     gameScene = new GameScene(gameView, gameTimePtr);
//     gameView->setScene(gameScene);
//     gameView->setRenderHint(QPainter::Antialiasing);

//     float xscale = gameView->width() / gameScene->sceneRect().width();
//     float yscale = gameView->height() / gameScene->sceneRect().height();
//     float scale = xscale < yscale ? xscale : yscale;
//     gameView->scale(scale, scale);

//     gameView->show();
//     qCDebug(LOG_GUI) << "Game view initialized";

//     timer = new QTimer(this);
//     QObject::connect(timer, &QTimer::timeout, gameScene, &GameScene::updateVisualRobots);
//     QObject::connect(timer, &QTimer::timeout, this, &MainWindow::processCommand);
//     timer->start(1000 / 60);

//     return;
// }

void MainWindow::initGameView()
{
    gameView = ui->GameMapgraphicsView;
    gameTimePtr = ui->GameTime;
    gameView->setAttribute(Qt::WA_TransparentForMouseEvents, true);

    gameScene = new GameScene(worldStateManager,gameView, gameTimePtr, navigator, "dark");
    gameView->setScene(gameScene);
    gameView->setRenderHint(QPainter::Antialiasing);

    gameView->fitInView(gameScene->sceneRect(), Qt::KeepAspectRatio); // Ensure game scene fits view on load

    gameView->show();
    qCDebug(LOG_GUI) << "Game view initialized";

    timer = new QTimer(this);
    QObject::connect(timer, &QTimer::timeout, gameScene, &GameScene::updateVisualRobots);
    QObject::connect(timer, &QTimer::timeout, this, &MainWindow::processCommand);
    QObject::connect(timer, &QTimer::timeout, this, &MainWindow::updateAllRobotStatus);
    QObject::connect(timer, &QTimer::timeout, this, &MainWindow::updateGameState);
    timer->start(1000 / 60);

    return;
}

void MainWindow::initLogo()
{
    QString frameName = QString("LogoFrame");

    QFrame *logoFrame = this->findChild<QFrame *>(frameName);

    if (logoFrame)
    {
        // Create a new QLabel
        QLabel *imageLabel = new QLabel(logoFrame);
        imageLabel->setObjectName(QString("LogoImageLabel"));

        // Set up layout for the frame
        QVBoxLayout *layout = new QVBoxLayout(logoFrame);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->addWidget(imageLabel);
        logoFrame->setLayout(layout);

        // Load and set the image
        QString imageFileName = QString(":/sfurs_logo.jpg");
        QPixmap pixmap(imageFileName);

        if (!pixmap.isNull())
        {
            imageLabel->setPixmap(pixmap.scaled(logoFrame->width(), logoFrame->height(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation));
            imageLabel->setScaledContents(false);
            // qCDebug(LOG_GUI) << "Loaded image for" << frameName << ":" << imageFileName;
        }
        else
        {
            qWarning() << "Failed to load image for" << frameName << ":" << imageFileName;
            imageLabel->setText("Image not found");
        }
    }
    else
    {
        qWarning() << "Frame not found:" << frameName;

        // List all child widgets to help with debugging
        QList<QWidget *> children = this->findChildren<QWidget *>();
        qCDebug(LOG_GUI) << "Available child widgets:";
        for (QWidget *child : children)
        {
            qCDebug(LOG_GUI) << " -" << child->objectName();
        }
    }
}

void MainWindow::initBall()
{
    QString frameName = QString("BallFrame");
    QFrame *ballFrame = this->findChild<QFrame *>(frameName);
    if (ballFrame)
    {
        QLabel *imageLabel = new QLabel(ballFrame);
        imageLabel->setObjectName(QString("BallImageLabel"));
        QVBoxLayout *layout = new QVBoxLayout(ballFrame);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->addWidget(imageLabel);
        ballFrame->setLayout(layout);

        QString imageFileName = QString(":/ball.jpg");
        QPixmap pixmap(imageFileName);
        if (!pixmap.isNull())
        {
            imageLabel->setPixmap(pixmap.scaled(ballFrame->width(), ballFrame->height(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation));
            imageLabel->setScaledContents(false);
        }
        else
        {
            qWarning() << "Failed to load image for" << frameName << ":" << imageFileName;
            imageLabel->setText("Image not found");
        }
    }
    else
    {
        qWarning() << "Frame not found:" << frameName;
    }
}

void MainWindow::initRobotStatus()
{
    qCDebug(LOG_GUI) << "Initializing robot status...";

    for (int i = 0; i <= 5; ++i)
    {
        QString frameName = QString("Robot%1Frame").arg(i);

        QFrame *robotFrame = this->findChild<QFrame *>(frameName);

        if (robotFrame)
        {
            // Create a new QLabel
            QLabel *imageLabel = new QLabel(robotFrame);
            imageLabel->setObjectName(QString("Robot%1ImageLabel").arg(i));

            // Set up layout for the frame
            QVBoxLayout *layout = new QVBoxLayout(robotFrame);
            layout->setContentsMargins(0, 0, 0, 0);
            layout->addWidget(imageLabel);
            robotFrame->setLayout(layout);

            // Load and set the image
            QString imageFileName = QString(":/robot_%1.jpg").arg(i);
            QPixmap pixmap(imageFileName);

            if (!pixmap.isNull())
            {
                imageLabel->setPixmap(pixmap.scaled(robotFrame->width(), robotFrame->height(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation));
                imageLabel->setScaledContents(false);
                // qCDebug(LOG_GUI) << "Loaded image for" << frameName << ":" << imageFileName;
            }
            else
            {
                qWarning() << "Failed to load image for" << frameName << ":" << imageFileName;
                imageLabel->setText("Image not found");
            }
        }
        else
        {
            qWarning() << "Frame not found:" << frameName;

            // List all child widgets to help with debugging
            QList<QWidget *> children = this->findChildren<QWidget *>();
            qCDebug(LOG_GUI) << "Available child widgets:";
            for (QWidget *child : children)
            {
                qCDebug(LOG_GUI) << " -" << child->objectName();
            }
        }
    }

    qCDebug(LOG_GUI) << "Robot status initialization complete.";
}

void MainWindow::updateGameState()
{
    QLabel *gameStateLabel = this->findChild<QLabel *>("label");
    gameStateLabel->setText(StateNames[this->worldStateManager->getGameState().getRefereeState()]);
}

void MainWindow::updateAllRobotStatus()
{
    WorldState ws = this->worldStateManager->getCurrentState();
    const int NUM_ROBOTS = 6;
    vector<int> notFoundRobots;

    for (int i = 0; i < NUM_ROBOTS; i++)
    {
        updateRobotStatus(i, ws, notFoundRobots);
    }

    // Log a warning if all robots were not found
    if (this->shouldPrintRobotNotFoundWarning && notFoundRobots.size() == NUM_ROBOTS)
    {
        // Build a string of the robot numbers that were not found
        ostringstream notFoundRobotsStr;
        for (int i = 0; i < notFoundRobots.size(); ++i)
        {
            notFoundRobotsStr << i;
            if (i < notFoundRobots.size() - 1)
            {
                notFoundRobotsStr << ", ";
            }
        }
        QString notFoundRobotsStrQ = QString::fromStdString(notFoundRobotsStr.str());
        qWarning() << "Robots not found:" << notFoundRobotsStrQ;
        this->shouldPrintRobotNotFoundWarning = false;
    }

    updateBallStatus(ws);
}

void MainWindow::updateRobotStatus(int robot_number, WorldState ws, vector<int> &notFoundRobots)
{
    QString robotFieldLabelName = QString("Robot%1FieldLabel").arg(robot_number);
    QLabel *robotFieldLabel = this->findChild<QLabel *>(robotFieldLabelName);
    QString robotCoordLabelName = QString("Robot%1CoordLabel").arg(robot_number);
    QLabel *robotCoordLabel = this->findChild<QLabel *>(robotCoordLabelName);

    if (robotCoordLabel && robotFieldLabel)
    {
        if (ws.blueRobotData.size() > robot_number && ws.yellowRobotData.size() > robot_number)
        {
            float curr_x, curr_y, curr_theta;
            QVector<RobotData> friends = worldStateManager->getCurrentState().friendlyRobots();
            if (friends.isEmpty()) {
                return;
            }

            curr_x = friends[robot_number].x;
            curr_y = friends[robot_number].y;
            curr_theta = friends[robot_number].angle;

            if (curr_x >= -5400 && curr_x <= 5400 && curr_y >= -3600 && curr_y <= 3600)
            {
                robotCoordLabel->setText(QString("Coord: (%1, %2), %3°")
                                             .arg(curr_x, 0, 'f', 2)
                                             .arg(curr_y, 0, 'f', 2)
                                             .arg(curr_theta, 0, 'f', 2));
                robotFieldLabel->setText("Status: On Field");
            }
            else
            {
                robotCoordLabel->setText("Coord: (NAN, NAN), NAN°");
                robotFieldLabel->setText("Status: Out of Field");
            }
        }
        else
        {
            robotCoordLabel->setText("Coord: (NAN, NAN), NAN°");
            robotFieldLabel->setText("Status: N/A");

            notFoundRobots.push_back(robot_number);
        }
    }
    else
    {
        if (!robotFieldLabel)
        {
            qWarning() << "Field label not found:" << robotFieldLabelName;
        }
        if (!robotCoordLabel)
        {
            qWarning() << "Coord label not found:" << robotCoordLabelName;
        }
    }
}

void MainWindow::updateBallStatus(WorldState ws)
{
    QString ballFieldLabelName = QString("BallFieldLabel");
    QLabel *ballFieldLabel = this->findChild<QLabel *>(ballFieldLabelName);
    QString ballCoordLabelName = QString("BallCoordLabel");
    QLabel *ballCoordLabel = this->findChild<QLabel *>(ballCoordLabelName);

    if (ballCoordLabel && ballFieldLabel)
    {
        if (ws.ballData.x != NAN && ws.ballData.y != NAN && ws.ballData.z != NAN)
        {
            float curr_x, curr_y, curr_z;
            curr_x = ws.ballData.x;
            curr_y = ws.ballData.y;
            curr_z = ws.ballData.z;
            if (curr_x >= -5400 && curr_x <= 5400 && curr_y >= -3600 && curr_y <= 3600)
            {
                ballCoordLabel->setText("Coord: (" + QString::number(curr_x, 'f', 2) + ", " + QString::number(curr_y, 'f', 2) + ")" + ", " + QString::number(curr_z, 'f', 2));
                ballFieldLabel->setText("Status: On Field");
            }
            else
            {
                ballCoordLabel->setText("Coord: (NAN, NAN), NAN");
                ballFieldLabel->setText("Status: Out of Field");
            }
        }
        else
        {
            ballCoordLabel->setText("Coord: (NAN, NAN), NAN");
            ballFieldLabel->setText("Status: N/A");
        }
    }
    else
    {
        if (!ballFieldLabel)
        {
            qWarning() << "Field label not found:" << ballFieldLabelName;
        }
        if (!ballCoordLabel)
        {
            qWarning() << "Coord label not found:" << ballCoordLabelName;
        }
    }
}

void MainWindow::RobotNumberChange(int robot_number)
{
    qCDebug(LOG_AGENT_CONTROL) << "Robot number changed to: " + QString::number(robot_number);

    // RobotInstruction is used here to reset the robot's saved speed to 0.
    RobotInstruction instruction;
    instruction.id = robot_number_int;
    instruction.v_x = 0;
    instruction.v_y = 0;
    instruction.v_phi = 0;
    if (netMaster != nullptr)
    {
        QMetaObject::invokeMethod(netMaster, "SetRobotInstruction", Qt::QueuedConnection, Q_ARG(RobotInstruction, instruction));
    }

    robot_number_int = robot_number;
}

void MainWindow::RobotLeftpushButton()
{
    // network->sendMsg(QString("Moving robot left.").toUtf8());
    qCDebug(LOG_AGENT_CONTROL, "Moving robot left!");
    vector_update(left);
}

void MainWindow::RobotRightpushButton()
{
    qCDebug(LOG_AGENT_CONTROL, "Moving robot right!");
    vector_update(right);
}

void MainWindow::RobotForwardpushButton()
{
    // network->sendMsg(QString("Moving robot forward.").toUtf8());
    qCDebug(LOG_AGENT_CONTROL, "Moving robot forward!");
    vector_update(forward);
}

void MainWindow::RobotBackpushButton()
{
    // network->sendMsg(QString("Moving robot back.").toUtf8());
    qCDebug(LOG_AGENT_CONTROL, "Moving robot back!");
    vector_update(backwards);
}
void MainWindow::RobotRotateLeftpushButton()
{
    qCDebug(LOG_AGENT_CONTROL, "Rotate robot left");
    vector_update(rotateLeft);
}

void MainWindow::RobotRotateRightpushButton()
{
    // network->sendMsg(QString("Moving robot right.").toUtf8());
    qCDebug(LOG_AGENT_CONTROL, "Rotate robot right!");
    vector_update(rotateRight);
}

void MainWindow::RobotKickpushButton()
{
    qCDebug(LOG_AGENT_CONTROL, "Robot kick!");
    vector_update(kick);
}

void MainWindow::RobotDribblepushButton()
{
    qCDebug(LOG_AGENT_CONTROL, "Robot Dribble!");
    vector_update(dribble);
}

// 1. QSet <- Qevent keypress
// 2. function that on each tick  (reuse the exisiting timer) /
//    enumerates though set and edits instructions accordingly
// 3. function that on key-release, remove from Qset

void MainWindow::setNetMaster(NetMaster *nm)
{
    netMaster = nm;
}

void MainWindow::setAgentMaster(AgentMaster *am)
{
    agentMaster = am;
}

void MainWindow::setAgentControl(AgentControl *ac)
{
    agentControl = ac;
}

void MainWindow::incrementElapsedTime()
{
    if (activeKeys.contains(Qt::Key_K))
    {
        elapsedTime += 100;
        if (elapsedTime < 2000)
        {
            QTimer::singleShot(100, this, SLOT(incrementElapsedTime()));
        }
    }
}

void MainWindow::processCommand()
{
    if (activeKeys.contains(Qt::Key_0))
    {
        RobotNumberChange(0);
    }
    if (activeKeys.contains(Qt::Key_1))
    {
        RobotNumberChange(1);
    }
    if (activeKeys.contains(Qt::Key_2))
    {
        RobotNumberChange(2);
    }
    if (activeKeys.contains(Qt::Key_3))
    {
        RobotNumberChange(3);
    }
    if (activeKeys.contains(Qt::Key_4))
    {
        RobotNumberChange(4);
    }
    if (activeKeys.contains(Qt::Key_5))
    {
        RobotNumberChange(5);
    }
    // if (activeKeys.contains(Qt::Key_6)) {
    //     RobotNumberChange(6);
    // }
    RobotInstruction instruction;
    instruction.id = robot_number_int;
    instruction.v_x = 0;
    instruction.v_y = 0;
    instruction.v_phi = 0;
    instruction.kick = 0;
    instruction.dribble = 0;
    instruction.chipper = 0;
    if (activeKeys.contains(Qt::Key_W))
    {
        instruction.v_x += 1;
        // RobotForwardpushButton();
    }
    if (activeKeys.contains(Qt::Key_A))
    {
        instruction.v_y += 1;
        // RobotLeftpushButton();
    }
    if (activeKeys.contains(Qt::Key_S))
    {
        instruction.v_x -= 1;
        // RobotBackpushButton();
    }
    if (activeKeys.contains(Qt::Key_D))
    {
        instruction.v_y -= 1;
        // RobotRightpushButton();
    }
    if (activeKeys.contains(Qt::Key_Q))
    {
        instruction.v_phi += 1;
        // RobotRotateLeftpushButton();
    }
    if (activeKeys.contains(Qt::Key_E))
    {
        instruction.v_phi -= 1;
        // RobotRotateRightpushButton();
    }
    if (activeKeys.contains(Qt::Key_K))
    {
        instruction.kick = 5;
        // std::cout << "Kick: " << instruction.kick << std::endl;
        // if (elapsedTime >= 2000) { // 2000 ms = 2 seconds
        //     instruction.kick = true;
        //     qCDebug(LOG_AGENT_CONTROL, "Robot " + QString::number(instruction.id) + " kicking!");
        //     QMetaObject::invokeMethod(netMaster, "setKickSpeed", Qt::QueuedConnection, Q_ARG(int, instruction.id), Q_ARG(int, 1));
        //     elapsedTime = 0;
        // }
        // else {
        //     QTimer::singleShot(100, this, SLOT(incrementElapsedTime()));
        // }
        // RobotKickpushButton();
    }
    if (activeKeys.contains(Qt::Key_C))
    {
        instruction.chipper = 3;
    }
    if (activeKeys.contains(Qt::Key_Shift))
    {
        instruction.dribble = 2;
        // instruction.dribble = true;
        // qCDebug(LOG_AGENT_CONTROL, "Robot " + QString::number(instruction.id) + " dribbling!");
        // QMetaObject::invokeMethod(netMaster, "setDribblerSpeed", Qt::QueuedConnection, Q_ARG(int, instruction.id), Q_ARG(int, 1));
        // RobotDribblepushButton();
    }
    // if ((instruction.v_x != 0 || instruction.v_y != 0 || instruction.v_phi != 0 || instruction.kick || instruction.dribble) && netMaster != nullptr) {
    if (netMaster != nullptr)
    {
        if (instruction.v_x != 0 || instruction.v_y != 0 || instruction.v_phi != 0 || instruction.kick || instruction.dribble)
        {
            qCDebug(LOG_AGENT_CONTROL) << "Robot " + QString::number(instruction.id) + " moving with v_x: " + QString::number(instruction.v_x) + ", v_y: " + QString::number(instruction.v_y) + ", v_phi: " + QString::number(instruction.v_phi) + ", kick: " + QString::number(instruction.kick) + ", dribble: " + QString::number(instruction.dribble);
        }
        // setRobotPosSpeed(instruction); < invoke using QMetaObject::invokeMethod
        QMetaObject::invokeMethod(netMaster, "SetRobotInstruction", Qt::QueuedConnection, Q_ARG(RobotInstruction, instruction));
    }

    if (activeKeys.contains(Qt::Key_0) || activeKeys.contains(Qt::Key_1) || activeKeys.contains(Qt::Key_2) || activeKeys.contains(Qt::Key_3) || activeKeys.contains(Qt::Key_4) || activeKeys.contains(Qt::Key_5 || (activeKeys.contains(Qt::Key_K) && instruction.kick) || (activeKeys.contains(Qt::Key_C) && instruction.chipper)))
    {
        // qWarning() << "Swapped to Robot #" << instruction.id;
        activeKeys.remove(Qt::Key_0);
        activeKeys.remove(Qt::Key_1);
        activeKeys.remove(Qt::Key_2);
        activeKeys.remove(Qt::Key_3);
        activeKeys.remove(Qt::Key_4);
        activeKeys.remove(Qt::Key_5);
        activeKeys.remove(Qt::Key_K);
        activeKeys.remove(Qt::Key_C);
    }
    // std::cout << "Active keys: " << activeKeys.size() << std::endl;
}

void MainWindow::vector_update(int command_number)
{
    current_command_vector[1] = robot_number_int;
    current_command_vector[2] = command_number;

    QString text;
    int id = robot_number_int - 1;
    for (int i = 0; i < current_command_vector.size(); i++)
    {
        text.append(QString::number(current_command_vector[i]) + " ");
    }

    qCDebug(LOG_AGENT_CONTROL) << text;
}

void MainWindow::AutoModeRadioButton()
{
    // logBrowser->append("Auto mode selected!");
    qCDebug(LOG_AGENT_CONTROL, "Auto mode selected!");
    autoModeButton->setChecked(true);
    manualModeButton->setChecked(false);
    // vector_update(modeSwitch);
    QMetaObject::invokeMethod(agentControl, "setManualControl", Qt::QueuedConnection, Q_ARG(bool, false));
}

void MainWindow::ManualModeRadioButton()
{
    // logBrowser->append("Manual mode selected!");
    qCDebug(LOG_AGENT_CONTROL, "Manual mode selected!");
    manualModeButton->setChecked(true);
    autoModeButton->setChecked(false);
    // vector_update(modeSwitch);
    QMetaObject::invokeMethod(agentControl, "setManualControl", Qt::QueuedConnection, Q_ARG(bool, true));
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    timer->stop();
    delete timer;
    QMainWindow::closeEvent(event);
}

// void MainWindow::keyPressEvent(QKrobot_number_inteyEvent *event) {
//     switch (event->key()) {
//         case Qt::Key_W:
//             RobotForwardpushButton();
//             break;
//         case Qt::Key_A:
//             RobotLeftpushButton();
//             break;
//         case Qt::Key_S:
//             RobotBackpushButton();
//             break;
//         case Qt::Key_D:
//             RobotRightpushButton();
//             break;robot_number_int
//         case Qt::Key_Q:
//             RobotRotateLeftpushButton();
//             break;
//         case Qt::Key_E:
//             RobotRotateRightpushButton();
//             break;
//         case Qt::Key_K:
//             RobotKickpushButton();
//             break;
//         case Qt::Key_1:
//         case Qt::Key_2:
//         case Qt::Key_3:
//         case Qt::Key_4:
//         case Qt::Key_5:
//         case Qt::Key_6:robot_number_int::Key_0);
//             break;
//         default:
//             QMainWindow::keyPressEvent(event);
//     }
// }

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    activeKeys.insert(event->key());
    // qWarning() << "Key pressed: " << event->key();
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    // QMainWindow::keyReleaseEvent(event);
    // if (activeKeys.contains(Qt::Key_K)) {
    //     QMetaObject::invokeMethod(netMaster, "setKickSpeed", Qt::QueuedConnection, Q_ARG(int, robot_number_int), Q_ARG(int, 0));
    // }
    // if (activeKeys.contains(Qt::Key_Shift)) {
    //     QMetaObject::invokeMethod(netMaster, "setDribblerSpeed", Qt::QueuedConnection, Q_ARG(int, robot_number_int), Q_ARG(int, 0));
    // }
    activeKeys.remove(event->key());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showEvent(QShowEvent *event)
{
    QMainWindow::showEvent(event);
    if (ui->GameMapgraphicsView->scene())
    {
        ui->GameMapgraphicsView->fitInView(ui->GameMapgraphicsView->scene()->sceneRect(), Qt::KeepAspectRatio);
    }
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    if (ui->GameMapgraphicsView->scene())
    {
        ui->GameMapgraphicsView->fitInView(ui->GameMapgraphicsView->scene()->sceneRect(), Qt::KeepAspectRatio);
    }
}

void MainWindow::onDarkModeSelected()
{
    applyStyleSheet("dark");
}

void MainWindow::onLightModeSelected()
{
    applyStyleSheet("light");
}

void MainWindow::onSimulatorRadioButton()
{
    qCDebug(LOG_GUI, "Simulator mode selected! (WIP)");
    QMetaObject::invokeMethod(netMaster, "setIsSerial", Qt::QueuedConnection, Q_ARG(bool, false));
}

void MainWindow::onControlWireRadioButton()
{
    qCDebug(LOG_GUI, "Control wire mode selected! (WIP)");
    QMetaObject::invokeMethod(netMaster, "setIsSerial", Qt::QueuedConnection, Q_ARG(bool, true));
}

void MainWindow::onFlipView()
{
    gameScene->flipView();
}

void MainWindow::applyStyleSheet(const QString &mode)
{
    QString styleSheetPath = mode == "dark" ? ":/dark.qss" : ":/light.qss";
    QFile file(styleSheetPath);
    if (file.open(QFile::ReadOnly | QFile::Text))
    {
        QString styleSheet = QLatin1String(file.readAll());
        qApp->setStyleSheet(styleSheet);
        file.close();

        // Update the game scene mode
        if (gameScene)
        {
            gameScene->updateMode(mode);
        }
    }
    else
    {
        qDebug() << "Failed to load stylesheet:" << styleSheetPath;
    }
}

void MainWindow::handleRefereeIPChange()
{
    qCDebug(LOG_GUI) << "Referee IP changed to" << ui->RefereeIPInput->text();
    QString ip = ui->RefereeIPInput->text();
    QMetaObject::invokeMethod(netMaster, "setRefereeIP", Qt::QueuedConnection, Q_ARG(QString, ip));
    qCDebug(LOG_GUI) << "Referee IP set";
}

void MainWindow::handleRefereePortChange()
{
    qCDebug(LOG_GUI) << "Referee port changed to" << ui->RefereePortInput->text();
    quint16 port = ui->RefereePortInput->text().toUShort();
    QMetaObject::invokeMethod(netMaster, "setRefereePort", Qt::QueuedConnection, Q_ARG(quint16, port));
    qCDebug(LOG_GUI) << "Referee port set";
}

void MainWindow::handleSSLVisionIPChange()
{
    qCDebug(LOG_GUI) << "Vision IP changed to" << ui->VisionIPInput->text();
    QString ip = ui->VisionIPInput->text();
    QMetaObject::invokeMethod(netMaster, "setVisionIP", Qt::QueuedConnection, Q_ARG(QString, ip));
    qCDebug(LOG_GUI) << "Vision IP set";
}

void MainWindow::handleSSLVisionPortChange()
{
    qCDebug(LOG_GUI) << "Vision port changed to" << ui->VisionPortInput->text();
    quint16 port = ui->VisionPortInput->text().toUShort();
    QMetaObject::invokeMethod(netMaster, "setVisionPort", Qt::QueuedConnection, Q_ARG(quint16, port));
    qCDebug(LOG_GUI) << "Vision port set";
}

void MainWindow::handleSimulatorIPChange()
{
    qCDebug(LOG_GUI) << "Simulator IP changed to" << ui->RobotControlIPInput->text();
    QString ip = ui->RobotControlIPInput->text();
    QMetaObject::invokeMethod(netMaster, "setControlIP", Qt::QueuedConnection, Q_ARG(QString, ip));
    qCDebug(LOG_GUI) << "Simulator IP set";
}

void MainWindow::handleSimulatorPortBlueChange()
{
    qCDebug(LOG_GUI) << "Simulator Blue port changed to" << ui->RobotControlBluePortInput->text();
    quint16 port = ui->RobotControlBluePortInput->text().toUShort();
    QMetaObject::invokeMethod(netMaster, "setControlBluePort", Qt::QueuedConnection, Q_ARG(quint16, port));
    qCDebug(LOG_GUI) << "Simulator Blue port set";
}

void MainWindow::handleSimulatorPortYellowChange()
{
    qCDebug(LOG_GUI) << "Simulator Yellow port changed to" << ui->RobotControlYellowPortInput->text();
    quint16 port = ui->RobotControlYellowPortInput->text().toUShort();
    QMetaObject::invokeMethod(netMaster, "setControlYellowPort", Qt::QueuedConnection, Q_ARG(quint16, port));
    qCDebug(LOG_GUI) << "Simulator Yellow port set";
}

void MainWindow::handleWirePortChange()
{
    qCDebug(LOG_GUI) << "Wire port changed to" << ui->RobotWirePortInput->text();
    QString port = ui->RobotWirePortInput->text();
    QMetaObject::invokeMethod(netMaster, "setRobotControlPort", Qt::QueuedConnection, Q_ARG(QString, port));
    qCDebug(LOG_GUI) << "Wire port set";
}

void MainWindow::handleWireBaudRateChange()
{
    qCDebug(LOG_GUI) << "Wire baud rate changed to" << ui->RobotWireBaudRateInput->text();
    int baudRate = ui->RobotWireBaudRateInput->text().toInt();
    QMetaObject::invokeMethod(netMaster, "setRobotControlBaudRate", Qt::QueuedConnection, Q_ARG(int, baudRate));
    qCDebug(LOG_GUI) << "Wire baud rate set";
}

void MainWindow::onBlueTeamRadioButton()
{
    QMetaObject::invokeMethod(netMaster, "setFriendlyTeam", Qt::QueuedConnection, Q_ARG(Team, BLUE));
    worldStateManager->setFriendlyTeam(BLUE);
}

void MainWindow::onYellowTeamRadioButton()
{
    QMetaObject::invokeMethod(netMaster, "setFriendlyTeam", Qt::QueuedConnection, Q_ARG(Team, YELLOW));
    worldStateManager->setFriendlyTeam(YELLOW);
}

void MainWindow::updateScore(int blueScore, int yellowScore){
    ui->ScoreLabel->setText("Blue Score: " + QString::number(blueScore) + ", Yellow Score: " + QString::number(yellowScore));
}


void MainWindow::AstarRobotIDSelectSpinBox(){
    astar_robo_id = ui->ID->value();
}

void MainWindow::AstarRobotIsBlueTeam(){{
    astar_is_blue_team = !astar_is_blue_team;
}}

void MainWindow::AstarGoalXSelectSpinBox(){
    astar_x_cordinate = ui->x_cordinate->value();
}

void MainWindow::AstarGoalYSelectSpinBox(){
    astar_y_cordinate = ui->y_cordinate->value();
}

void MainWindow::AstarRunButton(){
    // QMetaObject::invokeMethod(agentControl, "runAstarForRobot", Qt::QueuedConnection, Q_ARG(int, astar_robo_id), Q_ARG(bool, astar_is_blue_team), Q_ARG(double, astar_x_cordinate), Q_ARG(double, astar_y_cordinate));
    // emit clearAndNavigateTo(astar_robo_id, astar_x_cordinate, astar_y_cordinate, RotationSpec::WRT_ROTATION);
    emit clearAndNavigateTo(astar_robo_id, astar_x_cordinate, astar_y_cordinate, RotationSpec::WITH_ROTATION);
    // emit clearAndNavigateTo(astar_robo_id, astar_x_cordinate, astar_y_cordinate, RotationSpec::NO_ROTATION);
}


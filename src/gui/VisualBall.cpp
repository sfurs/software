#include "VisualBall.h"
#include <QPainter>

VisualBall::VisualBall() : VisualBall(0,0,0) {}

// constructor that initializes robots based on team and robot number
VisualBall::VisualBall(qreal x,qreal y,qreal z) 
{
    
    this->color = QColor(255, 165, 0);  // Orange
    this->x = x;
    this->y = y;
    this->z = z;
    this->setPos(mapToParent(x,y));

}

QRectF VisualBall::boundingRect() const
{
    // boundingRect
    qreal adjust = 2;
    qreal p = 90.0;

    QPoint topLeft;
    topLeft.setX(-p - adjust);
    topLeft.setY(-p - adjust);
    QPoint bottomRight;
    bottomRight.setX(p + adjust);
    bottomRight.setY(p + adjust);
    return QRect(topLeft, bottomRight);//QRectF(-180.0 - adjust, -180.0 - adjust, 180.0 + adjust, 180.0 + adjust);
}


QPainterPath VisualBall::shape() const
{
    QPainterPath path;
    QPointF center;
    center.setX(0);
    center.setY(0);
    path.addEllipse(center,90,90);//.addRect(-90, -90, 90, 90);
    return path;
}

void VisualBall::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    int radius = 55;
    painter->setBrush(color);
    QPointF center;
    center.setX(0);
    center.setY(0);
    painter->drawEllipse(center,radius,radius);
}


void VisualBall::updateVisuals(float x, float y, float z, bool isFlipped)
{
    // Flip and scale the coordinates for the rotated field
    if (!isFlipped) {
        this->setPos(y + 3700, x + 5200);
    } else {
        this->setPos(-y + 3700, -x + 5200);
    }

    if (z > 0) {
        this->color = Qt::red;
    } else  {
        this->color = QColor(255, 165, 0);  // Orange
    }

    // Adjust visibility check for the new coordinate system
    if (x < -5200 || x > 5200 || y < -3700 || y > 3700) {
        this->setVisible(false);
    } else {
        this->setVisible(true);
    }
    update();
}

#include "reflex.h"
#include "../agent/controls/AgentControl.h"
#include "../agent/controls/Navigator.h"
// #include "./shared/GameState.h"
// #include "./shared/RefereeData.h"

Reflex *Reflex::pReflex = nullptr;

Reflex::Reflex(AgentControl *control, Navigator *navigator) : agentControl(control), navigator(navigator) {}

void Reflex::setWorldStateManager(WorldStateManager *worldStateManager)
{
    this->pWorldStateManager = worldStateManager;
    connect(pWorldStateManager, &WorldStateManager::worldStateReflex, this, &Reflex::worldStateReflex);
    connect(pWorldStateManager, &WorldStateManager::gameStateReflex, this, &Reflex::gameStateReflex);
}

void Reflex::worldStateReflex(WorldState ws)
{
    // ws.printState();
    // std::cout << "Reflex::worldStateReflex" << std::endl;
    // TODO: add reflex logic here
}

void Reflex::gameStateReflex(GameState gs)
{ // TODO: check if the game state is passed into the reflex system
    // Add your reflex logic here for game state changes
    // if (false)
    // { // TODO: Halt logic for halt command
    //     WorldState currentState = pWorldStateManager->getCurrentState();
    //     // currentState.printState();
    // }

    // get referee commands
    RefereeData currentRefereeData = gs.getRefereeData();

    if (currentRefereeData.command == RefereeCommand::HALT)
    {
        // send halt command to robots
        // std::cout << "Halt command issued" << std::endl;
        this->agentControl->haltAllRobots();

        // gs.printState();
    }
    else if (currentRefereeData.command == RefereeCommand::STOP)
    {
        // sent stop command to robots
        // std::cout << "Stop command issued" << std::endl;
        this->agentControl->stopAllRobots();
        // gs.printState();
    }
    else if (currentRefereeData.command == RefereeCommand::TIMEOUT_YELLOW)
    {
        // sent halt command to robots
        // std::cout << "TIMEOUT_YELLOW command issued" << std::endl;
        this->agentControl->haltAllRobots();
        // gs.printState();
    }
    else if (currentRefereeData.command == RefereeCommand::TIMEOUT_BLUE)
    {
        // sent halt command to robots
        // std::cout << "TIMEOUT_BLUE command issued" << std::endl;
        this->agentControl->haltAllRobots();
        // gs.printState();
    }
    else if (currentRefereeData.command == RefereeCommand::DIRECT_FREE_BLUE || currentRefereeData.command == RefereeCommand::DIRECT_FREE_YELLOW)
    {
        // std::cout << "DIRECT_FREE_BLUE OR DIRECT_FREE_YELLOW command issued" << std::endl;
        WorldState ws = pWorldStateManager->getCurrentState();
        BallData ball = ws.ballData;
        // QMap<int, RobotData> robots = agentControl->getRobotData();
        bool isDefendingFreeKick = true;
        //get ball position, then set robot pos

        //if we are defending
        if(isDefendingFreeKick)
        {
            this->navigator->moveAwayFromBall(500);
        }
        else{
            int closestToBall = this->navigator->findClosestToBall();
            this->navigator->navigateTo(closestToBall, ball.x, ball.y, IN_CMD);
        }
          
    }

    //
}

void Reflex::haltRobot(int robotID)
{
    // TODO: implement robot halting logic here
}

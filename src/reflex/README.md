## Overview: Reflex Module

The Reflex module provides a real-time response system that reacts to changes in both the world state and game state. It operates as a singleton pattern to ensure consistent handling of reflex actions across the system.

## Files

### 1. `reflex.cpp` & `reflex.h`
- **Reflex class**:  
  The `Reflex` class is a singleton that handles immediate responses to changes in the game environment, including world state updates and game state transitions. It uses `AgentControl` functions to execute behaviors such as moving and stopping.

  - **Functions**:
    - `setWorldStateManager(WorldStateManager* worldStateManager)`:
    Connects the reflex system to the world state manager and sets up signal/slot connections for state updates
    - `worldStateReflex(WorldState ws)`:
    Handles reflexive responses to changes in the world state (robot positions, ball position, etc.)
    - `gameStateReflex(GameState gs)`:
    Handles reflexive responses to changes in game state (referee commands, game stages, etc.)
    Currently, responses for 'HALT' and 'STOP' referee commands are implemented. When the referee sends these commands out, all robots will halt (or stop, depending on the command). When 'TIMEOUT_YELLOW' and 'TIMEOUT_BLUE' commands are sent by the referees, the robots are instructed to halt.
    - `haltRobot(int robotID)`:
    (private) Emergency stops a specific robot

  - **Signals/Slots**:
    - Connected to `WorldStateManager::worldStateReflex`
    - Connected to `WorldStateManager::gameStateReflex`

  - **Key Members**:
    - `static Reflex* pReflex`: Singleton instance pointer
    - `WorldStateManager* pWorldStateManager`: Pointer to the world state manager for accessing game data

The Reflex system is designed to provide immediate responses to game events without requiring complex decision-making processes. It acts as a safety and reactive layer between the world state and robot control systems.

#ifndef REFLEX_H
#define REFLEX_H

#include "../worldstate/worldState.h"
#include "../worldstate/GameState.h"
#include "../worldstate/worldStateManager.h"
#include "../agent/controls/AgentControl.h"
#include "../agent/controls/Navigator.h"
#include <QObject>
#include <iostream>

/**
 * @brief The Reflex class is a singleton that handles reflex logic for the robot
 *
 */
class Reflex : public QObject
{
    Q_OBJECT
private:
    /**
     * @brief The pointer to the Reflex instance
     */
    static Reflex *pReflex;
    /**
     * @brief The pointer to the WorldStateManager instance
     */
    WorldStateManager *pWorldStateManager = nullptr;

    /**
     * @brief Halts the robot with the given robot ID
     *
     * @param robotID The ID of the robot to halt
     */
    void haltRobot(int robotID);

    AgentControl *agentControl; // Pointer to the AgentControl object
    Navigator *navigator;       // Pointer to the Navigator object

public:
    /**
     * @brief The constructor for the Reflex object
     *
     * @param control The pointer to the AgentControl instance
     */
    Reflex(AgentControl *control, Navigator *navigator); // constructor
    /**
     * @brief Sets the WorldStateManager instance
     *
     * @param worldStateManager The pointer to the WorldStateManager instance
     */
    void setWorldStateManager(WorldStateManager *worldStateManager);
    /**
     * @brief Gets the singleton instance of Reflex
     *
     * @return The pointer to the Reflex instance
     */

public slots:
    /**
     * @brief Handles world state changes
     * @param ws The current world state
     */
    void worldStateReflex(WorldState ws);
    /**
     * @brief Handles game state changes
     * @param gs The current game state
     */
    void gameStateReflex(GameState gs);
};

#endif // REFLEX_H

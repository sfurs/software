# SKYNET SSL Robot Soccer System

## Overview
This is a robotic soccer system implementing the SSL (Small Size League) protocol. The system connects to GrSim (SSL simulator) to control autonomous soccer-playing robots, processing real-time vision data and referee commands.

## Directory Structure

### `/agent`
Handles robot behavior and autonomous decision-making.
- **Key Files**: 
  - `Agent.cpp/h`: Individual robot control with ball possession and movement logic
  - `AgentControl.cpp/h`: Low-level robot movement commands (move, rotate, kick, dribble)
  - `AgentMaster.cpp/h`: Coordinates multiple agents and manages control modes

### `/gui`
Qt-based visualization system for game state and robot control.
- **Key Files**:
  - `mainwindow.cpp/h`: Main interface showing game state and robot status
  - `GameScene.cpp/h`: Renders soccer field with precise dimensions and markings
  - `VisualRobot.cpp/h`: Robot visualization with team colors and status
  - `VisualBall.cpp/h`: Ball visualization and tracking

### `/network`
UDP-based communication with GrSim and referee system.
- **Key Files**:
  - `NetMaster.cpp/h`: Central network coordinator for vision and referee data
  - `VisionProtobuf.cpp/h`: Processes SSL vision system data
  - `RobotInstruction.h`: Defines robot movement command structure (TODO: Move to shared)

### `/pch`
Precompiled headers for faster compilation.

### `/python`
Python bindings for the system. Contains all the python submodules as well as the python setup script.

### `/reflex`
Real-time response system for immediate game state changes.
- **Key Files**:
  - `reflex.cpp/h`: Handles emergency responses and quick reactions to game events

### `/shared`
Common data structures/interfaces used across modules.
- **Key Files**:
  - `FieldData.h`: Field state and robot positions
  - `RefereeData.h`: Referee commands and game stages
  - `TeamInfo.h`: Team scores, cards, and status

### `/worldstate`
Game state management and world model.
- **Key Files**:
  - `worldState.cpp/h`: Tracks ball and robot positions
  - `GameState.cpp/h`: Manages game rules and referee states
  - `worldStateManager.cpp/h`: Thread-safe state management

## Build System
- Uses CMake with Qt6
- Requires protobuf for network communication
- Builds both debug and release configurations

## Dependencies
- Qt6 (Core, Widgets, Network)
- Google Protocol Buffers
- OpenGL (for field visualization)
- GrSim (SSL simulator)

## Running the System
1. Start GrSim simulator:

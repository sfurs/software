## PlantUML Usage for Reflex System UML

This document explains how to modify and maintain the Reflex System UML diagram.

### Key Components to Consider When Modifying

1. **External Systems**
   - When adding new external systems, place them in appropriate packages (e.g., "Sim", "Cam", "BaseStation")
   - Follow the existing pattern for external interfaces

2. **Main System Components** 
   - Group related classes into logical packages (e.g., "GUI", "Network Input", "State Management")
   - Include key methods and attributes in class definitions
   - Maintain clear package boundaries

3. **Relationships**
   - Use arrows to show dependencies between components:
     - Simple arrow (-->) for dependencies
     - Inheritance shown with (<|--)
     - Composition shown with (*--)
   - Keep relationships organized by system area

### Example Package Structure

ARG ENV_IMAGE=registry.gitlab.com/sfurs/software/env
ARG ENV_TAG=latest-develop
FROM ${ENV_IMAGE}:${ENV_TAG} AS builder

# Set the working directory
WORKDIR /app

# Copy the SFU Robot Soccer software
COPY . /app

# Build the software
RUN rm -rf build && \
    mkdir -p build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=Debug -GNinja .. && \
    ninja

# Set up the environment for running Qt applications
ENV QT_X11_NO_MITSHM=1

RUN if [ "$(uname -s)" = "Darwin" ]; then \
        echo "Setting up environment for macOS" && \
        export DISPLAY=host.docker.internal:0 && \
        export QT_DEBUG_PLUGINS=1 && \
        export DISPLAY=:0 && \
        export XAUTHORITY=/root/.Xauthority && \
        export DBUS_SESSION_BUS_ADDRESS=unix:path=/var/run/dbus/system_bus_socket ; \
    fi

CMD ["./build/skynet"]


variables:
  GIT_SUBMODULE_STRATEGY: recursive
  DOCKER_TLS_CERTDIR: "/certs"
  # For Docker cache
  DOCKER_DRIVER: overlay2
  DOCKER_BUILDKIT: 1
  DOCKER_CLI_EXPERIMENTAL: enabled
  # For Dockerfile.env
  ENV_IMAGE: $CI_REGISTRY_IMAGE/env
  ENV_TAG: $CI_COMMIT_SHA
  CACHED_ENV_TAG: $LATEST_CI_DEVELOP_TAG
  # For Dockerfile
  APP_IMAGE: $CI_REGISTRY_IMAGE
  APP_TAG: $CI_COMMIT_SHA
  # Default develop* tag, set dynamically when needed
  LATEST_CI_DEVELOP_TAG: latest-develop # defaults to develop by default
  # Determined based on branch name
  IS_DEPLOY_PIPELINE: false
  IS_MERGE_REQUEST_PIPELINE: false
  # Application env vars
  QT_QPA_PLATFORM: offscreen


stages:
  - env-build
  - app-build
  - app-test
  - deploy


workflow:
  rules:
    # -----
    # Run if there are commits to develop* branches
    - if: '$CI_COMMIT_BRANCH =~ /^develop.*/'
      changes:
        # If the env needs to be rebuilt
        - Dockerfile.env
      variables: 
        CACHED_ENV_TAG: $ENV_TAG
        LATEST_CI_DEVELOP_TAG: "latest-$CI_COMMIT_BRANCH"
        IS_DEPLOY_PIPELINE: true

    - if: '$CI_COMMIT_BRANCH =~ /^develop.*/'
      variables: 
        LATEST_CI_DEVELOP_TAG: "latest-$CI_COMMIT_BRANCH"
        IS_DEPLOY_PIPELINE: true

    # -----
    # Run if there are commits to merge requests targeting develop* branches
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^develop.*/'
      changes:
        # If the env needs to be rebuilt
        - Dockerfile.env
      variables:
        CACHED_ENV_TAG: $ENV_TAG
        LATEST_CI_DEVELOP_TAG: "latest-$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
        IS_MERGE_REQUEST_PIPELINE: true

    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^develop.*/'
      variables:
        LATEST_CI_DEVELOP_TAG: "latest-$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
        IS_MERGE_REQUEST_PIPELINE: true

    # Otherwise, don't run
    - when: never


.default-template:
  before_script:
    - >
      echo "Relevant environment variables:";
      echo "--- CI vars ----";
      echo "- CI_COMMIT_BRANCH: ${CI_COMMIT_BRANCH}";
      echo "- CI_PIPELINE_SOURCE: ${CI_PIPELINE_SOURCE}";
      echo "- CI_MERGE_REQUEST_TARGET_BRANCH_NAME: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}";
      echo "--- Build logic vars ----";
      echo "- IS_DEPLOY_PIPELINE=${IS_DEPLOY_PIPELINE}";
      echo "- IS_MERGE_REQUEST_PIPELINE=${IS_MERGE_REQUEST_PIPELINE}";
      echo "- ENV_IMAGE=${ENV_IMAGE}";
      echo "- ENV_TAG=${ENV_TAG}";
      echo "- CACHED_ENV_TAG=${CACHED_ENV_TAG}";
      echo "- APP_IMAGE:APP_TAG=${APP_IMAGE}:${APP_TAG}";
      echo "- APP_IMAGE=${APP_IMAGE}";
      echo "- APP_TAG=${APP_TAG}";
      echo "- LATEST_CI_DEVELOP_TAG: ${LATEST_CI_DEVELOP_TAG}";


######################################
# Env
#
env:build:
  stage: env-build
  image: docker:27
  services:
    - docker:27-dind
  before_script:
    - !reference [.default-template, before_script]
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - >
      docker build
      --cache-to "type=inline"
      --cache-from "type=registry,ref=${ENV_IMAGE}:${LATEST_CI_DEVELOP_TAG}"
      -f Dockerfile.env
      -t $ENV_IMAGE:$ENV_TAG
      .
    # WARN: Need to push here else future stages can't access
    - docker push $ENV_IMAGE:$ENV_TAG
  # Generally, only run when there is a change to the Dockerfile.env
  rules:
    # base cases should be covered by workflow
    - changes:
        - Dockerfile.env
    - when: never





######################################
# Test
#

build:app:test:
  # Use latest env image to build the application code
  # Doing this should be cheaper and faster than building a Docker image for each commit to a merge request
  # Also don't need to `docker push` the resulting APP_IMAGE:APP_TAG per merge request
  # Can put off building and saving the APP_IMAGE for commits to deploy* branches (when the MR gets merged in)
  stage: app-test
  image:
    name: $ENV_IMAGE:$CACHED_ENV_TAG
    entrypoint:
      - "" # override, otherwise runs skynet with no args and hangs
  before_script:
    - !reference [.default-template, before_script]
  script:
    # GitLab CI will change the cwd to $CI_PROJECT_DIR by default, containing our repo code
    - rm -rf build
    - mkdir -p build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE=Debug -G Ninja ..
    - ninja
  artifacts:
    paths:
      - build/
    expose_as: build
    expire_in: 1 week
  cache:
    # TODO: Investigate better caching mechanism for app builds
    key: python-libs-test
    paths:
      - build/python_libs


run:app:test:
  stage: app-test
  image: $ENV_IMAGE:$CACHED_ENV_TAG
  needs:
    - job: build:app:test
      artifacts: true
  script:
    - ./build/skynet --version
    - ./build/test/unit_tests







######################################
# Deploy
# - only deploy once all builds & tests are successful
# - deploy env and app separately: env deploys should be relatively rare
#

build:app:deploy:
  stage: deploy
  image: docker:27
  services:
    - docker:27-dind
  before_script:
    - !reference [.default-template, before_script]
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    # TODO: Maybe can copy previous `build/` into env image instead of rebuilding?
    # Rebuilding would provide better sanity though
    - >
      docker build
      -f Dockerfile
      --cache-to "type=inline"
      --cache-from "type=registry,ref=${APP_IMAGE}:${LATEST_CI_DEVELOP_TAG}"
      --build-arg "ENV_IMAGE=${ENV_IMAGE}"
      --build-arg "ENV_TAG=${CACHED_ENV_TAG}"
      -t $APP_IMAGE:$APP_TAG
      .
    # WARN: Need to push here else future stages can't access
    - docker push $APP_IMAGE:$APP_TAG
  rules:
    - if: $IS_DEPLOY_PIPELINE == "true"
      when: on_success
    - when: never


push:env:deploy:
  stage: deploy
  needs:
    - env:build
    - build:app:deploy
  image: docker:27
  services:
    - docker:27-dind
  before_script:
    - !reference [.default-template, before_script]
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $ENV_IMAGE:$ENV_TAG
    - docker tag $ENV_IMAGE:$ENV_TAG $ENV_IMAGE:$LATEST_CI_DEVELOP_TAG
    - docker push $ENV_IMAGE:$LATEST_CI_DEVELOP_TAG
  rules:
    # Run in commits to develop branches
    - if: $IS_DEPLOY_PIPELINE == "true"
      changes:
        paths:
          - Dockerfile.env
      when: on_success
    - when: never


push:app:deploy:
  stage: deploy
  needs:
    - build:app:deploy
  image: docker:27
  services:
    - docker:27-dind
  before_script:
    - !reference [.default-template, before_script]
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $APP_IMAGE:$APP_TAG
    - docker tag $APP_IMAGE:$APP_TAG $APP_IMAGE:$LATEST_CI_DEVELOP_TAG
    - docker push $APP_IMAGE:$LATEST_CI_DEVELOP_TAG
  rules:
    # Run in commits to develop branches
    - if: $IS_DEPLOY_PIPELINE == "true"
      when: on_success
    - when: never


#include <boost/ut.hpp>

#include "NavigatorCommandParams.h"

namespace ut = boost::ut;

ut::suite<"NavigatorCommandParams"> NavigatorCommandParams = [] {
    // terse removes the need to write `expect()`
    using namespace boost::ut::literals;
    using namespace boost::ut::operators::terse;
    //using namespace boost::ut will clash with boost::ut::operators::terse;

    // Try out Behaviour-Driven Development
    using namespace boost::ut::bdd;

    "MoveToParams"_test = [] {
        scenario("Constructor") = [] {
            // Test fixtures, setup
            constexpr static float FIXTURE_X = 6.0f;
            constexpr static float FIXTURE_Y = 10.0f;

            // terse overloads only work ergonomically when comparing with ut::literals
            constexpr static auto EXPECT_X = 6.0_f;
            constexpr static auto EXPECT_Y = 10.0_f;

            // Don't have C++20 so gotta use streams
            ut::log << "Before:\n\t( x =" << FIXTURE_X <<  ", y =" << FIXTURE_Y << ")\n";

            given("coordinates (x, y)") = [] {
                when("I construct the MoveToParams") = [] {
                    MoveToParams params{FIXTURE_X, FIXTURE_Y};

                    then("its fields are equal to what I set") = [&params] {
                        params.x == EXPECT_X;
                        params.y == EXPECT_Y;
                    };

                    then("its fields are not equal to something else") = [&params] {
                        params.x != 0.0_f;
                        params.y != 0.0_f;
                    };
                };
            };
        };
    };
};


#include <boost/ut.hpp>
#include <cassert>

namespace ut = boost::ut;

ut::suite<"Examples"> examples = [] {
    // Commonly used idiom, seen in Boost.Test docs
    using namespace boost::ut;
    using namespace boost::ut::operators;

    "Baseline"_test = [] {

        "[pass]"_test = [] {
            expect(1 + 1 == 2);
            expect(true);
        };

        "[fail]"_test = [] {
            expect(aborts(
                [] {
                    expect(fatal(2_i == 1));
                }
            ));
        };
    };
};



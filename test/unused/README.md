# [DEPRECATED] Unit Testing with Boost.Test

We've decided to go with Boost::ut instead of Boost.Test. Boost::ut is compatible with C++17 (only slightly less convenient logging), and its tests require no macros. It supports many different testing philosophies out of the box, namely Behaviour-Driven Development (BDD), Spec syntax, and Gherkin syntax.

It also supports test fixtures, setup, and teardown, fulfilling pretty much all of our requirements.

## Structure

Have a `main.cpp` to avoid recompiling the Boost.Test headers every time we recompile or change a test.

## Usage

Compile our codebase in the regular fashion. For example ...

```sh
mkdir build
cd build
cmake -GNinja ..
ninja
```

Then run the tests:

```sh
./test/unit_tests
```

By default, the Boost.Test runner will output all failed tests, even the failures we expect. We can tell the test runner to only print unexpected failures and uncaught C++ exceptions. More information can be found [here](https://live.boost.org/doc/libs/1_87_0/libs/test/doc/html/boost_test/utf_reference/rt_param_reference/log_level.html)

```sh
./test/unit_tests --log_level=cpp_exception
```

Can also print out all the test runner arguments like so:
```sh
./test/unit_tests --help
```

#include <boost/test/unit_test.hpp>

// Commonly used idiom, seen in Boost.Test docs
namespace utf = boost::unit_test;


BOOST_AUTO_TEST_SUITE(
    Starter_Example,
    * utf::expected_failures(1)
)


BOOST_AUTO_TEST_CASE(Example_Pass) {
    // The values should evaluate to true, will fail if not
    BOOST_TEST(1 + 1 == 2);
    BOOST_TEST(true);

    BOOST_CHECK(true);
}

/**
* For cases where we expect failures.
* The number given as an argument is the expected number of failed asserts.
*
* https://live.boost.org/doc/libs/1_81_0/libs/test/doc/html/boost_test/testing_tools/expected_failures.html
*/
BOOST_AUTO_TEST_CASE(
    Example_ExpectFail,
    * utf::expected_failures(3)
) {
    // The values should evaluate to true, will fail if not
    BOOST_TEST(1 + 1 == 3);
    BOOST_TEST(false);

    BOOST_CHECK(false);
}

BOOST_AUTO_TEST_SUITE_END()


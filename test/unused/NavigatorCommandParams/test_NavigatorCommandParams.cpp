#include <boost/test/unit_test.hpp>

#include "NavigatorCommandParams.h"

// Standard idiom
namespace utf = boost::unit_test;

// Some values we'll use throughout
constexpr static float SOME_X = 6.0f;
constexpr static float SOME_Y = 10.0f;

BOOST_AUTO_TEST_SUITE(
    NavigatorCommandParams,
    * utf::expected_failures(1)
)

BOOST_AUTO_TEST_CASE(MoveToParams_Constructor) {
    MoveToParams params{SOME_X, SOME_Y};

    // Values initialized as expected
    BOOST_TEST(params.x == SOME_X);
    BOOST_TEST(params.y == SOME_Y);
}

BOOST_AUTO_TEST_CASE(
    MoveToParams_Constructor_NotEqual,
    * utf::expected_failures(2)
) {
    MoveToParams params{0.0f, 0.0f};

    // Expected to fail
    BOOST_TEST(params.x == SOME_X);
    BOOST_TEST(params.y == SOME_Y);
}

BOOST_AUTO_TEST_CASE(MoveToParams_Constructor_Fuzz) {
    // placeholder
    // try to construct 1000 random MoveToParms
    BOOST_CHECK(true);
}


BOOST_AUTO_TEST_SUITE_END()

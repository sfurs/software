# Unit Testing with `boost::ut`

`boost::ut` is a single header testing library, its tests require no macros, and it is unofficially compatible with C++17 (with only slightly less convenient logging).
It supports test fixtures, setup, and teardown, fulfilling pretty much all of our requirements.
It also supports many different testing philosophies out of the box, namely Behaviour-Driven Development (BDD), Spec syntax, and Gherkin syntax.

We might include the header in our repo explicitly in the future, but for now, we are just fetching it from GitHub using CMake's FetchContent feature.

## Structure

We have a mostly empty `main.cpp` since tests declared in a `boost::ut::suite` are automatically registered by the test runner.
The unit tests for a feature/class are contained in its related subdirectory and in a `boost::ut::suite`.

The `test/unused/` folder contains a small set of tests written using Boost.Test, but we decided to go with `boost::ut` for its modern conveniences and fast compilation time.

## Usage

### 1. Compile our codebase in the regular fashion.

```sh
# from the project root
software$ mkdir build
software$ cd build
build$ cmake -GNinja ..
build$ ninja
```

### 2. Then run the tests like this:

```sh
build$ ./test/unit_tests
```

Some example output from a successful run is given below.
By default, the test runner will output all failed tests, even the failures we expect.

```sh
# from the build directory
build$ ./test/unit_tests

Running test "Baseline"...
  Running test "[pass]"...
  Running test "[fail]"... FAILED
in: :0 - test condition:  [2 == 1]


Before:
        ( x = 6 , y = 10 )
Suite 'NavigatorCommandParams': all tests passed (3 asserts in 6 tests)

===============================================================================
Suite Examples
tests:   3 | 2 failed
asserts: 6 | 1 passed | 2 failed

Suite 'global': all tests passed (0 asserts in 0 tests)

Before:
        ( x = 6 , y = 10 )
Suite 'NavigatorCommandParams': all tests passed (3 asserts in 6 tests)
Suite 'Examples': all tests passed (3 asserts in 3 tests)
Suite 'global': all tests passed (0 asserts in 0 tests)
```

**Note!!!** Even though a failure was reported, this was expected, so it says `Suite 'Examples': all tests passed (3 asserts in 3 tests)`.

To change this behaviour, we would need to create a custom `cfg::reporter`.
We can tell the test runner to only print unexpected failures and uncaught C++ exceptions.

```cpp
// This section is kind of involved, so will do later
// Example of custom reporter can be found here: https://github.com/boost-ext/ut/blob/69d500f3dc51eb020b7a91d8349cf9749a54b0b9/example/cfg/reporter.cpp
```

Can also print out all the test runner arguments like so:
```sh
build$ ./test/unit_tests --help
```

More information can be found [on the website](https://boost-ext.github.io/ut/), or in the [repo](https://github.com/boost-ext/ut?tab=readme-ov-file).
There are also many good examples in the repo, under `example/`!


